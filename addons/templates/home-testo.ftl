<div class="container-fluid">
	<div class="row px-5">
		<div class="col-12">
			<div class="content">
				${content.getData()}
			</div>
		</div>
	</div>
</div>