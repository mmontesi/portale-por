<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>
<#assign categoryPropertyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryPropertyLocalService")>

<#assign vocabolarioBeneficiari = vocabularyService.getGroupVocabulary(groupId, "Beneficiari")>
<#assign beneficiari = categoryService.getVocabularyCategories(vocabolarioBeneficiari['vocabularyId'], -1,-1,null)>
<#assign vocabolarioOpportunita = vocabularyService.getGroupVocabulary(groupId, "Opportunità")>
<#assign opportunita = categoryService.getVocabularyCategories(vocabolarioOpportunita['vocabularyId'], -1,-1,null)>

<#assign risultati = {} >
<#list beneficiari as beneficiario>
	<#assign listOpportunita = [] >
	<#list opportunita as curOpportunita>
		<#assign beneficiario = "">
		<#attempt>
			<#assign beneficiarioProps = categoryPropertyService.getCategoryProperty(curOpportunita['categoryId']?number, 'beneficiari')['value'] >
			<#list beneficiarioProps?split("-") as beneficiarioProp>
				<#if beneficiarioProp == beneficiario["name"]>
					<#assign listOpportunita = listOpportunita + [curOpportunita["name"]] >
				</#if>
			</#list>
		<#recover>
		</#attempt>
	</#list>
	<#assign risultati += { beneficiario["name"]: listOpportunita }>
</#list>
<div class="container-fluid ricerca-bandi py-4 mb-4">
	<div class="row px-5">
		<div class="col-md-4 d-flex">
			<div class="row align-items-center">
				<div class="col-md-4">

				</div>
				<div class="col-md-8">
					<p class="testo">
						Cerca tra le opportunità di finanziamento del POR in base alle tue esigenze
					</p>
				</div>
			</div>
		</div>
		<div class="col-md-7 offset-md-1">
			<div class="form-group-autofit">
				<div class="form-group-item">
					<label for="formInlineAutofitSelectElement1">
						<span class="text-truncate-inline">
							<span class="text-truncate" title="Select Element">Sei</span>
						</span>
					</label>
					<select name="secondCategoryFilter" class="form-control first-select">
						<option value="">seleziona</option>
						<#list beneficiari as beneficiario>
							<option data-category-id='${beneficiario["categoryId"]}'>${beneficiario['name']}</option>
						</#list>
					</select>
				</div>
				<div class="form-group-item">
					<div class="form-group">
						<label for="formInlineAutofitSelectElement1">
							<span class="text-truncate-inline">
								<span class="text-truncate" title="Select Element">e vuoi</span>
							</span>
						</label>
						<select name="secondCategoryFilter" class="form-control second-select">
							<option value="">scegli</option>
						</select>
					</div>
				</div>
				<div class="btn-group-item align-self-end">
					<a class="btn btn-por-default search" href="#">Vai</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

var results = {

	<#list risultati?keys as key> 
		<#assign risultatiValues = risultati[key] >
		"${key}": [
		<#list risultatiValues as risultatiValue>
			"${risultatiValue}"
			<#if risultatiValues?last != risultatiValue>
			,
			</#if>
		</#list>
		]
		<#if risultati?keys?last != key>
			,
		</#if>
	</#list>
};

var opportunitaList = {
	<#list opportunita as curOpportunita>
		'${curOpportunita["name"]}': '${curOpportunita["categoryId"]}'
		<#if opportunita?last != curOpportunita>
			,
		</#if>
	</#list>
}

$(document).ready(function(){
	$('.first-select').on('change', function(event) {
		var beneficiarioSelezionato = $(event.target).val();
		var opportunita = results[beneficiarioSelezionato];
		var html = '<option>-</option>';
		if (opportunita.length > 0) {
			$.each(opportunita, function(index, element){
				html += '<option data-category-id="' + opportunitaList[element] + '">' + element + '</option>';
			});
		}
		$('.second-select').html(html);
	});

	$('.search').on('click', function(event){
		event.preventDefault();
		var firstCategoryId = $('.first-select option:selected').data('categoryId');
		var secondCategoryId = $('.second-select option:selected').data('categoryId');
		var params = 'p_o_r_categoryIds=' + firstCategoryId + '%2C' + secondCategoryId;
		location.href = '${resultsPage.getFriendlyUrl()}?' + params;
	});
});

</script>