<#assign imagesPath = themeDisplay.getPathThemeImages() />



<div class="row">
    <div class="col-sm-8 col-md-8 col-lg-8">
       <b><h1 class="titoloPagina">${.vars['reserved-article-title'].data} </h1></b>
        <div class="content">
			${content.getData()}
		</div>

    </div>
    
		<div class="col-sm-4 col-md-4 col-lg-4">
			<div class="card">
			    <div class="card-body">
					<h4 class="card-title">DIRIGENTE DI SEZIONE</h4><br>
						<ul class="list-group" style="list-style-type: none;">
							<li class="item">
								<p><a href="${leaderName.leaderLink.getData()}" target=”_blank”>
								<img class="icon"  src="${imagesPath}/icon/.png" />
									${leaderName.getData()}
								</a></p>
							</li>
							<li class="item">  
								<p><img class="icon"  src="${imagesPath}/icon/.png" />
								${leaderName.seat.getData()}</p>
							</li>
							<li class="item"> 
								<p><img class="icon"  src="${imagesPath}/icon/.png" />
								${leaderName.phone.getData()} </p>
							</li>
							<li class="item">
								<p><img class="icon"  src="${imagesPath}/icon/.png" /><a href="mailto:indirizzo@email.com"> ${leaderName.email.getData()}</a></p>
							</li>
							<li class="item">
								<#if leaderName.pecEmail.getData()!= ""><p><img class="icon" src="${imagesPath}/icon/.png" /></#if><a href="mailto:indirizzo@email.com">${leaderName.pecEmail.getData()}</a></p>
							</li>
						</ul>
				</div>
			</div>
		</div>

</div>


<style>


.card{
    background-color: rgba(154, 151, 151, 0.1);
}


</style>