<#assign imagesPath = themeDisplay.getPathThemeImages() />

<div class="row p-4 px-md-5 py-md-0 stato-di-attuazione stato-di-attuazione-bg">
	<div class="col">
		<div class="row py-4">
			<div class="col-md-auto align-self-center">
				<h2 class="titolo">${.vars['reserved-article-title'].data}</h2>
			</div>
		</div>
		<div class="row py-md-4 d-flex justify-content-around">
			<div class="col-md-4 py-4 py-md-0 finanziamenti-erogati d-flex flex-md-column justify-content-md-between">
				<div class="row justify-content-center align-items-center">
					<div class="col-8 col-md-6">
						<img alt="thumbnail" class="w-100" src="${imagesPath}/custom/stato-di-avanzamento/dotazione-complessiva.svg" height="126">
					</div>
				</div>
				<div class="row justify-content-md-center align-items-center">
					<div class="col-auto d-flex flex-column align-items-start align-items-md-center">
						<span class="dato">${finanziamenti_erogati.getData()?number?string("#,###")}</span>
						<p class="testo text-center"><@liferay.language key="por.execution.loans-granted" /></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 py-4 py-md-0 progetti-finanziati d-flex flex-md-column justify-content-md-between">
				<div class="row justify-content-center align-items-center">
					<div class="col-8 col-md-6">
						<img alt="thumbnail" class="w-100" src="${imagesPath}/custom/stato-di-avanzamento/progetti-finanziati.svg" height="126">
					</div>
				</div>
				<div class="row justify-content-md-center align-items-center">
					<div class="col-auto d-flex flex-column align-items-start align-items-md-center">
						<span class="dato">${progetti_finanziati.getData()?number?string("#,###")}</span>
						<p class="testo text-center"><@liferay.language key="por.execution.projects-funded" /></p>
					</div>
				</div>
			</div>
			<div class="col-md-4 py-4 py-md-0 ettari d-flex flex-md-column justify-content-md-between">
				<div class="row justify-content-center align-items-center">
					<div class="col-8 col-md-6">
						<img alt="thumbnail" class="w-100" src="${imagesPath}/custom/stato-di-avanzamento/finanziamenti-erogati.svg" height="126">
					</div>
				</div>
				<div class="row justify-content-md-center align-items-center">
					<div class="col-auto d-flex flex-column align-items-start align-items-md-center">
						<span class="dato">${ettari_per_misure_di_superficie.getData()?number?string("#,###")}</span>
						<p class="testo text-center"><@liferay.language key="por.execution.surface-measurements" /></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>