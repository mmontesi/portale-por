<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />


<div class="fondo">
	<div class="content">
		<div class="row">
			<div class="col-sm-4 col-md-4 col-lg-4">
				<h1 id="titoloFondo">${title.getData()}</h1>
			</div>
			<div class="col-sm-8 col-md-8 col-lg-8">				
				<div class="content">
					${content.getData()}
				</div>
				
				<#if currentArticleRelatedLinks?has_content>
					<h2><@liferay.language key="por.documents" /></h2>
					
					<#list currentArticleRelatedLinks as related_entry>
						<#assign relatedAssetEntryId = related_entry.getEntryId2() />
						<#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
						<#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
						<#assign assetRenderer = relatedAssetEntry.getAssetRenderer() />
						
						<#if relatedAssetEntry.getClassName()?string == "com.liferay.document.library.kernel.model.DLFileEntry" >
			
							<div class="col media pt-3 d-flex align-items-center">
								<#assign portalURL = themeDisplay.getPortalURL() />
								<#assign slash = "/" />
								<#assign fileEntry = dlAppLocalService.getFileEntry(relatedAssetEntryPrimKey) />
								<#assign repositoryId = fileEntry.getRepositoryId() />
								<#assign folderId = fileEntry.getFolderId() />
								<#assign fileVersion = fileEntry.getFileVersion() />
								<#assign modifiedDate = fileVersion.getModifiedDate()?time />
								<#assign fileName = fileEntry.getFileName() />
								<#assign previewURL = portalURL + "/documents/" + repositoryId + "/" + folderId + "/" + fileName + "/" + fileEntry.getUuid() + "?t=" + modifiedDate />	
											
								<#if (assetRenderer.assetObject.extension)?has_content>
									<a class="document-title" target="_blank" href="${previewURL?string}">
										<img class="mr-3 file-extension" id="imageDoc" alt="${assetRenderer.assetObject.extension?html}" src="${imagesPath}/custom/estensioni/${assetRenderer.assetObject.extension?html}.svg" />
									</a>
								</#if>
								<div class="media-body">
									<p class="m-0">${relatedAssetEntry.description?string}</p>
								</div>
							</div>
						</#if>
					</#list>
				</#if>
			</div>
		</div>
	</div>
</div>



<style>
.immagineFondo{
 opacity: 0.65;
  background-image: linear-gradient(281deg, rgba(17, 48, 110, 0.86), rgba(236, 102, 8, 0));
  width: 100%;
}

.titoloFondo{
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  font-family: TitilliumWeb;
  font-size: 27px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: left;
  color: #11306d;   
    
}
.descrizioneFondo{
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  font-family: TitilliumWeb;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #000000;
}

.titoloVideo{
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  font-family: TitilliumWeb;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.69;
  letter-spacing: normal;
  text-align: left;
  color: #2e2e2e;
}
.titoloDocumenti{
  -webkit-backdrop-filter: blur(30px);
  backdrop-filter: blur(30px);
  font-family: TitilliumWeb;
  font-size: 18px;
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.33;
  letter-spacing: normal;
  text-align: left;
  color: #11306e;    
}
.descrizioneDocumenti{
  font-family: TitilliumWeb;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #000000;
}    
    
#imageDoc{
  width: 26.5px;
  height: 28.7px;
    
}

</style>