<footer id="footer" class="container-fluid mt-3">
	<div class="row py-1 valuta-il-sito">
		<div class="col d-flex justify-content-center align-items-center">
			<a href="${rate_this_site.getFriendlyUrl()}"><@liferay.language key="por.footer.rate-this-site" /></a>
		</div>
	</div>
	<div class="row py-4">
		<div class="col-md-4 px-4 px-md-5 regione">
			<#if logo.getData()?? && logo.getData() != "">
				<img alt="${logo.getAttribute("alt")}" data-fileentryid="${logo.getAttribute("fileEntryId")}" src="${logo.getData()}" />
			</#if>
		</div>
	</div>
	<div class="row py-md-4">
		<div class="col-md-4 px-4 px-md-5 py-3 py-md-0 contatti">
			<p class="mb-1 section-title"><@liferay.language key="por.footer.office-contacts" /></p>
			<hr class="d-none d-sm-none d-md-block my-2">
			${sede.getData()}
		</div>

		<div class="col-md-4 px-4 px-md-5 py-3 py-md-0 link-utili">
			<p class="mb-1 section-title"><@liferay.language key="por.footer.useful-links" /></p>
			<hr class=" d-none d-sm-none d-md-block my-2">
			<ul class="px-0">
				<#if hr_link_utili.label_link_utili.getSiblings()?has_content>
					<#list hr_link_utili.label_link_utili.getSiblings() as cur_link>
						<#assign link = cur_link.URL_link_utili.getData()!"#" >
						<li>
							<a href="${link}" target="_blank" >
								${cur_link.getData()}
							</a>
						</li>
					</#list>
				</#if>
			</ul>
		</div>
		<div class="col-md-4 px-4 px-md-5 py-3 py-md-0 d-flex flex-column justify-content-around">
			<div class="col-md-auto d-none d-sm-none d-md-block search">
				<@liferay.search_bar default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
			</div>

			<div class="col-md-auto d-flex justify-content-md-end justify-content-center social">
				<a>
			        <span class="icon-stack mx-1">
			          <i class="icon-circle icon-stack-base"></i>
			          <i class="icon-facebook"></i>
			        </span>
				</a>
				<a>
			        <span class="icon-stack mx-1">
			          <i class="icon-circle icon-stack-base"></i>
			          <i class="icon-linkedin"></i>
			        </span>
				</a>
				<a>
			        <span class="icon-stack mx-1">
			          <i class="icon-circle icon-stack-base"></i>
			          <i class="icon-twitter"></i>
			        </span>
				</a>
				<a>
			        <span class="icon-stack mx-1">
			          <i class="icon-circle icon-stack-base"></i>
			          <i class="icon-youtube-play"></i>
			        </span>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col px-4 px-md-5">
			<hr class="my-2">
		</div>
	</div>
	<div class="row px-4 px-md-5 pb-4 legals">
		<div class="col-md-5 d-flex align-items-center p-0">
			<#if hr_legals.label_legals.getSiblings()?has_content>
				<ul class="d-flex flex-md-row w-100 flex-column justify-content-between flex-wrap m-0 p-0">
					<#list hr_legals.label_legals.getSiblings() as cur_legal>
						<#assign link_legals = cur_legal.URL_legals.getData()!"#" >
						<li class="py-2">
							<a href="${link_legals}" target="_blank">
								${cur_legal.getData()}
							</a>
						</li>
					</#list>
				</ul>
			</#if>
		</div>
		<div class="col-12 p-0 d-block d-md-none">
			<hr class="my-2">
		</div>
		<div class="col-md-auto ml-auto d-flex flex-column p-2 p-md-0">
			<#assign link_diritti = label_diritti.URL_diritti.getData()!"#" >
			<a href="${link_diritti}" target="_blank" class="py-2 ml-md-auto text-white mx-sm-auto align-self-center">${label_diritti.getData()}</a>
		</div>
	</div>
</footer>