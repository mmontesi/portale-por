<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign public_warning_extention = readPublicWarningText.readPublicWarningDocument.publicWarningsDocumentType.getData()!"">
<#assign public_warning_extention_image ="${imagesPath}/custom/estensioni/${public_warning_extention}.svg">

<#assign look_determination_extention =lookDeterminationTitle.lookDeterminationDocument.determinationDocumentType.getData()!"">
<#assign look_determination_extention_image ="${imagesPath}/custom/estensioni/${look_determination_extention}.svg">

<#assign consult_burp_image = "${imagesPath}/custom/generali/burp.svg" />
<#assign go_to_site_image = "${imagesPath}/custom/generali/sito-esterno.svg" />
<#assign professional_training_section_image = "${imagesPath}/custom/temi/formazione-professionale.svg" />


<div class="container-fluid pr-0 bando-maggiori-informazioni">
	<div class="row">
		<div class="col-md-12 pr-0">
			<#if ImmaginePerMaggioriInfo.getData()?? && ImmaginePerMaggioriInfo.getData() != "">
				<img class="immagine" alt="${ImmaginePerMaggioriInfo.getAttribute("alt")}" data-fileentryid="${ImmaginePerMaggioriInfo.getAttribute("fileEntryId")}" src="${ImmaginePerMaggioriInfo.getData()}" />
			</#if>
		</div>
	</div>
	<div class="row box-informazioni ml-0">
		<div class="col-md-12 p-4">
			<h3>${moreInfoTitle.getData()}</h3>

			<#if readPublicWarningText.getData() != "">
				<p class="avviso-pubblico">
					<img src="${public_warning_extention_image}" class="doc-type" alt="documentType">
					<a href="${readPublicWarningText.readPublicWarningDocument.getData()}">
						${readPublicWarningText.getData()}
					</a>
				</p>
			</#if>
			<#if lookDeterminationTitle.getData() != "">
				<p class="determinazione">
					<img src="${look_determination_extention_image}" class="doc-type" alt="documentType">
					<a href="${lookDeterminationTitle.lookDeterminationDocument.getData()}">
						${lookDeterminationTitle.getData()}
					</a>
				</p>
			</#if>
			<#if consultBurpTitle.consultBurpLink.consultBurpImage.getData() != "">
				<p class="burp">
					<img class="" alt="Consulta il Burp" src="${consult_burp_image}" />
					<a href="${consultBurpTitle.consultBurpLink.getData()}" target=”_blank”>
						${consultBurpTitle.getData()}
					</a>
				</p>
			</#if>
			<#if goToSiteTitle.goToSiteLink.goToSiteImage.getData() != "">
				<p class="sito-esterno">
					<img class="" alt="Visita il sito" src="${go_to_site_image}" />
					<a href="${goToSiteTitle.goToSiteLink.getData()}" target=”_blank”>
						${goToSiteTitle.getData()}
					</a>
				</p>
			</#if>
			<#if professionalTrainingSectionTitle.professionalTrainingSectionLink.professionalTrainingSectionImage.getData() != "">
				<p class="formazione-professionale">
					<img class="" alt="Sezione Formazione Professionale" src="${professional_training_section_image}" />
					<a href="${professionalTrainingSectionTitle.professionalTrainingSectionLink.getData()}" target=”_blank”>
						${professionalTrainingSectionTitle.getData()}
					</a>
				</p>
			</#if>


			<#if role.getSiblings()?has_content>
				<#list role.getSiblings() as cur_role>
					<section class="responsabili my-3">
						<p>${cur_role.getData()}</p>
						<#if cur_role.roleName.getData() != "">
							<p>
								<a href="${cur_role.roleLink.getData()}" target=”_blank”>
									${cur_role.roleName.getData()}
								</a>
								${cur_role.rankingList.getData()}
							</p>
						</#if>
						<#if cur_role.phone.getData() != "">
							<p>
								${cur_role.phone.getData()}
							</p>
						</#if>
						<#if cur_role.email.getData() != "">
							<p>
								<a href="mailto:${cur_role.email.getData()}">${cur_role.email.getData()}</a>
							</p>
						</#if>
						<#if cur_role.pec.getData()!= "">
							<p>
								<a href="mailto:${cur_role.pec.getData()}">${cur_role.pec.getData()}</a>
							</p>
						</#if>
					</section>
				</#list>
			</#if>
		</div>
	</div>
</div>