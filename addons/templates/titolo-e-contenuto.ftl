<div class="titolo-e-contenuto">
	<div class="row">
		<div class="col-12">
			<h1>${.vars['reserved-article-title'].data}</h1>
			<div class="content">${content.getData()}</div>
		</div>
	</div>
</div>