<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />

<div class="row">
		<div class="archivio">
		<h3 class="titoloPagina">${.vars['reserved-article-title'].data}</h3>
			${content.getData()}
		</div>
   <!-- <div class="col-sm-8 col-md-8 col-lg-8">
		<#if currentArticleRelatedLinks?has_content>
		
			<#list currentArticleRelatedLinks as related_entry>
				<#assign relatedAssetEntryId = related_entry.getEntryId2() />
				<#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
				<#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
				<#assign assetRenderer = relatedAssetEntry.getAssetRenderer() />
				<#if relatedAssetEntry.getClassName()?string == "com.liferay.document.library.kernel.model.DLFileEntry" >
				
		
			<div class="col media pt-3 ml-0 d-flex align-items-center">
				
				<#assign portalURL = themeDisplay.getPortalURL() />
				<#assign slash = "/" />
				<#assign fileEntry = dlAppLocalService.getFileEntry(relatedAssetEntryPrimKey) />
				<#assign repositoryId = fileEntry.getRepositoryId() />
				<#assign folderId = fileEntry.getFolderId() />
				<#assign fileVersion = fileEntry.getFileVersion() />
				<#assign modifiedDate = fileVersion.getModifiedDate()?time />
				<#assign fileName = fileEntry.getFileName() />									                         <#assign previewURL = portalURL + "/documents/" + repositoryId + "/" + folderId + "/" + fileName + "/" + fileEntry.getUuid() + "?t=" + modifiedDate />	
						
				<#if (assetRenderer.assetObject.extension)?has_content>
													
				<a class="document-title" target="_blank" href="${previewURL?string}"><img class="mr-3 file-extension" id="imageDocAut" alt="${assetRenderer.assetObject.extension?html}" src="${imagesPath}/extensions/${assetRenderer.assetObject.extension?html}.png" /></a>

				</#if>
				
				<div class="media-body">		
				<a class="document-title" target="_blank">${relatedAssetEntry.description?string}</a>
				</div>
			</div>
		
				</#if>
			</#list>
		</#if>
			<ul class="pagination">
				<li class="arrow unavailable"><a href="">&laquo;</a></li>
				<li class="current"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li class="unavailable"><a href="">&hellip;</a></li>
				<li><a href="">6</a></li>
				<li class="arrow"><a href="">&raquo;</a></li>
			</ul>
	</div> -->	
</div>