<div class="atterraggio-form">
	<div class="atterraggio-form-inner text-center p-4">
		<h1>${text.getData()}</h1>
		<a href="${buttonLabel.buttonLink.getFriendlyUrl()}" class="btn btn-primary mt-4">
			${buttonLabel.getData()}
		</a>
	</div>
</div>