<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />

<div class="row">
    <div class="col-sm-8 col-md-8 col-lg-8">
       <b><h1 class="titoloPagina">${.vars['reserved-article-title'].data} </h1></b>
        <div class="content">
			${content.getData()}
		</div>
        <br>
            <hr/>
        <br>
        
    
           	<#if currentArticleRelatedLinks?has_content>
				<h3><@liferay.language key="por.documents" /></h3>
			
				<#list currentArticleRelatedLinks as related_entry>
					<#assign relatedAssetEntryId = related_entry.getEntryId2() />
					<#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
					<#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
					<#assign assetRenderer = relatedAssetEntry.getAssetRenderer() />
					<#if relatedAssetEntry.getClassName()?string == "com.liferay.document.library.kernel.model.DLFileEntry" >
					
			
				<div class="col media pt-3 ml-0 d-flex align-items-center">
	                <#assign portalURL = themeDisplay.getPortalURL() />
				    <#assign slash = "/" />
				    <#assign fileEntry = dlAppLocalService.getFileEntry(relatedAssetEntryPrimKey) />
				    <#assign repositoryId = fileEntry.getRepositoryId() />
				    <#assign folderId = fileEntry.getFolderId() />
        			<#assign fileVersion = fileEntry.getFileVersion() />
        			<#assign modifiedDate = fileVersion.getModifiedDate()?time />
        			<#assign fileName = fileEntry.getFileName() />
					<#assign previewURL = portalURL + "/documents/" + repositoryId + "/" + folderId + "/" + fileName + "/" + fileEntry.getUuid() + "?t=" + modifiedDate />	
							
					<#if (assetRenderer.assetObject.extension)?has_content>
											
								
                    <a class="document-title" target="_blank" href="${previewURL?string}"><img class="mr-3 file-extension" id="imageDocAut" alt="${assetRenderer.assetObject.extension?html}" src="${imagesPath}/custom/estensioni/${assetRenderer.assetObject.extension?html}.svg" /></a>
 
					</#if>
					
					<div class="media-body">		
					  <a class="document-title" target="_blank" href="${previewURL?string}">${relatedAssetEntry.description?string}</a>
					</div>
				</div>
			
					</#if>
				</#list>
			</#if>
    </div>
    
            <div class="col-sm-4 col-md-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">DIRIGENTE DI SEZIONE</h4><br>
							<ul class="list-group" style="list-style-type: none;">
								<li class="item">
									<p><a href="${leaderName.leaderLink.getData()}" target=”_blank”>
										<img class="icon"  src="${imagesPath}/icon/.png" />
									        ${leaderName.getData()} 
									</a></p>
								</li>
								<li class="item">  
									<p><img class="icon"  src="${imagesPath}/icon/.png" />
									${leaderName.seat.getData()}</p>
								</li>
								<li class="item"> 
									<p><img class="icon"  src="${imagesPath}/icon/.png" />
									${leaderName.phone.getData()} </p>
								</li>
								<li class="item">
									<p><img class="icon"  src="${imagesPath}/icon/.png" /><a href="${leaderName.email.getData()}"> ${leaderName.email.getData()}</a></p>
								</li>
								<li class="item">
									<#if leaderName.pecEmail.getData()!= ""><p><img class="icon" src="${imagesPath}/icon/.png" /></#if><a href="${leaderName.pecEmail.getData()}">${leaderName.pecEmail.getData()}</a></p>
								</li>
							</ul>
						<div>
					<div>
                </div>

</div>


<style>


.card{
    background-color: rgba(154, 151, 151, 0.1);
}


</style>