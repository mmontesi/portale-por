<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>
<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>

<#assign bando_title = .vars['reserved-article-title'].data >

<#assign consult_burp_icon = "${imagesPath}/custom/generali/burp.svg" />
<#assign go_to_site_icon = "${imagesPath}/custom/generali/sito-esterno.svg" />
<#assign professional_training_section_icon = "${imagesPath}/custom/temi/formazione-professionale.svg" />

<#assign phone_icon = "${imagesPath}/custom/contatti/telefono.svg" />
<#assign email_icon = "${imagesPath}/custom/contatti/email.svg" />
<#assign pec_icon = "${imagesPath}/custom/contatti/pec.svg" />

<#assign informations_icon = "${imagesPath}/custom/generali/info.svg" />
<#assign questions_icon = "${imagesPath}/custom/generali/domande.svg" />

<#assign hrefPartecipa = "">
<#if partecipa_link.getData()?has_content && partecipa_link.getData() != "">
	<#assign hrefPartecipa = partecipa_link.getData()>
<#elseif partecipa_link.partecipa_email.getData()?has_content && partecipa_link.partecipa_email.getData() != "">
	<#assign hrefPartecipa = "mailto:" + partecipa_link.partecipa_email.getData()>
</#if>

<div class="container-fluid p-0 bando">
	<div class="row">
		<div class="col-12">
			<h1 class="titolo mb-3">
				${bando_title}
			</h1>
		</div>
	</div>
	<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) >
	<#if categories?has_content>
		<div class="row">
			<div class="col-12 my-3 categories-content">
				<#assign categoriesVocabularyMap = {} >
				<#list categories as category>
					<#assign vocabularyName = vocabularyService.getVocabulary(category['vocabularyId']?number)['name'] >
					<#if categoriesVocabularyMap[vocabularyName]?has_content>
						<#assign temp = [] >
						<#assign temp += [category] >
						<#assign temp += categoriesVocabularyMap[vocabularyName] >
						<#assign categoriesVocabularyMap += {vocabularyName: temp} >
					<#else>
						<#assign categoriesVocabularyMap += {vocabularyName: [category]} >
					</#if>
				</#list>
				<#if categoriesVocabularyMap['Fondo']?has_content>
					<#list categoriesVocabularyMap['Fondo'] as category>
						<#assign badgeClass = "primary" >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
				<#if categoriesVocabularyMap['Assi']?has_content>
					<#list categoriesVocabularyMap['Assi'] as category>
						<#assign badgeClass = "asse" >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
				<#if categoriesVocabularyMap['Azioni']?has_content>
					<#list categoriesVocabularyMap['Azioni'] as category>
						<#assign badgeClass = "azione" >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand"><@liferay.language key="por.action-badge" />&nbsp;${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
				<#if categoriesVocabularyMap['Tema']?has_content>
					<#list categoriesVocabularyMap['Tema'] as category>
						<#assign badgeClass = "secondary" >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
				<#if categoriesVocabularyMap['Modalità bando']?has_content>
					<#list categoriesVocabularyMap['Modalità bando'] as category>
						<#assign badgeClass = category.getTitle('it_IT')?lower_case?replace(" ", "-") >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
				<#if categoriesVocabularyMap['Attuazione bando']?has_content>
					<#list categoriesVocabularyMap['Attuazione bando'] as category>
						<#assign badgeClass = category.getTitle('it_IT')?lower_case?replace(" ", "-") >
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</#if>
			</div>
		</div>
	</#if>
	<div class="row">
		<div class="col-md-8">
			<div aria-orientation="vertical" class="panel-group mb-4" id="accordionInfo" role="tablist">
				<div class="panel">
					<button
						aria-controls="collapseInfo"
						aria-expanded="false"
						class="btn btn-unstyled collapse-icon collapsed panel-header panel-header-link p-0 border-top"
						data-parent="#accordionInfo"
						data-target="#accordionInfoCollapse"
						data-toggle="collapse"
						id="accordionInfoHeading"
						role="tab"
						type="button"
					>
						<span class="panel-title">
							<h2 class="h3 m-0">
								<img alt="thumbnail" src="${informations_icon}">
								<@liferay.language key="por.announcement.informations" />
							</h2>
						</span>
						<span class="collapse-icon-closed">
							<i class="icon-2x icon-angle-down"></i>
						</span>
						<span class="collapse-icon-open">
							<i class="icon-2x icon-angle-up"></i>
						</span>
					</button>
					<div aria-labelledby="accordionInfoHeading" class="panel-collapse collapse" id="accordionInfoCollapse" role="tabpanel">
						<div class="panel-body p-4">
							<div class="container-fluid px-0">
								<div class="row p-4 mb-4 quando-partecipare">
									<div class="col-md-8">
										<h3 class="titolo-sezione">
											<@liferay.language key="por.announcement.when-to-partecipate" />
										</h3>
										<#assign from_Data = getterUtil.getString(bando_da.getData())>
										<#assign to_Data = getterUtil.getString(bando_a.getData())>
										<#if validator.isNotNull(from_Data)>
											<#assign from_DateObj = dateUtil.parseDate("yyyy-MM-dd", from_Data, locale)>
											<span class="data">${dateUtil.getDate(from_DateObj, "dd/MM/yyyy" , locale)}</span>
										</#if>
										<#if validator.isNotNull(to_Data)>
											<#assign to_DateObj = dateUtil.parseDate("yyyy-MM-dd", to_Data, locale)>
											<span class="data"> - ${dateUtil.getDate(to_DateObj, "dd/MM/yyyy", locale)}</span>
										</#if>
									</div>
									<div class="col-md-4 d-flex justify-content-center align-items-center">
										<a class="btn btn-por-default btn-block btn-sm partecipa" href="${hrefPartecipa}" target="_blank">
											<@liferay.language key="por.announcement.partecipates" />
										</a>
									</div>
								</div>
								<#if descrizione.getData()?has_content>
									<div class="row descrizione">
										<div class="col">
											${descrizione.getData()}
										</div>
									</div>
								</#if>
								<div class="row chi-partecipa">
									<div class="col">
										<h3 class="titolo-sezione">
											<@liferay.language key="por.announcement.who-can-partecipates" />
										</h3>
										${chiPartecipa.getData()}
									</div>
								</div>
								<div class="row cosa-finanzia">
									<div class="col">
										<h3 class="titolo-sezione">
											<@liferay.language key="por.announcement.what-finances" />
										</h3>
										${cosaFinanzia.getData()}
									</div>
								</div>
								<#if (risorseProgetto.getData()?? && risorseProgetto.getData()?trim != "") && (risorseTotali.getData()?? && risorseTotali.getData()?trim != "") >
									<div class="row d-flex risorse-disponibili text-center border-top border-bottom border-info">
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-auto">
													<@liferay.language key="por.announcement.project-resources" />
												</div>
												<div class="col-md-auto">
													&euro;&nbsp;${risorseProgetto.getData()?number?string("#,###")}
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="row">
												<div class="col-md-auto">
													<@liferay.language key="por.announcement.total-resources" />
												</div>
												<div class="col-md-auto">
													&euro;&nbsp;${risorseTotali.getData()?number?string("#,###")}
												</div>
											</div>
										</div>
									</div>
								</#if>
								<div class="row p-4 my-4 come-partecipare">
									<div class="col-12">
										<div class="row">
											<div class="col">
												<h3 class="titolo-sezione"><@liferay.language key="por.announcement.how-to-partecipate" /></h3>
												${comePartecipare.getData()}
											</div>
										</div>
										<#if comePartecipare.documento.getSiblings()?has_content>
											<#list comePartecipare.documento.getSiblings() as documento >
												<#assign documento_data = jsonFactoryUtil.createJSONObject(documento.data)>
												<#assign document_extention = documento_data.getString("title")?keep_after_last(".")>
												<div class="row documenti">
													<div class="col">
														<img src="${imagesPath}/custom/estensioni/${document_extention}.svg" class="m-2 doc-type" alt="documentType">
														<a href="${documento.getData()}">
															${documento.titoloDocumento.getData()}
														</a>
													</div>
												</div>
											</#list>
										</#if>
										<div class="row d-flex justify-content-end">
											<div class="col-md-12">
												<a class="btn btn-por-default btn-block btn-sm partecipa pull-right" href="${hrefPartecipa}" target="_blank">
													<@liferay.language key="por.announcement.partecipates" />
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button
						aria-controls="collapseDomande"
						aria-expanded="false"
						class="btn btn-unstyled collapse-icon collapsed panel-header panel-header-link p-0 border-top"
						data-parent="#accordionDomande"
						data-target="#accordionDomandeCollapse"
						data-toggle="collapse"
						id="accordionDomandeHeading"
						role="tab"
						type="button"
					>
						<span class="panel-title">
							<h2 class="h3 m-0">
								<img alt="thumbnail" src="${questions_icon}">
								<@liferay.language key="por.announcement.questions" />
							</h2>
						</span>
						<span class="collapse-icon-closed">
							<i class="icon-2x icon-angle-down"></i>
						</span>
						<span class="collapse-icon-open">
							<i class="icon-2x icon-angle-up"></i>
						</span>
					</button>
					<div aria-labelledby="accordionDomandeHeading" class="panel-collapse collapse" id="accordionDomandeCollapse" role="tabpanel">
						<div class="panel-body p-4">
							<div class="container-fluid px-0">
								<div class="row domande-frequenti">
									<div class="col-md-12">
										<h3 class="h2 titolo-sezione">
											<@liferay.language key="por.announcement.faq" />
										</h3>
										<#if domanda.getSiblings()?has_content>
											<#list domanda.getSiblings() as cur_domanda>
												<p class="domanda">${cur_domanda.getData()}</p>
												${cur_domanda.risposta.getData()}
											</#list>
										</#if>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<#if currentArticleRelatedLinks?has_content>
				<div class="row aggiornamenti">
					<div class="col-12">
						<h2 class="mb-4 titolo-sezione">
							<@liferay.language key="por.announcement.updates" />
						</h2>
						<#list currentArticleRelatedLinks as related_entry>
							<#assign
								relatedAssetEntryId = related_entry.getEntryId2()
								relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId)
								relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK()
								assetRenderer = relatedAssetEntry.getAssetRenderer()
								entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
								entrySummary = htmlUtil.escape(assetRenderer.getSummary(locale))
							/>
							<#if relatedAssetEntry.getClassName()?string == "com.liferay.journal.model.JournalArticle" >
								<#assign journalArticle = journalArticleLocalService.getLatestArticle(relatedAssetEntryPrimKey) >
								<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", relatedAssetEntryPrimKey) >
								<#list categories as category>
									<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>

									<#if categoryVocabulary == "Tipologia notizia">
										<#assign labelTipologia = category.getTitle(locale) >
										<#assign badgeAggiornamentiClass = category['name']?lower_case?replace(" ", "-") >
									<#else>
										<#assign labelTipologia = "">
										<#assign badgeAggiornamentiClass = "secondary" >
									</#if>
								</#list>

								<div class="row">
									<div class="col-12">
								<span class="badge badge-por badge-${badgeAggiornamentiClass} m-0">
									<span class="badge-item badge-item-expand">${labelTipologia}</span>
								</span>
									</div>
								</div>
								<div class="row d-flex align-items-center">
									<div class="col-auto data-pubblicazione">
										<span class="my-2">${relatedAssetEntry.getPublishDate()?string["dd/MM/yyyy"]}</span>
									</div>
									<div class="col-auto titolo-notizia">
										<h3 class="my-2">${entryTitle}</h3>
									</div>
								</div>
								<div class="row">
									<div class="col sommario">
										<p>${entrySummary}</p>
									</div>
								</div>
								<div class="row continua-a-leggere">
									<div class="col">
										<a class="read-more" href="/${themeDisplay.getLocale()?substring(0, 2)}/web${themeDisplay.getScopeGroup()['friendlyURL']}/-/${journalArticle['urlTitle']}">
											<@liferay.language key="por.keep-reading" />
											<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
										</a>
									</div>
								</div>
							</#if>
						</#list>
					</div>
				</div>
			</#if>
		</div>
		<div class="col-md-4 bando-maggiori-informazioni">
			<div class="row">
				<div class="col-md-12">
					<#if immagine.getData()?? && immagine.getData() != "">
						<img class="immagine" alt="${immagine.getAttribute("alt")}" data-fileentryid="${immagine.getAttribute("fileEntryId")}" src="${immagine.getData()}" />
					</#if>
				</div>
			</div>
			<div class="row box-informazioni m-0 pl-md-0">
				<div class="col-md-12 p-4">
					<h3>
						<@liferay.language key="por.announcement.more-info" />
					</h3>

					<#if avvisoPubblico.getData()?? && avvisoPubblico.getData() != "">
						<p class="avviso-pubblico">
							<#assign avviso_pubblico_data = jsonFactoryUtil.createJSONObject(avvisoPubblico.data)>
							<#assign document_extention = avviso_pubblico_data.getString("title")?keep_after_last(".")>
							<img src="${imagesPath}/custom/estensioni/${document_extention}.svg" class="doc-type" alt="documentType">
							<a href="${avvisoPubblico.getData()}">
								<@liferay.language key="por.announcement.read-public-warning" />
							</a>
						</p>
					</#if>
					<#if determinazione.getData()?? && determinazione.getData() != "">
						<p class="determinazione">
							<#assign determinazione_data = jsonFactoryUtil.createJSONObject(determinazione.data)>
							<#assign document_extention = determinazione_data.getString("title")?keep_after_last(".")>
							<img src="${imagesPath}/custom/estensioni/${document_extention}.svg" class="doc-type" alt="documentType">
							<a href="${determinazione.getData()}">
								<@liferay.language key="por.announcement.determination" />
							</a>
						</p>
					</#if>
					<#if burp.getData() != "">
						<p class="burp">
							<img class="" alt="Consulta il Burp" src="${consult_burp_icon}" />
							<a href="${burp.getData()}" target=”_blank”>
								<@liferay.language key="por.announcement.burp" />
							</a>
						</p>
					</#if>
					<#if sito.getData() != "">
						<p class="sito-esterno">
							<img class="" alt="Visita il sito" src="${go_to_site_icon}" />
							<a href="${sito.getData()}" target=”_blank”>
								<@liferay.language key="por.announcement.external-site" />
							</a>
						</p>
					</#if>
					<#if formazioneProfessionale.getData() != "">
						<p class="formazione-professionale">
							<img class="" alt="Sezione Formazione Professionale" src="${professional_training_section_icon}" />
							<a href="${formazioneProfessionale.getData()}" target=”_blank”>
								<@liferay.language key="por.announcement.professional-training-section" />
							</a>
						</p>
					</#if>
					<#if responsabileDiProcedimento.getSiblings()?has_content>
						<#list responsabileDiProcedimento.getSiblings() as cur_responsabile>
							<section class="responsabili my-3">
								<p class="responsabile-procedimento text-bold">
									<@liferay.language key="por.announcement.proceedings-person-in-charge" />
								</p>
								<p>
									<a href="${cur_responsabile.procedimento_link.getData()}" target=”_blank”>
										${cur_responsabile.getData()}
									</a>
									<#if cur_responsabile.procedimento_graduatoria.getData() != "">
										<#assign result = "">
										<#list cur_responsabile.procedimento_graduatoria['optionsMap']?keys as key >
											<#if cur_responsabile.procedimento_graduatoria['data'] == key >
												<#assign result = cur_responsabile.procedimento_graduatoria['optionsMap'][key] >
											</#if>
										</#list>
										(${result})
									</#if>
								</p>

								<#if cur_responsabile.procedimento_telefono.getData() != "">
									<p>
										<img class="" alt="Sezione Formazione Professionale" src="${phone_icon}" />
										${cur_responsabile.procedimento_telefono.getData()}
									</p>
								</#if>
								<#if cur_responsabile.procedimento_email.getData() != "">
									<p>
										<img class="" alt="Sezione Formazione Professionale" src="${email_icon}" />
										<a href="mailto:${cur_responsabile.procedimento_email.getData()}">
											${cur_responsabile.procedimento_email.getData()}
										</a>
									</p>
								</#if>
								<#if cur_responsabile.procedimento_pec.getData()!= "">
									<p>
										<img class="" alt="Sezione Formazione Professionale" src="${pec_icon}" />
										<a href="mailto:${cur_responsabile.procedimento_pec.getData()}">
											${cur_responsabile.procedimento_pec.getData()}
										</a>
									</p>
								</#if>
							</#list>
						</section>
					</#if>
					<#if responsabileDiAzione.getSiblings()?has_content>
						<section class="responsabili my-3">
							<p class="responsabile-procedimento text-bold">
								<@liferay.language key="por.announcement.action-person-in-charge" />
							</p>
							<#if responsabileDiAzione.azione_link.getData() != "">
								<p>
									<a href="${responsabileDiAzione.azione_link.getData()}" target=”_blank”>
										${responsabileDiAzione.getData()}
									</a>
								</p>
							</#if>
							<#if responsabileDiAzione.azione_telefono.getData() != "">
								<p>
									<img class="" alt="Sezione Formazione Professionale" src="${phone_icon}" />
									${responsabileDiAzione.azione_telefono.getData()}
								</p>
							</#if>
							<#if responsabileDiAzione.azione_email.getData() != "">
								<p>
									<img class="" alt="Sezione Formazione Professionale" src="${email_icon}" />
									<a href="mailto:${responsabileDiAzione.azione_email.getData()}">
										${responsabileDiAzione.azione_email.getData()}
									</a>
								</p>
							</#if>
							<#if responsabileDiAzione.azione_pec.getData()!= "">
								<p>
									<img class="" alt="Sezione Formazione Professionale" src="${pec_icon}" />
									<a href="mailto:${responsabileDiAzione.azione_pec.getData()}">
										${responsabileDiAzione.azione_pec.getData()}
									</a>
								</p>
							</#if>
						</section>
					</#if>
					<#if altriContatti.getData() != "">
						<p>
							${altriContatti.getData()}
						</p>
					</#if>
				</div>
			</div>
		</div>
	</div>
</div>