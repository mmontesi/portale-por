<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />

<#assign
	prevArrow = "<button class='slick-prev slick-arrow m-3' aria-label='Previous' type='button' style='border: none; background: transparent; cursor: pointer;'><i class='lexicon-icon icon-angle-left icon-2x' aria-hidden='true'></i></button>"
	nextArrow = "<button class='slick-prev slick-arrow m-3' aria-label='Next' type='button' style='border: none; background: transparent; cursor: pointer;'><i class='lexicon-icon icon-angle-right icon-2x' aria-hidden='true'></i></button>"
>

<div class="row">
	<div class="col-md-4">
		<h1>${.vars['reserved-article-title'].data}</h1>
	</div>
    <div class="col-md-8">
        ${content.getData()}
    </div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="priorita-carousel por-carousel">
			<#if textSlider.getSiblings()?has_content>
				<div class="slider d-flex flex-row"
				     data-slick='{"autoplay": false, "arrows": true, "autoplaySpeed": 3000, "slidesToShow": 1, "slidesToScroll": 1, "dots": true, "infinite": true, "speed": 300, "pauseOnHover": true, "pauseOnFocus": true}'
				     data-prevArrow="${prevArrow}"
				     data-nextArrow="${nextArrow}"
				>
					<#list textSlider.getSiblings() as cur_textSlider >
						<div class="container-fluid my-5 d-flex flex-column justify-content-center">
							<div class="row">
								<div class="col-md-4">
									<#if cur_textSlider.getData()?? && cur_textSlider.getData() != "">
										<p>${cur_textSlider.getData()} </p>
									</#if>
									<#if cur_textSlider.textSlider2.getData()?? && cur_textSlider.textSlider2.getData() != "">
										<p>${cur_textSlider.textSlider2.getData()}</p>
									</#if>
								</div>
								<div class="col-md-8">
									${cur_textSlider.sliderContent.getData()}
								</div>
							</div>
						</div>
					</#list>
				</div>
			</#if>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-8 offset-md-4">
		<div class="row">
			<div class="col-12">
				<div class="my-3 embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="${videoContent.getData()}" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<#if plan.getSiblings()?has_content>
			<div class="row">
				<div class="col-12 px-0">
					<div aria-orientation="vertical" class="panel-group mb-4" id="accordionPlans" role="tablist">
						<div class="panel">
							<#list plan.getSiblings() as cur_plan>
								<#assign index = cur_plan?index>
								<button
									aria-controls="collapseInfo"
									aria-expanded="false"
									class="btn btn-unstyled collapse-icon collapsed panel-header panel-header-link border-top"
									data-parent="#accordionPlans"
									data-target="#accordionPlan${index}"
									data-toggle="collapse"
									id="accordionPlan${index}Heading"
									role="tab"
									type="button"
								>
									<span class="panel-title">
										<h2 class="h3 m-0">
											${cur_plan.getData()}
										</h2>
									</span>
									<span class="collapse-icon-closed">
										<i class="icon-2x icon-angle-down"></i>
									</span>
									<span class="collapse-icon-open">
										<i class="icon-2x icon-angle-up"></i>
									</span>
								</button>

								<div aria-labelledby="accordionPlan${index}Heading" class="panel-collapse collapse" id="accordionPlan${index}" role="tabpanel">
									<div class="panel-body p-0 p-md-4">
										<div class="card card-body" style="box-shadow:none">
											${cur_plan.planDetails.getData()}
											<#if cur_plan.planDocument.getSiblings()?has_content>
												<#list cur_plan.planDocument.getSiblings() as cur_document> <br>
													<div class="row">
														<div class="col-md-12 d-flex">
															<img class="mr-2 documentImage" src="${imagesPath}/custom/estensioni/${cur_document.documentType.getData()}.svg">
															<a class="text-truncate" href="${cur_document.getData()}">
																${cur_document.documentText.getData()}
															</a>
														</div>
													</div>
												</#list>
											</#if>
										</div>
									</div>
								</div>
							</#list>
						</div>
					</div>
				</div>
			</div>
		</#if>
	</div>
</div>