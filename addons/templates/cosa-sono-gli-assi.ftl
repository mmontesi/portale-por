<#assign layoutService = serviceLocator.findService("com.liferay.portal.kernel.service.LayoutLocalService") />
<#assign serviceContext = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"].getServiceContext() />
<#assign themeDisplay = serviceContext.getThemeDisplay() />

<div class="cosa-sono-gli-assi mt-4">
	<div class="row">
		<div class="col-sm-8 col-md-8 col-lg-8">
			<h1>${.vars['reserved-article-title'].data}</h1>
			<div class="content mt-4">${content.getData()}</div>
			<#if videoUrl.getData()?has_content && videoUrl.getData() != "">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="${videoUrl.getData()}" allowfullscreen></iframe>
				</div>
			</#if>
		</div>

			
		<div class="col-md-4 col-lg-4">
				
			<h2 class="h3"><@liferay.language key="por.priority-axys" /></h2>
			
			<#if repeatableLink.getSiblings()?has_content> 
				<div class="list-item mt-3">
				<#list repeatableLink.getSiblings() as cur_repeatableLink> 
					<#attempt>
					<#assign layout = layoutService.getLayout(groupId,false,cur_repeatableLink.getData()?number) />
					<#assign pageTitle = layout.getName(locale) />

					<a href="${cur_repeatableLink.getFriendlyUrl()}">${pageTitle}</a> <br/><br/>
					<#recover>
					</#attempt>
				</#list>
				</div>
			</#if>

		</div>

	</div>
</div>