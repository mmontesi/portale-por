<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign document_extention = documentsTitle.document.documentType.getData()!"">
<#assign document_extention_image ="${imagesPath}/custom/estensioni/${document_extention}.svg">

<#assign go_to_site_image = "${imagesPath}/custom/generali/sito-esterno.svg" />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />

<#assign categories = [] >
<#list currentArticleAssetEntry.getCategories() as category>
	<#assign categories = categories + [category] >
</#list>

<div class="container-fluid p-0 bando">
	<div class="row">
		<div class="col-sm-8 col-md-8 col-lg-8">
			<h1>${.vars['reserved-article-title'].data}</h1>
			<#if categories?has_content>
				<div class="categories-content pt-2 mt-5 mb-5">
					<#list categories as category>
						<#assign badgeClass = "primary" >
						<#assign vocabularyName = vocabularyService.getVocabulary(category['vocabularyId']?number)['name'] >
						<#if vocabularyName == "Assi">
							<#assign badgeClass = "asse" >
						<#elseif vocabularyName == "Azioni">
							<#assign badgeClass = "azione" >
						<#elseif vocabularyName == "Tema" || vocabularyName == "Beneficiari">
							<#assign badgeClass = "secondary" >
						<#elseif vocabularyName == "Approfondimenti">
							<#assign badgeClass = "insights" >
						<#else>
							<#assign badgeClass = category.getTitle('it_IT')?lower_case?replace(" ", "-") >
						</#if>
						<span class="badge badge-por badge-${badgeClass}">
							<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
						</span>
					</#list>
				</div>
			</#if>
			<div class="grande-progetto">
				${content.getData()}
			</div>
		</div>
		<div class="col-md-4 bando-maggiori-informazioni">
			<div class="row">
				<div class="col-md-12 pr-0">
					<#if moreInfoImage.getData()?? && moreInfoImage.getData() != "">
						<img class="immagine" alt="${moreInfoImage.getAttribute("alt")}" data-fileentryid="${moreInfoImage.getAttribute("fileEntryId")}" src="${moreInfoImage.getData()}" />
					</#if>
				</div>
			</div>
			<div class="row box-informazioni ml-0">
				<div class="col-md-12 p-4">
					<h3><@liferay.language key="por.more-information" /></h3>

					<#if documentsTitle.getSiblings()?has_content> 
						<#list documentsTitle.getSiblings() as cur_documentsTitle>
							<p class="avviso-pubblico">
								<img src="${document_extention_image}" class="doc-type" alt="documentType">
								<a href="${documentsTitle.document.getData()}"> 
									${cur_documentsTitle.getData()}
								</a>
							</p>
							
						</#list>
					</#if>
					<#if goToSiteTitle.goToSiteLink.getData()?has_content && goToSiteTitle.goToSiteLink.getData() != "">
						<p class="sito-esterno">
							<img class="" alt="Visita il sito" src="${go_to_site_image}" />
							<a href="${goToSiteTitle.goToSiteLink.getData()}" target=”_blank”>
								${goToSiteTitle.getData()}
							</a>
						</p>
					</#if>
				</div>
			</div>
		</div>
	</div>
</div>