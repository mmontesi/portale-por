<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />

<#assign title = .vars['reserved-article-title'].data />
<#assign date = .vars['reserved-article-display-date'].data>
<#assign articleURL = themeDisplay.getURLPortal() + "/web/guest/-/" + .vars['reserved-article-url-title'].data/>

<#assign categories = [] >
<#list currentArticleAssetEntry.getCategories() as category>
	<#assign categories = categories + [category] >
</#list>

<div class="row">
	<div class="col-sm-8 col-md-8 col-lg-8">
		<div class="row">
			<div class="col titolo">
				<h1>${.vars['reserved-article-title'].data}</h1>
			</div>
		</div>
		<#if categories?has_content>
			<div class="categories-content pt-2">
				<#list categories as category>
					<#assign badgeClass = "primary" >
					<#assign vocabularyName = vocabularyService.getVocabulary(category['vocabularyId']?number)['name'] >
					<#if vocabularyName == "Assi">
						<#assign badgeClass = "asse" >
					<#elseif vocabularyName == "Azioni">
						<#assign badgeClass = "azione" >
					<#elseif vocabularyName == "Tema" || vocabularyName == "Beneficiari">
						<#assign badgeClass = "secondary" >
					<#elseif vocabularyName == "Approfondimenti">
						<#assign badgeClass = "insights" >
					<#else>
						<#assign badgeClass = category.getTitle('it_IT')?lower_case?replace(" ", "-") >
					</#if>
					<span class="badge badge-por badge-${badgeClass}">
						<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
					</span>
				</#list>
			</div>
		</#if>
		<div class="row pt-4">
			<div class="col data-pubblicazione">
				<#setting locale = localeUtil.getDefault()>
				<#assign date = date?datetime("EEE, d MMM yyyy HH:mm:ss Z")>

				<#assign dateTimeFormat = "dd/MM/yyyy">
				<#assign date = date?string(dateTimeFormat)>
				<p class="my-3">${date}</p>
			</div>
		</div>
		<div class="row">
			<div class ="col contenuto">
				${content.getData()}
			</div>
			<div class="my-3 embed-responsive embed-responsive-16by9">
				<iframe class="embed-responsive-item" src="${videoContent.getData()}" allowfullscreen></iframe>
			</div>
		</div>
		<hr/>
	</div>

	<div class="col-sm-4 col-md-4 col-lg-4">
		<div class="row">
			
			<div class="col d-flex py-3 border-top border-bottom align-items-center justify-content-between">
				<span>Condividi</span>
				<!--<span><@liferay.language key="por.share" /></span> -->
				<a href="https://www.facebook.com/sharer/sharer.php?u=" target="new">
					<img class="btn-social facebook" src="${imagesPath}/custom/social/facebook.png" alt="facebook">
				</a>
				<a href="https://twitter.com/intent/tweet?text=${title}&amp;url=${articleURL}" target="new">
					<img class="btn-social twitter" src="${imagesPath}/custom/social/twitter.png" alt="twitter">
				</a>
				<a href="https://www.linkedin.com/sharing/share-offsite/?url=${articleURL}" target="new">
					<img class="btn-social linkedin" src="${imagesPath}/custom/social/linkedin.png" alt="linkedin">
				</a>
				<a href="#" target="new">
					<img class="btn-social whatsapp" src="${imagesPath}/custom/social/whatsapp.png" alt="whatsapp">
				</a>
				<a href="mailto:?subject=${title}&amp;body=${articleURL}">
					<img class="btn-social email" src="${imagesPath}/custom/social/email.png" alt="email">
				</a>
			</div>
		</div>
		<br>
			
				<#if currentArticleRelatedLinks?has_content>
					<h3><@liferay.language key="por.documents" /></h3>
			
				<#list currentArticleRelatedLinks as related_entry>
					<#assign relatedAssetEntryId = related_entry.getEntryId2() />
					<#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
					<#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
					<#assign assetRenderer = relatedAssetEntry.getAssetRenderer() />
					<#if relatedAssetEntry.getClassName()?string == "com.liferay.document.library.kernel.model.DLFileEntry" >
			
					<div class="col media pt-3 d-flex align-items-center">
						<#assign portalURL = themeDisplay.getPortalURL() />
						<#assign slash = "/" />
						<#assign fileEntry = dlAppLocalService.getFileEntry(relatedAssetEntryPrimKey) />
						<#assign repositoryId = fileEntry.getRepositoryId() />
						<#assign folderId = fileEntry.getFolderId() />
						<#assign fileVersion = fileEntry.getFileVersion() />
						<#assign modifiedDate = fileVersion.getModifiedDate()?time />
						<#assign fileName = fileEntry.getFileName() />
						<#assign previewURL = portalURL + "/documents/" + repositoryId + "/" + folderId + "/" + fileName + "/" + fileEntry.getUuid() + "?t=" + modifiedDate />	
									
						<#if (assetRenderer.assetObject.extension)?has_content>
					
							<a class="document-title" target="_blank" href="${previewURL?string}"><img class="mr-3 file-extension" id="imageDoc" alt="${assetRenderer.assetObject.extension?html}" src="${imagesPath}/custom/estensioni/${assetRenderer.assetObject.extension?html}.svg" /></a>

						</#if>
						<div class="media-body">
							<p class="m-0">${relatedAssetEntry.description?string}</p>
						</div>
					</div>
				</#if>
			</#list>
		</#if>

	</div>
</div>