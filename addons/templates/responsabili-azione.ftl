<#assign imagesPath = themeDisplay.getPathThemeImages() />

<div class="row">
	<div class="col-12">
		<h1 class="titoloPagina">${.vars['reserved-article-title'].data} </h1>
			<div class="content mt-4">
				${content.getData()}
			</div>
		<div class="content axes">
			<table class="table">
				<thead>
					<tr>
					<th scope="col" class="table-title"><@liferay.language key="por.axis" /></th>
					<th scope="col" class="table-title"><@liferay.language key="por.action" /></th>
					<th scope="col" class="table-title"><@liferay.language key="por.responsible-section" /></th>
					</tr>
				</thead>
				<tbody>
				<#if axes.getSiblings()?has_content>
					<#list axes.getSiblings() as cur_axes>
					
					<tr>
					<th scope="row">${cur_axes.getData()}</th>
					<th>${cur_axes.action.getData()}</th>
					<td>${cur_axes.responsibleSection.getData()}</td>
					</tr>
					</#list> 
				</#if>
					
				</tbody>	
			</table>
		</div>
	</div>
	
	<!--<div class="col-sm-4 col-md-4 col-lg-4">
		<br>
		<div class="card">
			<div class="card-body">
				<h4 class="card-title"><@liferay.language key="por.section-manager" /></h4><br>
					<ul class="list-group" style="list-style-type: none;">
						<li class="item">
							<p><a href="${leaderName.leaderLink.getData()}" target=”_blank”>
							<img class="icon"  src="${imagesPath}/icon/.png" />
								${leaderName.getData()}
							</a></p>
						</li>
						<li class="item">  
							<p><img class="icon"  src="${imagesPath}/icon/.png" />
							${leaderName.seat.getData()}</p>
						</li>
						<li class="item"> 
							<p><img class="icon"  src="${imagesPath}/icon/.png" />
							${leaderName.phone.getData()} </p>
						</li>
						<li class="item">
							<p><img class="icon"  src="${imagesPath}/icon/.png" /><a href="mailto:${leaderName.email.getData()}"> ${leaderName.email.getData()}</a></p>
						</li>
						<li class="item">
							<#if leaderName.pecEmail.getData()!= ""><p><img class="icon" src="${imagesPath}/icon/.png" /></#if><a href="mailto:${leaderName.pecEmail.getData()}">${leaderName.pecEmail.getData()}</a></p>
						</li>
					</ul>
			</div>
		</div>
	</div>-->
</div>

<style>
.table-title{
	text-align: center;
}
</style>