<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
<#assign assetLinkLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetLinkLocalService") />
<#assign assetEntryLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetEntryLocalService") />
<#assign dlAppLocalService = serviceLocator.findService("com.liferay.document.library.kernel.service.DLAppLocalService") />

<#assign currentArticle = journalArticleLocalService.getArticle(groupId, .vars['reserved-article-id'].data) />
<#assign currentArticleResourcePrimKey = currentArticle.getResourcePrimKey() />
<#assign currentArticleAssetEntry = assetEntryLocalService.getEntry("com.liferay.journal.model.JournalArticle", currentArticleResourcePrimKey) />
<#assign currentArticleAssetEntryId = currentArticleAssetEntry.getEntryId() />
<#assign currentArticleRelatedLinks = assetLinkLocalService.getDirectLinks(currentArticleAssetEntryId) />



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="contenuto-con-documenti">
	<div class="row mt-4">
		<div class="col sm-8 col-md-8 col-lg-8">

			<h1>${.vars['reserved-article-title'].data}</h1>
			<div class="content">
				${content.getData()}
			</div>
			<#if plan.getSiblings()?has_content>
				<div class="row">
					<div class="col-12 px-0">
						<div aria-orientation="vertical" class="panel-group mb-4" id="accordionPlans" role="tablist">
							<div class="panel">
								<#list plan.getSiblings() as cur_plan>
									<#assign index = cur_plan?index>

									<button
										aria-controls="collapseInfo"
										aria-expanded="false"
										class="btn btn-unstyled collapse-icon collapsed panel-header panel-header-link border-top"
										data-parent="#accordionPlans"
										data-target="#accordionPlan${index}"
										data-toggle="collapse"
										id="accordionPlan${index}Heading"
										role="tab"
										type="button"
									>
										<span class="panel-title">
											<h2 class="h3 m-0">
												${cur_plan.getData()}
											</h2>
										</span>
										<span class="collapse-icon-closed">
											<i class="icon-2x icon-angle-down"></i>
										</span>
										<span class="collapse-icon-open">
											<i class="icon-2x icon-angle-up"></i>
										</span>
									</button>

									<div aria-labelledby="accordionPlan${index}Heading" class="panel-collapse collapse" id="accordionPlan${index}" role="tabpanel">
										<div class="panel-body p-4">
											<div class="card card-body" style="box-shadow:none">
												${cur_plan.planDetails.getData()}

												<#if cur_plan.planDocument.getSiblings()?has_content>
													<#list cur_plan.planDocument.getSiblings() as cur_document> <br>
														<#assign src ="">
														<#if cur_document.documentType.getData() == "pdf" && cur_document.documentType.getData() != "">
																<#assign src ="${imagesPath}/custom/estensioni/pdf.svg">
															<#elseif cur_document.documentType.getData() == "doc" && cur_document.documentType.getData() != "">
																<#assign src ="${imagesPath}/custom/estensioni/doc.svg">
															<#elseif cur_document.documentType.getData() == "xls" && cur_document.documentType.getData() != "">
																<#assign src ="${imagesPath}/custom/estensioni/xls.svg">
															<#elseif cur_document.documentType.getData() == "csv" && cur_document.documentType.getData() != "">
																<#assign src ="${imagesPath}/custom/estensioni/xls.svg">
															<#elseif cur_document.documentType.getData() == "zip" && cur_document.documentType.getData() != "">
																<#assign src ="${imagesPath}/custom/estensioni/zip.svg">
														</#if>

														<div class="row">
															<div class="col-12 d-flex">
																<a class="text-truncate" href="${cur_document.getData()}">
																	<img class="mr-2 documentImage" src="${src}">
																	${cur_document.documentTitle.getData()}
																</a>
															</div>
														</div>
													</#list>
												</#if>
											</div>
										</div>
									</div>



								</#list>
							</div>
						</div>
					</div>
				</div>
			</#if>
		</div>

		<div class="col-sm-4 col-md-4 col-lg-4">

				<#if currentArticleRelatedLinks?has_content>
					<h3><@liferay.language key="por.documents" /></h3>

					<#list currentArticleRelatedLinks as related_entry>
						<#assign relatedAssetEntryId = related_entry.getEntryId2() />
						<#assign relatedAssetEntry = assetEntryLocalService.getEntry(relatedAssetEntryId) />
						<#assign relatedAssetEntryPrimKey = relatedAssetEntry.getClassPK() />
						<#assign assetRenderer = relatedAssetEntry.getAssetRenderer() />
						<#if relatedAssetEntry.getClassName()?string == "com.liferay.document.library.kernel.model.DLFileEntry" >

							<div class="col media pt-3 d-flex align-items-center">
								<#assign portalURL = themeDisplay.getPortalURL() />
								<#assign slash = "/" />
								<#assign fileEntry = dlAppLocalService.getFileEntry(relatedAssetEntryPrimKey) />
								<#assign repositoryId = fileEntry.getRepositoryId() />
								<#assign folderId = fileEntry.getFolderId() />
								<#assign fileVersion = fileEntry.getFileVersion() />
								<#assign modifiedDate = fileVersion.getModifiedDate()?time />
								<#assign fileName = fileEntry.getFileName() />
								<#assign previewURL = portalURL + "/documents/" + repositoryId + "/" + folderId + "/" + fileName + "/" + fileEntry.getUuid() + "?t=" + modifiedDate />

								<#if (assetRenderer.assetObject.extension)?has_content>


								<a class="document-title" target="_blank" href="${previewURL?string}">
									<img class="mr-3 file-extension" id="imageDoc"
										alt="${assetRenderer.assetObject.extension?html}"
										src="${imagesPath}/custom/estensioni/${assetRenderer.assetObject.extension?html}.svg"/>
								</a>

								</#if>
								<div class="media-body">

								<p class="m-0">${relatedAssetEntry.description?string}</p>

								</div>
							</div>
						</#if>
					</#list>
				</#if>
		</div>
	</div>
</div>