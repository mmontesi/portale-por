<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>
<#assign journalArticleService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")> 

<#assign journalArticleId = .vars['reserved-article-id'].data>
<#assign journalArticle = journalArticleService.getArticle(groupId, journalArticleId)>
<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >
<#assign actualCategory = "" >
<#list categories as category>
	<#if vocabularyService.getVocabulary(category['vocabularyId']?number)['name'] == "Assi">
		<#assign actualCategory = category.getTitle(locale) >
	</#if>
</#list> 
<h2>${actualCategory}</h2>
<h1 class="axe-title">${.vars['reserved-article-title'].data} </h1>
<div class="content-wrapper">
	${content.getData()}
</div>
<div class="card bg-light p-4">
	<h4><@liferay.language key="por.specific-target" /></h4><br>
	<p>${boxContent.getData()}</p>
</div>