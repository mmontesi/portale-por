<#assign backgroundURL = "" />

<#if image.getData()?? && image.getData() != "">
	<#assign backgroundURL = image.getData() />
</#if>

<div class="hero-image">
	<div class="image-container" style="background: linear-gradient(to right, rgba(0, 0, 0, 0.57), rgba(72, 72, 72, 0.57) 44%, rgba(2, 2, 2, 0.57) 72%, rgba(0, 0, 0, 0.57), rgba(84, 84, 84, 0.57)), url(${backgroundURL}) 50% / cover no-repeat;">
		<div class="container-fluid h-100 p-0">
			<div class="row d-flex h-100 justify-content-between">
				<div class="col-md-6 content">
					<div class="row">
						<div class="col">
							${image.content.getData()}
						</div>
					</div>
				</div>
				<div class="col-md-6 align-self-end">
					<div class="row">
						<div class="col">
							<img alt="thumbnail" class="notes-image pull-right mb-2" src="https://via.placeholder.com/30x40/000000">
						</div>
					</div>
					<div class="row">
						<div class="col">
							<span class="notes pull-right">
								<span class="notes pull-right">${image.notes.getData()}</span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>