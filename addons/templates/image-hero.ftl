<div class="container-fluid px-0 image-hero">
	<div class="row">
		<div class="col-md-12">
			<#if image.getData()?? && image.getData() != "">
				<img alt="${image.getAttribute("alt")}" data-fileentryid="${image.getAttribute("fileEntryId")}" src="${image.getData()}" />
			</#if>
		</div>
	</div>
</div>