<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign phone_icon = "${imagesPath}/custom/contatti/telefono.svg" />
<#assign email_icon = "${imagesPath}/custom/contatti/email.svg" />
<#assign pec_icon = "${imagesPath}/custom/contatti/pec.svg" />
<#assign location_icon = "${imagesPath}/custom/contatti/sezione.svg" />

<div class="row">
	<div class="col-md-8">
		<h1 class="web-content-title">
			${.vars['reserved-article-title'].data}
		</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		${content.getData()}
	</div>
	<div class="col-md-4">
		<#if responsible.getSiblings()?has_content>
			<#list responsible.getSiblings() as cur_responsible>
				<ul class="list-unstyled">
					<li>
						<a href="#responsible-${cur_responsible?index}">
							${cur_responsible.getData()}
						</a>
					</li>
				</ul>
			</#list>
		</#if>
	</div>
</div>
<#if responsible.getSiblings()?has_content>
	<#list responsible.getSiblings() as cur_responsible>
		<div class="row my-3" id="responsible-${cur_responsible?index}">
			<div class="col-md-8 box-content">
				<h2 class="h3 pt-3 border-top">${cur_responsible.getData()}</h2>
				${cur_responsible.boxContent.getData()}
				<#if cur_responsible.document.getSiblings()?has_content>
					<#list cur_responsible.document.getSiblings() as cur_document>
						<#assign document_extention_img ="${imagesPath}/custom/estensioni/${cur_document.documentType.getData()}.svg">
						<div class="my-2">
							<a href="${cur_document.getData()}">
								<img alt="${cur_document.documentType.getData()?string}" class="mr-2 documentImage" src="${document_extention_img}">
								${cur_document.textDocument.getData()}
							</a>
						</div>
					</#list>
				</#if>
			</div>
			<div class="col-md-4">
				<div class="box-contatti mb-0 p-4">
					<h3 class=""><@liferay.language key="por.responsibles.section-manager" /></h3>
					<ul class="list-group list-unstyled">
						<li class="item">
							<a href="${cur_responsible.leaderName.leaderLink.getData()}" target=”_blank”>
								<img alt="" class="icon" src="" />
								${cur_responsible.leaderName.getData()}
							</a>
						</li>
						<li class="item">
							<img alt="" class="icon" src="${location_icon}" />
							${cur_responsible.leaderName.seat.getData()}
						</li>
						<li class="item">
							<img alt="" class="icon" src="${phone_icon}" />
							${cur_responsible.leaderName.phone.getData()}
						</li>
						<li class="item">
							<img alt="" class="icon" src="${email_icon}" />
							<a href="mailto:${cur_responsible.leaderName.email.getData()}">
								${cur_responsible.leaderName.email.getData()}
							</a>
						</li>
						<li class="item">
							<#if cur_responsible.leaderName.pecEmail.getData()!= "">
								<img alt="" class="icon" src="${pec_icon}" />
								<a href="mailto:${cur_responsible.leaderName.pecEmail.getData()}">
									${cur_responsible.leaderName.pecEmail.getData()}
								</a>
							</#if>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</#list>
</#if>

<style>
	.box-contatti{
		background-color: rgba(154, 151, 151, 0.1);
	}
</style>