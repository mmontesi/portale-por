<#if !entries?has_content>
	<#if themeDisplay.isSignedIn()>
		<div class="alert alert-info">
			<@liferay.language key="there-are-no-menu-items-to-display" />
		</div>
	</#if>
<#else>
	<#assign
		portletDisplay = themeDisplay.getPortletDisplay()
		navbarId = "navbar_" + portletDisplay.getId()
		navItems = entries
	/>

	<div class="d-none d-md-block">
		<div id="${navbarId}">
			<ul aria-label="<@liferay.language key="site-pages" />" class="nav navbar-nav nav-tabs navbar-site px-5" role="menubar">

				<#list navItems as navItem>
					<#assign showChildren = (displayDepth != 1) && navItem.hasBrowsableChildren() />
					<#assign isClickable = navItem.getLayout().getExpandoBridge().getAttribute("pagina-cliccabile", false)>

					<#if navItem.isBrowsable() || showChildren>
						<#assign
							nav_item_attr_has_popup = ""
							nav_item_css_class = "lfr-nav-item nav-item"
							nav_item_href_link = ""
							nav_item_link_css_class = "nav-link py-2 px-1 dropdown-toggle dropdown-item"
						/>

						<#if showChildren>
							<#assign nav_item_attr_has_popup = "aria-haspopup='true'" />
							<#assign
								nav_item_css_class = "${nav_item_css_class} dropdown"
								nav_item_link_css_class = "${nav_item_link_css_class}"
							/>
						</#if>

						<#if getterUtil.getBoolean(isClickable)>
							<#assign nav_item_href_link = "href='${navItem.getURL()}'" />
						</#if>

						<#if navItem.isSelected()>
							<#assign nav_item_css_class = "${nav_item_css_class} selected active" />
						</#if>

						<li class="${nav_item_css_class}" id="layout_${navItem.getLayoutId()}" role="presentation">
							<a
								${nav_item_href_link}
								class="${nav_item_link_css_class} text-center"
								role="menuitem"
							>
								<span class="text-uppercase">
									${navItem.getName()}
								</span>
							</a>

							<#if showChildren>
								<ul aria-expanded="false" class="child-menu dropdown-menu rounded-0" role="menu">
								<#list navItem.getBrowsableChildren() as childNavigationItem>
									<#assign showSubChildren = (displayDepth != 1) && childNavigationItem.hasBrowsableChildren() />

									<#if childNavigationItem.isBrowsable() || showSubChildren>
										<#assign
											nav_child_item_attr_has_popup = ""
											nav_child_item_css_class = "lfr-nav-item nav-item"
											nav_child_item_href_link = ""
											nav_child_item_link_css_class = "collapse-icon btn btn-link text-muted"
											nav_child_item_panel_group_css_class = ""
											nav_child_item_panel_css_class = ""
										/>

										<#if showSubChildren>
											<#assign nav_child_item_attr_has_popup = "aria-haspopup='true'" />
											<#assign nav_child_item_panel_group_css_class = "panel-group panel-group-fluid" />
											<#assign nav_child_item_panel_css_class = "panel" />
										</#if>

										<#if childNavigationItem.isBrowsable()>
											<#assign nav_child_item_href_link = "href='${childNavigationItem.getURL()}'" />
										</#if>

										<#if childNavigationItem.isSelected()>
											<#assign nav_child_item_css_class = "${nav_child_item_css_class} selected" />
										</#if>

											<li class="${nav_child_item_css_class}" id="layout_${childNavigationItem.getLayoutId()}" role="presentation">
												<div aria-orientation="vertical" class="${nav_child_item_panel_group_css_class}" role="tablist">
													<div aria-expanded="false" class="child-menu ${nav_child_item_panel_css_class}" role="menu">
														<div
															class="d-inline-flex m-0 p-0 bg-white panel-header"
															role="tab"
														>
															<a
																class="dropdown-item bg-white p-3"
																style="max-width: 90%"
																${nav_child_item_href_link}
																${childNavigationItem.getTarget()}
																${nav_child_item_attr_has_popup}
															>
																<span class="">
																	${childNavigationItem.getName()}
																</span>
															</a>
															<button
																aria-controls="collapse-${childNavigationItem?counter}"
																aria-expanded="false"
																data-parent="#${navbarId}"
																data-target="#accordion${navItem?counter}Collapse${childNavigationItem?counter}"
																data-toggle="collapse"
																id="accordion${navItem?counter}Heading${childNavigationItem?counter}"
																type="button"
																class="collapsed ${nav_child_item_link_css_class}"
															>
																<#if showSubChildren>
																	<span
																		aria-controls="accordion${navItem?counter}Collapse${childNavigationItem?counter}"
																		aria-expanded="true"
																		aria-labelledby="layout_${childNavigationItem.getLayoutId()}"
																		data-toggle="collapse"
																		id="accordion${navItem?counter}Heading${childNavigationItem?counter}"
																		role="menuitem"
																		class="collapse-icon-closed"
																	>
																		<i class="icon-large icon-angle-down"></i>
																	</span>
																	<span
																		aria-controls="accordion${navItem?counter}Collapse${childNavigationItem?counter}"
																		aria-expanded="true"
																		aria-labelledby="layout_${childNavigationItem.getLayoutId()}"
																		data-toggle="collapse"
																		id="accordion${navItem?counter}Heading${childNavigationItem?counter}"
																		role="menuitem"
																		class="collapse-icon-open"
																	>
																		<i class="icon-large icon-angle-up"></i>
																	</span>
																</#if>
															</button>
														</div>
														<#if showSubChildren>
															<div
																aria-labelledby="accordion${navItem?counter}Heading${childNavigationItem?counter}"
																class="panel-collapse collapse"
																id="accordion${navItem?counter}Collapse${childNavigationItem?counter}"
																role="tabpanel"
															>
																<div class="panel-body py-0">
																	<ul aria-expanded="false" class="child-menu list-unstyled" role="menu">
																		<#list childNavigationItem.getBrowsableChildren() as subChildNavigationItem>
																			<#assign nav_sub_child_css_class = "lfr-nav-item nav-item p-2" />

																			<#if subChildNavigationItem.isSelected()>
																				<#assign nav_sub_child_css_class = " ${nav_sub_child_css_class} selected" />
																			</#if>

																			<li class="${nav_sub_child_css_class}" id="layout_${subChildNavigationItem.getLayoutId()}" role="presentation">
																				<a
																					aria-labelledby="layout_${subChildNavigationItem.getLayoutId()}"
																					href="${subChildNavigationItem.getURL()}"
																					${subChildNavigationItem.getTarget()}
																					role="menuitem"
																				>
																					<span class="">
																						${subChildNavigationItem.getName()}
																					</span>
																				</a>
																			</li>
																		</#list>
																	</ul>
																</div>
															</div>
														</#if>
													</div>
												</div>
											</li>
										</#if>
									</#list>
								</ul>
							</#if>
						</li>
					</#if>
				</#list>
			</ul>
		</div>
	</div>
	<div class="d-block d-sm-block d-md-none">
		<div class="hidden-print sidenav-fixed sidenav-menu-slider sidenav-right main-navigation-mobile closed" aria-expanded="false" role="navigation" id="${navbarId}">
			<div class="sidebar sidebar-inverse sidenav-menu p-0 bg-white">
				<div class="sidebar-header d-flex flex-column align-items-start justify-content-center">
					<button class="button-close-menu-mobile">
						<@clay["icon"] symbol="times" />
					</button>
					<h6 class="text-uppercase mt-2 px-1 close-text"><@liferay.language key="por.close-menu" /></h6>
				</div>
				<div class="sidebar-body p-0">
					<nav class="menubar menubar-transparent">
						<div class="menubar-collapse" id="menubarVerticalCollapse0">
							<#list navItems as navItem>
								<#assign showChildren = navItem.hasBrowsableChildren() />
								<ul class="nav nav-nested root-level">
									<li class="nav-item">
										<#if navItem.isBrowsable() || showChildren>
											<#assign
												nav_item_href_link = ""
												nav_item_attr_controls = ""
												nav_item_attr_expanded = ""
												nav_item_attr_has_popup = ""
												nav_item_data_toggle = ""
												nav_item_role = ""
												nav_item_css_class = "nav-link px-4"
											/>

											<#if navItem.isBrowsable()>
												<#assign nav_item_href_link = "href='${navItem.getURL()}'" />
											</#if>

											<#if navItem.isSelected()>
												<#assign nav_item_css_class = "${nav_item_css_class} selected active" />
											</#if>
											<#--
											<div class="d-flex justify-content-between">
												<a ${nav_item_href_link} class="${nav_item_css_class}">
													${navItem.getName()}
												</a>
											-->
												<#if showChildren>
													<#assign
														nav_item_href_link = "href='#menubarVerticalCollapse" + navItem?counter + "'"
														nav_item_attr_controls = "aria-controls='menubarVerticalCollapse" + navItem?counter + "'"
														nav_item_attr_expanded = "aria-expanded='false'"
														nav_item_attr_has_popup = "aria-haspopup='true'"
														nav_item_data_toggle = "data-toggle='collapse'"
														nav_item_role="role='button'"
													/>
												</#if>
												<a
													${nav_item_href_link}
													${nav_item_data_toggle}
													${nav_item_attr_expanded}
													${nav_item_attr_controls}
													${nav_item_role}
													class="${nav_item_css_class} collapsed collapse-icon"
												>
													<span class="mr-auto">
														${navItem.getName()}
													</span>
													<#if showChildren>
														<span class="text-muted collapse-icon-closed">
															<i class="icon-large icon-angle-down"></i>
														</span>
														<span class="text-muted collapse-icon-open">
															<i class="icon-large icon-angle-up"></i>
														</span>
													</#if>
												</a>
											<#--
											</div>
											-->
											<#if showChildren>
												<div class="collapse" id="menubarVerticalCollapse${navItem?counter}">
													<ul class="nav nav-stacked child-level">
														<#list navItem.getBrowsableChildren() as child>
															<#assign showGrandChildren = child.hasBrowsableChildren() />
															<li class="nav-item">
																<a class="nav-link" href="${child.getURL()}">
																	${child.getName()}
																</a>
																<#if showGrandChildren>
																	<#list child.getBrowsableChildren() as grandChild>
																		<a class="nav-link" href="${grandChild.getURL()}">
																			${grandChild.getName()}
																		</a>
																	</#list>
																</#if>
															</li>
														</#list>
													</ul>
												</div>
											</#if>
										</#if>
									</li>
								</ul>
							</#list>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>

	<@liferay_aui.script use="liferay-navigation-interaction">
		var navigation = A.one('#${navbarId}');

		Liferay.Data.NAV_INTERACTION_LIST_SELECTOR = '.navbar-site';
		Liferay.Data.NAV_LIST_SELECTOR = '.navbar-site';

		if (navigation) {
			navigation.plug(Liferay.NavigationInteraction);
		}
	</@>
</#if>