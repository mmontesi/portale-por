<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<div class="container px-4 modello-3 galleria-notizie">
	<#if entries?has_content>
		<#list entries as entry>
			<div class="row pb-3">
				<#if (entry.className = "com.liferay.journal.model.JournalArticle")>
					<#assign
						entry = entry
						assetRenderer = entry.getAssetRenderer()
						entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
						viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
						journalArticle = assetRenderer.getAssetObject()
						document = saxReaderUtil.read(journalArticle.getContent())
						rootElement = document.getRootElement()
					>

					<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >

					<#assign labelTema = "">
					<#list categories as category>
						<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>
						
						<#if categoryVocabulary == "Tipologia notizia">
							<#assign labelTema = category>
						</#if>
					</#list>

					<div class="col-4">
						<div class="immagine">
							<#if assetRenderer.getThumbnailPath(renderRequest)??>
								<a href="${viewURL}">
									<div title="${entryTitle}" class="img" style="background: url('${assetRenderer.getThumbnailPath(renderRequest)}') 50%/cover no-repeat;">
										<span class="top-right badge badge-por badge-${labelTema.getTitle('it_IT')?lower_case?replace(" ", "-")} m-0">
											<span class="badge-item badge-item-expand">${labelTema.getTitle(locale)}</span>
										</span>
									</div>
								</a>
							</#if>
						</div>
					</div>
					<div class="col-8 d-flex flex-column justify-content-around justify-content-md-center">
						<div class="row d-none d-md-block abstract">
							<div class="col mb-1">
								<span>${entry.getPublishDate()?string["dd/MM/yyyy"]}</span>
							</div>
						</div>
						<div class="row titolo">
							<div class="col">
								<h3>${entryTitle}</h3>
							</div>
						</div>
						<div class="row continua-a-leggere">
							<div class="col">
								<a class="read-more" href="${viewURL}">
									<@liferay.language key="por.keep-reading" />
									<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
								</a>
							</div>
						</div>
					</div>
				</#if>
			</div>
		</#list>
	</#if>
</div>