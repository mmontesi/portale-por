<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#if entries?has_content>
	<div class="news-in-evidenza cards-group">
		<div class="row">
			<div class="col">
				<h2 class="adt-title">${themeDisplay.getPortletDisplay().getTitle()}</h2>
			</div>
		</div>
		<div class="d-none d-md-block">
			<div class="row">
				<#list entries as entry>
					<#assign
						entry = entry
						assetRenderer = entry.getAssetRenderer()
						entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
						viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
						journalArticle = assetRenderer.getAssetObject()
						document = saxReaderUtil.read(journalArticle.getContent())
						rootElement = document.getRootElement()
					>

					<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >

					<#assign labelTema = "">
					<#list categories as category>
						<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>

						<#if categoryVocabulary == "Tipologia notizia">
							<#assign labelTema = category>
						</#if>
					</#list>
					<div class="col-md-4 news">
						<div class="card-type-asset form-check form-check-card form-check-top-left image-card">
							<div class="card">
								<div class="aspect-ratio card-item-first">
									<div class="custom-control custom-checkbox">
										<#if assetRenderer.getThumbnailPath(renderRequest)??>
											<#assign thumbnail = (assetRenderer.getThumbnailPath(renderRequest))!"https://dummyimage.com/600x400/000/fff">
											<a href="${viewURL}">
												<label style="background-image: url('${thumbnail}');"></label>
											</a>
										</#if>
									</div>
								</div>
								<span class="card-badge bottom-right badge badge-por badge-${labelTema.getTitle('it_IT')?lower_case?replace(" ", "-")} m-0">
									<span class="badge-item badge-item-expand">${labelTema.getTitle(locale)}</span>
								</span>
								<div class="card-body px-4">
									<div class="card-row">
										<div class="autofit-col autofit-col-expand justify-content-start">
											<div class="row metadata">
												<div class="col-auto card-title d-flex align-items-center">
													<span>${entry.getPublishDate()?string["dd/MM/yyyy"]}</span>
												</div>
											</div>
											<div class="card-detail titolo d-flex align-items-center my-auto">
												<h3 class="mb-0"><a class="" href="${viewURL}" title="${entryTitle}">${entryTitle}</a></h3>
											</div>
											<div class="row maggiori-dettagli">
												<div class="col">
													<a class="pull-right read-more" href="${viewURL}">
														<@liferay.language key="por.keep-reading" />
														<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</#list>
			</div>
		</div>
		<div class="d-block d-sm-block d-md-none row cards-group-carousel por-carousel">
			<div class="slider" data-slick='{"autoplay": false, "arrows": false, "autoplaySpeed": 3000, "slidesToShow": 1, "slidesToScroll": 1, "dots": false, "infinite": true, "speed": 300, "pauseOnHover": true, "pauseOnFocus": true, "centerMode": true, "centerPadding": "30px"}' >
				<#list entries as entry>
					<#assign
						entry = entry
						assetRenderer = entry.getAssetRenderer()
						entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
						viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
					>
					<div class="card-type-asset form-check form-check-card form-check-top-left image-card">
						<div class="card">
							<div class="aspect-ratio card-item-first">
								<div class="custom-control custom-checkbox">
									<#if assetRenderer.getThumbnailPath(renderRequest)??>
										<#assign thumbnail = (assetRenderer.getThumbnailPath(renderRequest))!"https://dummyimage.com/600x400/000/fff">
										<a href="${viewURL}">
											<label style="background-image: url('${thumbnail}');"></label>
										</a>
									</#if>
								</div>
							</div>
							<span class="card-badge bottom-right badge badge-por badge-${labelTema.getTitle('it_IT')?lower_case?replace(" ", "-")} m-0">
								<span class="badge-item badge-item-expand">${labelTema.getTitle(locale)}</span>
							</span>
							<div class="card-body">
								<div class="card-row">
									<div class="autofit-col autofit-col-expand justify-content-start">
										<div class="row metadata pb-3">
											<div class="col-auto card-title d-flex align-items-center">
												<span>${entry.getPublishDate()?string["dd/MM/yyyy"]}</span>
											</div>
										</div>
										<div class="my-auto card-detail titolo">
											<h3 class="mb-0"><a class="" href="${viewURL}" title="${entryTitle}">${entryTitle}</a></h3>
										</div>
										<div class="row maggiori-dettagli">
											<div class="col">
												<a class="pull-right read-more" href="${viewURL}">
													<@liferay.language key="por.keep-reading" />
													<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</#list>
			</div>
		</div>
	</div>
</#if>

<style>
	.cards-group label {
		background-size: cover;
		background-position: 50%;
	}
</style>