<nav class="navbar navbar-expand-lg has-megamenu p-0">
	<button class="custom-navbar-toggler" type="button" aria-controls="nav100" aria-expanded="false" aria-label="Toggle navigation" data-target="#nav100">

	</button>
	<div class="navbar-collapsable" id="nav100" style="display: none;">
		<div class="overlay" style="display: none;"></div>
		<div class="close-div sr-only">
			<button class="btn close-menu" type="button"><span class="it-close"></span>close</button>
		</div>
		<div class="menu-wrapper">
			<div class="d-flex bd-highlight align-items-center">
				<div class=" bd-highlight">
					<ul class="navbar-nav">
						<#if entries?has_content>
							<#list entries as navigationEntry>
								<li class="nav-item"><a class="nav-link" href="${navigationEntry.getURL()}"><span>${navigationEntry.getName()}</span></a></li>
							</#list>
						</#if>
					</ul>
				</div>
				<div class="ml-auto bd-highlight reserved-area">
					<svg class="icon icon-primary icon-logout">
						<use xlink:href="../svg/sprite.svg#it-download"></use>
					</svg>
					&nbsp;
					<span>Area riservata</span>
					&nbsp;
					<a href="#">
						<svg class="icon icon-primary">
							<use xlink:href="../svg/sprite.svg#it-expand"></use>
						</svg>
					</a>
				</div>

			</div>
		</div>
	</div>
</nav>
