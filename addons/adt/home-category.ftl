<#assign AssetCategoryPropertyLocalService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryPropertyLocalService")>
<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#if entries?has_content>
	<#assign categories = entries[0].categories>
	<div class="d-none d-md-block">
		<div class="home-category mt-3 px-4 py-2">
			<h2 class="adt-title">${themeDisplay.getPortletDisplay().getTitle()}</h2>
			<#if categories?has_content>
				<ul class="list-unstyled list-element">
					<#list categories as curCategory>

						<#assign imgSrc = 'https://via.placeholder.com/30' >
						<#assign categoryRef = AssetCategoryPropertyLocalService.getCategoryProperty(curCategory['categoryId']?number, 'img')['value'] >
						<#attempt>
							<#assign imgSrc = imagesPath + "/custom/temi/" + categoryRef + ".svg" >
							<#recover>
						</#attempt>

						<#assign url = '#' >
						<#attempt>
							<#assign url = AssetCategoryPropertyLocalService.getCategoryProperty(curCategory['categoryId']?number, 'url')['value'] >
						<#recover>
						</#attempt>
						<li class="px-4 py-1 tema">
							<img alt="" src="${imgSrc}" height="40">
							<a href="/${themeDisplay.getLocale()?substring(0, 2)}/web${themeDisplay.getScopeGroup()['friendlyURL']}/${url}?p_r_p_categoryId=${curCategory['categoryId']}">
								<span class="name ml-5">${curCategory.getTitle(locale)}</span>
							</a>
						</li>
					</#list>
				</ul>
			</#if>
		</div>
	</div>
</#if>