<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>
<#if entries?has_content>
	<div class="container-fluid lista-azioni">
		<div class="row">
			<div class="col">
				<h2 class="h3 adt-title">${themeDisplay.getPortletDisplay().getTitle()}</h2>
			</div>
		</div>
		<#list entries as entry>
			<#assign
				entry = entry
				assetRenderer = entry.getAssetRenderer()
				entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
				viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
				journalArticle = assetRenderer.getAssetObject()
			>
			<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >
			<#assign labelAzione = "">
			<#list categories as category>
				<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>
				
				<#if categoryVocabulary == "Azioni">
					<#assign labelAzione = category.getTitle(locale)>
				</#if>
			</#list>
			<div class="row">
				<div class="col">
					<ul class="list-unstyled">
						<li>
							<a class="azione" href="${viewURL}" target="_blank">${labelAzione}&nbsp;${entry.getTitle(locale)}</a>
						</li>
					</ul>
				</div>
			</div>
		</#list>
	</div>
</#if>