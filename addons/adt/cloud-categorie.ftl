<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>

<#assign allCategories = []>
<#if entries?has_content>
	<#list entries as vocabulary>
		<#assign categories = vocabulary.categories>
		<#if categories?has_content>
			<#list categories as category>
				<#assign allCategories = allCategories + [category]>
			</#list>
		</#if>
	</#list>
</#if>

<#if allCategories?has_content >
	<div class="cloud-categorie">
		<div class="filter-by"><@liferay.language key="por.filter-by" /></div>
		<#list allCategories as category>
			<#assign badgeClass = "primary" >
			<#assign vocabularyName = vocabularyService.getVocabulary(category['vocabularyId']?number)['name'] >
			<#if vocabularyName == "Assi">
				<#assign badgeClass = "asse" >
			<#elseif vocabularyName == "Azioni">
				<#assign badgeClass = "azione" >
			<#elseif vocabularyName == "Tema" || vocabularyName == "Beneficiari" || vocabularyName == "Beneficiari Extra">
				<#assign badgeClass = "secondary" >
			<#elseif vocabularyName == "Approfondimenti">
				<#assign badgeClass = "insights" >
			<#elseif vocabularyName == "Opportunità">
				<#assign badgeClass = "secondary" >
			<#else>
				<#assign badgeClass = category.getTitle('it_IT')?lower_case?replace(" ", "-") >
			</#if>
			<#assign categoryURL = renderResponse.createRenderURL() />

			${categoryURL.setParameter("resetCur", "true")}
			${categoryURL.setParameter("categoryId", category.getCategoryId()?string)}
			<a class="badge badge-por badge-${badgeClass}" href="${categoryURL}">
				<span class="badge-item badge-item-expand">${category.getTitle(locale)}</span>
			</a>
		</#list>
	</div>
</#if>