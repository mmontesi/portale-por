<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#assign dateNodeSelector = "//dynamic-element[@name='from']" />

<div class="container px-4 modello-3">
	<div class="row">
		<div class="col">
			<h2 class="adt-title pt-1">${themeDisplay.getPortletDisplay().getTitle()}</h2>
		</div>
	</div>
	<#if entries?has_content>
		<#list entries as entry>
			<div class="row pb-3">
				<#if (entry.className = "com.liferay.journal.model.JournalArticle")>
					<#assign
						entry = entry
						assetRenderer = entry.getAssetRenderer()
						entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
						viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
						journalArticle = assetRenderer.getAssetObject()
						document = saxReaderUtil.read(journalArticle.getContent())
						rootElement = document.getRootElement()
					>

					<#assign dateNodeValue = (rootElement.selectSingleNode(dateNodeSelector).getStringValue())! "">
					<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >

					<#assign labelTema = "">
					<#assign labelModalita = "">
					<#list categories as category>
						<#assign categoryName = category.getTitle(locale)>
						<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>
						
						<#if categoryVocabulary == "Tema">
							<#assign labelTema = categoryName>
						</#if>
						<#if categoryVocabulary == "Modalità bando">
							<#assign labelModalita = categoryName>
						</#if>
					</#list>
					<#if (dateNodeValue)?has_content>
						<#assign date = dateNodeValue?string />
					</#if>

					<div class="col-4">
						<div class="immagine">
							<#if assetRenderer.getThumbnailPath(renderRequest)??>
								<a href="${viewURL}">
									<div title="${entryTitle}" class="img" style="background: url('${assetRenderer.getThumbnailPath(renderRequest)}') 50%/cover no-repeat;"></div>
								</a>
							</#if>
						</div>
					</div>
					<div class="col-8 d-flex flex-column justify-content-around justify-content-md-center">
						<#attempt>
							<#assign article = journalArticleLocalService.getLatestArticle(entry.getClassPK())>
							<#assign expandoValueData = article.getExpandoBridge().getAttribute("icona-multimedia")?first>
						<#recover>
							<#assign expandoValueData = "Nessuna" >
						</#attempt>

						<#if expandoValueData != "Nessuna">
							<#assign categories = entry.getCategories()>
							<#if categories?has_content>
								<div class="row d-none d-md-block metadata">
									<div class="col-md-12 mb-3">
										<div class="row">
											<div class="col-md-1">
												<img alt="${expandoValueData}" src="${imagesPath}/categorie/${expandoValueData?lower_case}.svg" />
											</div>
											<div class="col-md-auto d-flex align-items-end">
												<span class="categoria">${categories?last.name}</span>
											</div>
										</div>
									</div>
								</div>
							</#if>
						</#if>
						<div class="row titolo">
							<div class="col">
								<h3><a href="${viewURL}">${entryTitle}</a></h3>
							</div>
						</div>
						<#assign summary = htmlUtil.escape(assetRenderer.getSummary(renderRequest, renderResponse))>
						<div class="row d-none d-md-block abstract">
							<div class="col mb-1">
								<span>${htmlUtil.unescape(stringUtil.shorten(summary, 120))}</span>
							</div>
						</div>
						<div class="row continua-a-leggere">
							<div class="col">
								<a class="read-more" href="${viewURL}">
									<@liferay.language key="por.keep-reading" />
									<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
								</a>
							</div>
						</div>
					</div>
				</#if>
			</div>
			<#assign articleId = journalArticle.getArticleId() />
			<#assign languageId = localeUtil.toLanguageId(locale) />
			<#assign articleDisplay = journalArticleLocalService.getArticleDisplay(groupId, articleId?string, "normal", languageId?string,  themeDisplay) />
		</#list>
	</#if>
</div>