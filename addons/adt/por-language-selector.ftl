<#if entries?has_content>
	<div class="d-flex flex-row">
		<div class="dropdown nav-item por-language-selector">
			<#list entries as entry>
				<#if !entry.isDisabled()>
					<#if entry.isSelected()>
						<#assign label = entry.getLongDisplayName()[0..2]?upper_case />
						<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-link p-0" data-toggle="dropdown" href="#1" role="button">
							<span class="navbar-text-truncate mx-2">${label}</span>
							<@clay["icon"] symbol="angle-down" />
						</a>
					</#if>
				</#if>
			</#list>
			<div aria-labelledby="" class="dropdown-menu">
				<#list entries as entry>
					<#if !entry.isDisabled() && ! entry.isSelected()>
						<#assign label = entry.getLongDisplayName()[0..2]?upper_case />
						<h4 class="my-0">
							<@liferay_aui["a"]
								cssClass="dropdown-item language-entry-short-text"
								href=entry.getURL()
								label=label
								lang=entry.getW3cLanguageId()
							/>
						</h4>
					</#if>
				</#list>
			</div>
		</div>
	</div>
</#if>