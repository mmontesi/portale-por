<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign vocabularyService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetVocabularyLocalService")>
<#assign categoryService = serviceLocator.findService("com.liferay.asset.kernel.service.AssetCategoryLocalService")>

<#assign dateNodeSelector = "//dynamic-element[@name='from']" />

<#assign imagesPath = themeDisplay.getPathThemeImages() />

<#if entries?has_content>
	<div class="d-none d-md-block">
		<div class="container px-4 modello-3 galleria-bandi">
			<div class="row">
				<div class="col">
					<h2 class="adt-title">${themeDisplay.getPortletDisplay().getTitle()}</h2>
				</div>
			</div>
			<#list entries as entry>
				<div class="row pb-3">
					<#if (entry.className = "com.liferay.journal.model.JournalArticle")>
						<#assign
							entry = entry
							assetRenderer = entry.getAssetRenderer()
							entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale))
							viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, assetRenderer, entry, !stringUtil.equals(assetLinkBehavior, "showFullContent"))
							journalArticle = assetRenderer.getAssetObject()
							document = saxReaderUtil.read(journalArticle.getContent())
							rootElement = document.getRootElement()
						>

						<#assign dateNodeValue = (rootElement.selectSingleNode(dateNodeSelector).getStringValue())! "">
						<#assign categories = categoryService.getCategories("com.liferay.journal.model.JournalArticle", journalArticle.getResourcePrimKey()) >

						<#assign labelTema = "">
						<#assign labelModalita = "">
						<#list categories as category>
							<#assign categoryName = category.getTitle(locale)>
							<#assign categoryVocabulary =  vocabularyService.getVocabulary(category['vocabularyId']?number)['name']>

							<#if categoryVocabulary == "Tema">
								<#assign labelTema = categoryName>
							</#if>
							<#if categoryVocabulary == "Modalità bando">
								<#assign labelModalita = categoryName>
							</#if>
						</#list>

						<#assign date = "" />
						<#if (dateNodeValue)?has_content>
							<#assign date = dateNodeValue?string />
							<#assign date = date?remove_beginning("\n\t\t\t") />
							<#assign date = date?remove_ending("\n\t\t") />
						</#if>

						<#attempt>
							<#assign date_DateObj = dateUtil.parseDate("yyyy-MM-dd", date, locale) />
							<#assign date = dateUtil.getDate(date_DateObj, "dd/MM/yyyy", locale) />
							<#recover>
								<#assign date = "" />
						</#attempt>
						<div class="col-4">
							<div class="immagine">
								<#if assetRenderer.getThumbnailPath(renderRequest)??>
									<a href="${viewURL}">
										<div title="${entryTitle}" class="img" style="background: url('${assetRenderer.getThumbnailPath(renderRequest)}') 50%/cover no-repeat;">
											<span class="top-right badge badge-por badge-secondary m-0">
												<span class="badge-item badge-item-expand">${labelTema}</span>
											</span>
										</div>
									</a>
								</#if>
							</div>
						</div>
						<div class="col-8 d-flex flex-column justify-content-around justify-content-md-center">
							<div class="row d-none d-md-block abstract">
								<div class="col mb-1">
									<span><@liferay.language key="por.from" />${date}</span>
								</div>
							</div>
							<div class="row titolo">
								<div class="col">
									<h3>${entryTitle}</h3>
								</div>
							</div>
							<div class="row continua-a-leggere">
								<div class="col">
									<a class="read-more" href="${viewURL}">
										<@liferay.language key="por.keep-reading" />
										<span class="hide-accessible"><@liferay.language key="about" />${entryTitle}</span>
									</a>
								</div>
							</div>
						</div>
					</#if>
				</div>
			</#list>
		</div>
	</div>
</#if>