create table POR_AuditEvent (
	uuid_ VARCHAR(75) null,
	auditEventId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	descriptionEventAudit VARCHAR(75) null
);

create table POR_DettaglioProcesso (
	uuid_ VARCHAR(75) null,
	dettaglioProcessoId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	processoId LONG,
	isVisible BOOLEAN,
	testoDettaglio VARCHAR(1000) null,
	descerizioneBreve VARCHAR(75) null,
	dlFileId LONG,
	nomeWorkflow VARCHAR(75) null,
	statoWorkflow VARCHAR(75) null
);

create table POR_DocumentoProcesso (
	uuid_ VARCHAR(75) null,
	documentoProcessoId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	processoId LONG,
	dettaglioProcessoId LONG,
	dlFileId LONG
);

create table POR_EmailConfigurazione (
	uuid_ VARCHAR(75) null,
	emailConfigurazioneId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	body VARCHAR(1000) null,
	subject VARCHAR(75) null,
	from_ VARCHAR(75) null
);

create table POR_GruppiUtentiProcesso (
	uuid_ VARCHAR(75) null,
	gupId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	gupUserId LONG,
	gupGroupId LONG
);

create table POR_Processo (
	uuid_ VARCHAR(75) null,
	processoId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nomeProcesso VARCHAR(1000) null,
	descrizioneProcesso TEXT null,
	isVisible BOOLEAN,
	folderId LONG,
	nomeWorkflow VARCHAR(75) null,
	statoWorkflow VARCHAR(75) null,
	gruppoMittenteId LONG
);

create table POR_UserProcesso (
	uuid_ VARCHAR(75) null,
	userProcessoId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	userIdProcesso LONG,
	processoId LONG
);