create index IX_6C8D61E5 on POR_AuditEvent (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_815F1DA7 on POR_AuditEvent (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_EF6731B4 on POR_DettaglioProcesso (processoId);
create index IX_9D3C9B19 on POR_DettaglioProcesso (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_D73313DB on POR_DettaglioProcesso (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_2468DEF7 on POR_DocumentoProcesso (dettaglioProcessoId, processoId);
create index IX_B134A11D on POR_DocumentoProcesso (processoId);
create index IX_976917C2 on POR_DocumentoProcesso (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C78A6C4 on POR_DocumentoProcesso (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_64258D7F on POR_EmailConfigurazione (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_306167C1 on POR_EmailConfigurazione (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_B3CBE902 on POR_GruppiUtentiProcesso (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_E9DEC804 on POR_GruppiUtentiProcesso (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_37FD7506 on POR_Processo (isVisible);
create index IX_79AA8018 on POR_Processo (statoWorkflow[$COLUMN_LENGTH:75$]);
create index IX_F8E4FAC6 on POR_Processo (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_82D97AC8 on POR_Processo (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_7D843ECC on POR_UserProcesso (processoId);
create index IX_DF4AC3B7 on POR_UserProcesso (userIdProcesso);
create index IX_8317631 on POR_UserProcesso (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_C753D4F3 on POR_UserProcesso (uuid_[$COLUMN_LENGTH:75$], groupId);