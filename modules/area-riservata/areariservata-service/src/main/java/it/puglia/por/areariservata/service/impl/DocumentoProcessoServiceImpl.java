/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import it.puglia.por.areariservata.model.DocumentoProcesso;
import it.puglia.por.areariservata.service.base.DocumentoProcessoServiceBaseImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the documento processo remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.puglia.por.areariservata.service.DocumentoProcessoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoServiceBaseImpl
 * @see it.puglia.por.areariservata.service.DocumentoProcessoServiceUtil
 */
public class DocumentoProcessoServiceImpl
        extends DocumentoProcessoServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link it.puglia.por.areariservata.service.DocumentoProcessoServiceUtil} to access the documento processo remote service.
     */

    public List<DocumentoProcesso> getAllDocumentsFromProcesso(Long idProcesso) {
        List<DocumentoProcesso> resultList = new ArrayList<>();
        return documentoProcessoFinder.findByProcessoOrDettaglio(0L, idProcesso);
    }

    public List<DocumentoProcesso> getAllDocumentsFromDettaglio(Long idProcesso, Long idDettaglio) {
        List<DocumentoProcesso> resultList = new ArrayList<>();
        return documentoProcessoFinder.findByProcessoAndDettaglio(idDettaglio, idProcesso);
    }
}