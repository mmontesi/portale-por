/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.puglia.por.areariservata.model.EmailConfigurazione;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EmailConfigurazione in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see EmailConfigurazione
 * @generated
 */
@ProviderType
public class EmailConfigurazioneCacheModel implements CacheModel<EmailConfigurazione>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailConfigurazioneCacheModel)) {
			return false;
		}

		EmailConfigurazioneCacheModel emailConfigurazioneCacheModel = (EmailConfigurazioneCacheModel)obj;

		if (emailConfigurazioneId == emailConfigurazioneCacheModel.emailConfigurazioneId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, emailConfigurazioneId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", emailConfigurazioneId=");
		sb.append(emailConfigurazioneId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", body=");
		sb.append(body);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", from=");
		sb.append(from);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EmailConfigurazione toEntityModel() {
		EmailConfigurazioneImpl emailConfigurazioneImpl = new EmailConfigurazioneImpl();

		if (uuid == null) {
			emailConfigurazioneImpl.setUuid("");
		}
		else {
			emailConfigurazioneImpl.setUuid(uuid);
		}

		emailConfigurazioneImpl.setEmailConfigurazioneId(emailConfigurazioneId);
		emailConfigurazioneImpl.setGroupId(groupId);
		emailConfigurazioneImpl.setCompanyId(companyId);
		emailConfigurazioneImpl.setUserId(userId);

		if (userName == null) {
			emailConfigurazioneImpl.setUserName("");
		}
		else {
			emailConfigurazioneImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			emailConfigurazioneImpl.setCreateDate(null);
		}
		else {
			emailConfigurazioneImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			emailConfigurazioneImpl.setModifiedDate(null);
		}
		else {
			emailConfigurazioneImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (body == null) {
			emailConfigurazioneImpl.setBody("");
		}
		else {
			emailConfigurazioneImpl.setBody(body);
		}

		if (subject == null) {
			emailConfigurazioneImpl.setSubject("");
		}
		else {
			emailConfigurazioneImpl.setSubject(subject);
		}

		if (from == null) {
			emailConfigurazioneImpl.setFrom("");
		}
		else {
			emailConfigurazioneImpl.setFrom(from);
		}

		emailConfigurazioneImpl.resetOriginalValues();

		return emailConfigurazioneImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		emailConfigurazioneId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		body = objectInput.readUTF();
		subject = objectInput.readUTF();
		from = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(emailConfigurazioneId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (body == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(body);
		}

		if (subject == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(subject);
		}

		if (from == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(from);
		}
	}

	public String uuid;
	public long emailConfigurazioneId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String body;
	public String subject;
	public String from;
}