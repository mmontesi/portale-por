/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.puglia.por.areariservata.model.DocumentoProcesso;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DocumentoProcesso in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcesso
 * @generated
 */
@ProviderType
public class DocumentoProcessoCacheModel implements CacheModel<DocumentoProcesso>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentoProcessoCacheModel)) {
			return false;
		}

		DocumentoProcessoCacheModel documentoProcessoCacheModel = (DocumentoProcessoCacheModel)obj;

		if (documentoProcessoId == documentoProcessoCacheModel.documentoProcessoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, documentoProcessoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", documentoProcessoId=");
		sb.append(documentoProcessoId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", processoId=");
		sb.append(processoId);
		sb.append(", dettaglioProcessoId=");
		sb.append(dettaglioProcessoId);
		sb.append(", dlFileId=");
		sb.append(dlFileId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DocumentoProcesso toEntityModel() {
		DocumentoProcessoImpl documentoProcessoImpl = new DocumentoProcessoImpl();

		if (uuid == null) {
			documentoProcessoImpl.setUuid("");
		}
		else {
			documentoProcessoImpl.setUuid(uuid);
		}

		documentoProcessoImpl.setDocumentoProcessoId(documentoProcessoId);
		documentoProcessoImpl.setGroupId(groupId);
		documentoProcessoImpl.setCompanyId(companyId);
		documentoProcessoImpl.setUserId(userId);

		if (userName == null) {
			documentoProcessoImpl.setUserName("");
		}
		else {
			documentoProcessoImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			documentoProcessoImpl.setCreateDate(null);
		}
		else {
			documentoProcessoImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			documentoProcessoImpl.setModifiedDate(null);
		}
		else {
			documentoProcessoImpl.setModifiedDate(new Date(modifiedDate));
		}

		documentoProcessoImpl.setProcessoId(processoId);
		documentoProcessoImpl.setDettaglioProcessoId(dettaglioProcessoId);
		documentoProcessoImpl.setDlFileId(dlFileId);

		documentoProcessoImpl.resetOriginalValues();

		return documentoProcessoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		documentoProcessoId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		processoId = objectInput.readLong();

		dettaglioProcessoId = objectInput.readLong();

		dlFileId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(documentoProcessoId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(processoId);

		objectOutput.writeLong(dettaglioProcessoId);

		objectOutput.writeLong(dlFileId);
	}

	public String uuid;
	public long documentoProcessoId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long processoId;
	public long dettaglioProcessoId;
	public long dlFileId;
}