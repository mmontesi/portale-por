/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionFactory;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermissionFactory;
import com.liferay.portal.kernel.service.ServiceContext;
import it.puglia.por.areariservata.constants.ActionKeys;
import it.puglia.por.areariservata.constants.ProcessoConstants;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.base.ProcessoServiceBaseImpl;

/**
 * The implementation of the processo remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.puglia.por.areariservata.service.ProcessoService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoServiceBaseImpl
 * @see it.puglia.por.areariservata.service.ProcessoServiceUtil
 */
public class ProcessoServiceImpl extends ProcessoServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link it.puglia.por.areariservata.service.ProcessoServiceUtil} to access the processo remote service.
     */

    private static volatile PortletResourcePermission
            _portletResourcePermission   =
            PortletResourcePermissionFactory.getInstance(
                    ProcessoServiceImpl.class, "_portletResourcePermission",
                    ProcessoConstants.RESOURCE_NAME);

    private static volatile ModelResourcePermission<Processo>
            _processoModelResourcePermission =
            ModelResourcePermissionFactory.getInstance(
                    ProcessoServiceImpl.class, "_processoModelResourcePermission",
                    Processo.class);

    public Processo addProcesso(long groupId, long userId, long companyId, long currentUserId, long scopeGroupId, String nomeProcesso, String descrizioneProcesso, String workflowName, String startingState, long gruppoMittenteId, long[] idUserToAssign, ServiceContext serviceContext)
            throws PortalException {

        _portletResourcePermission.check(
                getPermissionChecker(), serviceContext.getScopeGroupId(),
                ActionKeys.ADD_PROCESSO);

        return processoLocalService.addProcesso(
                groupId,
                userId,
                companyId,
                currentUserId,
                scopeGroupId,
                nomeProcesso,
                descrizioneProcesso,
                workflowName,
                startingState,
                gruppoMittenteId,
                idUserToAssign,
                serviceContext);
    }


    public Processo getProcesso(long processoId) throws PortalException {
        _processoModelResourcePermission.check(
                getPermissionChecker(), processoId, ActionKeys.SHOW_PROCESSO);
        return processoLocalService.getProcesso(processoId);
    }


}