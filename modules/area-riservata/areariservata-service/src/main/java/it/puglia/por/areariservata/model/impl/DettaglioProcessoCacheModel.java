/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.puglia.por.areariservata.model.DettaglioProcesso;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing DettaglioProcesso in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcesso
 * @generated
 */
@ProviderType
public class DettaglioProcessoCacheModel implements CacheModel<DettaglioProcesso>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DettaglioProcessoCacheModel)) {
			return false;
		}

		DettaglioProcessoCacheModel dettaglioProcessoCacheModel = (DettaglioProcessoCacheModel)obj;

		if (dettaglioProcessoId == dettaglioProcessoCacheModel.dettaglioProcessoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, dettaglioProcessoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", dettaglioProcessoId=");
		sb.append(dettaglioProcessoId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", processoId=");
		sb.append(processoId);
		sb.append(", isVisible=");
		sb.append(isVisible);
		sb.append(", testoDettaglio=");
		sb.append(testoDettaglio);
		sb.append(", descerizioneBreve=");
		sb.append(descerizioneBreve);
		sb.append(", dlFileId=");
		sb.append(dlFileId);
		sb.append(", nomeWorkflow=");
		sb.append(nomeWorkflow);
		sb.append(", statoWorkflow=");
		sb.append(statoWorkflow);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public DettaglioProcesso toEntityModel() {
		DettaglioProcessoImpl dettaglioProcessoImpl = new DettaglioProcessoImpl();

		if (uuid == null) {
			dettaglioProcessoImpl.setUuid("");
		}
		else {
			dettaglioProcessoImpl.setUuid(uuid);
		}

		dettaglioProcessoImpl.setDettaglioProcessoId(dettaglioProcessoId);
		dettaglioProcessoImpl.setGroupId(groupId);
		dettaglioProcessoImpl.setCompanyId(companyId);
		dettaglioProcessoImpl.setUserId(userId);

		if (userName == null) {
			dettaglioProcessoImpl.setUserName("");
		}
		else {
			dettaglioProcessoImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			dettaglioProcessoImpl.setCreateDate(null);
		}
		else {
			dettaglioProcessoImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			dettaglioProcessoImpl.setModifiedDate(null);
		}
		else {
			dettaglioProcessoImpl.setModifiedDate(new Date(modifiedDate));
		}

		dettaglioProcessoImpl.setProcessoId(processoId);
		dettaglioProcessoImpl.setIsVisible(isVisible);

		if (testoDettaglio == null) {
			dettaglioProcessoImpl.setTestoDettaglio("");
		}
		else {
			dettaglioProcessoImpl.setTestoDettaglio(testoDettaglio);
		}

		if (descerizioneBreve == null) {
			dettaglioProcessoImpl.setDescerizioneBreve("");
		}
		else {
			dettaglioProcessoImpl.setDescerizioneBreve(descerizioneBreve);
		}

		dettaglioProcessoImpl.setDlFileId(dlFileId);

		if (nomeWorkflow == null) {
			dettaglioProcessoImpl.setNomeWorkflow("");
		}
		else {
			dettaglioProcessoImpl.setNomeWorkflow(nomeWorkflow);
		}

		if (statoWorkflow == null) {
			dettaglioProcessoImpl.setStatoWorkflow("");
		}
		else {
			dettaglioProcessoImpl.setStatoWorkflow(statoWorkflow);
		}

		dettaglioProcessoImpl.resetOriginalValues();

		return dettaglioProcessoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		dettaglioProcessoId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		processoId = objectInput.readLong();

		isVisible = objectInput.readBoolean();
		testoDettaglio = objectInput.readUTF();
		descerizioneBreve = objectInput.readUTF();

		dlFileId = objectInput.readLong();
		nomeWorkflow = objectInput.readUTF();
		statoWorkflow = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(dettaglioProcessoId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(processoId);

		objectOutput.writeBoolean(isVisible);

		if (testoDettaglio == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(testoDettaglio);
		}

		if (descerizioneBreve == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(descerizioneBreve);
		}

		objectOutput.writeLong(dlFileId);

		if (nomeWorkflow == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nomeWorkflow);
		}

		if (statoWorkflow == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statoWorkflow);
		}
	}

	public String uuid;
	public long dettaglioProcessoId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long processoId;
	public boolean isVisible;
	public String testoDettaglio;
	public String descerizioneBreve;
	public long dlFileId;
	public String nomeWorkflow;
	public String statoWorkflow;
}