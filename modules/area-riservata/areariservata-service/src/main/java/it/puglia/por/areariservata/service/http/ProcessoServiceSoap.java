/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.puglia.por.areariservata.service.ProcessoServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link ProcessoServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link it.puglia.por.areariservata.model.ProcessoSoap}.
 * If the method in the service utility returns a
 * {@link it.puglia.por.areariservata.model.Processo}, that is translated to a
 * {@link it.puglia.por.areariservata.model.ProcessoSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoServiceHttp
 * @see it.puglia.por.areariservata.model.ProcessoSoap
 * @see ProcessoServiceUtil
 * @generated
 */
@ProviderType
public class ProcessoServiceSoap {
	public static it.puglia.por.areariservata.model.ProcessoSoap addProcesso(
		long groupId, long userId, long companyId, long currentUserId,
		long scopeGroupId, String nomeProcesso, String descrizioneProcesso,
		String workflowName, String startingState, long gruppoMittenteId,
		long[] idUserToAssign,
		com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws RemoteException {
		try {
			it.puglia.por.areariservata.model.Processo returnValue = ProcessoServiceUtil.addProcesso(groupId,
					userId, companyId, currentUserId, scopeGroupId,
					nomeProcesso, descrizioneProcesso, workflowName,
					startingState, gruppoMittenteId, idUserToAssign,
					serviceContext);

			return it.puglia.por.areariservata.model.ProcessoSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static it.puglia.por.areariservata.model.ProcessoSoap getProcesso(
		long processoId) throws RemoteException {
		try {
			it.puglia.por.areariservata.model.Processo returnValue = ProcessoServiceUtil.getProcesso(processoId);

			return it.puglia.por.areariservata.model.ProcessoSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ProcessoServiceSoap.class);
}