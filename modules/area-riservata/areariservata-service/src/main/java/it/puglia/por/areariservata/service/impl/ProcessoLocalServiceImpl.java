/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetLinkConstants;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.*;
import com.liferay.portal.spring.extender.service.ServiceReference;
import it.puglia.por.areariservata.common.AreaRiservataEnumUtils;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.model.UserProcesso;
import it.puglia.por.areariservata.service.UserProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.base.ProcessoLocalServiceBaseImpl;
import it.puglia.por.areariservata.service.persistence.ProcessoUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The implementation of the processo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link it.puglia.por.areariservata.service.ProcessoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.ProcessoLocalServiceUtil
 */
public class ProcessoLocalServiceImpl extends ProcessoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link
     * it.puglia.por.areariservata.service.ProcessoLocalServiceUtil} to access the
     * processo local service.
     */

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    @ServiceReference(type = Portal.class)
    private Portal _portal;

    @Indexable(type = IndexableType.REINDEX)
    public Processo addProcesso(
            long groupId, long userId, long companyId, long currentUserId,
            long scopeGroupIp, String nomeProcesso, String descrizioneProcesso, String workflowName,
            String startingState, long gruppoMittenteId,
            long[] idUserToAssign, ServiceContext serviceContext)
            throws PortalException {


        //validate(title);

        Date now        = new Date();
        long processoId = counterLocalService.increment();

        Processo processo = processoPersistence.create(processoId);
        processo.setUuid(serviceContext.getUuid());

        // Audit fields
        processo.setGroupId(groupId);
        processo.setCompanyId(companyId);
        processo.setUserId(currentUserId);
        processo.setGroupId(scopeGroupIp);
        processo.setIsVisible(true);
        processo.setCreateDate(serviceContext.getCreateDate(now));
        processo.setModifiedDate(serviceContext.getModifiedDate(now));

        // Other fields
        processo.setGruppoMittenteId(gruppoMittenteId);
        processo.setNomeProcesso(nomeProcesso);
        processo.setDescrizioneProcesso(descrizioneProcesso);
        processo.setNomeWorkflow(workflowName);
        processo.setStatoWorkflow(startingState);

        // Additional fields
        for (Long idUserFromPage : idUserToAssign) {
            long         userProcessoId = CounterLocalServiceUtil.increment(UserProcesso.class.getName());
            UserProcesso userProcesso   = UserProcessoLocalServiceUtil.createUserProcesso(userProcessoId);
            userProcesso.setCompanyId(companyId);
            userProcesso.setUserId(currentUserId);
            userProcesso.setGroupId(scopeGroupIp);
            userProcesso.setCreateDate(processo.getCreateDate());
            userProcesso.setModifiedDate(processo.getModifiedDate());
            userProcesso.setUserIdProcesso(idUserFromPage);
            userProcesso.setProcessoId(processoId);

            UserProcessoLocalServiceUtil.updateUserProcesso(userProcesso);

        }

        processo = processoPersistence.update(processo);

        // Resources
        resourceLocalService.addResources(
                processo.getCompanyId(), processo.getGroupId(), userId,
                Processo.class.getName(), processo.getProcessoId(), false, true, false);

        // Asset
        updateAsset(
                userId, processo, serviceContext.getAssetCategoryIds(),
                serviceContext.getAssetTagNames(),
                serviceContext.getAssetLinkEntryIds(),
                serviceContext.getAssetPriority());


        return processo;
    }

    @Override
    public Processo deleteProcesso(Processo processo) throws PortalException {
        processo.setIsVisible(false);
        processoPersistence.updateImpl(processo);

        // Resources
        resourceLocalService.deleteResource(
                processo.getCompanyId(), Processo.class.getName(),
                ResourceConstants.SCOPE_INDIVIDUAL, processo.getProcessoId());

        return processo;
    }

    public List<Processo> findAllByStato(String statoWorkflow) {

        List<Processo> result = new ArrayList<Processo>();

        try {
            result.addAll(ProcessoUtil.findBystato(statoWorkflow));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    public List<Processo> findByIsVisible(int start, int end) {

        List<Processo> result = new ArrayList<Processo>();

        try {
            result.addAll(ProcessoUtil.findByisVisible(true, start, end));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    public int countByIsVisible() {
        try {
            return ProcessoUtil.countByisVisible(true);
        } catch (Exception ex) {
            _log.error(ex.getMessage());
        }

        return 0;
    }

    public int countByIsVisibleAndInScadenza() { //TODO FARE DYNAMIC QUERY
        try {
            return ((Long) ProcessoUtil.findByisVisible(true)
                    .stream()
                    .filter(p -> p.getStatoWorkflow().equalsIgnoreCase("VALIDAZIONE")).count()).intValue();
        } catch (Exception ex) {
            _log.error(ex.getMessage());
        }

        return 0;
    }

    public List<Processo> findByIsVisibleAndIsInScadenza(int start, int end, OrderByComparator<Processo> obc) {

        List<Processo> result = new ArrayList<Processo>();

        try {
            result.addAll(
                    ProcessoUtil.findByisVisible(true, start, end, obc)
                            .stream()
                            .filter(p -> p.getStatoWorkflow().equalsIgnoreCase("VALIDAZIONE"))
                            .collect(Collectors.toList()));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    public List<Processo> getProcessiVisibili(int start, int end, OrderByComparator<Processo> obc) {
        return ProcessoUtil.findByisVisible(true, start, end, obc);
    }

    public List<Processo> getProcessiVisibiliByStato(int start, int end, OrderByComparator<Processo> obc, Long idStato) {
        List<Processo> result = new ArrayList<Processo>();

        try {
            result.addAll(
                    ProcessoUtil.findByisVisible(true, start, end, obc)
                            .stream()
                            .filter(p -> {
                                if (idStato != null && idStato > 0) {
                                    Enum statoEnum = AreaRiservataEnumUtils.getEnumByIdEnum(idStato);
                                    if (statoEnum != null) {
                                        return p.getStatoWorkflow().equalsIgnoreCase(statoEnum.name());
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return true;
                                }
                            }).collect(Collectors.toList()));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    public int countByIsVisibleAndStato(String stato) {
        try {

            return ((Long) ProcessoUtil.findByisVisible(true)
                    .stream()
                    .filter(p -> {
                        if (stato != null && !"0".equalsIgnoreCase(stato) && !"".equalsIgnoreCase(stato)) {
                            return ((p.getStatoWorkflow().equalsIgnoreCase(stato != null ? AreaRiservataEnumUtils.getEnumByStato(stato).name() : "")));
                        } else {
                            return true;
                        }
                    }).count()).intValue();

        } catch (Exception ex) {
            _log.error(ex.getMessage());
        }

        return 0;
    }


    public int countProcessoInScadenza() {
        try {
            return ProcessoUtil.countBystato("VALIDAZIONE");
        } catch (Exception ex) {
            _log.error(ex.getMessage());
        }

        return 0;
    }

    @Override
    public void updateAsset(
            long userId, Processo processo, long[] assetCategoryIds,
            String[] assetTagNames, long[] assetLinkEntryIds, Double priority)
            throws PortalException {

        boolean visible = false;

        if (processo.getIsVisible()) {
            visible = true;
        }

        String summary = HtmlUtil.extractText(
                StringUtil.shorten(processo.getDescrizioneProcesso(), 500));

        AssetEntry assetEntry = assetEntryLocalService.updateEntry(
                userId, processo.getGroupId(), processo.getCreateDate(),
                processo.getModifiedDate(), Processo.class.getName(), processo.getProcessoId(),
                processo.getUuid(), 0, assetCategoryIds, assetTagNames, true, visible,
                processo.getCreateDate(), null, null, null, ContentTypes.TEXT_HTML,
                processo.getNomeProcesso(), processo.getDescrizioneProcesso(), summary, null, null, 0, 0,
                priority);

        assetLinkLocalService.updateLinks(
                userId, assetEntry.getEntryId(), assetLinkEntryIds,
                AssetLinkConstants.TYPE_RELATED);
    }
}