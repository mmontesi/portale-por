/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.base.UserProcessoLocalServiceBaseImpl;
import it.puglia.por.areariservata.service.persistence.ProcessoUtil;
import it.puglia.por.areariservata.service.persistence.UserProcessoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * The implementation of the user processo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.puglia.por.areariservata.service.UserProcessoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.UserProcessoLocalServiceUtil
 */
public class UserProcessoLocalServiceImpl
        extends UserProcessoLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link it.puglia.por.areariservata.service.UserProcessoLocalServiceUtil} to access the user processo local service.
     */
    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    public List<User> getUtentiOfProcesso(Processo processo) {
        List<User> resultList = new ArrayList<>();

        try {
            UserProcessoUtil.findByprocessoId(processo.getProcessoId()).stream().forEach(userProcesso -> {
                resultList.add(UserLocalServiceUtil.fetchUser(userProcesso.getUserId()));
            });
        } catch (Exception e) {
            _log.error(e.getMessage());
        }


        return resultList;
    }

}