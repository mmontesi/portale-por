/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException;
import it.puglia.por.areariservata.model.DocumentoProcesso;
import it.puglia.por.areariservata.model.impl.DocumentoProcessoImpl;
import it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl;
import it.puglia.por.areariservata.service.persistence.DocumentoProcessoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the documento processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.DocumentoProcessoUtil
 * @generated
 */
@ProviderType
public class DocumentoProcessoPersistenceImpl extends BasePersistenceImpl<DocumentoProcesso>
	implements DocumentoProcessoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DocumentoProcessoUtil} to access the documento processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DocumentoProcessoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			DocumentoProcessoModelImpl.UUID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the documento processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if (!Objects.equals(uuid, documentoProcesso.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByUuid_First(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByUuid_First(uuid,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUuid_First(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByUuid_Last(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByUuid_Last(uuid,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findByUuid_PrevAndNext(
		long documentoProcessoId, String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, documentoProcesso, uuid,
					orderByComparator, true);

			array[1] = documentoProcesso;

			array[2] = getByUuid_PrevAndNext(session, documentoProcesso, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getByUuid_PrevAndNext(Session session,
		DocumentoProcesso documentoProcesso, String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (DocumentoProcesso documentoProcesso : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "documentoProcesso.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "documentoProcesso.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(documentoProcesso.uuid IS NULL OR documentoProcesso.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			DocumentoProcessoModelImpl.UUID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the documento processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByUUID_G(uuid, groupId);

		if (documentoProcesso == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchDocumentoProcessoException(msg.toString());
		}

		return documentoProcesso;
	}

	/**
	 * Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof DocumentoProcesso) {
			DocumentoProcesso documentoProcesso = (DocumentoProcesso)result;

			if (!Objects.equals(uuid, documentoProcesso.getUuid()) ||
					(groupId != documentoProcesso.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<DocumentoProcesso> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					DocumentoProcesso documentoProcesso = list.get(0);

					result = documentoProcesso;

					cacheResult(documentoProcesso);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DocumentoProcesso)result;
		}
	}

	/**
	 * Removes the documento processo where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the documento processo that was removed
	 */
	@Override
	public DocumentoProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByUUID_G(uuid, groupId);

		return remove(documentoProcesso);
	}

	/**
	 * Returns the number of documento processos where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "documentoProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "documentoProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(documentoProcesso.uuid IS NULL OR documentoProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "documentoProcesso.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			DocumentoProcessoModelImpl.UUID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.COMPANYID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the documento processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if (!Objects.equals(uuid, documentoProcesso.getUuid()) ||
							(companyId != documentoProcesso.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findByUuid_C_PrevAndNext(
		long documentoProcessoId, String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, documentoProcesso,
					uuid, companyId, orderByComparator, true);

			array[1] = documentoProcesso;

			array[2] = getByUuid_C_PrevAndNext(session, documentoProcesso,
					uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getByUuid_C_PrevAndNext(Session session,
		DocumentoProcesso documentoProcesso, String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (DocumentoProcesso documentoProcesso : findByUuid_C(uuid,
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "documentoProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "documentoProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(documentoProcesso.uuid IS NULL OR documentoProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "documentoProcesso.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSO = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByprocesso",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByprocesso",
			new String[] { Long.class.getName() },
			DocumentoProcessoModelImpl.PROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROCESSO = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByprocesso",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the documento processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocesso(long processoId) {
		return findByprocesso(processoId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the documento processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocesso(long processoId, int start,
		int end) {
		return findByprocesso(processoId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocesso(long processoId, int start,
		int end, OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findByprocesso(processoId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocesso(long processoId, int start,
		int end, OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO;
			finderArgs = new Object[] { processoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSO;
			finderArgs = new Object[] { processoId, start, end, orderByComparator };
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if ((processoId != documentoProcesso.getProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocesso_First(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocesso_First(processoId,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocesso_First(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findByprocesso(processoId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocesso_Last(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocesso_Last(processoId,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocesso_Last(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countByprocesso(processoId);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findByprocesso(processoId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where processoId = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findByprocesso_PrevAndNext(
		long documentoProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getByprocesso_PrevAndNext(session, documentoProcesso,
					processoId, orderByComparator, true);

			array[1] = documentoProcesso;

			array[2] = getByprocesso_PrevAndNext(session, documentoProcesso,
					processoId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getByprocesso_PrevAndNext(Session session,
		DocumentoProcesso documentoProcesso, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(processoId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where processoId = &#63; from the database.
	 *
	 * @param processoId the processo ID
	 */
	@Override
	public void removeByprocesso(long processoId) {
		for (DocumentoProcesso documentoProcesso : findByprocesso(processoId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByprocesso(long processoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROCESSO;

		Object[] finderArgs = new Object[] { processoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROCESSO_PROCESSOID_2 = "documentoProcesso.processoId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DETTAGLIOPROCESSO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBydettaglioProcesso",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBydettaglioProcesso", new String[] { Long.class.getName() },
			DocumentoProcessoModelImpl.DETTAGLIOPROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_DETTAGLIOPROCESSO = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBydettaglioProcesso", new String[] { Long.class.getName() });

	/**
	 * Returns all the documento processos where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId) {
		return findBydettaglioProcesso(dettaglioProcessoId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos where dettaglioProcessoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end) {
		return findBydettaglioProcesso(dettaglioProcessoId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findBydettaglioProcesso(dettaglioProcessoId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO;
			finderArgs = new Object[] { dettaglioProcessoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DETTAGLIOPROCESSO;
			finderArgs = new Object[] {
					dettaglioProcessoId,
					
					start, end, orderByComparator
				};
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if ((dettaglioProcessoId != documentoProcesso.getDettaglioProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_DETTAGLIOPROCESSO_DETTAGLIOPROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findBydettaglioProcesso_First(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchBydettaglioProcesso_First(dettaglioProcessoId,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchBydettaglioProcesso_First(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findBydettaglioProcesso(dettaglioProcessoId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchBydettaglioProcesso_Last(dettaglioProcessoId,
				orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countBydettaglioProcesso(dettaglioProcessoId);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findBydettaglioProcesso(dettaglioProcessoId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findBydettaglioProcesso_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getBydettaglioProcesso_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, orderByComparator,
					true);

			array[1] = documentoProcesso;

			array[2] = getBydettaglioProcesso_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getBydettaglioProcesso_PrevAndNext(
		Session session, DocumentoProcesso documentoProcesso,
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_DETTAGLIOPROCESSO_DETTAGLIOPROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dettaglioProcessoId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where dettaglioProcessoId = &#63; from the database.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 */
	@Override
	public void removeBydettaglioProcesso(long dettaglioProcessoId) {
		for (DocumentoProcesso documentoProcesso : findBydettaglioProcesso(
				dettaglioProcessoId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where dettaglioProcessoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countBydettaglioProcesso(long dettaglioProcessoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_DETTAGLIOPROCESSO;

		Object[] finderArgs = new Object[] { dettaglioProcessoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_DETTAGLIOPROCESSO_DETTAGLIOPROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_DETTAGLIOPROCESSO_DETTAGLIOPROCESSOID_2 =
		"documentoProcesso.dettaglioProcessoId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByprocessoAndDettaglio",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByprocessoAndDettaglio",
			new String[] { Long.class.getName(), Long.class.getName() },
			DocumentoProcessoModelImpl.DETTAGLIOPROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.PROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROCESSOANDDETTAGLIO = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByprocessoAndDettaglio",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId) {
		return findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end) {
		return findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO;
			finderArgs = new Object[] { dettaglioProcessoId, processoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO;
			finderArgs = new Object[] {
					dettaglioProcessoId, processoId,
					
					start, end, orderByComparator
				};
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if ((dettaglioProcessoId != documentoProcesso.getDettaglioProcessoId()) ||
							(processoId != documentoProcesso.getProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_DETTAGLIOPROCESSOID_2);

			query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_PROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				qPos.add(processoId);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocessoAndDettaglio_First(dettaglioProcessoId,
				processoId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append(", processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findByprocessoAndDettaglio(dettaglioProcessoId,
				processoId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocessoAndDettaglio_Last(dettaglioProcessoId,
				processoId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append(", processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countByprocessoAndDettaglio(dettaglioProcessoId, processoId);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findByprocessoAndDettaglio(dettaglioProcessoId,
				processoId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findByprocessoAndDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getByprocessoAndDettaglio_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, processoId,
					orderByComparator, true);

			array[1] = documentoProcesso;

			array[2] = getByprocessoAndDettaglio_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, processoId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getByprocessoAndDettaglio_PrevAndNext(
		Session session, DocumentoProcesso documentoProcesso,
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_DETTAGLIOPROCESSOID_2);

		query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_PROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dettaglioProcessoId);

		qPos.add(processoId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 */
	@Override
	public void removeByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId) {
		for (DocumentoProcesso documentoProcesso : findByprocessoAndDettaglio(
				dettaglioProcessoId, processoId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROCESSOANDDETTAGLIO;

		Object[] finderArgs = new Object[] { dettaglioProcessoId, processoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_DETTAGLIOPROCESSOID_2);

			query.append(_FINDER_COLUMN_PROCESSOANDDETTAGLIO_PROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				qPos.add(processoId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROCESSOANDDETTAGLIO_DETTAGLIOPROCESSOID_2 =
		"documentoProcesso.dettaglioProcessoId = ? AND ";
	private static final String _FINDER_COLUMN_PROCESSOANDDETTAGLIO_PROCESSOID_2 =
		"documentoProcesso.processoId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByprocessoOrDettaglio",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO =
		new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED,
			DocumentoProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByprocessoOrDettaglio",
			new String[] { Long.class.getName(), Long.class.getName() },
			DocumentoProcessoModelImpl.DETTAGLIOPROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.PROCESSOID_COLUMN_BITMASK |
			DocumentoProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROCESSOORDETTAGLIO = new FinderPath(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByprocessoOrDettaglio",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @return the matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId) {
		return findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end) {
		return findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching documento processos
	 */
	@Override
	public List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO;
			finderArgs = new Object[] { dettaglioProcessoId, processoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO;
			finderArgs = new Object[] {
					dettaglioProcessoId, processoId,
					
					start, end, orderByComparator
				};
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DocumentoProcesso documentoProcesso : list) {
					if ((dettaglioProcessoId != documentoProcesso.getDettaglioProcessoId()) ||
							(processoId != documentoProcesso.getProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_DETTAGLIOPROCESSOID_2);

			query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_PROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				qPos.add(processoId);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocessoOrDettaglio_First(dettaglioProcessoId,
				processoId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append(", processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		List<DocumentoProcesso> list = findByprocessoOrDettaglio(dettaglioProcessoId,
				processoId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo
	 * @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso findByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByprocessoOrDettaglio_Last(dettaglioProcessoId,
				processoId, orderByComparator);

		if (documentoProcesso != null) {
			return documentoProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("dettaglioProcessoId=");
		msg.append(dettaglioProcessoId);

		msg.append(", processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDocumentoProcessoException(msg.toString());
	}

	/**
	 * Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	 */
	@Override
	public DocumentoProcesso fetchByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		int count = countByprocessoOrDettaglio(dettaglioProcessoId, processoId);

		if (count == 0) {
			return null;
		}

		List<DocumentoProcesso> list = findByprocessoOrDettaglio(dettaglioProcessoId,
				processoId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param documentoProcessoId the primary key of the current documento processo
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso[] findByprocessoOrDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = findByPrimaryKey(documentoProcessoId);

		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso[] array = new DocumentoProcessoImpl[3];

			array[0] = getByprocessoOrDettaglio_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, processoId,
					orderByComparator, true);

			array[1] = documentoProcesso;

			array[2] = getByprocessoOrDettaglio_PrevAndNext(session,
					documentoProcesso, dettaglioProcessoId, processoId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DocumentoProcesso getByprocessoOrDettaglio_PrevAndNext(
		Session session, DocumentoProcesso documentoProcesso,
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_DETTAGLIOPROCESSOID_2);

		query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_PROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(dettaglioProcessoId);

		qPos.add(processoId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(documentoProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DocumentoProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 */
	@Override
	public void removeByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId) {
		for (DocumentoProcesso documentoProcesso : findByprocessoOrDettaglio(
				dettaglioProcessoId, processoId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the dettaglio processo ID
	 * @param processoId the processo ID
	 * @return the number of matching documento processos
	 */
	@Override
	public int countByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROCESSOORDETTAGLIO;

		Object[] finderArgs = new Object[] { dettaglioProcessoId, processoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DOCUMENTOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_DETTAGLIOPROCESSOID_2);

			query.append(_FINDER_COLUMN_PROCESSOORDETTAGLIO_PROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(dettaglioProcessoId);

				qPos.add(processoId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROCESSOORDETTAGLIO_DETTAGLIOPROCESSOID_2 =
		"documentoProcesso.dettaglioProcessoId = ? AND ";
	private static final String _FINDER_COLUMN_PROCESSOORDETTAGLIO_PROCESSOID_2 = "documentoProcesso.processoId = ?";

	public DocumentoProcessoPersistenceImpl() {
		setModelClass(DocumentoProcesso.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the documento processo in the entity cache if it is enabled.
	 *
	 * @param documentoProcesso the documento processo
	 */
	@Override
	public void cacheResult(DocumentoProcesso documentoProcesso) {
		entityCache.putResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoImpl.class, documentoProcesso.getPrimaryKey(),
			documentoProcesso);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				documentoProcesso.getUuid(), documentoProcesso.getGroupId()
			}, documentoProcesso);

		documentoProcesso.resetOriginalValues();
	}

	/**
	 * Caches the documento processos in the entity cache if it is enabled.
	 *
	 * @param documentoProcessos the documento processos
	 */
	@Override
	public void cacheResult(List<DocumentoProcesso> documentoProcessos) {
		for (DocumentoProcesso documentoProcesso : documentoProcessos) {
			if (entityCache.getResult(
						DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
						DocumentoProcessoImpl.class,
						documentoProcesso.getPrimaryKey()) == null) {
				cacheResult(documentoProcesso);
			}
			else {
				documentoProcesso.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all documento processos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(DocumentoProcessoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the documento processo.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DocumentoProcesso documentoProcesso) {
		entityCache.removeResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoImpl.class, documentoProcesso.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((DocumentoProcessoModelImpl)documentoProcesso,
			true);
	}

	@Override
	public void clearCache(List<DocumentoProcesso> documentoProcessos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DocumentoProcesso documentoProcesso : documentoProcessos) {
			entityCache.removeResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
				DocumentoProcessoImpl.class, documentoProcesso.getPrimaryKey());

			clearUniqueFindersCache((DocumentoProcessoModelImpl)documentoProcesso,
				true);
		}
	}

	protected void cacheUniqueFindersCache(
		DocumentoProcessoModelImpl documentoProcessoModelImpl) {
		Object[] args = new Object[] {
				documentoProcessoModelImpl.getUuid(),
				documentoProcessoModelImpl.getGroupId()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
			documentoProcessoModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		DocumentoProcessoModelImpl documentoProcessoModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					documentoProcessoModelImpl.getUuid(),
					documentoProcessoModelImpl.getGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		if ((documentoProcessoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					documentoProcessoModelImpl.getOriginalUuid(),
					documentoProcessoModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new documento processo with the primary key. Does not add the documento processo to the database.
	 *
	 * @param documentoProcessoId the primary key for the new documento processo
	 * @return the new documento processo
	 */
	@Override
	public DocumentoProcesso create(long documentoProcessoId) {
		DocumentoProcesso documentoProcesso = new DocumentoProcessoImpl();

		documentoProcesso.setNew(true);
		documentoProcesso.setPrimaryKey(documentoProcessoId);

		String uuid = PortalUUIDUtil.generate();

		documentoProcesso.setUuid(uuid);

		documentoProcesso.setCompanyId(companyProvider.getCompanyId());

		return documentoProcesso;
	}

	/**
	 * Removes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param documentoProcessoId the primary key of the documento processo
	 * @return the documento processo that was removed
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso remove(long documentoProcessoId)
		throws NoSuchDocumentoProcessoException {
		return remove((Serializable)documentoProcessoId);
	}

	/**
	 * Removes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the documento processo
	 * @return the documento processo that was removed
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso remove(Serializable primaryKey)
		throws NoSuchDocumentoProcessoException {
		Session session = null;

		try {
			session = openSession();

			DocumentoProcesso documentoProcesso = (DocumentoProcesso)session.get(DocumentoProcessoImpl.class,
					primaryKey);

			if (documentoProcesso == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDocumentoProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(documentoProcesso);
		}
		catch (NoSuchDocumentoProcessoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DocumentoProcesso removeImpl(DocumentoProcesso documentoProcesso) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(documentoProcesso)) {
				documentoProcesso = (DocumentoProcesso)session.get(DocumentoProcessoImpl.class,
						documentoProcesso.getPrimaryKeyObj());
			}

			if (documentoProcesso != null) {
				session.delete(documentoProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (documentoProcesso != null) {
			clearCache(documentoProcesso);
		}

		return documentoProcesso;
	}

	@Override
	public DocumentoProcesso updateImpl(DocumentoProcesso documentoProcesso) {
		boolean isNew = documentoProcesso.isNew();

		if (!(documentoProcesso instanceof DocumentoProcessoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(documentoProcesso.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(documentoProcesso);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in documentoProcesso proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom DocumentoProcesso implementation " +
				documentoProcesso.getClass());
		}

		DocumentoProcessoModelImpl documentoProcessoModelImpl = (DocumentoProcessoModelImpl)documentoProcesso;

		if (Validator.isNull(documentoProcesso.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			documentoProcesso.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (documentoProcesso.getCreateDate() == null)) {
			if (serviceContext == null) {
				documentoProcesso.setCreateDate(now);
			}
			else {
				documentoProcesso.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!documentoProcessoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				documentoProcesso.setModifiedDate(now);
			}
			else {
				documentoProcesso.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (documentoProcesso.isNew()) {
				session.save(documentoProcesso);

				documentoProcesso.setNew(false);
			}
			else {
				documentoProcesso = (DocumentoProcesso)session.merge(documentoProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!DocumentoProcessoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { documentoProcessoModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					documentoProcessoModelImpl.getUuid(),
					documentoProcessoModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { documentoProcessoModelImpl.getProcessoId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
				args);

			args = new Object[] {
					documentoProcessoModelImpl.getDettaglioProcessoId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_DETTAGLIOPROCESSO,
				args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO,
				args);

			args = new Object[] {
					documentoProcessoModelImpl.getDettaglioProcessoId(),
					documentoProcessoModelImpl.getProcessoId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOANDDETTAGLIO,
				args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO,
				args);

			args = new Object[] {
					documentoProcessoModelImpl.getDettaglioProcessoId(),
					documentoProcessoModelImpl.getProcessoId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOORDETTAGLIO,
				args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { documentoProcessoModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalUuid(),
						documentoProcessoModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						documentoProcessoModelImpl.getUuid(),
						documentoProcessoModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
					args);

				args = new Object[] { documentoProcessoModelImpl.getProcessoId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
					args);
			}

			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalDettaglioProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_DETTAGLIOPROCESSO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO,
					args);

				args = new Object[] {
						documentoProcessoModelImpl.getDettaglioProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_DETTAGLIOPROCESSO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DETTAGLIOPROCESSO,
					args);
			}

			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalDettaglioProcessoId(),
						documentoProcessoModelImpl.getOriginalProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOANDDETTAGLIO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO,
					args);

				args = new Object[] {
						documentoProcessoModelImpl.getDettaglioProcessoId(),
						documentoProcessoModelImpl.getProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOANDDETTAGLIO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOANDDETTAGLIO,
					args);
			}

			if ((documentoProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						documentoProcessoModelImpl.getOriginalDettaglioProcessoId(),
						documentoProcessoModelImpl.getOriginalProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOORDETTAGLIO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO,
					args);

				args = new Object[] {
						documentoProcessoModelImpl.getDettaglioProcessoId(),
						documentoProcessoModelImpl.getProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOORDETTAGLIO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOORDETTAGLIO,
					args);
			}
		}

		entityCache.putResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DocumentoProcessoImpl.class, documentoProcesso.getPrimaryKey(),
			documentoProcesso, false);

		clearUniqueFindersCache(documentoProcessoModelImpl, false);
		cacheUniqueFindersCache(documentoProcessoModelImpl);

		documentoProcesso.resetOriginalValues();

		return documentoProcesso;
	}

	/**
	 * Returns the documento processo with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the documento processo
	 * @return the documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDocumentoProcessoException {
		DocumentoProcesso documentoProcesso = fetchByPrimaryKey(primaryKey);

		if (documentoProcesso == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDocumentoProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return documentoProcesso;
	}

	/**
	 * Returns the documento processo with the primary key or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	 *
	 * @param documentoProcessoId the primary key of the documento processo
	 * @return the documento processo
	 * @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso findByPrimaryKey(long documentoProcessoId)
		throws NoSuchDocumentoProcessoException {
		return findByPrimaryKey((Serializable)documentoProcessoId);
	}

	/**
	 * Returns the documento processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the documento processo
	 * @return the documento processo, or <code>null</code> if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
				DocumentoProcessoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		DocumentoProcesso documentoProcesso = (DocumentoProcesso)serializable;

		if (documentoProcesso == null) {
			Session session = null;

			try {
				session = openSession();

				documentoProcesso = (DocumentoProcesso)session.get(DocumentoProcessoImpl.class,
						primaryKey);

				if (documentoProcesso != null) {
					cacheResult(documentoProcesso);
				}
				else {
					entityCache.putResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
						DocumentoProcessoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoProcessoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return documentoProcesso;
	}

	/**
	 * Returns the documento processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param documentoProcessoId the primary key of the documento processo
	 * @return the documento processo, or <code>null</code> if a documento processo with the primary key could not be found
	 */
	@Override
	public DocumentoProcesso fetchByPrimaryKey(long documentoProcessoId) {
		return fetchByPrimaryKey((Serializable)documentoProcessoId);
	}

	@Override
	public Map<Serializable, DocumentoProcesso> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, DocumentoProcesso> map = new HashMap<Serializable, DocumentoProcesso>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			DocumentoProcesso documentoProcesso = fetchByPrimaryKey(primaryKey);

			if (documentoProcesso != null) {
				map.put(primaryKey, documentoProcesso);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoProcessoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (DocumentoProcesso)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_DOCUMENTOPROCESSO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (DocumentoProcesso documentoProcesso : (List<DocumentoProcesso>)q.list()) {
				map.put(documentoProcesso.getPrimaryKeyObj(), documentoProcesso);

				cacheResult(documentoProcesso);

				uncachedPrimaryKeys.remove(documentoProcesso.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(DocumentoProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DocumentoProcessoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the documento processos.
	 *
	 * @return the documento processos
	 */
	@Override
	public List<DocumentoProcesso> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the documento processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @return the range of documento processos
	 */
	@Override
	public List<DocumentoProcesso> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the documento processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of documento processos
	 */
	@Override
	public List<DocumentoProcesso> findAll(int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the documento processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of documento processos
	 * @param end the upper bound of the range of documento processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of documento processos
	 */
	@Override
	public List<DocumentoProcesso> findAll(int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DocumentoProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DocumentoProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_DOCUMENTOPROCESSO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DOCUMENTOPROCESSO;

				if (pagination) {
					sql = sql.concat(DocumentoProcessoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DocumentoProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the documento processos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (DocumentoProcesso documentoProcesso : findAll()) {
			remove(documentoProcesso);
		}
	}

	/**
	 * Returns the number of documento processos.
	 *
	 * @return the number of documento processos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DOCUMENTOPROCESSO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return DocumentoProcessoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the documento processo persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(DocumentoProcessoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_DOCUMENTOPROCESSO = "SELECT documentoProcesso FROM DocumentoProcesso documentoProcesso";
	private static final String _SQL_SELECT_DOCUMENTOPROCESSO_WHERE_PKS_IN = "SELECT documentoProcesso FROM DocumentoProcesso documentoProcesso WHERE documentoProcessoId IN (";
	private static final String _SQL_SELECT_DOCUMENTOPROCESSO_WHERE = "SELECT documentoProcesso FROM DocumentoProcesso documentoProcesso WHERE ";
	private static final String _SQL_COUNT_DOCUMENTOPROCESSO = "SELECT COUNT(documentoProcesso) FROM DocumentoProcesso documentoProcesso";
	private static final String _SQL_COUNT_DOCUMENTOPROCESSO_WHERE = "SELECT COUNT(documentoProcesso) FROM DocumentoProcesso documentoProcesso WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "documentoProcesso.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DocumentoProcesso exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DocumentoProcesso exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(DocumentoProcessoPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}