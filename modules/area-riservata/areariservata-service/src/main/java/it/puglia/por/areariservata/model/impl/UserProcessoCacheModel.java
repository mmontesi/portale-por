/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.puglia.por.areariservata.model.UserProcesso;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing UserProcesso in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see UserProcesso
 * @generated
 */
@ProviderType
public class UserProcessoCacheModel implements CacheModel<UserProcesso>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserProcessoCacheModel)) {
			return false;
		}

		UserProcessoCacheModel userProcessoCacheModel = (UserProcessoCacheModel)obj;

		if (userProcessoId == userProcessoCacheModel.userProcessoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, userProcessoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", userProcessoId=");
		sb.append(userProcessoId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", userIdProcesso=");
		sb.append(userIdProcesso);
		sb.append(", processoId=");
		sb.append(processoId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public UserProcesso toEntityModel() {
		UserProcessoImpl userProcessoImpl = new UserProcessoImpl();

		if (uuid == null) {
			userProcessoImpl.setUuid("");
		}
		else {
			userProcessoImpl.setUuid(uuid);
		}

		userProcessoImpl.setUserProcessoId(userProcessoId);
		userProcessoImpl.setGroupId(groupId);
		userProcessoImpl.setCompanyId(companyId);
		userProcessoImpl.setUserId(userId);

		if (userName == null) {
			userProcessoImpl.setUserName("");
		}
		else {
			userProcessoImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			userProcessoImpl.setCreateDate(null);
		}
		else {
			userProcessoImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			userProcessoImpl.setModifiedDate(null);
		}
		else {
			userProcessoImpl.setModifiedDate(new Date(modifiedDate));
		}

		userProcessoImpl.setUserIdProcesso(userIdProcesso);
		userProcessoImpl.setProcessoId(processoId);

		userProcessoImpl.resetOriginalValues();

		return userProcessoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		userProcessoId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		userIdProcesso = objectInput.readLong();

		processoId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(userProcessoId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(userIdProcesso);

		objectOutput.writeLong(processoId);
	}

	public String uuid;
	public long userProcessoId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long userIdProcesso;
	public long processoId;
}