/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import it.puglia.por.areariservata.service.base.AreaRiservataUsersLocalServiceBaseImpl;

import java.util.*;

/**
 * The implementation of the area riservata users local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.puglia.por.areariservata.service.AreaRiservataUsersLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AreaRiservataUsersLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil
 */
public class AreaRiservataUsersLocalServiceImpl
        extends AreaRiservataUsersLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil} to access the area riservata users local service.
     */

    private static <T> List<T> safeSubList(List<T> list, int fromIndex, int toIndex) {
        int size = list.size();
        if (fromIndex >= size || toIndex <= 0 || fromIndex >= toIndex) {
            return Collections.emptyList();
        }

        fromIndex = Math.max(0, fromIndex);
        toIndex   = Math.min(size, toIndex);

        return list.subList(fromIndex, toIndex);
    }

    public Map<UserGroup, List<User>> getUserGroupAndUserOfAreaRiservata() {

        Map<UserGroup, List<User>> userGroupUserMap = new HashMap<>();

        UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())
                .stream()
                .filter(userGroup -> userGroup.getName().startsWith("OPR"))
                .forEach(userGroup -> userGroupUserMap.put(userGroup, UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId())));

        return userGroupUserMap;

    }

    public List<User> getUserOfAreaRiservata(int start, int end) {

        List<User> users = new ArrayList<>();

        UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())
                .stream()
                .filter(userGroup -> userGroup.getName().startsWith("OPR"))
                .forEach(userGroup -> users.addAll(UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId())));

        // No Unique
        Set<User>  uniqueUsers = new HashSet<>(users);
        List<User> result      = new ArrayList<>(uniqueUsers);

        return safeSubList(result, start, end);

    }

    public int countUserOfAreaRiservata() {

        List<User> users = new ArrayList<>();

        UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())
                .stream()
                .filter(userGroup -> userGroup.getName().startsWith("OPR"))
                .forEach(userGroup -> users.addAll(UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId())));

        // No Unique
        Set<User> uniqueUsers = new HashSet<>(users);

        return uniqueUsers.size();

    }

}