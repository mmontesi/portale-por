package it.puglia.por.areariservata.internal.security.permission.resource;


import com.liferay.exportimport.kernel.staging.permission.StagingPermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionFactory;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.StagedModelPermissionLogic;
import com.liferay.portal.kernel.util.HashMapDictionary;
import it.puglia.por.areariservata.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.constants.PortletAreaRiservataConstants;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.ProcessoLocalService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import java.util.Dictionary;

@Component(immediate = true, service = {})
public class ProcessoModelResourcePermissionRegistrar {

    @Reference private                                                                                 ProcessoLocalService                         _processoLocalService;
    @Reference(target = "(resource.name=" + PortletAreaRiservataConstants.RESOURCE_NAME + ")") private PortletResourcePermission                    _portletResourcePermission;
    private                                                                                            ServiceRegistration<ModelResourcePermission> _serviceRegistration;
    @Reference private                                                                                 StagingPermission                            _stagingPermission;

    @Activate
    public void activate(BundleContext bundleContext) {
        Dictionary<String, Object> properties = new HashMapDictionary<>();

        properties.put("model.class.name", Processo.class.getName());

        _serviceRegistration = bundleContext.registerService(
                ModelResourcePermission.class,
                ModelResourcePermissionFactory.create(
                        Processo.class, Processo::getProcessoId,
                        _processoLocalService::getProcesso,
                        _portletResourcePermission,
                        (modelResourcePermission, consumer) -> consumer.accept(
                                new StagedModelPermissionLogic<>(
                                        _stagingPermission, AreaRiservataPortletKeys.PORTLET_NAME,
                                        Processo::getProcessoId))),
                properties);
    }

    @Deactivate
    public void deactivate() {
        _serviceRegistration.unregister();
    }


}

