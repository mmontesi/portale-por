/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException;
import it.puglia.por.areariservata.model.EmailConfigurazione;
import it.puglia.por.areariservata.model.impl.EmailConfigurazioneImpl;
import it.puglia.por.areariservata.model.impl.EmailConfigurazioneModelImpl;
import it.puglia.por.areariservata.service.persistence.EmailConfigurazionePersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the email configurazione service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailConfigurazionePersistence
 * @see it.puglia.por.areariservata.service.persistence.EmailConfigurazioneUtil
 * @generated
 */
@ProviderType
public class EmailConfigurazionePersistenceImpl extends BasePersistenceImpl<EmailConfigurazione>
	implements EmailConfigurazionePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmailConfigurazioneUtil} to access the email configurazione persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmailConfigurazioneImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			EmailConfigurazioneModelImpl.UUID_COLUMN_BITMASK |
			EmailConfigurazioneModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the email configuraziones where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email configuraziones where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @return the range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email configuraziones where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid(String uuid, int start,
		int end, OrderByComparator<EmailConfigurazione> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email configuraziones where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid(String uuid, int start,
		int end, OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<EmailConfigurazione> list = null;

		if (retrieveFromCache) {
			list = (List<EmailConfigurazione>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailConfigurazione emailConfigurazione : list) {
					if (!Objects.equals(uuid, emailConfigurazione.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EmailConfigurazioneModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email configurazione in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione findByUuid_First(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByUuid_First(uuid,
				orderByComparator);

		if (emailConfigurazione != null) {
			return emailConfigurazione;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchEmailConfigurazioneException(msg.toString());
	}

	/**
	 * Returns the first email configurazione in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUuid_First(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		List<EmailConfigurazione> list = findByUuid(uuid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email configurazione in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione findByUuid_Last(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByUuid_Last(uuid,
				orderByComparator);

		if (emailConfigurazione != null) {
			return emailConfigurazione;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchEmailConfigurazioneException(msg.toString());
	}

	/**
	 * Returns the last email configurazione in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUuid_Last(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<EmailConfigurazione> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63;.
	 *
	 * @param emailConfigurazioneId the primary key of the current email configurazione
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione[] findByUuid_PrevAndNext(
		long emailConfigurazioneId, String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = findByPrimaryKey(emailConfigurazioneId);

		Session session = null;

		try {
			session = openSession();

			EmailConfigurazione[] array = new EmailConfigurazioneImpl[3];

			array[0] = getByUuid_PrevAndNext(session, emailConfigurazione,
					uuid, orderByComparator, true);

			array[1] = emailConfigurazione;

			array[2] = getByUuid_PrevAndNext(session, emailConfigurazione,
					uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailConfigurazione getByUuid_PrevAndNext(Session session,
		EmailConfigurazione emailConfigurazione, String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EmailConfigurazioneModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(emailConfigurazione);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmailConfigurazione> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email configuraziones where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (EmailConfigurazione emailConfigurazione : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(emailConfigurazione);
		}
	}

	/**
	 * Returns the number of email configuraziones where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching email configuraziones
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "emailConfigurazione.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "emailConfigurazione.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(emailConfigurazione.uuid IS NULL OR emailConfigurazione.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			EmailConfigurazioneModelImpl.UUID_COLUMN_BITMASK |
			EmailConfigurazioneModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the email configurazione where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByUUID_G(uuid, groupId);

		if (emailConfigurazione == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchEmailConfigurazioneException(msg.toString());
		}

		return emailConfigurazione;
	}

	/**
	 * Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof EmailConfigurazione) {
			EmailConfigurazione emailConfigurazione = (EmailConfigurazione)result;

			if (!Objects.equals(uuid, emailConfigurazione.getUuid()) ||
					(groupId != emailConfigurazione.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<EmailConfigurazione> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					EmailConfigurazione emailConfigurazione = list.get(0);

					result = emailConfigurazione;

					cacheResult(emailConfigurazione);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EmailConfigurazione)result;
		}
	}

	/**
	 * Removes the email configurazione where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the email configurazione that was removed
	 */
	@Override
	public EmailConfigurazione removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = findByUUID_G(uuid, groupId);

		return remove(emailConfigurazione);
	}

	/**
	 * Returns the number of email configuraziones where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching email configuraziones
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "emailConfigurazione.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "emailConfigurazione.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(emailConfigurazione.uuid IS NULL OR emailConfigurazione.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "emailConfigurazione.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED,
			EmailConfigurazioneImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			EmailConfigurazioneModelImpl.UUID_COLUMN_BITMASK |
			EmailConfigurazioneModelImpl.COMPANYID_COLUMN_BITMASK |
			EmailConfigurazioneModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the email configuraziones where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @return the range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<EmailConfigurazione> list = null;

		if (retrieveFromCache) {
			list = (List<EmailConfigurazione>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (EmailConfigurazione emailConfigurazione : list) {
					if (!Objects.equals(uuid, emailConfigurazione.getUuid()) ||
							(companyId != emailConfigurazione.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EmailConfigurazioneModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (emailConfigurazione != null) {
			return emailConfigurazione;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchEmailConfigurazioneException(msg.toString());
	}

	/**
	 * Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		List<EmailConfigurazione> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (emailConfigurazione != null) {
			return emailConfigurazione;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchEmailConfigurazioneException(msg.toString());
	}

	/**
	 * Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	 */
	@Override
	public EmailConfigurazione fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<EmailConfigurazione> list = findByUuid_C(uuid, companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param emailConfigurazioneId the primary key of the current email configurazione
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione[] findByUuid_C_PrevAndNext(
		long emailConfigurazioneId, String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = findByPrimaryKey(emailConfigurazioneId);

		Session session = null;

		try {
			session = openSession();

			EmailConfigurazione[] array = new EmailConfigurazioneImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, emailConfigurazione,
					uuid, companyId, orderByComparator, true);

			array[1] = emailConfigurazione;

			array[2] = getByUuid_C_PrevAndNext(session, emailConfigurazione,
					uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EmailConfigurazione getByUuid_C_PrevAndNext(Session session,
		EmailConfigurazione emailConfigurazione, String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EmailConfigurazioneModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(emailConfigurazione);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EmailConfigurazione> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the email configuraziones where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (EmailConfigurazione emailConfigurazione : findByUuid_C(uuid,
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(emailConfigurazione);
		}
	}

	/**
	 * Returns the number of email configuraziones where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching email configuraziones
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EMAILCONFIGURAZIONE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "emailConfigurazione.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "emailConfigurazione.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(emailConfigurazione.uuid IS NULL OR emailConfigurazione.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "emailConfigurazione.companyId = ?";

	public EmailConfigurazionePersistenceImpl() {
		setModelClass(EmailConfigurazione.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");
			dbColumnNames.put("from", "from_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the email configurazione in the entity cache if it is enabled.
	 *
	 * @param emailConfigurazione the email configurazione
	 */
	@Override
	public void cacheResult(EmailConfigurazione emailConfigurazione) {
		entityCache.putResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneImpl.class, emailConfigurazione.getPrimaryKey(),
			emailConfigurazione);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				emailConfigurazione.getUuid(), emailConfigurazione.getGroupId()
			}, emailConfigurazione);

		emailConfigurazione.resetOriginalValues();
	}

	/**
	 * Caches the email configuraziones in the entity cache if it is enabled.
	 *
	 * @param emailConfiguraziones the email configuraziones
	 */
	@Override
	public void cacheResult(List<EmailConfigurazione> emailConfiguraziones) {
		for (EmailConfigurazione emailConfigurazione : emailConfiguraziones) {
			if (entityCache.getResult(
						EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
						EmailConfigurazioneImpl.class,
						emailConfigurazione.getPrimaryKey()) == null) {
				cacheResult(emailConfigurazione);
			}
			else {
				emailConfigurazione.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all email configuraziones.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(EmailConfigurazioneImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the email configurazione.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmailConfigurazione emailConfigurazione) {
		entityCache.removeResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneImpl.class, emailConfigurazione.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((EmailConfigurazioneModelImpl)emailConfigurazione,
			true);
	}

	@Override
	public void clearCache(List<EmailConfigurazione> emailConfiguraziones) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmailConfigurazione emailConfigurazione : emailConfiguraziones) {
			entityCache.removeResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
				EmailConfigurazioneImpl.class,
				emailConfigurazione.getPrimaryKey());

			clearUniqueFindersCache((EmailConfigurazioneModelImpl)emailConfigurazione,
				true);
		}
	}

	protected void cacheUniqueFindersCache(
		EmailConfigurazioneModelImpl emailConfigurazioneModelImpl) {
		Object[] args = new Object[] {
				emailConfigurazioneModelImpl.getUuid(),
				emailConfigurazioneModelImpl.getGroupId()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
			emailConfigurazioneModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		EmailConfigurazioneModelImpl emailConfigurazioneModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					emailConfigurazioneModelImpl.getUuid(),
					emailConfigurazioneModelImpl.getGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		if ((emailConfigurazioneModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					emailConfigurazioneModelImpl.getOriginalUuid(),
					emailConfigurazioneModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new email configurazione with the primary key. Does not add the email configurazione to the database.
	 *
	 * @param emailConfigurazioneId the primary key for the new email configurazione
	 * @return the new email configurazione
	 */
	@Override
	public EmailConfigurazione create(long emailConfigurazioneId) {
		EmailConfigurazione emailConfigurazione = new EmailConfigurazioneImpl();

		emailConfigurazione.setNew(true);
		emailConfigurazione.setPrimaryKey(emailConfigurazioneId);

		String uuid = PortalUUIDUtil.generate();

		emailConfigurazione.setUuid(uuid);

		emailConfigurazione.setCompanyId(companyProvider.getCompanyId());

		return emailConfigurazione;
	}

	/**
	 * Removes the email configurazione with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param emailConfigurazioneId the primary key of the email configurazione
	 * @return the email configurazione that was removed
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione remove(long emailConfigurazioneId)
		throws NoSuchEmailConfigurazioneException {
		return remove((Serializable)emailConfigurazioneId);
	}

	/**
	 * Removes the email configurazione with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the email configurazione
	 * @return the email configurazione that was removed
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione remove(Serializable primaryKey)
		throws NoSuchEmailConfigurazioneException {
		Session session = null;

		try {
			session = openSession();

			EmailConfigurazione emailConfigurazione = (EmailConfigurazione)session.get(EmailConfigurazioneImpl.class,
					primaryKey);

			if (emailConfigurazione == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmailConfigurazioneException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(emailConfigurazione);
		}
		catch (NoSuchEmailConfigurazioneException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmailConfigurazione removeImpl(
		EmailConfigurazione emailConfigurazione) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(emailConfigurazione)) {
				emailConfigurazione = (EmailConfigurazione)session.get(EmailConfigurazioneImpl.class,
						emailConfigurazione.getPrimaryKeyObj());
			}

			if (emailConfigurazione != null) {
				session.delete(emailConfigurazione);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (emailConfigurazione != null) {
			clearCache(emailConfigurazione);
		}

		return emailConfigurazione;
	}

	@Override
	public EmailConfigurazione updateImpl(
		EmailConfigurazione emailConfigurazione) {
		boolean isNew = emailConfigurazione.isNew();

		if (!(emailConfigurazione instanceof EmailConfigurazioneModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(emailConfigurazione.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(emailConfigurazione);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in emailConfigurazione proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom EmailConfigurazione implementation " +
				emailConfigurazione.getClass());
		}

		EmailConfigurazioneModelImpl emailConfigurazioneModelImpl = (EmailConfigurazioneModelImpl)emailConfigurazione;

		if (Validator.isNull(emailConfigurazione.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			emailConfigurazione.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (emailConfigurazione.getCreateDate() == null)) {
			if (serviceContext == null) {
				emailConfigurazione.setCreateDate(now);
			}
			else {
				emailConfigurazione.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!emailConfigurazioneModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				emailConfigurazione.setModifiedDate(now);
			}
			else {
				emailConfigurazione.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (emailConfigurazione.isNew()) {
				session.save(emailConfigurazione);

				emailConfigurazione.setNew(false);
			}
			else {
				emailConfigurazione = (EmailConfigurazione)session.merge(emailConfigurazione);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!EmailConfigurazioneModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { emailConfigurazioneModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					emailConfigurazioneModelImpl.getUuid(),
					emailConfigurazioneModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((emailConfigurazioneModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						emailConfigurazioneModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { emailConfigurazioneModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((emailConfigurazioneModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						emailConfigurazioneModelImpl.getOriginalUuid(),
						emailConfigurazioneModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						emailConfigurazioneModelImpl.getUuid(),
						emailConfigurazioneModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}
		}

		entityCache.putResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
			EmailConfigurazioneImpl.class, emailConfigurazione.getPrimaryKey(),
			emailConfigurazione, false);

		clearUniqueFindersCache(emailConfigurazioneModelImpl, false);
		cacheUniqueFindersCache(emailConfigurazioneModelImpl);

		emailConfigurazione.resetOriginalValues();

		return emailConfigurazione;
	}

	/**
	 * Returns the email configurazione with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the email configurazione
	 * @return the email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEmailConfigurazioneException {
		EmailConfigurazione emailConfigurazione = fetchByPrimaryKey(primaryKey);

		if (emailConfigurazione == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEmailConfigurazioneException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return emailConfigurazione;
	}

	/**
	 * Returns the email configurazione with the primary key or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	 *
	 * @param emailConfigurazioneId the primary key of the email configurazione
	 * @return the email configurazione
	 * @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione findByPrimaryKey(long emailConfigurazioneId)
		throws NoSuchEmailConfigurazioneException {
		return findByPrimaryKey((Serializable)emailConfigurazioneId);
	}

	/**
	 * Returns the email configurazione with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the email configurazione
	 * @return the email configurazione, or <code>null</code> if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
				EmailConfigurazioneImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		EmailConfigurazione emailConfigurazione = (EmailConfigurazione)serializable;

		if (emailConfigurazione == null) {
			Session session = null;

			try {
				session = openSession();

				emailConfigurazione = (EmailConfigurazione)session.get(EmailConfigurazioneImpl.class,
						primaryKey);

				if (emailConfigurazione != null) {
					cacheResult(emailConfigurazione);
				}
				else {
					entityCache.putResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
						EmailConfigurazioneImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
					EmailConfigurazioneImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return emailConfigurazione;
	}

	/**
	 * Returns the email configurazione with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param emailConfigurazioneId the primary key of the email configurazione
	 * @return the email configurazione, or <code>null</code> if a email configurazione with the primary key could not be found
	 */
	@Override
	public EmailConfigurazione fetchByPrimaryKey(long emailConfigurazioneId) {
		return fetchByPrimaryKey((Serializable)emailConfigurazioneId);
	}

	@Override
	public Map<Serializable, EmailConfigurazione> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, EmailConfigurazione> map = new HashMap<Serializable, EmailConfigurazione>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			EmailConfigurazione emailConfigurazione = fetchByPrimaryKey(primaryKey);

			if (emailConfigurazione != null) {
				map.put(primaryKey, emailConfigurazione);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
					EmailConfigurazioneImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (EmailConfigurazione)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_EMAILCONFIGURAZIONE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (EmailConfigurazione emailConfigurazione : (List<EmailConfigurazione>)q.list()) {
				map.put(emailConfigurazione.getPrimaryKeyObj(),
					emailConfigurazione);

				cacheResult(emailConfigurazione);

				uncachedPrimaryKeys.remove(emailConfigurazione.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(EmailConfigurazioneModelImpl.ENTITY_CACHE_ENABLED,
					EmailConfigurazioneImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the email configuraziones.
	 *
	 * @return the email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email configuraziones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @return the range of email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the email configuraziones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findAll(int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the email configuraziones.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of email configuraziones
	 * @param end the upper bound of the range of email configuraziones (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of email configuraziones
	 */
	@Override
	public List<EmailConfigurazione> findAll(int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmailConfigurazione> list = null;

		if (retrieveFromCache) {
			list = (List<EmailConfigurazione>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_EMAILCONFIGURAZIONE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMAILCONFIGURAZIONE;

				if (pagination) {
					sql = sql.concat(EmailConfigurazioneModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<EmailConfigurazione>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the email configuraziones from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (EmailConfigurazione emailConfigurazione : findAll()) {
			remove(emailConfigurazione);
		}
	}

	/**
	 * Returns the number of email configuraziones.
	 *
	 * @return the number of email configuraziones
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMAILCONFIGURAZIONE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return EmailConfigurazioneModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the email configurazione persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(EmailConfigurazioneImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_EMAILCONFIGURAZIONE = "SELECT emailConfigurazione FROM EmailConfigurazione emailConfigurazione";
	private static final String _SQL_SELECT_EMAILCONFIGURAZIONE_WHERE_PKS_IN = "SELECT emailConfigurazione FROM EmailConfigurazione emailConfigurazione WHERE emailConfigurazioneId IN (";
	private static final String _SQL_SELECT_EMAILCONFIGURAZIONE_WHERE = "SELECT emailConfigurazione FROM EmailConfigurazione emailConfigurazione WHERE ";
	private static final String _SQL_COUNT_EMAILCONFIGURAZIONE = "SELECT COUNT(emailConfigurazione) FROM EmailConfigurazione emailConfigurazione";
	private static final String _SQL_COUNT_EMAILCONFIGURAZIONE_WHERE = "SELECT COUNT(emailConfigurazione) FROM EmailConfigurazione emailConfigurazione WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "emailConfigurazione.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmailConfigurazione exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EmailConfigurazione exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(EmailConfigurazionePersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid", "from"
			});
}