/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import it.puglia.por.areariservata.model.Processo;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Processo in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Processo
 * @generated
 */
@ProviderType
public class ProcessoCacheModel implements CacheModel<Processo>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProcessoCacheModel)) {
			return false;
		}

		ProcessoCacheModel processoCacheModel = (ProcessoCacheModel)obj;

		if (processoId == processoCacheModel.processoId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, processoId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(31);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", processoId=");
		sb.append(processoId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nomeProcesso=");
		sb.append(nomeProcesso);
		sb.append(", descrizioneProcesso=");
		sb.append(descrizioneProcesso);
		sb.append(", isVisible=");
		sb.append(isVisible);
		sb.append(", folderId=");
		sb.append(folderId);
		sb.append(", nomeWorkflow=");
		sb.append(nomeWorkflow);
		sb.append(", statoWorkflow=");
		sb.append(statoWorkflow);
		sb.append(", gruppoMittenteId=");
		sb.append(gruppoMittenteId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Processo toEntityModel() {
		ProcessoImpl processoImpl = new ProcessoImpl();

		if (uuid == null) {
			processoImpl.setUuid("");
		}
		else {
			processoImpl.setUuid(uuid);
		}

		processoImpl.setProcessoId(processoId);
		processoImpl.setGroupId(groupId);
		processoImpl.setCompanyId(companyId);
		processoImpl.setUserId(userId);

		if (userName == null) {
			processoImpl.setUserName("");
		}
		else {
			processoImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			processoImpl.setCreateDate(null);
		}
		else {
			processoImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			processoImpl.setModifiedDate(null);
		}
		else {
			processoImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nomeProcesso == null) {
			processoImpl.setNomeProcesso("");
		}
		else {
			processoImpl.setNomeProcesso(nomeProcesso);
		}

		if (descrizioneProcesso == null) {
			processoImpl.setDescrizioneProcesso("");
		}
		else {
			processoImpl.setDescrizioneProcesso(descrizioneProcesso);
		}

		processoImpl.setIsVisible(isVisible);
		processoImpl.setFolderId(folderId);

		if (nomeWorkflow == null) {
			processoImpl.setNomeWorkflow("");
		}
		else {
			processoImpl.setNomeWorkflow(nomeWorkflow);
		}

		if (statoWorkflow == null) {
			processoImpl.setStatoWorkflow("");
		}
		else {
			processoImpl.setStatoWorkflow(statoWorkflow);
		}

		processoImpl.setGruppoMittenteId(gruppoMittenteId);

		processoImpl.resetOriginalValues();

		return processoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		processoId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nomeProcesso = objectInput.readUTF();
		descrizioneProcesso = objectInput.readUTF();

		isVisible = objectInput.readBoolean();

		folderId = objectInput.readLong();
		nomeWorkflow = objectInput.readUTF();
		statoWorkflow = objectInput.readUTF();

		gruppoMittenteId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(processoId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nomeProcesso == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nomeProcesso);
		}

		if (descrizioneProcesso == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(descrizioneProcesso);
		}

		objectOutput.writeBoolean(isVisible);

		objectOutput.writeLong(folderId);

		if (nomeWorkflow == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nomeWorkflow);
		}

		if (statoWorkflow == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(statoWorkflow);
		}

		objectOutput.writeLong(gruppoMittenteId);
	}

	public String uuid;
	public long processoId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nomeProcesso;
	public String descrizioneProcesso;
	public boolean isVisible;
	public long folderId;
	public String nomeWorkflow;
	public String statoWorkflow;
	public long gruppoMittenteId;
}