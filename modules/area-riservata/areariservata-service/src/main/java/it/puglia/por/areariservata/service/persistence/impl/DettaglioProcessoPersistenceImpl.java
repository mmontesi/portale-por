/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException;
import it.puglia.por.areariservata.model.DettaglioProcesso;
import it.puglia.por.areariservata.model.impl.DettaglioProcessoImpl;
import it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl;
import it.puglia.por.areariservata.service.persistence.DettaglioProcessoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the dettaglio processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.DettaglioProcessoUtil
 * @generated
 */
@ProviderType
public class DettaglioProcessoPersistenceImpl extends BasePersistenceImpl<DettaglioProcesso>
	implements DettaglioProcessoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link DettaglioProcessoUtil} to access the dettaglio processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = DettaglioProcessoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			DettaglioProcessoModelImpl.UUID_COLUMN_BITMASK |
			DettaglioProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the dettaglio processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dettaglio processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @return the range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<DettaglioProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DettaglioProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DettaglioProcesso dettaglioProcesso : list) {
					if (!Objects.equals(uuid, dettaglioProcesso.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByUuid_First(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByUuid_First(uuid,
				orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUuid_First(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		List<DettaglioProcesso> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByUuid_Last(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByUuid_Last(uuid,
				orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<DettaglioProcesso> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63;.
	 *
	 * @param dettaglioProcessoId the primary key of the current dettaglio processo
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso[] findByUuid_PrevAndNext(
		long dettaglioProcessoId, String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = findByPrimaryKey(dettaglioProcessoId);

		Session session = null;

		try {
			session = openSession();

			DettaglioProcesso[] array = new DettaglioProcessoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, dettaglioProcesso, uuid,
					orderByComparator, true);

			array[1] = dettaglioProcesso;

			array[2] = getByUuid_PrevAndNext(session, dettaglioProcesso, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DettaglioProcesso getByUuid_PrevAndNext(Session session,
		DettaglioProcesso dettaglioProcesso, String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dettaglioProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DettaglioProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dettaglio processos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (DettaglioProcesso dettaglioProcesso : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(dettaglioProcesso);
		}
	}

	/**
	 * Returns the number of dettaglio processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching dettaglio processos
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "dettaglioProcesso.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "dettaglioProcesso.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(dettaglioProcesso.uuid IS NULL OR dettaglioProcesso.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			DettaglioProcessoModelImpl.UUID_COLUMN_BITMASK |
			DettaglioProcessoModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByUUID_G(uuid, groupId);

		if (dettaglioProcesso == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchDettaglioProcessoException(msg.toString());
		}

		return dettaglioProcesso;
	}

	/**
	 * Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof DettaglioProcesso) {
			DettaglioProcesso dettaglioProcesso = (DettaglioProcesso)result;

			if (!Objects.equals(uuid, dettaglioProcesso.getUuid()) ||
					(groupId != dettaglioProcesso.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<DettaglioProcesso> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					DettaglioProcesso dettaglioProcesso = list.get(0);

					result = dettaglioProcesso;

					cacheResult(dettaglioProcesso);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (DettaglioProcesso)result;
		}
	}

	/**
	 * Removes the dettaglio processo where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the dettaglio processo that was removed
	 */
	@Override
	public DettaglioProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = findByUUID_G(uuid, groupId);

		return remove(dettaglioProcesso);
	}

	/**
	 * Returns the number of dettaglio processos where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching dettaglio processos
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "dettaglioProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "dettaglioProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(dettaglioProcesso.uuid IS NULL OR dettaglioProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "dettaglioProcesso.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			DettaglioProcessoModelImpl.UUID_COLUMN_BITMASK |
			DettaglioProcessoModelImpl.COMPANYID_COLUMN_BITMASK |
			DettaglioProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @return the range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<DettaglioProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DettaglioProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DettaglioProcesso dettaglioProcesso : list) {
					if (!Objects.equals(uuid, dettaglioProcesso.getUuid()) ||
							(companyId != dettaglioProcesso.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		List<DettaglioProcesso> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<DettaglioProcesso> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param dettaglioProcessoId the primary key of the current dettaglio processo
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso[] findByUuid_C_PrevAndNext(
		long dettaglioProcessoId, String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = findByPrimaryKey(dettaglioProcessoId);

		Session session = null;

		try {
			session = openSession();

			DettaglioProcesso[] array = new DettaglioProcessoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, dettaglioProcesso,
					uuid, companyId, orderByComparator, true);

			array[1] = dettaglioProcesso;

			array[2] = getByUuid_C_PrevAndNext(session, dettaglioProcesso,
					uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DettaglioProcesso getByUuid_C_PrevAndNext(Session session,
		DettaglioProcesso dettaglioProcesso, String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dettaglioProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DettaglioProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dettaglio processos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (DettaglioProcesso dettaglioProcesso : findByUuid_C(uuid,
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(dettaglioProcesso);
		}
	}

	/**
	 * Returns the number of dettaglio processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching dettaglio processos
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DETTAGLIOPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "dettaglioProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "dettaglioProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(dettaglioProcesso.uuid IS NULL OR dettaglioProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "dettaglioProcesso.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSO = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByprocesso",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO =
		new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED,
			DettaglioProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByprocesso",
			new String[] { Long.class.getName() },
			DettaglioProcessoModelImpl.PROCESSOID_COLUMN_BITMASK |
			DettaglioProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROCESSO = new FinderPath(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByprocesso",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the dettaglio processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByprocesso(Long processoId) {
		return findByprocesso(processoId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the dettaglio processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @return the range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByprocesso(Long processoId, int start,
		int end) {
		return findByprocesso(processoId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByprocesso(Long processoId, int start,
		int end, OrderByComparator<DettaglioProcesso> orderByComparator) {
		return findByprocesso(processoId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findByprocesso(Long processoId, int start,
		int end, OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO;
			finderArgs = new Object[] { processoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSO;
			finderArgs = new Object[] { processoId, start, end, orderByComparator };
		}

		List<DettaglioProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DettaglioProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (DettaglioProcesso dettaglioProcesso : list) {
					if (!Objects.equals(processoId,
								dettaglioProcesso.getProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId.longValue());

				if (!pagination) {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByprocesso_First(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByprocesso_First(processoId,
				orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByprocesso_First(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		List<DettaglioProcesso> list = findByprocesso(processoId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso findByprocesso_Last(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByprocesso_Last(processoId,
				orderByComparator);

		if (dettaglioProcesso != null) {
			return dettaglioProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchDettaglioProcessoException(msg.toString());
	}

	/**
	 * Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	 */
	@Override
	public DettaglioProcesso fetchByprocesso_Last(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		int count = countByprocesso(processoId);

		if (count == 0) {
			return null;
		}

		List<DettaglioProcesso> list = findByprocesso(processoId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where processoId = &#63;.
	 *
	 * @param dettaglioProcessoId the primary key of the current dettaglio processo
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso[] findByprocesso_PrevAndNext(
		long dettaglioProcessoId, Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = findByPrimaryKey(dettaglioProcessoId);

		Session session = null;

		try {
			session = openSession();

			DettaglioProcesso[] array = new DettaglioProcessoImpl[3];

			array[0] = getByprocesso_PrevAndNext(session, dettaglioProcesso,
					processoId, orderByComparator, true);

			array[1] = dettaglioProcesso;

			array[2] = getByprocesso_PrevAndNext(session, dettaglioProcesso,
					processoId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected DettaglioProcesso getByprocesso_PrevAndNext(Session session,
		DettaglioProcesso dettaglioProcesso, Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(processoId.longValue());

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(dettaglioProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<DettaglioProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the dettaglio processos where processoId = &#63; from the database.
	 *
	 * @param processoId the processo ID
	 */
	@Override
	public void removeByprocesso(Long processoId) {
		for (DettaglioProcesso dettaglioProcesso : findByprocesso(processoId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(dettaglioProcesso);
		}
	}

	/**
	 * Returns the number of dettaglio processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the number of matching dettaglio processos
	 */
	@Override
	public int countByprocesso(Long processoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROCESSO;

		Object[] finderArgs = new Object[] { processoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DETTAGLIOPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSO_PROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId.longValue());

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROCESSO_PROCESSOID_2 = "dettaglioProcesso.processoId = ?";

	public DettaglioProcessoPersistenceImpl() {
		setModelClass(DettaglioProcesso.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the dettaglio processo in the entity cache if it is enabled.
	 *
	 * @param dettaglioProcesso the dettaglio processo
	 */
	@Override
	public void cacheResult(DettaglioProcesso dettaglioProcesso) {
		entityCache.putResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoImpl.class, dettaglioProcesso.getPrimaryKey(),
			dettaglioProcesso);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				dettaglioProcesso.getUuid(), dettaglioProcesso.getGroupId()
			}, dettaglioProcesso);

		dettaglioProcesso.resetOriginalValues();
	}

	/**
	 * Caches the dettaglio processos in the entity cache if it is enabled.
	 *
	 * @param dettaglioProcessos the dettaglio processos
	 */
	@Override
	public void cacheResult(List<DettaglioProcesso> dettaglioProcessos) {
		for (DettaglioProcesso dettaglioProcesso : dettaglioProcessos) {
			if (entityCache.getResult(
						DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
						DettaglioProcessoImpl.class,
						dettaglioProcesso.getPrimaryKey()) == null) {
				cacheResult(dettaglioProcesso);
			}
			else {
				dettaglioProcesso.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all dettaglio processos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(DettaglioProcessoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the dettaglio processo.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(DettaglioProcesso dettaglioProcesso) {
		entityCache.removeResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoImpl.class, dettaglioProcesso.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((DettaglioProcessoModelImpl)dettaglioProcesso,
			true);
	}

	@Override
	public void clearCache(List<DettaglioProcesso> dettaglioProcessos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (DettaglioProcesso dettaglioProcesso : dettaglioProcessos) {
			entityCache.removeResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
				DettaglioProcessoImpl.class, dettaglioProcesso.getPrimaryKey());

			clearUniqueFindersCache((DettaglioProcessoModelImpl)dettaglioProcesso,
				true);
		}
	}

	protected void cacheUniqueFindersCache(
		DettaglioProcessoModelImpl dettaglioProcessoModelImpl) {
		Object[] args = new Object[] {
				dettaglioProcessoModelImpl.getUuid(),
				dettaglioProcessoModelImpl.getGroupId()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
			dettaglioProcessoModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		DettaglioProcessoModelImpl dettaglioProcessoModelImpl,
		boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					dettaglioProcessoModelImpl.getUuid(),
					dettaglioProcessoModelImpl.getGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		if ((dettaglioProcessoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					dettaglioProcessoModelImpl.getOriginalUuid(),
					dettaglioProcessoModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new dettaglio processo with the primary key. Does not add the dettaglio processo to the database.
	 *
	 * @param dettaglioProcessoId the primary key for the new dettaglio processo
	 * @return the new dettaglio processo
	 */
	@Override
	public DettaglioProcesso create(long dettaglioProcessoId) {
		DettaglioProcesso dettaglioProcesso = new DettaglioProcessoImpl();

		dettaglioProcesso.setNew(true);
		dettaglioProcesso.setPrimaryKey(dettaglioProcessoId);

		String uuid = PortalUUIDUtil.generate();

		dettaglioProcesso.setUuid(uuid);

		dettaglioProcesso.setCompanyId(companyProvider.getCompanyId());

		return dettaglioProcesso;
	}

	/**
	 * Removes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param dettaglioProcessoId the primary key of the dettaglio processo
	 * @return the dettaglio processo that was removed
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso remove(long dettaglioProcessoId)
		throws NoSuchDettaglioProcessoException {
		return remove((Serializable)dettaglioProcessoId);
	}

	/**
	 * Removes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the dettaglio processo
	 * @return the dettaglio processo that was removed
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso remove(Serializable primaryKey)
		throws NoSuchDettaglioProcessoException {
		Session session = null;

		try {
			session = openSession();

			DettaglioProcesso dettaglioProcesso = (DettaglioProcesso)session.get(DettaglioProcessoImpl.class,
					primaryKey);

			if (dettaglioProcesso == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDettaglioProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(dettaglioProcesso);
		}
		catch (NoSuchDettaglioProcessoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected DettaglioProcesso removeImpl(DettaglioProcesso dettaglioProcesso) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(dettaglioProcesso)) {
				dettaglioProcesso = (DettaglioProcesso)session.get(DettaglioProcessoImpl.class,
						dettaglioProcesso.getPrimaryKeyObj());
			}

			if (dettaglioProcesso != null) {
				session.delete(dettaglioProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (dettaglioProcesso != null) {
			clearCache(dettaglioProcesso);
		}

		return dettaglioProcesso;
	}

	@Override
	public DettaglioProcesso updateImpl(DettaglioProcesso dettaglioProcesso) {
		boolean isNew = dettaglioProcesso.isNew();

		if (!(dettaglioProcesso instanceof DettaglioProcessoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(dettaglioProcesso.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(dettaglioProcesso);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in dettaglioProcesso proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom DettaglioProcesso implementation " +
				dettaglioProcesso.getClass());
		}

		DettaglioProcessoModelImpl dettaglioProcessoModelImpl = (DettaglioProcessoModelImpl)dettaglioProcesso;

		if (Validator.isNull(dettaglioProcesso.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			dettaglioProcesso.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (dettaglioProcesso.getCreateDate() == null)) {
			if (serviceContext == null) {
				dettaglioProcesso.setCreateDate(now);
			}
			else {
				dettaglioProcesso.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!dettaglioProcessoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				dettaglioProcesso.setModifiedDate(now);
			}
			else {
				dettaglioProcesso.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (dettaglioProcesso.isNew()) {
				session.save(dettaglioProcesso);

				dettaglioProcesso.setNew(false);
			}
			else {
				dettaglioProcesso = (DettaglioProcesso)session.merge(dettaglioProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!DettaglioProcessoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { dettaglioProcessoModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					dettaglioProcessoModelImpl.getUuid(),
					dettaglioProcessoModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { dettaglioProcessoModelImpl.getProcessoId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((dettaglioProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dettaglioProcessoModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { dettaglioProcessoModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((dettaglioProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dettaglioProcessoModelImpl.getOriginalUuid(),
						dettaglioProcessoModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						dettaglioProcessoModelImpl.getUuid(),
						dettaglioProcessoModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((dettaglioProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						dettaglioProcessoModelImpl.getOriginalProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
					args);

				args = new Object[] { dettaglioProcessoModelImpl.getProcessoId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSO,
					args);
			}
		}

		entityCache.putResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
			DettaglioProcessoImpl.class, dettaglioProcesso.getPrimaryKey(),
			dettaglioProcesso, false);

		clearUniqueFindersCache(dettaglioProcessoModelImpl, false);
		cacheUniqueFindersCache(dettaglioProcessoModelImpl);

		dettaglioProcesso.resetOriginalValues();

		return dettaglioProcesso;
	}

	/**
	 * Returns the dettaglio processo with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the dettaglio processo
	 * @return the dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDettaglioProcessoException {
		DettaglioProcesso dettaglioProcesso = fetchByPrimaryKey(primaryKey);

		if (dettaglioProcesso == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDettaglioProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return dettaglioProcesso;
	}

	/**
	 * Returns the dettaglio processo with the primary key or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	 *
	 * @param dettaglioProcessoId the primary key of the dettaglio processo
	 * @return the dettaglio processo
	 * @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso findByPrimaryKey(long dettaglioProcessoId)
		throws NoSuchDettaglioProcessoException {
		return findByPrimaryKey((Serializable)dettaglioProcessoId);
	}

	/**
	 * Returns the dettaglio processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the dettaglio processo
	 * @return the dettaglio processo, or <code>null</code> if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
				DettaglioProcessoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		DettaglioProcesso dettaglioProcesso = (DettaglioProcesso)serializable;

		if (dettaglioProcesso == null) {
			Session session = null;

			try {
				session = openSession();

				dettaglioProcesso = (DettaglioProcesso)session.get(DettaglioProcessoImpl.class,
						primaryKey);

				if (dettaglioProcesso != null) {
					cacheResult(dettaglioProcesso);
				}
				else {
					entityCache.putResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
						DettaglioProcessoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DettaglioProcessoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return dettaglioProcesso;
	}

	/**
	 * Returns the dettaglio processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param dettaglioProcessoId the primary key of the dettaglio processo
	 * @return the dettaglio processo, or <code>null</code> if a dettaglio processo with the primary key could not be found
	 */
	@Override
	public DettaglioProcesso fetchByPrimaryKey(long dettaglioProcessoId) {
		return fetchByPrimaryKey((Serializable)dettaglioProcessoId);
	}

	@Override
	public Map<Serializable, DettaglioProcesso> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, DettaglioProcesso> map = new HashMap<Serializable, DettaglioProcesso>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			DettaglioProcesso dettaglioProcesso = fetchByPrimaryKey(primaryKey);

			if (dettaglioProcesso != null) {
				map.put(primaryKey, dettaglioProcesso);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DettaglioProcessoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (DettaglioProcesso)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_DETTAGLIOPROCESSO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (DettaglioProcesso dettaglioProcesso : (List<DettaglioProcesso>)q.list()) {
				map.put(dettaglioProcesso.getPrimaryKeyObj(), dettaglioProcesso);

				cacheResult(dettaglioProcesso);

				uncachedPrimaryKeys.remove(dettaglioProcesso.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(DettaglioProcessoModelImpl.ENTITY_CACHE_ENABLED,
					DettaglioProcessoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the dettaglio processos.
	 *
	 * @return the dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the dettaglio processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @return the range of dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findAll(int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the dettaglio processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of dettaglio processos
	 * @param end the upper bound of the range of dettaglio processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of dettaglio processos
	 */
	@Override
	public List<DettaglioProcesso> findAll(int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<DettaglioProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<DettaglioProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_DETTAGLIOPROCESSO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DETTAGLIOPROCESSO;

				if (pagination) {
					sql = sql.concat(DettaglioProcessoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<DettaglioProcesso>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the dettaglio processos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (DettaglioProcesso dettaglioProcesso : findAll()) {
			remove(dettaglioProcesso);
		}
	}

	/**
	 * Returns the number of dettaglio processos.
	 *
	 * @return the number of dettaglio processos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DETTAGLIOPROCESSO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return DettaglioProcessoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the dettaglio processo persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(DettaglioProcessoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_DETTAGLIOPROCESSO = "SELECT dettaglioProcesso FROM DettaglioProcesso dettaglioProcesso";
	private static final String _SQL_SELECT_DETTAGLIOPROCESSO_WHERE_PKS_IN = "SELECT dettaglioProcesso FROM DettaglioProcesso dettaglioProcesso WHERE dettaglioProcessoId IN (";
	private static final String _SQL_SELECT_DETTAGLIOPROCESSO_WHERE = "SELECT dettaglioProcesso FROM DettaglioProcesso dettaglioProcesso WHERE ";
	private static final String _SQL_COUNT_DETTAGLIOPROCESSO = "SELECT COUNT(dettaglioProcesso) FROM DettaglioProcesso dettaglioProcesso";
	private static final String _SQL_COUNT_DETTAGLIOPROCESSO_WHERE = "SELECT COUNT(dettaglioProcesso) FROM DettaglioProcesso dettaglioProcesso WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "dettaglioProcesso.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No DettaglioProcesso exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No DettaglioProcesso exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(DettaglioProcessoPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}