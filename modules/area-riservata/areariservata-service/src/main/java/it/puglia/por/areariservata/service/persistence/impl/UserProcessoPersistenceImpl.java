/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.exception.NoSuchUserProcessoException;
import it.puglia.por.areariservata.model.UserProcesso;
import it.puglia.por.areariservata.model.impl.UserProcessoImpl;
import it.puglia.por.areariservata.model.impl.UserProcessoModelImpl;
import it.puglia.por.areariservata.service.persistence.UserProcessoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the user processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.UserProcessoUtil
 * @generated
 */
@ProviderType
public class UserProcessoPersistenceImpl extends BasePersistenceImpl<UserProcesso>
	implements UserProcessoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link UserProcessoUtil} to access the user processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = UserProcessoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			UserProcessoModelImpl.UUID_COLUMN_BITMASK |
			UserProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the user processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @return the range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<UserProcesso> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid(String uuid, int start, int end,
		OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<UserProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<UserProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserProcesso userProcesso : list) {
					if (!Objects.equals(uuid, userProcesso.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByUuid_First(String uuid,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByUuid_First(uuid, orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the first user processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUuid_First(String uuid,
		OrderByComparator<UserProcesso> orderByComparator) {
		List<UserProcesso> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByUuid_Last(String uuid,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByUuid_Last(uuid, orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the last user processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<UserProcesso> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<UserProcesso> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user processos before and after the current user processo in the ordered set where uuid = &#63;.
	 *
	 * @param userProcessoId the primary key of the current user processo
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso[] findByUuid_PrevAndNext(long userProcessoId,
		String uuid, OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = findByPrimaryKey(userProcessoId);

		Session session = null;

		try {
			session = openSession();

			UserProcesso[] array = new UserProcessoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, userProcesso, uuid,
					orderByComparator, true);

			array[1] = userProcesso;

			array[2] = getByUuid_PrevAndNext(session, userProcesso, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserProcesso getByUuid_PrevAndNext(Session session,
		UserProcesso userProcesso, String uuid,
		OrderByComparator<UserProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user processos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (UserProcesso userProcesso : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(userProcesso);
		}
	}

	/**
	 * Returns the number of user processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching user processos
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "userProcesso.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "userProcesso.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(userProcesso.uuid IS NULL OR userProcesso.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			UserProcessoModelImpl.UUID_COLUMN_BITMASK |
			UserProcessoModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the user processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchUserProcessoException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByUUID_G(uuid, groupId);

		if (userProcesso == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchUserProcessoException(msg.toString());
		}

		return userProcesso;
	}

	/**
	 * Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof UserProcesso) {
			UserProcesso userProcesso = (UserProcesso)result;

			if (!Objects.equals(uuid, userProcesso.getUuid()) ||
					(groupId != userProcesso.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<UserProcesso> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					UserProcesso userProcesso = list.get(0);

					result = userProcesso;

					cacheResult(userProcesso);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (UserProcesso)result;
		}
	}

	/**
	 * Removes the user processo where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the user processo that was removed
	 */
	@Override
	public UserProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = findByUUID_G(uuid, groupId);

		return remove(userProcesso);
	}

	/**
	 * Returns the number of user processos where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching user processos
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "userProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "userProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(userProcesso.uuid IS NULL OR userProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "userProcesso.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			UserProcessoModelImpl.UUID_COLUMN_BITMASK |
			UserProcessoModelImpl.COMPANYID_COLUMN_BITMASK |
			UserProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the user processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @return the range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<UserProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<UserProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserProcesso userProcesso : list) {
					if (!Objects.equals(uuid, userProcesso.getUuid()) ||
							(companyId != userProcesso.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator) {
		List<UserProcesso> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<UserProcesso> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user processos before and after the current user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param userProcessoId the primary key of the current user processo
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso[] findByUuid_C_PrevAndNext(long userProcessoId,
		String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = findByPrimaryKey(userProcessoId);

		Session session = null;

		try {
			session = openSession();

			UserProcesso[] array = new UserProcessoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, userProcesso, uuid,
					companyId, orderByComparator, true);

			array[1] = userProcesso;

			array[2] = getByUuid_C_PrevAndNext(session, userProcesso, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserProcesso getByUuid_C_PrevAndNext(Session session,
		UserProcesso userProcesso, String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_USERPROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user processos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (UserProcesso userProcesso : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userProcesso);
		}
	}

	/**
	 * Returns the number of user processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching user processos
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_USERPROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "userProcesso.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "userProcesso.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(userProcesso.uuid IS NULL OR userProcesso.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "userProcesso.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDPROCESSO =
		new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByuserIdProcesso",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO =
		new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByuserIdProcesso",
			new String[] { Long.class.getName() },
			UserProcessoModelImpl.USERIDPROCESSO_COLUMN_BITMASK |
			UserProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERIDPROCESSO = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByuserIdProcesso",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user processos where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @return the matching user processos
	 */
	@Override
	public List<UserProcesso> findByuserIdProcesso(long userIdProcesso) {
		return findByuserIdProcesso(userIdProcesso, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user processos where userIdProcesso = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdProcesso the user ID processo
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @return the range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end) {
		return findByuserIdProcesso(userIdProcesso, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdProcesso the user ID processo
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator) {
		return findByuserIdProcesso(userIdProcesso, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userIdProcesso the user ID processo
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO;
			finderArgs = new Object[] { userIdProcesso };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERIDPROCESSO;
			finderArgs = new Object[] {
					userIdProcesso,
					
					start, end, orderByComparator
				};
		}

		List<UserProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<UserProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserProcesso userProcesso : list) {
					if ((userIdProcesso != userProcesso.getUserIdProcesso())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_USERIDPROCESSO_USERIDPROCESSO_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userIdProcesso);

				if (!pagination) {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByuserIdProcesso_First(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByuserIdProcesso_First(userIdProcesso,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userIdProcesso=");
		msg.append(userIdProcesso);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByuserIdProcesso_First(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator) {
		List<UserProcesso> list = findByuserIdProcesso(userIdProcesso, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByuserIdProcesso_Last(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByuserIdProcesso_Last(userIdProcesso,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userIdProcesso=");
		msg.append(userIdProcesso);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByuserIdProcesso_Last(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator) {
		int count = countByuserIdProcesso(userIdProcesso);

		if (count == 0) {
			return null;
		}

		List<UserProcesso> list = findByuserIdProcesso(userIdProcesso,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user processos before and after the current user processo in the ordered set where userIdProcesso = &#63;.
	 *
	 * @param userProcessoId the primary key of the current user processo
	 * @param userIdProcesso the user ID processo
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso[] findByuserIdProcesso_PrevAndNext(
		long userProcessoId, long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = findByPrimaryKey(userProcessoId);

		Session session = null;

		try {
			session = openSession();

			UserProcesso[] array = new UserProcessoImpl[3];

			array[0] = getByuserIdProcesso_PrevAndNext(session, userProcesso,
					userIdProcesso, orderByComparator, true);

			array[1] = userProcesso;

			array[2] = getByuserIdProcesso_PrevAndNext(session, userProcesso,
					userIdProcesso, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserProcesso getByuserIdProcesso_PrevAndNext(Session session,
		UserProcesso userProcesso, long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_USERIDPROCESSO_USERIDPROCESSO_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userIdProcesso);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user processos where userIdProcesso = &#63; from the database.
	 *
	 * @param userIdProcesso the user ID processo
	 */
	@Override
	public void removeByuserIdProcesso(long userIdProcesso) {
		for (UserProcesso userProcesso : findByuserIdProcesso(userIdProcesso,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userProcesso);
		}
	}

	/**
	 * Returns the number of user processos where userIdProcesso = &#63;.
	 *
	 * @param userIdProcesso the user ID processo
	 * @return the number of matching user processos
	 */
	@Override
	public int countByuserIdProcesso(long userIdProcesso) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERIDPROCESSO;

		Object[] finderArgs = new Object[] { userIdProcesso };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_USERIDPROCESSO_USERIDPROCESSO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userIdProcesso);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERIDPROCESSO_USERIDPROCESSO_2 = "userProcesso.userIdProcesso = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOID =
		new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByprocessoId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID =
		new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, UserProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByprocessoId",
			new String[] { Long.class.getName() },
			UserProcessoModelImpl.PROCESSOID_COLUMN_BITMASK |
			UserProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROCESSOID = new FinderPath(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByprocessoId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the user processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the matching user processos
	 */
	@Override
	public List<UserProcesso> findByprocessoId(long processoId) {
		return findByprocessoId(processoId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @return the range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByprocessoId(long processoId, int start,
		int end) {
		return findByprocessoId(processoId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the user processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByprocessoId(long processoId, int start,
		int end, OrderByComparator<UserProcesso> orderByComparator) {
		return findByprocessoId(processoId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user processos where processoId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param processoId the processo ID
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching user processos
	 */
	@Override
	public List<UserProcesso> findByprocessoId(long processoId, int start,
		int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID;
			finderArgs = new Object[] { processoId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROCESSOID;
			finderArgs = new Object[] { processoId, start, end, orderByComparator };
		}

		List<UserProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<UserProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (UserProcesso userProcesso : list) {
					if ((processoId != userProcesso.getProcessoId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_USERPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOID_PROCESSOID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId);

				if (!pagination) {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first user processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByprocessoId_First(long processoId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByprocessoId_First(processoId,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the first user processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByprocessoId_First(long processoId,
		OrderByComparator<UserProcesso> orderByComparator) {
		List<UserProcesso> list = findByprocessoId(processoId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last user processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo
	 * @throws NoSuchUserProcessoException if a matching user processo could not be found
	 */
	@Override
	public UserProcesso findByprocessoId_Last(long processoId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByprocessoId_Last(processoId,
				orderByComparator);

		if (userProcesso != null) {
			return userProcesso;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("processoId=");
		msg.append(processoId);

		msg.append("}");

		throw new NoSuchUserProcessoException(msg.toString());
	}

	/**
	 * Returns the last user processo in the ordered set where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	 */
	@Override
	public UserProcesso fetchByprocessoId_Last(long processoId,
		OrderByComparator<UserProcesso> orderByComparator) {
		int count = countByprocessoId(processoId);

		if (count == 0) {
			return null;
		}

		List<UserProcesso> list = findByprocessoId(processoId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the user processos before and after the current user processo in the ordered set where processoId = &#63;.
	 *
	 * @param userProcessoId the primary key of the current user processo
	 * @param processoId the processo ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso[] findByprocessoId_PrevAndNext(long userProcessoId,
		long processoId, OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = findByPrimaryKey(userProcessoId);

		Session session = null;

		try {
			session = openSession();

			UserProcesso[] array = new UserProcessoImpl[3];

			array[0] = getByprocessoId_PrevAndNext(session, userProcesso,
					processoId, orderByComparator, true);

			array[1] = userProcesso;

			array[2] = getByprocessoId_PrevAndNext(session, userProcesso,
					processoId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected UserProcesso getByprocessoId_PrevAndNext(Session session,
		UserProcesso userProcesso, long processoId,
		OrderByComparator<UserProcesso> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_USERPROCESSO_WHERE);

		query.append(_FINDER_COLUMN_PROCESSOID_PROCESSOID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(UserProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(processoId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(userProcesso);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<UserProcesso> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the user processos where processoId = &#63; from the database.
	 *
	 * @param processoId the processo ID
	 */
	@Override
	public void removeByprocessoId(long processoId) {
		for (UserProcesso userProcesso : findByprocessoId(processoId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(userProcesso);
		}
	}

	/**
	 * Returns the number of user processos where processoId = &#63;.
	 *
	 * @param processoId the processo ID
	 * @return the number of matching user processos
	 */
	@Override
	public int countByprocessoId(long processoId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROCESSOID;

		Object[] finderArgs = new Object[] { processoId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_USERPROCESSO_WHERE);

			query.append(_FINDER_COLUMN_PROCESSOID_PROCESSOID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(processoId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROCESSOID_PROCESSOID_2 = "userProcesso.processoId = ?";

	public UserProcessoPersistenceImpl() {
		setModelClass(UserProcesso.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the user processo in the entity cache if it is enabled.
	 *
	 * @param userProcesso the user processo
	 */
	@Override
	public void cacheResult(UserProcesso userProcesso) {
		entityCache.putResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoImpl.class, userProcesso.getPrimaryKey(), userProcesso);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { userProcesso.getUuid(), userProcesso.getGroupId() },
			userProcesso);

		userProcesso.resetOriginalValues();
	}

	/**
	 * Caches the user processos in the entity cache if it is enabled.
	 *
	 * @param userProcessos the user processos
	 */
	@Override
	public void cacheResult(List<UserProcesso> userProcessos) {
		for (UserProcesso userProcesso : userProcessos) {
			if (entityCache.getResult(
						UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
						UserProcessoImpl.class, userProcesso.getPrimaryKey()) == null) {
				cacheResult(userProcesso);
			}
			else {
				userProcesso.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all user processos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(UserProcessoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the user processo.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(UserProcesso userProcesso) {
		entityCache.removeResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoImpl.class, userProcesso.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((UserProcessoModelImpl)userProcesso, true);
	}

	@Override
	public void clearCache(List<UserProcesso> userProcessos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (UserProcesso userProcesso : userProcessos) {
			entityCache.removeResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
				UserProcessoImpl.class, userProcesso.getPrimaryKey());

			clearUniqueFindersCache((UserProcessoModelImpl)userProcesso, true);
		}
	}

	protected void cacheUniqueFindersCache(
		UserProcessoModelImpl userProcessoModelImpl) {
		Object[] args = new Object[] {
				userProcessoModelImpl.getUuid(),
				userProcessoModelImpl.getGroupId()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
			userProcessoModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		UserProcessoModelImpl userProcessoModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					userProcessoModelImpl.getUuid(),
					userProcessoModelImpl.getGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		if ((userProcessoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					userProcessoModelImpl.getOriginalUuid(),
					userProcessoModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new user processo with the primary key. Does not add the user processo to the database.
	 *
	 * @param userProcessoId the primary key for the new user processo
	 * @return the new user processo
	 */
	@Override
	public UserProcesso create(long userProcessoId) {
		UserProcesso userProcesso = new UserProcessoImpl();

		userProcesso.setNew(true);
		userProcesso.setPrimaryKey(userProcessoId);

		String uuid = PortalUUIDUtil.generate();

		userProcesso.setUuid(uuid);

		userProcesso.setCompanyId(companyProvider.getCompanyId());

		return userProcesso;
	}

	/**
	 * Removes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userProcessoId the primary key of the user processo
	 * @return the user processo that was removed
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso remove(long userProcessoId)
		throws NoSuchUserProcessoException {
		return remove((Serializable)userProcessoId);
	}

	/**
	 * Removes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the user processo
	 * @return the user processo that was removed
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso remove(Serializable primaryKey)
		throws NoSuchUserProcessoException {
		Session session = null;

		try {
			session = openSession();

			UserProcesso userProcesso = (UserProcesso)session.get(UserProcessoImpl.class,
					primaryKey);

			if (userProcesso == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchUserProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(userProcesso);
		}
		catch (NoSuchUserProcessoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected UserProcesso removeImpl(UserProcesso userProcesso) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(userProcesso)) {
				userProcesso = (UserProcesso)session.get(UserProcessoImpl.class,
						userProcesso.getPrimaryKeyObj());
			}

			if (userProcesso != null) {
				session.delete(userProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (userProcesso != null) {
			clearCache(userProcesso);
		}

		return userProcesso;
	}

	@Override
	public UserProcesso updateImpl(UserProcesso userProcesso) {
		boolean isNew = userProcesso.isNew();

		if (!(userProcesso instanceof UserProcessoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(userProcesso.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(userProcesso);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in userProcesso proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom UserProcesso implementation " +
				userProcesso.getClass());
		}

		UserProcessoModelImpl userProcessoModelImpl = (UserProcessoModelImpl)userProcesso;

		if (Validator.isNull(userProcesso.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			userProcesso.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (userProcesso.getCreateDate() == null)) {
			if (serviceContext == null) {
				userProcesso.setCreateDate(now);
			}
			else {
				userProcesso.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!userProcessoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				userProcesso.setModifiedDate(now);
			}
			else {
				userProcesso.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (userProcesso.isNew()) {
				session.save(userProcesso);

				userProcesso.setNew(false);
			}
			else {
				userProcesso = (UserProcesso)session.merge(userProcesso);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!UserProcessoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { userProcessoModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					userProcessoModelImpl.getUuid(),
					userProcessoModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { userProcessoModelImpl.getUserIdProcesso() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDPROCESSO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO,
				args);

			args = new Object[] { userProcessoModelImpl.getProcessoId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((userProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userProcessoModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { userProcessoModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((userProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userProcessoModelImpl.getOriginalUuid(),
						userProcessoModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						userProcessoModelImpl.getUuid(),
						userProcessoModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((userProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userProcessoModelImpl.getOriginalUserIdProcesso()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDPROCESSO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO,
					args);

				args = new Object[] { userProcessoModelImpl.getUserIdProcesso() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_USERIDPROCESSO,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERIDPROCESSO,
					args);
			}

			if ((userProcessoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						userProcessoModelImpl.getOriginalProcessoId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID,
					args);

				args = new Object[] { userProcessoModelImpl.getProcessoId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROCESSOID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROCESSOID,
					args);
			}
		}

		entityCache.putResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
			UserProcessoImpl.class, userProcesso.getPrimaryKey(), userProcesso,
			false);

		clearUniqueFindersCache(userProcessoModelImpl, false);
		cacheUniqueFindersCache(userProcessoModelImpl);

		userProcesso.resetOriginalValues();

		return userProcesso;
	}

	/**
	 * Returns the user processo with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the user processo
	 * @return the user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso findByPrimaryKey(Serializable primaryKey)
		throws NoSuchUserProcessoException {
		UserProcesso userProcesso = fetchByPrimaryKey(primaryKey);

		if (userProcesso == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchUserProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return userProcesso;
	}

	/**
	 * Returns the user processo with the primary key or throws a {@link NoSuchUserProcessoException} if it could not be found.
	 *
	 * @param userProcessoId the primary key of the user processo
	 * @return the user processo
	 * @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso findByPrimaryKey(long userProcessoId)
		throws NoSuchUserProcessoException {
		return findByPrimaryKey((Serializable)userProcessoId);
	}

	/**
	 * Returns the user processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the user processo
	 * @return the user processo, or <code>null</code> if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
				UserProcessoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		UserProcesso userProcesso = (UserProcesso)serializable;

		if (userProcesso == null) {
			Session session = null;

			try {
				session = openSession();

				userProcesso = (UserProcesso)session.get(UserProcessoImpl.class,
						primaryKey);

				if (userProcesso != null) {
					cacheResult(userProcesso);
				}
				else {
					entityCache.putResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
						UserProcessoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
					UserProcessoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return userProcesso;
	}

	/**
	 * Returns the user processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userProcessoId the primary key of the user processo
	 * @return the user processo, or <code>null</code> if a user processo with the primary key could not be found
	 */
	@Override
	public UserProcesso fetchByPrimaryKey(long userProcessoId) {
		return fetchByPrimaryKey((Serializable)userProcessoId);
	}

	@Override
	public Map<Serializable, UserProcesso> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, UserProcesso> map = new HashMap<Serializable, UserProcesso>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			UserProcesso userProcesso = fetchByPrimaryKey(primaryKey);

			if (userProcesso != null) {
				map.put(primaryKey, userProcesso);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
					UserProcessoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (UserProcesso)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_USERPROCESSO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (UserProcesso userProcesso : (List<UserProcesso>)q.list()) {
				map.put(userProcesso.getPrimaryKeyObj(), userProcesso);

				cacheResult(userProcesso);

				uncachedPrimaryKeys.remove(userProcesso.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(UserProcessoModelImpl.ENTITY_CACHE_ENABLED,
					UserProcessoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the user processos.
	 *
	 * @return the user processos
	 */
	@Override
	public List<UserProcesso> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the user processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @return the range of user processos
	 */
	@Override
	public List<UserProcesso> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the user processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of user processos
	 */
	@Override
	public List<UserProcesso> findAll(int start, int end,
		OrderByComparator<UserProcesso> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the user processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of user processos
	 * @param end the upper bound of the range of user processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of user processos
	 */
	@Override
	public List<UserProcesso> findAll(int start, int end,
		OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<UserProcesso> list = null;

		if (retrieveFromCache) {
			list = (List<UserProcesso>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_USERPROCESSO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_USERPROCESSO;

				if (pagination) {
					sql = sql.concat(UserProcessoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<UserProcesso>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the user processos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (UserProcesso userProcesso : findAll()) {
			remove(userProcesso);
		}
	}

	/**
	 * Returns the number of user processos.
	 *
	 * @return the number of user processos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_USERPROCESSO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return UserProcessoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the user processo persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(UserProcessoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_USERPROCESSO = "SELECT userProcesso FROM UserProcesso userProcesso";
	private static final String _SQL_SELECT_USERPROCESSO_WHERE_PKS_IN = "SELECT userProcesso FROM UserProcesso userProcesso WHERE userProcessoId IN (";
	private static final String _SQL_SELECT_USERPROCESSO_WHERE = "SELECT userProcesso FROM UserProcesso userProcesso WHERE ";
	private static final String _SQL_COUNT_USERPROCESSO = "SELECT COUNT(userProcesso) FROM UserProcesso userProcesso";
	private static final String _SQL_COUNT_USERPROCESSO_WHERE = "SELECT COUNT(userProcesso) FROM UserProcesso userProcesso WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "userProcesso.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No UserProcesso exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No UserProcesso exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(UserProcessoPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}