/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.base;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.InfrastructureUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.service.AreaRiservataUsersLocalService;
import it.puglia.por.areariservata.service.persistence.AuditEventPersistence;
import it.puglia.por.areariservata.service.persistence.DettaglioProcessoPersistence;
import it.puglia.por.areariservata.service.persistence.DocumentoProcessoFinder;
import it.puglia.por.areariservata.service.persistence.DocumentoProcessoPersistence;
import it.puglia.por.areariservata.service.persistence.ProcessoPersistence;
import it.puglia.por.areariservata.service.persistence.UserProcessoPersistence;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the area riservata users local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link it.puglia.por.areariservata.service.impl.AreaRiservataUsersLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.impl.AreaRiservataUsersLocalServiceImpl
 * @see it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil
 * @generated
 */
@ProviderType
public abstract class AreaRiservataUsersLocalServiceBaseImpl
	extends BaseLocalServiceImpl implements AreaRiservataUsersLocalService,
		IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil} to access the area riservata users local service.
	 */

	/**
	 * Returns the area riservata users local service.
	 *
	 * @return the area riservata users local service
	 */
	public AreaRiservataUsersLocalService getAreaRiservataUsersLocalService() {
		return areaRiservataUsersLocalService;
	}

	/**
	 * Sets the area riservata users local service.
	 *
	 * @param areaRiservataUsersLocalService the area riservata users local service
	 */
	public void setAreaRiservataUsersLocalService(
		AreaRiservataUsersLocalService areaRiservataUsersLocalService) {
		this.areaRiservataUsersLocalService = areaRiservataUsersLocalService;
	}

	/**
	 * Returns the audit event local service.
	 *
	 * @return the audit event local service
	 */
	public it.puglia.por.areariservata.service.AuditEventLocalService getAuditEventLocalService() {
		return auditEventLocalService;
	}

	/**
	 * Sets the audit event local service.
	 *
	 * @param auditEventLocalService the audit event local service
	 */
	public void setAuditEventLocalService(
		it.puglia.por.areariservata.service.AuditEventLocalService auditEventLocalService) {
		this.auditEventLocalService = auditEventLocalService;
	}

	/**
	 * Returns the audit event persistence.
	 *
	 * @return the audit event persistence
	 */
	public AuditEventPersistence getAuditEventPersistence() {
		return auditEventPersistence;
	}

	/**
	 * Sets the audit event persistence.
	 *
	 * @param auditEventPersistence the audit event persistence
	 */
	public void setAuditEventPersistence(
		AuditEventPersistence auditEventPersistence) {
		this.auditEventPersistence = auditEventPersistence;
	}

	/**
	 * Returns the dettaglio processo local service.
	 *
	 * @return the dettaglio processo local service
	 */
	public it.puglia.por.areariservata.service.DettaglioProcessoLocalService getDettaglioProcessoLocalService() {
		return dettaglioProcessoLocalService;
	}

	/**
	 * Sets the dettaglio processo local service.
	 *
	 * @param dettaglioProcessoLocalService the dettaglio processo local service
	 */
	public void setDettaglioProcessoLocalService(
		it.puglia.por.areariservata.service.DettaglioProcessoLocalService dettaglioProcessoLocalService) {
		this.dettaglioProcessoLocalService = dettaglioProcessoLocalService;
	}

	/**
	 * Returns the dettaglio processo persistence.
	 *
	 * @return the dettaglio processo persistence
	 */
	public DettaglioProcessoPersistence getDettaglioProcessoPersistence() {
		return dettaglioProcessoPersistence;
	}

	/**
	 * Sets the dettaglio processo persistence.
	 *
	 * @param dettaglioProcessoPersistence the dettaglio processo persistence
	 */
	public void setDettaglioProcessoPersistence(
		DettaglioProcessoPersistence dettaglioProcessoPersistence) {
		this.dettaglioProcessoPersistence = dettaglioProcessoPersistence;
	}

	/**
	 * Returns the documento processo local service.
	 *
	 * @return the documento processo local service
	 */
	public it.puglia.por.areariservata.service.DocumentoProcessoLocalService getDocumentoProcessoLocalService() {
		return documentoProcessoLocalService;
	}

	/**
	 * Sets the documento processo local service.
	 *
	 * @param documentoProcessoLocalService the documento processo local service
	 */
	public void setDocumentoProcessoLocalService(
		it.puglia.por.areariservata.service.DocumentoProcessoLocalService documentoProcessoLocalService) {
		this.documentoProcessoLocalService = documentoProcessoLocalService;
	}

	/**
	 * Returns the documento processo persistence.
	 *
	 * @return the documento processo persistence
	 */
	public DocumentoProcessoPersistence getDocumentoProcessoPersistence() {
		return documentoProcessoPersistence;
	}

	/**
	 * Sets the documento processo persistence.
	 *
	 * @param documentoProcessoPersistence the documento processo persistence
	 */
	public void setDocumentoProcessoPersistence(
		DocumentoProcessoPersistence documentoProcessoPersistence) {
		this.documentoProcessoPersistence = documentoProcessoPersistence;
	}

	/**
	 * Returns the documento processo finder.
	 *
	 * @return the documento processo finder
	 */
	public DocumentoProcessoFinder getDocumentoProcessoFinder() {
		return documentoProcessoFinder;
	}

	/**
	 * Sets the documento processo finder.
	 *
	 * @param documentoProcessoFinder the documento processo finder
	 */
	public void setDocumentoProcessoFinder(
		DocumentoProcessoFinder documentoProcessoFinder) {
		this.documentoProcessoFinder = documentoProcessoFinder;
	}

	/**
	 * Returns the processo local service.
	 *
	 * @return the processo local service
	 */
	public it.puglia.por.areariservata.service.ProcessoLocalService getProcessoLocalService() {
		return processoLocalService;
	}

	/**
	 * Sets the processo local service.
	 *
	 * @param processoLocalService the processo local service
	 */
	public void setProcessoLocalService(
		it.puglia.por.areariservata.service.ProcessoLocalService processoLocalService) {
		this.processoLocalService = processoLocalService;
	}

	/**
	 * Returns the processo persistence.
	 *
	 * @return the processo persistence
	 */
	public ProcessoPersistence getProcessoPersistence() {
		return processoPersistence;
	}

	/**
	 * Sets the processo persistence.
	 *
	 * @param processoPersistence the processo persistence
	 */
	public void setProcessoPersistence(ProcessoPersistence processoPersistence) {
		this.processoPersistence = processoPersistence;
	}

	/**
	 * Returns the user processo local service.
	 *
	 * @return the user processo local service
	 */
	public it.puglia.por.areariservata.service.UserProcessoLocalService getUserProcessoLocalService() {
		return userProcessoLocalService;
	}

	/**
	 * Sets the user processo local service.
	 *
	 * @param userProcessoLocalService the user processo local service
	 */
	public void setUserProcessoLocalService(
		it.puglia.por.areariservata.service.UserProcessoLocalService userProcessoLocalService) {
		this.userProcessoLocalService = userProcessoLocalService;
	}

	/**
	 * Returns the user processo persistence.
	 *
	 * @return the user processo persistence
	 */
	public UserProcessoPersistence getUserProcessoPersistence() {
		return userProcessoPersistence;
	}

	/**
	 * Sets the user processo persistence.
	 *
	 * @param userProcessoPersistence the user processo persistence
	 */
	public void setUserProcessoPersistence(
		UserProcessoPersistence userProcessoPersistence) {
		this.userProcessoPersistence = userProcessoPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
	}

	public void destroy() {
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return AreaRiservataUsersLocalService.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = InfrastructureUtil.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = AreaRiservataUsersLocalService.class)
	protected AreaRiservataUsersLocalService areaRiservataUsersLocalService;
	@BeanReference(type = it.puglia.por.areariservata.service.AuditEventLocalService.class)
	protected it.puglia.por.areariservata.service.AuditEventLocalService auditEventLocalService;
	@BeanReference(type = AuditEventPersistence.class)
	protected AuditEventPersistence auditEventPersistence;
	@BeanReference(type = it.puglia.por.areariservata.service.DettaglioProcessoLocalService.class)
	protected it.puglia.por.areariservata.service.DettaglioProcessoLocalService dettaglioProcessoLocalService;
	@BeanReference(type = DettaglioProcessoPersistence.class)
	protected DettaglioProcessoPersistence dettaglioProcessoPersistence;
	@BeanReference(type = it.puglia.por.areariservata.service.DocumentoProcessoLocalService.class)
	protected it.puglia.por.areariservata.service.DocumentoProcessoLocalService documentoProcessoLocalService;
	@BeanReference(type = DocumentoProcessoPersistence.class)
	protected DocumentoProcessoPersistence documentoProcessoPersistence;
	@BeanReference(type = DocumentoProcessoFinder.class)
	protected DocumentoProcessoFinder documentoProcessoFinder;
	@BeanReference(type = it.puglia.por.areariservata.service.ProcessoLocalService.class)
	protected it.puglia.por.areariservata.service.ProcessoLocalService processoLocalService;
	@BeanReference(type = ProcessoPersistence.class)
	protected ProcessoPersistence processoPersistence;
	@BeanReference(type = it.puglia.por.areariservata.service.UserProcessoLocalService.class)
	protected it.puglia.por.areariservata.service.UserProcessoLocalService userProcessoLocalService;
	@BeanReference(type = UserProcessoPersistence.class)
	protected UserProcessoPersistence userProcessoPersistence;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
}