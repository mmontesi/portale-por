/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import it.puglia.por.areariservata.exception.NoSuchProcessoException;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.model.impl.ProcessoImpl;
import it.puglia.por.areariservata.model.impl.ProcessoModelImpl;
import it.puglia.por.areariservata.service.persistence.ProcessoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.ProcessoUtil
 * @generated
 */
@ProviderType
public class ProcessoPersistenceImpl extends BasePersistenceImpl<Processo>
	implements ProcessoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProcessoUtil} to access the processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProcessoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProcessoModelImpl.UUID_COLUMN_BITMASK |
			ProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching processos
	 */
	@Override
	public List<Processo> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @return the range of matching processos
	 */
	@Override
	public List<Processo> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByUuid(String uuid, int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the processos where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByUuid(String uuid, int start, int end,
		OrderByComparator<Processo> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Processo> list = null;

		if (retrieveFromCache) {
			list = (List<Processo>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Processo processo : list) {
					if (!Objects.equals(uuid, processo.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByUuid_First(String uuid,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByUuid_First(uuid, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the first processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUuid_First(String uuid,
		OrderByComparator<Processo> orderByComparator) {
		List<Processo> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByUuid_Last(String uuid,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByUuid_Last(uuid, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the last processo in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUuid_Last(String uuid,
		OrderByComparator<Processo> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Processo> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the processos before and after the current processo in the ordered set where uuid = &#63;.
	 *
	 * @param processoId the primary key of the current processo
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo[] findByUuid_PrevAndNext(long processoId, String uuid,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = findByPrimaryKey(processoId);

		Session session = null;

		try {
			session = openSession();

			Processo[] array = new ProcessoImpl[3];

			array[0] = getByUuid_PrevAndNext(session, processo, uuid,
					orderByComparator, true);

			array[1] = processo;

			array[2] = getByUuid_PrevAndNext(session, processo, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Processo getByUuid_PrevAndNext(Session session,
		Processo processo, String uuid,
		OrderByComparator<Processo> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(processo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Processo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the processos where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Processo processo : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(processo);
		}
	}

	/**
	 * Returns the number of processos where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching processos
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "processo.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "processo.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(processo.uuid IS NULL OR processo.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			ProcessoModelImpl.UUID_COLUMN_BITMASK |
			ProcessoModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchProcessoException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByUUID_G(String uuid, long groupId)
		throws NoSuchProcessoException {
		Processo processo = fetchByUUID_G(uuid, groupId);

		if (processo == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProcessoException(msg.toString());
		}

		return processo;
	}

	/**
	 * Returns the processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof Processo) {
			Processo processo = (Processo)result;

			if (!Objects.equals(uuid, processo.getUuid()) ||
					(groupId != processo.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<Processo> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					Processo processo = list.get(0);

					result = processo;

					cacheResult(processo);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Processo)result;
		}
	}

	/**
	 * Removes the processo where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the processo that was removed
	 */
	@Override
	public Processo removeByUUID_G(String uuid, long groupId)
		throws NoSuchProcessoException {
		Processo processo = findByUUID_G(uuid, groupId);

		return remove(processo);
	}

	/**
	 * Returns the number of processos where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching processos
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "processo.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "processo.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(processo.uuid IS NULL OR processo.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "processo.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProcessoModelImpl.UUID_COLUMN_BITMASK |
			ProcessoModelImpl.COMPANYID_COLUMN_BITMASK |
			ProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching processos
	 */
	@Override
	public List<Processo> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @return the range of matching processos
	 */
	@Override
	public List<Processo> findByUuid_C(String uuid, long companyId, int start,
		int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Processo> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the processos where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator<Processo> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Processo> list = null;

		if (retrieveFromCache) {
			list = (List<Processo>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Processo processo : list) {
					if (!Objects.equals(uuid, processo.getUuid()) ||
							(companyId != processo.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the first processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator) {
		List<Processo> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the last processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Processo> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the processos before and after the current processo in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param processoId the primary key of the current processo
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo[] findByUuid_C_PrevAndNext(long processoId, String uuid,
		long companyId, OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = findByPrimaryKey(processoId);

		Session session = null;

		try {
			session = openSession();

			Processo[] array = new ProcessoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, processo, uuid,
					companyId, orderByComparator, true);

			array[1] = processo;

			array[2] = getByUuid_C_PrevAndNext(session, processo, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Processo getByUuid_C_PrevAndNext(Session session,
		Processo processo, String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROCESSO_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals("")) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(processo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Processo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the processos where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Processo processo : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(processo);
		}
	}

	/**
	 * Returns the number of processos where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching processos
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROCESSO_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals("")) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "processo.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "processo.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(processo.uuid IS NULL OR processo.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "processo.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STATO = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBystato",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBystato",
			new String[] { String.class.getName() },
			ProcessoModelImpl.STATOWORKFLOW_COLUMN_BITMASK |
			ProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STATO = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBystato",
			new String[] { String.class.getName() });

	/**
	 * Returns all the processos where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @return the matching processos
	 */
	@Override
	public List<Processo> findBystato(String statoWorkflow) {
		return findBystato(statoWorkflow, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the processos where statoWorkflow = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param statoWorkflow the stato workflow
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @return the range of matching processos
	 */
	@Override
	public List<Processo> findBystato(String statoWorkflow, int start, int end) {
		return findBystato(statoWorkflow, start, end, null);
	}

	/**
	 * Returns an ordered range of all the processos where statoWorkflow = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param statoWorkflow the stato workflow
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findBystato(String statoWorkflow, int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return findBystato(statoWorkflow, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the processos where statoWorkflow = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param statoWorkflow the stato workflow
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findBystato(String statoWorkflow, int start, int end,
		OrderByComparator<Processo> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO;
			finderArgs = new Object[] { statoWorkflow };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STATO;
			finderArgs = new Object[] {
					statoWorkflow,
					
					start, end, orderByComparator
				};
		}

		List<Processo> list = null;

		if (retrieveFromCache) {
			list = (List<Processo>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Processo processo : list) {
					if (!Objects.equals(statoWorkflow,
								processo.getStatoWorkflow())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROCESSO_WHERE);

			boolean bindStatoWorkflow = false;

			if (statoWorkflow == null) {
				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_1);
			}
			else if (statoWorkflow.equals("")) {
				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_3);
			}
			else {
				bindStatoWorkflow = true;

				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatoWorkflow) {
					qPos.add(statoWorkflow);
				}

				if (!pagination) {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first processo in the ordered set where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findBystato_First(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchBystato_First(statoWorkflow, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("statoWorkflow=");
		msg.append(statoWorkflow);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the first processo in the ordered set where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchBystato_First(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator) {
		List<Processo> list = findBystato(statoWorkflow, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last processo in the ordered set where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findBystato_Last(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchBystato_Last(statoWorkflow, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("statoWorkflow=");
		msg.append(statoWorkflow);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the last processo in the ordered set where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchBystato_Last(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator) {
		int count = countBystato(statoWorkflow);

		if (count == 0) {
			return null;
		}

		List<Processo> list = findBystato(statoWorkflow, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the processos before and after the current processo in the ordered set where statoWorkflow = &#63;.
	 *
	 * @param processoId the primary key of the current processo
	 * @param statoWorkflow the stato workflow
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo[] findBystato_PrevAndNext(long processoId,
		String statoWorkflow, OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = findByPrimaryKey(processoId);

		Session session = null;

		try {
			session = openSession();

			Processo[] array = new ProcessoImpl[3];

			array[0] = getBystato_PrevAndNext(session, processo, statoWorkflow,
					orderByComparator, true);

			array[1] = processo;

			array[2] = getBystato_PrevAndNext(session, processo, statoWorkflow,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Processo getBystato_PrevAndNext(Session session,
		Processo processo, String statoWorkflow,
		OrderByComparator<Processo> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROCESSO_WHERE);

		boolean bindStatoWorkflow = false;

		if (statoWorkflow == null) {
			query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_1);
		}
		else if (statoWorkflow.equals("")) {
			query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_3);
		}
		else {
			bindStatoWorkflow = true;

			query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindStatoWorkflow) {
			qPos.add(statoWorkflow);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(processo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Processo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the processos where statoWorkflow = &#63; from the database.
	 *
	 * @param statoWorkflow the stato workflow
	 */
	@Override
	public void removeBystato(String statoWorkflow) {
		for (Processo processo : findBystato(statoWorkflow, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(processo);
		}
	}

	/**
	 * Returns the number of processos where statoWorkflow = &#63;.
	 *
	 * @param statoWorkflow the stato workflow
	 * @return the number of matching processos
	 */
	@Override
	public int countBystato(String statoWorkflow) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STATO;

		Object[] finderArgs = new Object[] { statoWorkflow };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROCESSO_WHERE);

			boolean bindStatoWorkflow = false;

			if (statoWorkflow == null) {
				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_1);
			}
			else if (statoWorkflow.equals("")) {
				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_3);
			}
			else {
				bindStatoWorkflow = true;

				query.append(_FINDER_COLUMN_STATO_STATOWORKFLOW_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindStatoWorkflow) {
					qPos.add(statoWorkflow);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STATO_STATOWORKFLOW_1 = "processo.statoWorkflow IS NULL";
	private static final String _FINDER_COLUMN_STATO_STATOWORKFLOW_2 = "processo.statoWorkflow = ?";
	private static final String _FINDER_COLUMN_STATO_STATOWORKFLOW_3 = "(processo.statoWorkflow IS NULL OR processo.statoWorkflow = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ISVISIBLE =
		new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByisVisible",
			new String[] {
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE =
		new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, ProcessoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByisVisible",
			new String[] { Boolean.class.getName() },
			ProcessoModelImpl.ISVISIBLE_COLUMN_BITMASK |
			ProcessoModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ISVISIBLE = new FinderPath(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByisVisible",
			new String[] { Boolean.class.getName() });

	/**
	 * Returns all the processos where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @return the matching processos
	 */
	@Override
	public List<Processo> findByisVisible(boolean isVisible) {
		return findByisVisible(isVisible, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the processos where isVisible = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isVisible the is visible
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @return the range of matching processos
	 */
	@Override
	public List<Processo> findByisVisible(boolean isVisible, int start, int end) {
		return findByisVisible(isVisible, start, end, null);
	}

	/**
	 * Returns an ordered range of all the processos where isVisible = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isVisible the is visible
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByisVisible(boolean isVisible, int start,
		int end, OrderByComparator<Processo> orderByComparator) {
		return findByisVisible(isVisible, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the processos where isVisible = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isVisible the is visible
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching processos
	 */
	@Override
	public List<Processo> findByisVisible(boolean isVisible, int start,
		int end, OrderByComparator<Processo> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE;
			finderArgs = new Object[] { isVisible };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ISVISIBLE;
			finderArgs = new Object[] { isVisible, start, end, orderByComparator };
		}

		List<Processo> list = null;

		if (retrieveFromCache) {
			list = (List<Processo>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Processo processo : list) {
					if ((isVisible != processo.isIsVisible())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROCESSO_WHERE);

			query.append(_FINDER_COLUMN_ISVISIBLE_ISVISIBLE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProcessoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(isVisible);

				if (!pagination) {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first processo in the ordered set where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByisVisible_First(boolean isVisible,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByisVisible_First(isVisible, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isVisible=");
		msg.append(isVisible);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the first processo in the ordered set where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByisVisible_First(boolean isVisible,
		OrderByComparator<Processo> orderByComparator) {
		List<Processo> list = findByisVisible(isVisible, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last processo in the ordered set where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo
	 * @throws NoSuchProcessoException if a matching processo could not be found
	 */
	@Override
	public Processo findByisVisible_Last(boolean isVisible,
		OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = fetchByisVisible_Last(isVisible, orderByComparator);

		if (processo != null) {
			return processo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isVisible=");
		msg.append(isVisible);

		msg.append("}");

		throw new NoSuchProcessoException(msg.toString());
	}

	/**
	 * Returns the last processo in the ordered set where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching processo, or <code>null</code> if a matching processo could not be found
	 */
	@Override
	public Processo fetchByisVisible_Last(boolean isVisible,
		OrderByComparator<Processo> orderByComparator) {
		int count = countByisVisible(isVisible);

		if (count == 0) {
			return null;
		}

		List<Processo> list = findByisVisible(isVisible, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the processos before and after the current processo in the ordered set where isVisible = &#63;.
	 *
	 * @param processoId the primary key of the current processo
	 * @param isVisible the is visible
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo[] findByisVisible_PrevAndNext(long processoId,
		boolean isVisible, OrderByComparator<Processo> orderByComparator)
		throws NoSuchProcessoException {
		Processo processo = findByPrimaryKey(processoId);

		Session session = null;

		try {
			session = openSession();

			Processo[] array = new ProcessoImpl[3];

			array[0] = getByisVisible_PrevAndNext(session, processo, isVisible,
					orderByComparator, true);

			array[1] = processo;

			array[2] = getByisVisible_PrevAndNext(session, processo, isVisible,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Processo getByisVisible_PrevAndNext(Session session,
		Processo processo, boolean isVisible,
		OrderByComparator<Processo> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROCESSO_WHERE);

		query.append(_FINDER_COLUMN_ISVISIBLE_ISVISIBLE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProcessoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(isVisible);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(processo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Processo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the processos where isVisible = &#63; from the database.
	 *
	 * @param isVisible the is visible
	 */
	@Override
	public void removeByisVisible(boolean isVisible) {
		for (Processo processo : findByisVisible(isVisible, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(processo);
		}
	}

	/**
	 * Returns the number of processos where isVisible = &#63;.
	 *
	 * @param isVisible the is visible
	 * @return the number of matching processos
	 */
	@Override
	public int countByisVisible(boolean isVisible) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ISVISIBLE;

		Object[] finderArgs = new Object[] { isVisible };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROCESSO_WHERE);

			query.append(_FINDER_COLUMN_ISVISIBLE_ISVISIBLE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(isVisible);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ISVISIBLE_ISVISIBLE_2 = "processo.isVisible = ?";

	public ProcessoPersistenceImpl() {
		setModelClass(Processo.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("uuid", "uuid_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the processo in the entity cache if it is enabled.
	 *
	 * @param processo the processo
	 */
	@Override
	public void cacheResult(Processo processo) {
		entityCache.putResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoImpl.class, processo.getPrimaryKey(), processo);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { processo.getUuid(), processo.getGroupId() }, processo);

		processo.resetOriginalValues();
	}

	/**
	 * Caches the processos in the entity cache if it is enabled.
	 *
	 * @param processos the processos
	 */
	@Override
	public void cacheResult(List<Processo> processos) {
		for (Processo processo : processos) {
			if (entityCache.getResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
						ProcessoImpl.class, processo.getPrimaryKey()) == null) {
				cacheResult(processo);
			}
			else {
				processo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all processos.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProcessoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the processo.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Processo processo) {
		entityCache.removeResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoImpl.class, processo.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProcessoModelImpl)processo, true);
	}

	@Override
	public void clearCache(List<Processo> processos) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Processo processo : processos) {
			entityCache.removeResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
				ProcessoImpl.class, processo.getPrimaryKey());

			clearUniqueFindersCache((ProcessoModelImpl)processo, true);
		}
	}

	protected void cacheUniqueFindersCache(ProcessoModelImpl processoModelImpl) {
		Object[] args = new Object[] {
				processoModelImpl.getUuid(), processoModelImpl.getGroupId()
			};

		finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
			Long.valueOf(1), false);
		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
			processoModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		ProcessoModelImpl processoModelImpl, boolean clearCurrent) {
		if (clearCurrent) {
			Object[] args = new Object[] {
					processoModelImpl.getUuid(), processoModelImpl.getGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		if ((processoModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			Object[] args = new Object[] {
					processoModelImpl.getOriginalUuid(),
					processoModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new processo with the primary key. Does not add the processo to the database.
	 *
	 * @param processoId the primary key for the new processo
	 * @return the new processo
	 */
	@Override
	public Processo create(long processoId) {
		Processo processo = new ProcessoImpl();

		processo.setNew(true);
		processo.setPrimaryKey(processoId);

		String uuid = PortalUUIDUtil.generate();

		processo.setUuid(uuid);

		processo.setCompanyId(companyProvider.getCompanyId());

		return processo;
	}

	/**
	 * Removes the processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param processoId the primary key of the processo
	 * @return the processo that was removed
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo remove(long processoId) throws NoSuchProcessoException {
		return remove((Serializable)processoId);
	}

	/**
	 * Removes the processo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the processo
	 * @return the processo that was removed
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo remove(Serializable primaryKey)
		throws NoSuchProcessoException {
		Session session = null;

		try {
			session = openSession();

			Processo processo = (Processo)session.get(ProcessoImpl.class,
					primaryKey);

			if (processo == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(processo);
		}
		catch (NoSuchProcessoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Processo removeImpl(Processo processo) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(processo)) {
				processo = (Processo)session.get(ProcessoImpl.class,
						processo.getPrimaryKeyObj());
			}

			if (processo != null) {
				session.delete(processo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (processo != null) {
			clearCache(processo);
		}

		return processo;
	}

	@Override
	public Processo updateImpl(Processo processo) {
		boolean isNew = processo.isNew();

		if (!(processo instanceof ProcessoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(processo.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(processo);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in processo proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Processo implementation " +
				processo.getClass());
		}

		ProcessoModelImpl processoModelImpl = (ProcessoModelImpl)processo;

		if (Validator.isNull(processo.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			processo.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (processo.getCreateDate() == null)) {
			if (serviceContext == null) {
				processo.setCreateDate(now);
			}
			else {
				processo.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!processoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				processo.setModifiedDate(now);
			}
			else {
				processo.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (processo.isNew()) {
				session.save(processo);

				processo.setNew(false);
			}
			else {
				processo = (Processo)session.merge(processo);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ProcessoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { processoModelImpl.getUuid() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
				args);

			args = new Object[] {
					processoModelImpl.getUuid(),
					processoModelImpl.getCompanyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
				args);

			args = new Object[] { processoModelImpl.getStatoWorkflow() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_STATO, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO,
				args);

			args = new Object[] { processoModelImpl.isIsVisible() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_ISVISIBLE, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((processoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { processoModelImpl.getOriginalUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { processoModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((processoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						processoModelImpl.getOriginalUuid(),
						processoModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						processoModelImpl.getUuid(),
						processoModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((processoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						processoModelImpl.getOriginalStatoWorkflow()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STATO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO,
					args);

				args = new Object[] { processoModelImpl.getStatoWorkflow() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STATO, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STATO,
					args);
			}

			if ((processoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						processoModelImpl.getOriginalIsVisible()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ISVISIBLE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE,
					args);

				args = new Object[] { processoModelImpl.isIsVisible() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_ISVISIBLE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISVISIBLE,
					args);
			}
		}

		entityCache.putResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
			ProcessoImpl.class, processo.getPrimaryKey(), processo, false);

		clearUniqueFindersCache(processoModelImpl, false);
		cacheUniqueFindersCache(processoModelImpl);

		processo.resetOriginalValues();

		return processo;
	}

	/**
	 * Returns the processo with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the processo
	 * @return the processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProcessoException {
		Processo processo = fetchByPrimaryKey(primaryKey);

		if (processo == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProcessoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return processo;
	}

	/**
	 * Returns the processo with the primary key or throws a {@link NoSuchProcessoException} if it could not be found.
	 *
	 * @param processoId the primary key of the processo
	 * @return the processo
	 * @throws NoSuchProcessoException if a processo with the primary key could not be found
	 */
	@Override
	public Processo findByPrimaryKey(long processoId)
		throws NoSuchProcessoException {
		return findByPrimaryKey((Serializable)processoId);
	}

	/**
	 * Returns the processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the processo
	 * @return the processo, or <code>null</code> if a processo with the primary key could not be found
	 */
	@Override
	public Processo fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
				ProcessoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Processo processo = (Processo)serializable;

		if (processo == null) {
			Session session = null;

			try {
				session = openSession();

				processo = (Processo)session.get(ProcessoImpl.class, primaryKey);

				if (processo != null) {
					cacheResult(processo);
				}
				else {
					entityCache.putResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
						ProcessoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
					ProcessoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return processo;
	}

	/**
	 * Returns the processo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param processoId the primary key of the processo
	 * @return the processo, or <code>null</code> if a processo with the primary key could not be found
	 */
	@Override
	public Processo fetchByPrimaryKey(long processoId) {
		return fetchByPrimaryKey((Serializable)processoId);
	}

	@Override
	public Map<Serializable, Processo> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Processo> map = new HashMap<Serializable, Processo>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Processo processo = fetchByPrimaryKey(primaryKey);

			if (processo != null) {
				map.put(primaryKey, processo);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
					ProcessoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Processo)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROCESSO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Processo processo : (List<Processo>)q.list()) {
				map.put(processo.getPrimaryKeyObj(), processo);

				cacheResult(processo);

				uncachedPrimaryKeys.remove(processo.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProcessoModelImpl.ENTITY_CACHE_ENABLED,
					ProcessoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the processos.
	 *
	 * @return the processos
	 */
	@Override
	public List<Processo> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @return the range of processos
	 */
	@Override
	public List<Processo> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of processos
	 */
	@Override
	public List<Processo> findAll(int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the processos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of processos
	 * @param end the upper bound of the range of processos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of processos
	 */
	@Override
	public List<Processo> findAll(int start, int end,
		OrderByComparator<Processo> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Processo> list = null;

		if (retrieveFromCache) {
			list = (List<Processo>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROCESSO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROCESSO;

				if (pagination) {
					sql = sql.concat(ProcessoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Processo>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the processos from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Processo processo : findAll()) {
			remove(processo);
		}
	}

	/**
	 * Returns the number of processos.
	 *
	 * @return the number of processos
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROCESSO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProcessoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the processo persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProcessoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROCESSO = "SELECT processo FROM Processo processo";
	private static final String _SQL_SELECT_PROCESSO_WHERE_PKS_IN = "SELECT processo FROM Processo processo WHERE processoId IN (";
	private static final String _SQL_SELECT_PROCESSO_WHERE = "SELECT processo FROM Processo processo WHERE ";
	private static final String _SQL_COUNT_PROCESSO = "SELECT COUNT(processo) FROM Processo processo";
	private static final String _SQL_COUNT_PROCESSO_WHERE = "SELECT COUNT(processo) FROM Processo processo WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "processo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Processo exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Processo exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProcessoPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}