/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.impl;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.util.ArrayList;
import java.util.List;

import it.puglia.por.areariservata.model.DettaglioProcesso;
import it.puglia.por.areariservata.service.base.DettaglioProcessoLocalServiceBaseImpl;
import it.puglia.por.areariservata.service.persistence.DettaglioProcessoUtil;

/**
 * The implementation of the dettaglio processo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.puglia.por.areariservata.service.DettaglioProcessoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.DettaglioProcessoLocalServiceUtil
 */
public class DettaglioProcessoLocalServiceImpl
	extends DettaglioProcessoLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link it.puglia.por.areariservata.service.DettaglioProcessoLocalServiceUtil} to access the dettaglio processo local service.
	 */
	
	private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
	
	public List<DettaglioProcesso> findByProcesso(long processoId) {

		List<DettaglioProcesso> result = new ArrayList<DettaglioProcesso>();

		try {
			result.addAll(DettaglioProcessoUtil.findByprocesso(processoId));
		} catch (Exception e) {
			_log.error(e.getMessage());
		}
		
		return result;
	}
	
	public int countByProcesso(long processoId) {
	    try {
	        return DettaglioProcessoUtil.countByprocesso(processoId);
	    } catch (Exception ex) {}

	    return 0;
	}
}