package it.puglia.por.areariservata.service.persistence.impl;

import com.liferay.portal.kernel.dao.orm.*;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import it.puglia.por.areariservata.model.DocumentoProcesso;
import it.puglia.por.areariservata.service.DocumentoProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.persistence.DocumentoProcessoFinder;

import java.util.ArrayList;
import java.util.List;

public class DocumentoProcessoFinderImpl extends DocumentoProcessoFinderBaseImpl implements DocumentoProcessoFinder {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    public List<DocumentoProcesso> findByProcesso(Long processoId) {

        List<DocumentoProcesso> resultList = new ArrayList<>();
        Session                 session    = null;

        try {

            session = openSession();
            ClassLoader classLoader = getClass().getClassLoader();
            Order       order       = OrderFactoryUtil.desc("createDate");

            DynamicQuery getDocumentiByProcessoAndDettaglio = DynamicQueryFactoryUtil.forClass(DocumentoProcesso.class, classLoader);
            getDocumentiByProcessoAndDettaglio
                    .add(RestrictionsFactoryUtil.eq("processoId", processoId))
                    .addOrder(order);

            resultList = DocumentoProcessoLocalServiceUtil.dynamicQuery(getDocumentiByProcessoAndDettaglio);

        } catch (Exception e) {
            _log.error(e.getMessage());
        } finally {
            closeSession(session);
        }

        return resultList;
    }

    public List<DocumentoProcesso> findByProcessoAndDettaglio(Long dettaglioProcessoId, Long processoId) {

        List<DocumentoProcesso> resultList = new ArrayList<>();
        Session                 session    = null;

        try {

            session = openSession();
            ClassLoader classLoader = getClass().getClassLoader();
            Order       order       = OrderFactoryUtil.desc("createDate");

            DynamicQuery getDocumentiByProcessoAndDettaglio = DynamicQueryFactoryUtil.forClass(DocumentoProcesso.class, classLoader);
            getDocumentiByProcessoAndDettaglio
                    .add(RestrictionsFactoryUtil.eq("processoId", processoId))
                    .add(RestrictionsFactoryUtil.eq("dettaglioProcessoId", dettaglioProcessoId))
                    .addOrder(order);

            resultList = DocumentoProcessoLocalServiceUtil.dynamicQuery(getDocumentiByProcessoAndDettaglio);

        } catch (Exception e) {
            _log.error(e.getMessage());
        } finally {
            closeSession(session);
        }

        return resultList;
    }

    public List<DocumentoProcesso> findByProcessoOrDettaglio(Long dettaglioProcessoId, Long processoId) {

        List<DocumentoProcesso> resultList = new ArrayList<>();
        Session                 session    = null;

        try {

            session = openSession();
            ClassLoader classLoader = getClass().getClassLoader();
            Order       order       = OrderFactoryUtil.desc("createDate");

            DynamicQuery getDocumentiByProcessoOrDettaglio = DynamicQueryFactoryUtil.forClass(DocumentoProcesso.class, classLoader);
            getDocumentiByProcessoOrDettaglio
                    .add(
                            RestrictionsFactoryUtil
                                    .or(
                                            RestrictionsFactoryUtil.eq("processoId", processoId),
                                            RestrictionsFactoryUtil.eq("dettaglioProcessoId", dettaglioProcessoId)
                                    ))
                    .addOrder(order);

            resultList = DocumentoProcessoLocalServiceUtil.dynamicQuery(getDocumentiByProcessoOrDettaglio);

        } catch (Exception e) {
            _log.error(e.getMessage());
        } finally {
            closeSession(session);
        }

        return resultList;
    }
}
