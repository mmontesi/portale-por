package it.puglia.por.areariservata.common;

import it.puglia.por.areariservata.common.enums.PartenariatoEnum;
import org.junit.Assert;
import org.junit.Test;

public class EnumTest {

    @Test
    public void testFullNameLength() {
        PartenariatoEnum partenariatoEnum = PartenariatoEnum.CONVOCAZIONE;
       boolean result = partenariatoEnum.name().equals("CONVOCAZIONE");

        Assert.assertTrue(result);
    }
}
