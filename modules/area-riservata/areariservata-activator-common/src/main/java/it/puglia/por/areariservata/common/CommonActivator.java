package it.puglia.por.areariservata.common;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * @author luigi
 */
public class CommonActivator implements BundleActivator {


    @Override
    public void start(BundleContext bundleContext) throws Exception {

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
    }

}