package it.puglia.por.areariservata.common.enums;

import java.util.ArrayList;
import java.util.List;

public enum SorveglianzaEnum {
    CONVOCAZIONE(1L,"Convocazione") {
        public List<SorveglianzaEnum> getAllowedStatusesList() {
            List<SorveglianzaEnum> statuses = new ArrayList<>();
            statuses.add(INCONTRO);
            return statuses;
        }
    },
    INCONTRO(10L,"Incontro") {
        public List<SorveglianzaEnum> getAllowedStatusesList() {
            List<SorveglianzaEnum> statuses = new ArrayList<>();
            statuses.add(VERBALE_SINTETICO);
            return statuses;
        }
    },
    VERBALE_SINTETICO(20L,"Verbale Sintetico") {
        public List<SorveglianzaEnum> getAllowedStatusesList() {
            List<SorveglianzaEnum> statuses = new ArrayList<>();
            statuses.add(VALIDAZIONE);
            return statuses;
        }
    },
    VALIDAZIONE(30L,"Validazione") {
        public List<SorveglianzaEnum> getAllowedStatusesList() {
            List<SorveglianzaEnum> statuses = new ArrayList<>();
            statuses.add(VERBALE_SINTETICO);
            statuses.add(VERBALE_DEFINITIVO);
            return statuses;
        }
    },
    VERBALE_DEFINITIVO(40L,"Verbale definitivo") {
        public List<SorveglianzaEnum> getAllowedStatusesList() {
            List<SorveglianzaEnum> statuses = new ArrayList<>();
            return statuses;
        }
    };

    public final String descrizione;
    public final Long id;


    SorveglianzaEnum(Long id,String descrizione) {
        this.id = id;
        this.descrizione = descrizione;
    }

    public Long getId() {
        return id;
    }

    public abstract List<SorveglianzaEnum> getAllowedStatusesList();

    public String getDescrizione() {
        return descrizione;
    }
}