package it.puglia.por.areariservata.common;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import it.puglia.por.areariservata.common.enums.PartenariatoEnum;
import it.puglia.por.areariservata.common.enums.SorveglianzaEnum;

import java.util.EnumSet;
import java.util.Optional;

public class AreaRiservataEnumUtils {

    private final static Log _log = LogFactoryUtil.getLog(AreaRiservataEnumUtils.class.getName());

    public static String getWorkflowDescription(String value) {
        String result = "";

        Optional<SorveglianzaEnum> sorveglianzaEnumResult   = EnumSet.allOf(SorveglianzaEnum.class).stream().filter(e -> e.name().equalsIgnoreCase(value)).findFirst();
        Optional<PartenariatoEnum> partenariatoEnumOptional = EnumSet.allOf(PartenariatoEnum.class).stream().filter(e -> e.name().equalsIgnoreCase(value)).findFirst();

        if (sorveglianzaEnumResult.isPresent()) {
            result = sorveglianzaEnumResult.get().getDescrizione();
        } else if (partenariatoEnumOptional.isPresent()) {
            result = partenariatoEnumOptional.get().getDescrizione();
        } else {
            _log.error("getEnumByIdEnum(" + value + ") - Stato non trovato!");
        }

        return result;
    }

    public static Enum getEnumByStato(String stato) {
        Enum result = null;

        if (SorveglianzaEnum.valueOf(stato) != null) {
            result = SorveglianzaEnum.valueOf(stato);
        } else if (PartenariatoEnum.valueOf(stato) != null) {
            result = PartenariatoEnum.valueOf(stato);
        } else {
            _log.error("getEnumByStato(" + stato + ") - Stato non trovato!");
        }

        return result;
    }

    public static Enum getEnumByIdEnum(Long idEnum) {
        Enum result = null;

        Optional<SorveglianzaEnum> sorveglianzaEnumResult   = EnumSet.allOf(SorveglianzaEnum.class).stream().filter(e -> e.id.equals(idEnum)).findFirst();
        Optional<PartenariatoEnum> partenariatoEnumOptional = EnumSet.allOf(PartenariatoEnum.class).stream().filter(e -> e.id.equals(idEnum)).findFirst();

        if (sorveglianzaEnumResult.isPresent()) {
            result = sorveglianzaEnumResult.get();
        } else if (partenariatoEnumOptional.isPresent()) {
            result = partenariatoEnumOptional.get();
        } else {
            _log.error("getEnumByIdEnum(" + idEnum + ") - Stato non trovato!");
        }

        return result;
    }
}
