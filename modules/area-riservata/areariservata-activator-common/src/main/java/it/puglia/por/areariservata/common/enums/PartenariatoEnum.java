package it.puglia.por.areariservata.common.enums;


import java.util.ArrayList;
import java.util.List;

public enum PartenariatoEnum {

    CONVOCAZIONE(101L, "Convocazione") {
        public List<PartenariatoEnum> getAllowedStatusesList() {
            List<PartenariatoEnum> statuses = new ArrayList<>();
            statuses.add(INCONTRO);
            return statuses;
        }
    },
    INCONTRO(110L, "Incontro") {
        public List<PartenariatoEnum> getAllowedStatusesList() {
            List<PartenariatoEnum> statuses = new ArrayList<>();
            statuses.add(VERBALE_DEFINITIVO);
            return statuses;
        }
    },
    VERBALE_DEFINITIVO(120L, "Verbale Definitivo") {
        public List<PartenariatoEnum> getAllowedStatusesList() {
            List<PartenariatoEnum> statuses = new ArrayList<>();
            statuses.add(FORMULAZIONE_BANDO);
            return statuses;
        }
    },
    FORMULAZIONE_BANDO(130L, "Formulazione Bando") {
        public List<PartenariatoEnum> getAllowedStatusesList() {
            List<PartenariatoEnum> statuses = new ArrayList<>();
            return statuses;
        }
    };

    public final String descrizione;
    public final Long id;

    PartenariatoEnum(Long id, String descrizione) {
        this.id = id;
        this.descrizione = descrizione;
    }

    public abstract List<PartenariatoEnum> getAllowedStatusesList();

    public String getDescrizione() {
        return descrizione;
    }

    public Long getId() {
        return id;
    }
}