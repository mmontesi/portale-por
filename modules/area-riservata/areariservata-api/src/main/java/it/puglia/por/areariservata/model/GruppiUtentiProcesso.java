/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the GruppiUtentiProcesso service. Represents a row in the &quot;POR_GruppiUtentiProcesso&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see GruppiUtentiProcessoModel
 * @see it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoImpl
 * @see it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoModelImpl
 * @generated
 */
@ImplementationClassName("it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoImpl")
@ProviderType
public interface GruppiUtentiProcesso extends GruppiUtentiProcessoModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<GruppiUtentiProcesso, Long> GUP_ID_ACCESSOR = new Accessor<GruppiUtentiProcesso, Long>() {
			@Override
			public Long get(GruppiUtentiProcesso gruppiUtentiProcesso) {
				return gruppiUtentiProcesso.getGupId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<GruppiUtentiProcesso> getTypeClass() {
				return GruppiUtentiProcesso.class;
			}
		};
}