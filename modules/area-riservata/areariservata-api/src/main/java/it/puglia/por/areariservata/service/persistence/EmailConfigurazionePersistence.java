/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException;
import it.puglia.por.areariservata.model.EmailConfigurazione;

/**
 * The persistence interface for the email configurazione service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.persistence.impl.EmailConfigurazionePersistenceImpl
 * @see EmailConfigurazioneUtil
 * @generated
 */
@ProviderType
public interface EmailConfigurazionePersistence extends BasePersistence<EmailConfigurazione> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmailConfigurazioneUtil} to access the email configurazione persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the email configuraziones where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid(String uuid);

	/**
	* Returns a range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid(String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid(String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid(String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public EmailConfigurazione findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public EmailConfigurazione findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63;.
	*
	* @param emailConfigurazioneId the primary key of the current email configurazione
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public EmailConfigurazione[] findByUuid_PrevAndNext(
		long emailConfigurazioneId, String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Removes all the email configuraziones where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of email configuraziones where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching email configuraziones
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public EmailConfigurazione findByUUID_G(String uuid, long groupId)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the email configurazione where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the email configurazione that was removed
	*/
	public EmailConfigurazione removeByUUID_G(String uuid, long groupId)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the number of email configuraziones where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching email configuraziones
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId);

	/**
	* Returns a range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public EmailConfigurazione findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public EmailConfigurazione findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public EmailConfigurazione fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param emailConfigurazioneId the primary key of the current email configurazione
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public EmailConfigurazione[] findByUuid_C_PrevAndNext(
		long emailConfigurazioneId, String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Removes all the email configuraziones where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching email configuraziones
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Caches the email configurazione in the entity cache if it is enabled.
	*
	* @param emailConfigurazione the email configurazione
	*/
	public void cacheResult(EmailConfigurazione emailConfigurazione);

	/**
	* Caches the email configuraziones in the entity cache if it is enabled.
	*
	* @param emailConfiguraziones the email configuraziones
	*/
	public void cacheResult(
		java.util.List<EmailConfigurazione> emailConfiguraziones);

	/**
	* Creates a new email configurazione with the primary key. Does not add the email configurazione to the database.
	*
	* @param emailConfigurazioneId the primary key for the new email configurazione
	* @return the new email configurazione
	*/
	public EmailConfigurazione create(long emailConfigurazioneId);

	/**
	* Removes the email configurazione with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione that was removed
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public EmailConfigurazione remove(long emailConfigurazioneId)
		throws NoSuchEmailConfigurazioneException;

	public EmailConfigurazione updateImpl(
		EmailConfigurazione emailConfigurazione);

	/**
	* Returns the email configurazione with the primary key or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public EmailConfigurazione findByPrimaryKey(long emailConfigurazioneId)
		throws NoSuchEmailConfigurazioneException;

	/**
	* Returns the email configurazione with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione, or <code>null</code> if a email configurazione with the primary key could not be found
	*/
	public EmailConfigurazione fetchByPrimaryKey(long emailConfigurazioneId);

	@Override
	public java.util.Map<java.io.Serializable, EmailConfigurazione> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the email configuraziones.
	*
	* @return the email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findAll();

	/**
	* Returns a range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findAll(int start, int end);

	/**
	* Returns an ordered range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator);

	/**
	* Returns an ordered range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of email configuraziones
	*/
	public java.util.List<EmailConfigurazione> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the email configuraziones from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of email configuraziones.
	*
	* @return the number of email configuraziones
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}