/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */

package it.puglia.por.areariservata.constants;

/**
 * @author Michele Perissinotto
 */
public class ActionKeys
	extends com.liferay.portal.kernel.security.permission.ActionKeys {

	public static final String ADD_PROCESSO = "ADD_PROCESSO";
	public static final String ADD_DETTAGLIO = "ADD_DETTAGLIO";
	public static final String DELETE_PROCESSO = "DELETE_PROCESSO";
	public static final String SUBSCRIBE_PROCESSO = "SUBSCRIBE_PROCESSO";
	public static final String SHOW_PROCESSO = "SHOW_PROCESSO";
	public static final String SHOW_PARTENARIATO = "SHOW_PARTENARIATO";


}