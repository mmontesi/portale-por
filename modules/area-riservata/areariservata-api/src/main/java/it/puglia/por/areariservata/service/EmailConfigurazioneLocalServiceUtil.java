/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for EmailConfigurazione. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.EmailConfigurazioneLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see EmailConfigurazioneLocalService
 * @see it.puglia.por.areariservata.service.base.EmailConfigurazioneLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.EmailConfigurazioneLocalServiceImpl
 * @generated
 */
@ProviderType
public class EmailConfigurazioneLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.EmailConfigurazioneLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the email configurazione to the database. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazione the email configurazione
	* @return the email configurazione that was added
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione addEmailConfigurazione(
		it.puglia.por.areariservata.model.EmailConfigurazione emailConfigurazione) {
		return getService().addEmailConfigurazione(emailConfigurazione);
	}

	/**
	* Creates a new email configurazione with the primary key. Does not add the email configurazione to the database.
	*
	* @param emailConfigurazioneId the primary key for the new email configurazione
	* @return the new email configurazione
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione createEmailConfigurazione(
		long emailConfigurazioneId) {
		return getService().createEmailConfigurazione(emailConfigurazioneId);
	}

	/**
	* Deletes the email configurazione from the database. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazione the email configurazione
	* @return the email configurazione that was removed
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione deleteEmailConfigurazione(
		it.puglia.por.areariservata.model.EmailConfigurazione emailConfigurazione) {
		return getService().deleteEmailConfigurazione(emailConfigurazione);
	}

	/**
	* Deletes the email configurazione with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione that was removed
	* @throws PortalException if a email configurazione with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione deleteEmailConfigurazione(
		long emailConfigurazioneId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteEmailConfigurazione(emailConfigurazioneId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.puglia.por.areariservata.model.EmailConfigurazione fetchEmailConfigurazione(
		long emailConfigurazioneId) {
		return getService().fetchEmailConfigurazione(emailConfigurazioneId);
	}

	/**
	* Returns the email configurazione matching the UUID and group.
	*
	* @param uuid the email configurazione's UUID
	* @param groupId the primary key of the group
	* @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione fetchEmailConfigurazioneByUuidAndGroupId(
		String uuid, long groupId) {
		return getService()
				   .fetchEmailConfigurazioneByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	/**
	* Returns the email configurazione with the primary key.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione
	* @throws PortalException if a email configurazione with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione getEmailConfigurazione(
		long emailConfigurazioneId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEmailConfigurazione(emailConfigurazioneId);
	}

	/**
	* Returns the email configurazione matching the UUID and group.
	*
	* @param uuid the email configurazione's UUID
	* @param groupId the primary key of the group
	* @return the matching email configurazione
	* @throws PortalException if a matching email configurazione could not be found
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione getEmailConfigurazioneByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getEmailConfigurazioneByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of email configuraziones
	*/
	public static java.util.List<it.puglia.por.areariservata.model.EmailConfigurazione> getEmailConfiguraziones(
		int start, int end) {
		return getService().getEmailConfiguraziones(start, end);
	}

	/**
	* Returns all the email configuraziones matching the UUID and company.
	*
	* @param uuid the UUID of the email configuraziones
	* @param companyId the primary key of the company
	* @return the matching email configuraziones, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.EmailConfigurazione> getEmailConfigurazionesByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService()
				   .getEmailConfigurazionesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of email configuraziones matching the UUID and company.
	*
	* @param uuid the UUID of the email configuraziones
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching email configuraziones, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.EmailConfigurazione> getEmailConfigurazionesByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.EmailConfigurazione> orderByComparator) {
		return getService()
				   .getEmailConfigurazionesByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of email configuraziones.
	*
	* @return the number of email configuraziones
	*/
	public static int getEmailConfigurazionesCount() {
		return getService().getEmailConfigurazionesCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the email configurazione in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazione the email configurazione
	* @return the email configurazione that was updated
	*/
	public static it.puglia.por.areariservata.model.EmailConfigurazione updateEmailConfigurazione(
		it.puglia.por.areariservata.model.EmailConfigurazione emailConfigurazione) {
		return getService().updateEmailConfigurazione(emailConfigurazione);
	}

	public static EmailConfigurazioneLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EmailConfigurazioneLocalService, EmailConfigurazioneLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(EmailConfigurazioneLocalService.class);

		ServiceTracker<EmailConfigurazioneLocalService, EmailConfigurazioneLocalService> serviceTracker =
			new ServiceTracker<EmailConfigurazioneLocalService, EmailConfigurazioneLocalService>(bundle.getBundleContext(),
				EmailConfigurazioneLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}