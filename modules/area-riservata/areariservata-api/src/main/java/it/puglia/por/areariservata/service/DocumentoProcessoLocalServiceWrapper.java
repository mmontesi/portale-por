/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DocumentoProcessoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoLocalService
 * @generated
 */
@ProviderType
public class DocumentoProcessoLocalServiceWrapper
	implements DocumentoProcessoLocalService,
		ServiceWrapper<DocumentoProcessoLocalService> {
	public DocumentoProcessoLocalServiceWrapper(
		DocumentoProcessoLocalService documentoProcessoLocalService) {
		_documentoProcessoLocalService = documentoProcessoLocalService;
	}

	/**
	* Adds the documento processo to the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was added
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso addDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return _documentoProcessoLocalService.addDocumentoProcesso(documentoProcesso);
	}

	/**
	* Creates a new documento processo with the primary key. Does not add the documento processo to the database.
	*
	* @param documentoProcessoId the primary key for the new documento processo
	* @return the new documento processo
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso createDocumentoProcesso(
		long documentoProcessoId) {
		return _documentoProcessoLocalService.createDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Deletes the documento processo from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was removed
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso deleteDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return _documentoProcessoLocalService.deleteDocumentoProcesso(documentoProcesso);
	}

	/**
	* Deletes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo that was removed
	* @throws PortalException if a documento processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso deleteDocumentoProcesso(
		long documentoProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _documentoProcessoLocalService.deleteDocumentoProcesso(documentoProcessoId);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _documentoProcessoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _documentoProcessoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _documentoProcessoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _documentoProcessoLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _documentoProcessoLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _documentoProcessoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _documentoProcessoLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso fetchDocumentoProcesso(
		long documentoProcessoId) {
		return _documentoProcessoLocalService.fetchDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Returns the documento processo matching the UUID and group.
	*
	* @param uuid the documento processo's UUID
	* @param groupId the primary key of the group
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso fetchDocumentoProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return _documentoProcessoLocalService.fetchDocumentoProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _documentoProcessoLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromDettaglio(
		Long idProcesso, Long idDettaglio) {
		return _documentoProcessoLocalService.getAllDocumentsFromDettaglio(idProcesso,
			idDettaglio);
	}

	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromProcesso(
		Long idProcesso) {
		return _documentoProcessoLocalService.getAllDocumentsFromProcesso(idProcesso);
	}

	/**
	* Returns the documento processo with the primary key.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo
	* @throws PortalException if a documento processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso getDocumentoProcesso(
		long documentoProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _documentoProcessoLocalService.getDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Returns the documento processo matching the UUID and group.
	*
	* @param uuid the documento processo's UUID
	* @param groupId the primary key of the group
	* @return the matching documento processo
	* @throws PortalException if a matching documento processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso getDocumentoProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _documentoProcessoLocalService.getDocumentoProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of documento processos
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessos(
		int start, int end) {
		return _documentoProcessoLocalService.getDocumentoProcessos(start, end);
	}

	/**
	* Returns all the documento processos matching the UUID and company.
	*
	* @param uuid the UUID of the documento processos
	* @param companyId the primary key of the company
	* @return the matching documento processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return _documentoProcessoLocalService.getDocumentoProcessosByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of documento processos matching the UUID and company.
	*
	* @param uuid the UUID of the documento processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching documento processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.DocumentoProcesso> orderByComparator) {
		return _documentoProcessoLocalService.getDocumentoProcessosByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of documento processos.
	*
	* @return the number of documento processos
	*/
	@Override
	public int getDocumentoProcessosCount() {
		return _documentoProcessoLocalService.getDocumentoProcessosCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _documentoProcessoLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _documentoProcessoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _documentoProcessoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _documentoProcessoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the documento processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was updated
	*/
	@Override
	public it.puglia.por.areariservata.model.DocumentoProcesso updateDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return _documentoProcessoLocalService.updateDocumentoProcesso(documentoProcesso);
	}

	@Override
	public DocumentoProcessoLocalService getWrappedService() {
		return _documentoProcessoLocalService;
	}

	@Override
	public void setWrappedService(
		DocumentoProcessoLocalService documentoProcessoLocalService) {
		_documentoProcessoLocalService = documentoProcessoLocalService;
	}

	private DocumentoProcessoLocalService _documentoProcessoLocalService;
}