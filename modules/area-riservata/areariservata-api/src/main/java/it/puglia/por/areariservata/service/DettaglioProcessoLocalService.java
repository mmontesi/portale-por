/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.exportimport.kernel.lar.PortletDataContext;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.DettaglioProcesso;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for DettaglioProcesso. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoLocalServiceUtil
 * @see it.puglia.por.areariservata.service.base.DettaglioProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.DettaglioProcessoLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface DettaglioProcessoLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DettaglioProcessoLocalServiceUtil} to access the dettaglio processo local service. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.DettaglioProcessoLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the dettaglio processo to the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public DettaglioProcesso addDettaglioProcesso(
		DettaglioProcesso dettaglioProcesso);

	public int countByProcesso(long processoId);

	/**
	* Creates a new dettaglio processo with the primary key. Does not add the dettaglio processo to the database.
	*
	* @param dettaglioProcessoId the primary key for the new dettaglio processo
	* @return the new dettaglio processo
	*/
	@Transactional(enabled = false)
	public DettaglioProcesso createDettaglioProcesso(long dettaglioProcessoId);

	/**
	* Deletes the dettaglio processo from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public DettaglioProcesso deleteDettaglioProcesso(
		DettaglioProcesso dettaglioProcesso);

	/**
	* Deletes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo that was removed
	* @throws PortalException if a dettaglio processo with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public DettaglioProcesso deleteDettaglioProcesso(long dettaglioProcessoId)
		throws PortalException;

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DynamicQuery dynamicQuery();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DettaglioProcesso fetchDettaglioProcesso(long dettaglioProcessoId);

	/**
	* Returns the dettaglio processo matching the UUID and group.
	*
	* @param uuid the dettaglio processo's UUID
	* @param groupId the primary key of the group
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DettaglioProcesso fetchDettaglioProcessoByUuidAndGroupId(
		String uuid, long groupId);

	public List<DettaglioProcesso> findByProcesso(long processoId);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	/**
	* Returns the dettaglio processo with the primary key.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo
	* @throws PortalException if a dettaglio processo with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DettaglioProcesso getDettaglioProcesso(long dettaglioProcessoId)
		throws PortalException;

	/**
	* Returns the dettaglio processo matching the UUID and group.
	*
	* @param uuid the dettaglio processo's UUID
	* @param groupId the primary key of the group
	* @return the matching dettaglio processo
	* @throws PortalException if a matching dettaglio processo could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public DettaglioProcesso getDettaglioProcessoByUuidAndGroupId(String uuid,
		long groupId) throws PortalException;

	/**
	* Returns a range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of dettaglio processos
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<DettaglioProcesso> getDettaglioProcessos(int start, int end);

	/**
	* Returns all the dettaglio processos matching the UUID and company.
	*
	* @param uuid the UUID of the dettaglio processos
	* @param companyId the primary key of the company
	* @return the matching dettaglio processos, or an empty list if no matches were found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<DettaglioProcesso> getDettaglioProcessosByUuidAndCompanyId(
		String uuid, long companyId);

	/**
	* Returns a range of dettaglio processos matching the UUID and company.
	*
	* @param uuid the UUID of the dettaglio processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching dettaglio processos, or an empty list if no matches were found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<DettaglioProcesso> getDettaglioProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the number of dettaglio processos.
	*
	* @return the number of dettaglio processos
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getDettaglioProcessosCount();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		PortletDataContext portletDataContext);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public String getOSGiServiceIdentifier();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Updates the dettaglio processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public DettaglioProcesso updateDettaglioProcesso(
		DettaglioProcesso dettaglioProcesso);
}