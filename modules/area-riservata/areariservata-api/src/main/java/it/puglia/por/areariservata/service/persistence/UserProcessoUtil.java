/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.UserProcesso;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the user processo service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.UserProcessoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.UserProcessoPersistenceImpl
 * @generated
 */
@ProviderType
public class UserProcessoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(UserProcesso userProcesso) {
		getPersistence().clearCache(userProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<UserProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<UserProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<UserProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static UserProcesso update(UserProcesso userProcesso) {
		return getPersistence().update(userProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static UserProcesso update(UserProcesso userProcesso,
		ServiceContext serviceContext) {
		return getPersistence().update(userProcesso, serviceContext);
	}

	/**
	* Returns all the user processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching user processos
	*/
	public static List<UserProcesso> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public static List<UserProcesso> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByUuid_First(String uuid,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUuid_First(String uuid,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByUuid_Last(String uuid,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the user processos before and after the current user processo in the ordered set where uuid = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso[] findByUuid_PrevAndNext(long userProcessoId,
		String uuid, OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(userProcessoId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the user processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of user processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching user processos
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchUserProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the user processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the user processo that was removed
	*/
	public static UserProcesso removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of user processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching user processos
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching user processos
	*/
	public static List<UserProcesso> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public static List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the user processos before and after the current user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso[] findByUuid_C_PrevAndNext(long userProcessoId,
		String uuid, long companyId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(userProcessoId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the user processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of user processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching user processos
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the user processos where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @return the matching user processos
	*/
	public static List<UserProcesso> findByuserIdProcesso(long userIdProcesso) {
		return getPersistence().findByuserIdProcesso(userIdProcesso);
	}

	/**
	* Returns a range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public static List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end) {
		return getPersistence().findByuserIdProcesso(userIdProcesso, start, end);
	}

	/**
	* Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .findByuserIdProcesso(userIdProcesso, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByuserIdProcesso(long userIdProcesso,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByuserIdProcesso(userIdProcesso, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByuserIdProcesso_First(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByuserIdProcesso_First(userIdProcesso, orderByComparator);
	}

	/**
	* Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByuserIdProcesso_First(
		long userIdProcesso, OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByuserIdProcesso_First(userIdProcesso,
			orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByuserIdProcesso_Last(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByuserIdProcesso_Last(userIdProcesso, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByuserIdProcesso_Last(long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByuserIdProcesso_Last(userIdProcesso, orderByComparator);
	}

	/**
	* Returns the user processos before and after the current user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso[] findByuserIdProcesso_PrevAndNext(
		long userProcessoId, long userIdProcesso,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByuserIdProcesso_PrevAndNext(userProcessoId,
			userIdProcesso, orderByComparator);
	}

	/**
	* Removes all the user processos where userIdProcesso = &#63; from the database.
	*
	* @param userIdProcesso the user ID processo
	*/
	public static void removeByuserIdProcesso(long userIdProcesso) {
		getPersistence().removeByuserIdProcesso(userIdProcesso);
	}

	/**
	* Returns the number of user processos where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @return the number of matching user processos
	*/
	public static int countByuserIdProcesso(long userIdProcesso) {
		return getPersistence().countByuserIdProcesso(userIdProcesso);
	}

	/**
	* Returns all the user processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching user processos
	*/
	public static List<UserProcesso> findByprocessoId(long processoId) {
		return getPersistence().findByprocessoId(processoId);
	}

	/**
	* Returns a range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public static List<UserProcesso> findByprocessoId(long processoId,
		int start, int end) {
		return getPersistence().findByprocessoId(processoId, start, end);
	}

	/**
	* Returns an ordered range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByprocessoId(long processoId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .findByprocessoId(processoId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public static List<UserProcesso> findByprocessoId(long processoId,
		int start, int end, OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByprocessoId(processoId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByprocessoId_First(long processoId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByprocessoId_First(processoId, orderByComparator);
	}

	/**
	* Returns the first user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByprocessoId_First(long processoId,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoId_First(processoId, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public static UserProcesso findByprocessoId_Last(long processoId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByprocessoId_Last(processoId, orderByComparator);
	}

	/**
	* Returns the last user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static UserProcesso fetchByprocessoId_Last(long processoId,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoId_Last(processoId, orderByComparator);
	}

	/**
	* Returns the user processos before and after the current user processo in the ordered set where processoId = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso[] findByprocessoId_PrevAndNext(
		long userProcessoId, long processoId,
		OrderByComparator<UserProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence()
				   .findByprocessoId_PrevAndNext(userProcessoId, processoId,
			orderByComparator);
	}

	/**
	* Removes all the user processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public static void removeByprocessoId(long processoId) {
		getPersistence().removeByprocessoId(processoId);
	}

	/**
	* Returns the number of user processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching user processos
	*/
	public static int countByprocessoId(long processoId) {
		return getPersistence().countByprocessoId(processoId);
	}

	/**
	* Caches the user processo in the entity cache if it is enabled.
	*
	* @param userProcesso the user processo
	*/
	public static void cacheResult(UserProcesso userProcesso) {
		getPersistence().cacheResult(userProcesso);
	}

	/**
	* Caches the user processos in the entity cache if it is enabled.
	*
	* @param userProcessos the user processos
	*/
	public static void cacheResult(List<UserProcesso> userProcessos) {
		getPersistence().cacheResult(userProcessos);
	}

	/**
	* Creates a new user processo with the primary key. Does not add the user processo to the database.
	*
	* @param userProcessoId the primary key for the new user processo
	* @return the new user processo
	*/
	public static UserProcesso create(long userProcessoId) {
		return getPersistence().create(userProcessoId);
	}

	/**
	* Removes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo that was removed
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso remove(long userProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().remove(userProcessoId);
	}

	public static UserProcesso updateImpl(UserProcesso userProcesso) {
		return getPersistence().updateImpl(userProcesso);
	}

	/**
	* Returns the user processo with the primary key or throws a {@link NoSuchUserProcessoException} if it could not be found.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public static UserProcesso findByPrimaryKey(long userProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchUserProcessoException {
		return getPersistence().findByPrimaryKey(userProcessoId);
	}

	/**
	* Returns the user processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo, or <code>null</code> if a user processo with the primary key could not be found
	*/
	public static UserProcesso fetchByPrimaryKey(long userProcessoId) {
		return getPersistence().fetchByPrimaryKey(userProcessoId);
	}

	public static java.util.Map<java.io.Serializable, UserProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the user processos.
	*
	* @return the user processos
	*/
	public static List<UserProcesso> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of user processos
	*/
	public static List<UserProcesso> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user processos
	*/
	public static List<UserProcesso> findAll(int start, int end,
		OrderByComparator<UserProcesso> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user processos
	*/
	public static List<UserProcesso> findAll(int start, int end,
		OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the user processos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of user processos.
	*
	* @return the number of user processos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static UserProcessoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserProcessoPersistence, UserProcessoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(UserProcessoPersistence.class);

		ServiceTracker<UserProcessoPersistence, UserProcessoPersistence> serviceTracker =
			new ServiceTracker<UserProcessoPersistence, UserProcessoPersistence>(bundle.getBundleContext(),
				UserProcessoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}