/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException;
import it.puglia.por.areariservata.model.DettaglioProcesso;

/**
 * The persistence interface for the dettaglio processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.persistence.impl.DettaglioProcessoPersistenceImpl
 * @see DettaglioProcessoUtil
 * @generated
 */
@ProviderType
public interface DettaglioProcessoPersistence extends BasePersistence<DettaglioProcesso> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DettaglioProcessoUtil} to access the dettaglio processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the dettaglio processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid(String uuid);

	/**
	* Returns a range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso[] findByUuid_PrevAndNext(
		long dettaglioProcessoId, String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Removes all the dettaglio processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of dettaglio processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching dettaglio processos
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the dettaglio processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the dettaglio processo that was removed
	*/
	public DettaglioProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the number of dettaglio processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching dettaglio processos
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId);

	/**
	* Returns a range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso[] findByUuid_C_PrevAndNext(
		long dettaglioProcessoId, String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Removes all the dettaglio processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching dettaglio processos
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Returns all the dettaglio processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByprocesso(Long processoId);

	/**
	* Returns a range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end);

	/**
	* Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByprocesso_First(Long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByprocesso_First(Long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso findByprocesso_Last(Long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public DettaglioProcesso fetchByprocesso_Last(Long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso[] findByprocesso_PrevAndNext(
		long dettaglioProcessoId, Long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator)
		throws NoSuchDettaglioProcessoException;

	/**
	* Removes all the dettaglio processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public void removeByprocesso(Long processoId);

	/**
	* Returns the number of dettaglio processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching dettaglio processos
	*/
	public int countByprocesso(Long processoId);

	/**
	* Caches the dettaglio processo in the entity cache if it is enabled.
	*
	* @param dettaglioProcesso the dettaglio processo
	*/
	public void cacheResult(DettaglioProcesso dettaglioProcesso);

	/**
	* Caches the dettaglio processos in the entity cache if it is enabled.
	*
	* @param dettaglioProcessos the dettaglio processos
	*/
	public void cacheResult(
		java.util.List<DettaglioProcesso> dettaglioProcessos);

	/**
	* Creates a new dettaglio processo with the primary key. Does not add the dettaglio processo to the database.
	*
	* @param dettaglioProcessoId the primary key for the new dettaglio processo
	* @return the new dettaglio processo
	*/
	public DettaglioProcesso create(long dettaglioProcessoId);

	/**
	* Removes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo that was removed
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso remove(long dettaglioProcessoId)
		throws NoSuchDettaglioProcessoException;

	public DettaglioProcesso updateImpl(DettaglioProcesso dettaglioProcesso);

	/**
	* Returns the dettaglio processo with the primary key or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso findByPrimaryKey(long dettaglioProcessoId)
		throws NoSuchDettaglioProcessoException;

	/**
	* Returns the dettaglio processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo, or <code>null</code> if a dettaglio processo with the primary key could not be found
	*/
	public DettaglioProcesso fetchByPrimaryKey(long dettaglioProcessoId);

	@Override
	public java.util.Map<java.io.Serializable, DettaglioProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the dettaglio processos.
	*
	* @return the dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findAll();

	/**
	* Returns a range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findAll(int start, int end);

	/**
	* Returns an ordered range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of dettaglio processos
	*/
	public java.util.List<DettaglioProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the dettaglio processos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of dettaglio processos.
	*
	* @return the number of dettaglio processos
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}