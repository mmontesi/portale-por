/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.puglia.por.areariservata.service.http.GruppiUtentiProcessoServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.http.GruppiUtentiProcessoServiceSoap
 * @generated
 */
@ProviderType
public class GruppiUtentiProcessoSoap implements Serializable {
	public static GruppiUtentiProcessoSoap toSoapModel(
		GruppiUtentiProcesso model) {
		GruppiUtentiProcessoSoap soapModel = new GruppiUtentiProcessoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setGupId(model.getGupId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setGupUserId(model.getGupUserId());
		soapModel.setGupGroupId(model.getGupGroupId());

		return soapModel;
	}

	public static GruppiUtentiProcessoSoap[] toSoapModels(
		GruppiUtentiProcesso[] models) {
		GruppiUtentiProcessoSoap[] soapModels = new GruppiUtentiProcessoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static GruppiUtentiProcessoSoap[][] toSoapModels(
		GruppiUtentiProcesso[][] models) {
		GruppiUtentiProcessoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new GruppiUtentiProcessoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new GruppiUtentiProcessoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static GruppiUtentiProcessoSoap[] toSoapModels(
		List<GruppiUtentiProcesso> models) {
		List<GruppiUtentiProcessoSoap> soapModels = new ArrayList<GruppiUtentiProcessoSoap>(models.size());

		for (GruppiUtentiProcesso model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new GruppiUtentiProcessoSoap[soapModels.size()]);
	}

	public GruppiUtentiProcessoSoap() {
	}

	public long getPrimaryKey() {
		return _gupId;
	}

	public void setPrimaryKey(long pk) {
		setGupId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getGupId() {
		return _gupId;
	}

	public void setGupId(long gupId) {
		_gupId = gupId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getGupUserId() {
		return _gupUserId;
	}

	public void setGupUserId(long gupUserId) {
		_gupUserId = gupUserId;
	}

	public long getGupGroupId() {
		return _gupGroupId;
	}

	public void setGupGroupId(long gupGroupId) {
		_gupGroupId = gupGroupId;
	}

	private String _uuid;
	private long _gupId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _gupUserId;
	private long _gupGroupId;
}