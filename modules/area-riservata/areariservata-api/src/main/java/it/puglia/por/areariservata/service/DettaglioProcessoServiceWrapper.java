/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DettaglioProcessoService}.
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoService
 * @generated
 */
@ProviderType
public class DettaglioProcessoServiceWrapper implements DettaglioProcessoService,
	ServiceWrapper<DettaglioProcessoService> {
	public DettaglioProcessoServiceWrapper(
		DettaglioProcessoService dettaglioProcessoService) {
		_dettaglioProcessoService = dettaglioProcessoService;
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _dettaglioProcessoService.getOSGiServiceIdentifier();
	}

	@Override
	public DettaglioProcessoService getWrappedService() {
		return _dettaglioProcessoService;
	}

	@Override
	public void setWrappedService(
		DettaglioProcessoService dettaglioProcessoService) {
		_dettaglioProcessoService = dettaglioProcessoService;
	}

	private DettaglioProcessoService _dettaglioProcessoService;
}