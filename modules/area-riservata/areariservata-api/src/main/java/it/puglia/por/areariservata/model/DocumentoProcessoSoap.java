/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.puglia.por.areariservata.service.http.DocumentoProcessoServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.http.DocumentoProcessoServiceSoap
 * @generated
 */
@ProviderType
public class DocumentoProcessoSoap implements Serializable {
	public static DocumentoProcessoSoap toSoapModel(DocumentoProcesso model) {
		DocumentoProcessoSoap soapModel = new DocumentoProcessoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setDocumentoProcessoId(model.getDocumentoProcessoId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setProcessoId(model.getProcessoId());
		soapModel.setDettaglioProcessoId(model.getDettaglioProcessoId());
		soapModel.setDlFileId(model.getDlFileId());

		return soapModel;
	}

	public static DocumentoProcessoSoap[] toSoapModels(
		DocumentoProcesso[] models) {
		DocumentoProcessoSoap[] soapModels = new DocumentoProcessoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DocumentoProcessoSoap[][] toSoapModels(
		DocumentoProcesso[][] models) {
		DocumentoProcessoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DocumentoProcessoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DocumentoProcessoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DocumentoProcessoSoap[] toSoapModels(
		List<DocumentoProcesso> models) {
		List<DocumentoProcessoSoap> soapModels = new ArrayList<DocumentoProcessoSoap>(models.size());

		for (DocumentoProcesso model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DocumentoProcessoSoap[soapModels.size()]);
	}

	public DocumentoProcessoSoap() {
	}

	public long getPrimaryKey() {
		return _documentoProcessoId;
	}

	public void setPrimaryKey(long pk) {
		setDocumentoProcessoId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getDocumentoProcessoId() {
		return _documentoProcessoId;
	}

	public void setDocumentoProcessoId(long documentoProcessoId) {
		_documentoProcessoId = documentoProcessoId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getProcessoId() {
		return _processoId;
	}

	public void setProcessoId(long processoId) {
		_processoId = processoId;
	}

	public long getDettaglioProcessoId() {
		return _dettaglioProcessoId;
	}

	public void setDettaglioProcessoId(long dettaglioProcessoId) {
		_dettaglioProcessoId = dettaglioProcessoId;
	}

	public long getDlFileId() {
		return _dlFileId;
	}

	public void setDlFileId(long dlFileId) {
		_dlFileId = dlFileId;
	}

	private String _uuid;
	private long _documentoProcessoId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _processoId;
	private long _dettaglioProcessoId;
	private long _dlFileId;
}