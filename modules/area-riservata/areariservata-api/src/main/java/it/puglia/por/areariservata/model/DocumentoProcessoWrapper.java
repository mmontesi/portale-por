/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link DocumentoProcesso}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcesso
 * @generated
 */
@ProviderType
public class DocumentoProcessoWrapper implements DocumentoProcesso,
	ModelWrapper<DocumentoProcesso> {
	public DocumentoProcessoWrapper(DocumentoProcesso documentoProcesso) {
		_documentoProcesso = documentoProcesso;
	}

	@Override
	public Class<?> getModelClass() {
		return DocumentoProcesso.class;
	}

	@Override
	public String getModelClassName() {
		return DocumentoProcesso.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("documentoProcessoId", getDocumentoProcessoId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("processoId", getProcessoId());
		attributes.put("dettaglioProcessoId", getDettaglioProcessoId());
		attributes.put("dlFileId", getDlFileId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long documentoProcessoId = (Long)attributes.get("documentoProcessoId");

		if (documentoProcessoId != null) {
			setDocumentoProcessoId(documentoProcessoId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long processoId = (Long)attributes.get("processoId");

		if (processoId != null) {
			setProcessoId(processoId);
		}

		Long dettaglioProcessoId = (Long)attributes.get("dettaglioProcessoId");

		if (dettaglioProcessoId != null) {
			setDettaglioProcessoId(dettaglioProcessoId);
		}

		Long dlFileId = (Long)attributes.get("dlFileId");

		if (dlFileId != null) {
			setDlFileId(dlFileId);
		}
	}

	@Override
	public Object clone() {
		return new DocumentoProcessoWrapper((DocumentoProcesso)_documentoProcesso.clone());
	}

	@Override
	public int compareTo(DocumentoProcesso documentoProcesso) {
		return _documentoProcesso.compareTo(documentoProcesso);
	}

	/**
	* Returns the company ID of this documento processo.
	*
	* @return the company ID of this documento processo
	*/
	@Override
	public long getCompanyId() {
		return _documentoProcesso.getCompanyId();
	}

	/**
	* Returns the create date of this documento processo.
	*
	* @return the create date of this documento processo
	*/
	@Override
	public Date getCreateDate() {
		return _documentoProcesso.getCreateDate();
	}

	/**
	* Returns the dettaglio processo ID of this documento processo.
	*
	* @return the dettaglio processo ID of this documento processo
	*/
	@Override
	public long getDettaglioProcessoId() {
		return _documentoProcesso.getDettaglioProcessoId();
	}

	/**
	* Returns the dl file ID of this documento processo.
	*
	* @return the dl file ID of this documento processo
	*/
	@Override
	public long getDlFileId() {
		return _documentoProcesso.getDlFileId();
	}

	/**
	* Returns the documento processo ID of this documento processo.
	*
	* @return the documento processo ID of this documento processo
	*/
	@Override
	public long getDocumentoProcessoId() {
		return _documentoProcesso.getDocumentoProcessoId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _documentoProcesso.getExpandoBridge();
	}

	/**
	* Returns the group ID of this documento processo.
	*
	* @return the group ID of this documento processo
	*/
	@Override
	public long getGroupId() {
		return _documentoProcesso.getGroupId();
	}

	/**
	* Returns the modified date of this documento processo.
	*
	* @return the modified date of this documento processo
	*/
	@Override
	public Date getModifiedDate() {
		return _documentoProcesso.getModifiedDate();
	}

	/**
	* Returns the primary key of this documento processo.
	*
	* @return the primary key of this documento processo
	*/
	@Override
	public long getPrimaryKey() {
		return _documentoProcesso.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _documentoProcesso.getPrimaryKeyObj();
	}

	/**
	* Returns the processo ID of this documento processo.
	*
	* @return the processo ID of this documento processo
	*/
	@Override
	public long getProcessoId() {
		return _documentoProcesso.getProcessoId();
	}

	/**
	* Returns the user ID of this documento processo.
	*
	* @return the user ID of this documento processo
	*/
	@Override
	public long getUserId() {
		return _documentoProcesso.getUserId();
	}

	/**
	* Returns the user name of this documento processo.
	*
	* @return the user name of this documento processo
	*/
	@Override
	public String getUserName() {
		return _documentoProcesso.getUserName();
	}

	/**
	* Returns the user uuid of this documento processo.
	*
	* @return the user uuid of this documento processo
	*/
	@Override
	public String getUserUuid() {
		return _documentoProcesso.getUserUuid();
	}

	/**
	* Returns the uuid of this documento processo.
	*
	* @return the uuid of this documento processo
	*/
	@Override
	public String getUuid() {
		return _documentoProcesso.getUuid();
	}

	@Override
	public int hashCode() {
		return _documentoProcesso.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _documentoProcesso.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _documentoProcesso.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _documentoProcesso.isNew();
	}

	@Override
	public void persist() {
		_documentoProcesso.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_documentoProcesso.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this documento processo.
	*
	* @param companyId the company ID of this documento processo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_documentoProcesso.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this documento processo.
	*
	* @param createDate the create date of this documento processo
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_documentoProcesso.setCreateDate(createDate);
	}

	/**
	* Sets the dettaglio processo ID of this documento processo.
	*
	* @param dettaglioProcessoId the dettaglio processo ID of this documento processo
	*/
	@Override
	public void setDettaglioProcessoId(long dettaglioProcessoId) {
		_documentoProcesso.setDettaglioProcessoId(dettaglioProcessoId);
	}

	/**
	* Sets the dl file ID of this documento processo.
	*
	* @param dlFileId the dl file ID of this documento processo
	*/
	@Override
	public void setDlFileId(long dlFileId) {
		_documentoProcesso.setDlFileId(dlFileId);
	}

	/**
	* Sets the documento processo ID of this documento processo.
	*
	* @param documentoProcessoId the documento processo ID of this documento processo
	*/
	@Override
	public void setDocumentoProcessoId(long documentoProcessoId) {
		_documentoProcesso.setDocumentoProcessoId(documentoProcessoId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_documentoProcesso.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_documentoProcesso.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_documentoProcesso.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this documento processo.
	*
	* @param groupId the group ID of this documento processo
	*/
	@Override
	public void setGroupId(long groupId) {
		_documentoProcesso.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this documento processo.
	*
	* @param modifiedDate the modified date of this documento processo
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_documentoProcesso.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_documentoProcesso.setNew(n);
	}

	/**
	* Sets the primary key of this documento processo.
	*
	* @param primaryKey the primary key of this documento processo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_documentoProcesso.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_documentoProcesso.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the processo ID of this documento processo.
	*
	* @param processoId the processo ID of this documento processo
	*/
	@Override
	public void setProcessoId(long processoId) {
		_documentoProcesso.setProcessoId(processoId);
	}

	/**
	* Sets the user ID of this documento processo.
	*
	* @param userId the user ID of this documento processo
	*/
	@Override
	public void setUserId(long userId) {
		_documentoProcesso.setUserId(userId);
	}

	/**
	* Sets the user name of this documento processo.
	*
	* @param userName the user name of this documento processo
	*/
	@Override
	public void setUserName(String userName) {
		_documentoProcesso.setUserName(userName);
	}

	/**
	* Sets the user uuid of this documento processo.
	*
	* @param userUuid the user uuid of this documento processo
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_documentoProcesso.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this documento processo.
	*
	* @param uuid the uuid of this documento processo
	*/
	@Override
	public void setUuid(String uuid) {
		_documentoProcesso.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<DocumentoProcesso> toCacheModel() {
		return _documentoProcesso.toCacheModel();
	}

	@Override
	public DocumentoProcesso toEscapedModel() {
		return new DocumentoProcessoWrapper(_documentoProcesso.toEscapedModel());
	}

	@Override
	public String toString() {
		return _documentoProcesso.toString();
	}

	@Override
	public DocumentoProcesso toUnescapedModel() {
		return new DocumentoProcessoWrapper(_documentoProcesso.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _documentoProcesso.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DocumentoProcessoWrapper)) {
			return false;
		}

		DocumentoProcessoWrapper documentoProcessoWrapper = (DocumentoProcessoWrapper)obj;

		if (Objects.equals(_documentoProcesso,
					documentoProcessoWrapper._documentoProcesso)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _documentoProcesso.getStagedModelType();
	}

	@Override
	public DocumentoProcesso getWrappedModel() {
		return _documentoProcesso;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _documentoProcesso.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _documentoProcesso.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_documentoProcesso.resetOriginalValues();
	}

	private final DocumentoProcesso _documentoProcesso;
}