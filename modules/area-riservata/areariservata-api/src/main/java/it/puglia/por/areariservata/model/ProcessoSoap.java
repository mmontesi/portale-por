/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.puglia.por.areariservata.service.http.ProcessoServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.http.ProcessoServiceSoap
 * @generated
 */
@ProviderType
public class ProcessoSoap implements Serializable {
	public static ProcessoSoap toSoapModel(Processo model) {
		ProcessoSoap soapModel = new ProcessoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setProcessoId(model.getProcessoId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNomeProcesso(model.getNomeProcesso());
		soapModel.setDescrizioneProcesso(model.getDescrizioneProcesso());
		soapModel.setIsVisible(model.isIsVisible());
		soapModel.setFolderId(model.getFolderId());
		soapModel.setNomeWorkflow(model.getNomeWorkflow());
		soapModel.setStatoWorkflow(model.getStatoWorkflow());
		soapModel.setGruppoMittenteId(model.getGruppoMittenteId());

		return soapModel;
	}

	public static ProcessoSoap[] toSoapModels(Processo[] models) {
		ProcessoSoap[] soapModels = new ProcessoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProcessoSoap[][] toSoapModels(Processo[][] models) {
		ProcessoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProcessoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProcessoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProcessoSoap[] toSoapModels(List<Processo> models) {
		List<ProcessoSoap> soapModels = new ArrayList<ProcessoSoap>(models.size());

		for (Processo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProcessoSoap[soapModels.size()]);
	}

	public ProcessoSoap() {
	}

	public long getPrimaryKey() {
		return _processoId;
	}

	public void setPrimaryKey(long pk) {
		setProcessoId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getProcessoId() {
		return _processoId;
	}

	public void setProcessoId(long processoId) {
		_processoId = processoId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNomeProcesso() {
		return _nomeProcesso;
	}

	public void setNomeProcesso(String nomeProcesso) {
		_nomeProcesso = nomeProcesso;
	}

	public String getDescrizioneProcesso() {
		return _descrizioneProcesso;
	}

	public void setDescrizioneProcesso(String descrizioneProcesso) {
		_descrizioneProcesso = descrizioneProcesso;
	}

	public boolean getIsVisible() {
		return _isVisible;
	}

	public boolean isIsVisible() {
		return _isVisible;
	}

	public void setIsVisible(boolean isVisible) {
		_isVisible = isVisible;
	}

	public long getFolderId() {
		return _folderId;
	}

	public void setFolderId(long folderId) {
		_folderId = folderId;
	}

	public String getNomeWorkflow() {
		return _nomeWorkflow;
	}

	public void setNomeWorkflow(String nomeWorkflow) {
		_nomeWorkflow = nomeWorkflow;
	}

	public String getStatoWorkflow() {
		return _statoWorkflow;
	}

	public void setStatoWorkflow(String statoWorkflow) {
		_statoWorkflow = statoWorkflow;
	}

	public long getGruppoMittenteId() {
		return _gruppoMittenteId;
	}

	public void setGruppoMittenteId(long gruppoMittenteId) {
		_gruppoMittenteId = gruppoMittenteId;
	}

	private String _uuid;
	private long _processoId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nomeProcesso;
	private String _descrizioneProcesso;
	private boolean _isVisible;
	private long _folderId;
	private String _nomeWorkflow;
	private String _statoWorkflow;
	private long _gruppoMittenteId;
}