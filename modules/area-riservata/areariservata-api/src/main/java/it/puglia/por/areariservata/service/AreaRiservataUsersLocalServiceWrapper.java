/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AreaRiservataUsersLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AreaRiservataUsersLocalService
 * @generated
 */
@ProviderType
public class AreaRiservataUsersLocalServiceWrapper
	implements AreaRiservataUsersLocalService,
		ServiceWrapper<AreaRiservataUsersLocalService> {
	public AreaRiservataUsersLocalServiceWrapper(
		AreaRiservataUsersLocalService areaRiservataUsersLocalService) {
		_areaRiservataUsersLocalService = areaRiservataUsersLocalService;
	}

	@Override
	public int countUserOfAreaRiservata() {
		return _areaRiservataUsersLocalService.countUserOfAreaRiservata();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _areaRiservataUsersLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.Map<com.liferay.portal.kernel.model.UserGroup, java.util.List<com.liferay.portal.kernel.model.User>> getUserGroupAndUserOfAreaRiservata() {
		return _areaRiservataUsersLocalService.getUserGroupAndUserOfAreaRiservata();
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.model.User> getUserOfAreaRiservata(
		int start, int end) {
		return _areaRiservataUsersLocalService.getUserOfAreaRiservata(start, end);
	}

	@Override
	public AreaRiservataUsersLocalService getWrappedService() {
		return _areaRiservataUsersLocalService;
	}

	@Override
	public void setWrappedService(
		AreaRiservataUsersLocalService areaRiservataUsersLocalService) {
		_areaRiservataUsersLocalService = areaRiservataUsersLocalService;
	}

	private AreaRiservataUsersLocalService _areaRiservataUsersLocalService;
}