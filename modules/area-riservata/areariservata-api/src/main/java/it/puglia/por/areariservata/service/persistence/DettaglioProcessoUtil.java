/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.DettaglioProcesso;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the dettaglio processo service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.DettaglioProcessoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.DettaglioProcessoPersistenceImpl
 * @generated
 */
@ProviderType
public class DettaglioProcessoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(DettaglioProcesso dettaglioProcesso) {
		getPersistence().clearCache(dettaglioProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DettaglioProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DettaglioProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DettaglioProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static DettaglioProcesso update(DettaglioProcesso dettaglioProcesso) {
		return getPersistence().update(dettaglioProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static DettaglioProcesso update(
		DettaglioProcesso dettaglioProcesso, ServiceContext serviceContext) {
		return getPersistence().update(dettaglioProcesso, serviceContext);
	}

	/**
	* Returns all the dettaglio processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByUuid_First(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUuid_First(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByUuid_Last(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso[] findByUuid_PrevAndNext(
		long dettaglioProcessoId, String uuid,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(dettaglioProcessoId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the dettaglio processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of dettaglio processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching dettaglio processos
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the dettaglio processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the dettaglio processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the dettaglio processo that was removed
	*/
	public static DettaglioProcesso removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of dettaglio processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching dettaglio processos
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByUuid_C_First(String uuid,
		long companyId, OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUuid_C_First(String uuid,
		long companyId, OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso[] findByUuid_C_PrevAndNext(
		long dettaglioProcessoId, String uuid, long companyId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(dettaglioProcessoId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the dettaglio processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of dettaglio processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching dettaglio processos
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the dettaglio processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByprocesso(Long processoId) {
		return getPersistence().findByprocesso(processoId);
	}

	/**
	* Returns a range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end) {
		return getPersistence().findByprocesso(processoId, start, end);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .findByprocesso(processoId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the dettaglio processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching dettaglio processos
	*/
	public static List<DettaglioProcesso> findByprocesso(Long processoId,
		int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByprocesso(processoId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByprocesso_First(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByprocesso_First(processoId, orderByComparator);
	}

	/**
	* Returns the first dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByprocesso_First(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocesso_First(processoId, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso findByprocesso_Last(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByprocesso_Last(processoId, orderByComparator);
	}

	/**
	* Returns the last dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	public static DettaglioProcesso fetchByprocesso_Last(Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocesso_Last(processoId, orderByComparator);
	}

	/**
	* Returns the dettaglio processos before and after the current dettaglio processo in the ordered set where processoId = &#63;.
	*
	* @param dettaglioProcessoId the primary key of the current dettaglio processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso[] findByprocesso_PrevAndNext(
		long dettaglioProcessoId, Long processoId,
		OrderByComparator<DettaglioProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence()
				   .findByprocesso_PrevAndNext(dettaglioProcessoId, processoId,
			orderByComparator);
	}

	/**
	* Removes all the dettaglio processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public static void removeByprocesso(Long processoId) {
		getPersistence().removeByprocesso(processoId);
	}

	/**
	* Returns the number of dettaglio processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching dettaglio processos
	*/
	public static int countByprocesso(Long processoId) {
		return getPersistence().countByprocesso(processoId);
	}

	/**
	* Caches the dettaglio processo in the entity cache if it is enabled.
	*
	* @param dettaglioProcesso the dettaglio processo
	*/
	public static void cacheResult(DettaglioProcesso dettaglioProcesso) {
		getPersistence().cacheResult(dettaglioProcesso);
	}

	/**
	* Caches the dettaglio processos in the entity cache if it is enabled.
	*
	* @param dettaglioProcessos the dettaglio processos
	*/
	public static void cacheResult(List<DettaglioProcesso> dettaglioProcessos) {
		getPersistence().cacheResult(dettaglioProcessos);
	}

	/**
	* Creates a new dettaglio processo with the primary key. Does not add the dettaglio processo to the database.
	*
	* @param dettaglioProcessoId the primary key for the new dettaglio processo
	* @return the new dettaglio processo
	*/
	public static DettaglioProcesso create(long dettaglioProcessoId) {
		return getPersistence().create(dettaglioProcessoId);
	}

	/**
	* Removes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo that was removed
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso remove(long dettaglioProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().remove(dettaglioProcessoId);
	}

	public static DettaglioProcesso updateImpl(
		DettaglioProcesso dettaglioProcesso) {
		return getPersistence().updateImpl(dettaglioProcesso);
	}

	/**
	* Returns the dettaglio processo with the primary key or throws a {@link NoSuchDettaglioProcessoException} if it could not be found.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo
	* @throws NoSuchDettaglioProcessoException if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso findByPrimaryKey(long dettaglioProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchDettaglioProcessoException {
		return getPersistence().findByPrimaryKey(dettaglioProcessoId);
	}

	/**
	* Returns the dettaglio processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo, or <code>null</code> if a dettaglio processo with the primary key could not be found
	*/
	public static DettaglioProcesso fetchByPrimaryKey(long dettaglioProcessoId) {
		return getPersistence().fetchByPrimaryKey(dettaglioProcessoId);
	}

	public static java.util.Map<java.io.Serializable, DettaglioProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the dettaglio processos.
	*
	* @return the dettaglio processos
	*/
	public static List<DettaglioProcesso> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of dettaglio processos
	*/
	public static List<DettaglioProcesso> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of dettaglio processos
	*/
	public static List<DettaglioProcesso> findAll(int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of dettaglio processos
	*/
	public static List<DettaglioProcesso> findAll(int start, int end,
		OrderByComparator<DettaglioProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the dettaglio processos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of dettaglio processos.
	*
	* @return the number of dettaglio processos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static DettaglioProcessoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DettaglioProcessoPersistence, DettaglioProcessoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DettaglioProcessoPersistence.class);

		ServiceTracker<DettaglioProcessoPersistence, DettaglioProcessoPersistence> serviceTracker =
			new ServiceTracker<DettaglioProcessoPersistence, DettaglioProcessoPersistence>(bundle.getBundleContext(),
				DettaglioProcessoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}