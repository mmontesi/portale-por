/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProcessoService}.
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoService
 * @generated
 */
@ProviderType
public class ProcessoServiceWrapper implements ProcessoService,
	ServiceWrapper<ProcessoService> {
	public ProcessoServiceWrapper(ProcessoService processoService) {
		_processoService = processoService;
	}

	@Override
	public it.puglia.por.areariservata.model.Processo addProcesso(
		long groupId, long userId, long companyId, long currentUserId,
		long scopeGroupId, String nomeProcesso, String descrizioneProcesso,
		String workflowName, String startingState, long gruppoMittenteId,
		long[] idUserToAssign,
		com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _processoService.addProcesso(groupId, userId, companyId,
			currentUserId, scopeGroupId, nomeProcesso, descrizioneProcesso,
			workflowName, startingState, gruppoMittenteId, idUserToAssign,
			serviceContext);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _processoService.getOSGiServiceIdentifier();
	}

	@Override
	public it.puglia.por.areariservata.model.Processo getProcesso(
		long processoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _processoService.getProcesso(processoId);
	}

	@Override
	public ProcessoService getWrappedService() {
		return _processoService;
	}

	@Override
	public void setWrappedService(ProcessoService processoService) {
		_processoService = processoService;
	}

	private ProcessoService _processoService;
}