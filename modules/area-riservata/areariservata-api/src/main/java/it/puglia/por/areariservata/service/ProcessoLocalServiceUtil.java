/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Processo. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.ProcessoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoLocalService
 * @see it.puglia.por.areariservata.service.base.ProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.ProcessoLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProcessoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.ProcessoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static it.puglia.por.areariservata.model.Processo addProcesso(
		long groupId, long userId, long companyId, long currentUserId,
		long scopeGroupIp, String nomeProcesso, String descrizioneProcesso,
		String workflowName, String startingState, long gruppoMittenteId,
		long[] idUserToAssign,
		com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addProcesso(groupId, userId, companyId, currentUserId,
			scopeGroupIp, nomeProcesso, descrizioneProcesso, workflowName,
			startingState, gruppoMittenteId, idUserToAssign, serviceContext);
	}

	/**
	* Adds the processo to the database. Also notifies the appropriate model listeners.
	*
	* @param processo the processo
	* @return the processo that was added
	*/
	public static it.puglia.por.areariservata.model.Processo addProcesso(
		it.puglia.por.areariservata.model.Processo processo) {
		return getService().addProcesso(processo);
	}

	public static int countByIsVisible() {
		return getService().countByIsVisible();
	}

	public static int countByIsVisibleAndInScadenza() {
		return getService().countByIsVisibleAndInScadenza();
	}

	public static int countByIsVisibleAndStato(String stato) {
		return getService().countByIsVisibleAndStato(stato);
	}

	public static int countProcessoInScadenza() {
		return getService().countProcessoInScadenza();
	}

	/**
	* Creates a new processo with the primary key. Does not add the processo to the database.
	*
	* @param processoId the primary key for the new processo
	* @return the new processo
	*/
	public static it.puglia.por.areariservata.model.Processo createProcesso(
		long processoId) {
		return getService().createProcesso(processoId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	/**
	* Deletes the processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param processoId the primary key of the processo
	* @return the processo that was removed
	* @throws PortalException if a processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.Processo deleteProcesso(
		long processoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProcesso(processoId);
	}

	/**
	* Deletes the processo from the database. Also notifies the appropriate model listeners.
	*
	* @param processo the processo
	* @return the processo that was removed
	* @throws PortalException
	*/
	public static it.puglia.por.areariservata.model.Processo deleteProcesso(
		it.puglia.por.areariservata.model.Processo processo)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProcesso(processo);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.puglia.por.areariservata.model.Processo fetchProcesso(
		long processoId) {
		return getService().fetchProcesso(processoId);
	}

	/**
	* Returns the processo matching the UUID and group.
	*
	* @param uuid the processo's UUID
	* @param groupId the primary key of the group
	* @return the matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static it.puglia.por.areariservata.model.Processo fetchProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return getService().fetchProcessoByUuidAndGroupId(uuid, groupId);
	}

	public static java.util.List<it.puglia.por.areariservata.model.Processo> findAllByStato(
		String statoWorkflow) {
		return getService().findAllByStato(statoWorkflow);
	}

	public static java.util.List<it.puglia.por.areariservata.model.Processo> findByIsVisible(
		int start, int end) {
		return getService().findByIsVisible(start, end);
	}

	public static java.util.List<it.puglia.por.areariservata.model.Processo> findByIsVisibleAndIsInScadenza(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.Processo> obc) {
		return getService().findByIsVisibleAndIsInScadenza(start, end, obc);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	public static java.util.List<it.puglia.por.areariservata.model.Processo> getProcessiVisibili(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.Processo> obc) {
		return getService().getProcessiVisibili(start, end, obc);
	}

	public static java.util.List<it.puglia.por.areariservata.model.Processo> getProcessiVisibiliByStato(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.Processo> obc,
		Long idStato) {
		return getService().getProcessiVisibiliByStato(start, end, obc, idStato);
	}

	/**
	* Returns the processo with the primary key.
	*
	* @param processoId the primary key of the processo
	* @return the processo
	* @throws PortalException if a processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.Processo getProcesso(
		long processoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProcesso(processoId);
	}

	/**
	* Returns the processo matching the UUID and group.
	*
	* @param uuid the processo's UUID
	* @param groupId the primary key of the group
	* @return the matching processo
	* @throws PortalException if a matching processo could not be found
	*/
	public static it.puglia.por.areariservata.model.Processo getProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProcessoByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of processos
	*/
	public static java.util.List<it.puglia.por.areariservata.model.Processo> getProcessos(
		int start, int end) {
		return getService().getProcessos(start, end);
	}

	/**
	* Returns all the processos matching the UUID and company.
	*
	* @param uuid the UUID of the processos
	* @param companyId the primary key of the company
	* @return the matching processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.Processo> getProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService().getProcessosByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of processos matching the UUID and company.
	*
	* @param uuid the UUID of the processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.Processo> getProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.Processo> orderByComparator) {
		return getService()
				   .getProcessosByUuidAndCompanyId(uuid, companyId, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of processos.
	*
	* @return the number of processos
	*/
	public static int getProcessosCount() {
		return getService().getProcessosCount();
	}

	public static void updateAsset(long userId,
		it.puglia.por.areariservata.model.Processo processo,
		long[] assetCategoryIds, String[] assetTagNames,
		long[] assetLinkEntryIds, Double priority)
		throws com.liferay.portal.kernel.exception.PortalException {
		getService()
			.updateAsset(userId, processo, assetCategoryIds, assetTagNames,
			assetLinkEntryIds, priority);
	}

	/**
	* Updates the processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param processo the processo
	* @return the processo that was updated
	*/
	public static it.puglia.por.areariservata.model.Processo updateProcesso(
		it.puglia.por.areariservata.model.Processo processo) {
		return getService().updateProcesso(processo);
	}

	public static ProcessoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProcessoLocalService, ProcessoLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ProcessoLocalService.class);

		ServiceTracker<ProcessoLocalService, ProcessoLocalService> serviceTracker =
			new ServiceTracker<ProcessoLocalService, ProcessoLocalService>(bundle.getBundleContext(),
				ProcessoLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}