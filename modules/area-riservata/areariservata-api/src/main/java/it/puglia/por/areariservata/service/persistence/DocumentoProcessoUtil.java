/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.DocumentoProcesso;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the documento processo service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.DocumentoProcessoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.DocumentoProcessoPersistenceImpl
 * @generated
 */
@ProviderType
public class DocumentoProcessoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(DocumentoProcesso documentoProcesso) {
		getPersistence().clearCache(documentoProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<DocumentoProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<DocumentoProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<DocumentoProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static DocumentoProcesso update(DocumentoProcesso documentoProcesso) {
		return getPersistence().update(documentoProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static DocumentoProcesso update(
		DocumentoProcesso documentoProcesso, ServiceContext serviceContext) {
		return getPersistence().update(documentoProcesso, serviceContext);
	}

	/**
	* Returns all the documento processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByUuid_First(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUuid_First(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByUuid_Last(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findByUuid_PrevAndNext(
		long documentoProcessoId, String uuid,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(documentoProcessoId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the documento processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of documento processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching documento processos
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the documento processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the documento processo that was removed
	*/
	public static DocumentoProcesso removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of documento processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching documento processos
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByUuid_C_First(String uuid,
		long companyId, OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUuid_C_First(String uuid,
		long companyId, OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findByUuid_C_PrevAndNext(
		long documentoProcessoId, String uuid, long companyId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(documentoProcessoId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the documento processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of documento processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching documento processos
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the documento processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocesso(long processoId) {
		return getPersistence().findByprocesso(processoId);
	}

	/**
	* Returns a range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end) {
		return getPersistence().findByprocesso(processoId, start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findByprocesso(processoId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByprocesso(processoId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocesso_First(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocesso_First(processoId, orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocesso_First(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocesso_First(processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocesso_Last(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocesso_Last(processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocesso_Last(long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocesso_Last(processoId, orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findByprocesso_PrevAndNext(
		long documentoProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocesso_PrevAndNext(documentoProcessoId, processoId,
			orderByComparator);
	}

	/**
	* Removes all the documento processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public static void removeByprocesso(long processoId) {
		getPersistence().removeByprocesso(processoId);
	}

	/**
	* Returns the number of documento processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public static int countByprocesso(long processoId) {
		return getPersistence().countByprocesso(processoId);
	}

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId) {
		return getPersistence().findBydettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end) {
		return getPersistence()
				   .findBydettaglioProcesso(dettaglioProcessoId, start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findBydettaglioProcesso(dettaglioProcessoId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBydettaglioProcesso(dettaglioProcessoId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findBydettaglioProcesso_First(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findBydettaglioProcesso_First(dettaglioProcessoId,
			orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchBydettaglioProcesso_First(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchBydettaglioProcesso_First(dettaglioProcessoId,
			orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findBydettaglioProcesso_Last(dettaglioProcessoId,
			orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchBydettaglioProcesso_Last(dettaglioProcessoId,
			orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findBydettaglioProcesso_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findBydettaglioProcesso_PrevAndNext(documentoProcessoId,
			dettaglioProcessoId, orderByComparator);
	}

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	*/
	public static void removeBydettaglioProcesso(long dettaglioProcessoId) {
		getPersistence().removeBydettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @return the number of matching documento processos
	*/
	public static int countBydettaglioProcesso(long dettaglioProcessoId) {
		return getPersistence().countBydettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId) {
		return getPersistence()
				   .findByprocessoAndDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end) {
		return getPersistence()
				   .findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByprocessoAndDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoAndDettaglio_First(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoAndDettaglio_First(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoAndDettaglio_Last(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoAndDettaglio_Last(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findByprocessoAndDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoAndDettaglio_PrevAndNext(documentoProcessoId,
			dettaglioProcessoId, processoId, orderByComparator);
	}

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	*/
	public static void removeByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId) {
		getPersistence()
			.removeByprocessoAndDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public static int countByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId) {
		return getPersistence()
				   .countByprocessoAndDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId) {
		return getPersistence()
				   .findByprocessoOrDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end) {
		return getPersistence()
				   .findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			start, end);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public static List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByprocessoOrDettaglio(dettaglioProcessoId, processoId,
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoOrDettaglio_First(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoOrDettaglio_First(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public static DocumentoProcesso findByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoOrDettaglio_Last(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static DocumentoProcesso fetchByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByprocessoOrDettaglio_Last(dettaglioProcessoId,
			processoId, orderByComparator);
	}

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso[] findByprocessoOrDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		OrderByComparator<DocumentoProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence()
				   .findByprocessoOrDettaglio_PrevAndNext(documentoProcessoId,
			dettaglioProcessoId, processoId, orderByComparator);
	}

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	*/
	public static void removeByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId) {
		getPersistence()
			.removeByprocessoOrDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public static int countByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId) {
		return getPersistence()
				   .countByprocessoOrDettaglio(dettaglioProcessoId, processoId);
	}

	/**
	* Caches the documento processo in the entity cache if it is enabled.
	*
	* @param documentoProcesso the documento processo
	*/
	public static void cacheResult(DocumentoProcesso documentoProcesso) {
		getPersistence().cacheResult(documentoProcesso);
	}

	/**
	* Caches the documento processos in the entity cache if it is enabled.
	*
	* @param documentoProcessos the documento processos
	*/
	public static void cacheResult(List<DocumentoProcesso> documentoProcessos) {
		getPersistence().cacheResult(documentoProcessos);
	}

	/**
	* Creates a new documento processo with the primary key. Does not add the documento processo to the database.
	*
	* @param documentoProcessoId the primary key for the new documento processo
	* @return the new documento processo
	*/
	public static DocumentoProcesso create(long documentoProcessoId) {
		return getPersistence().create(documentoProcessoId);
	}

	/**
	* Removes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo that was removed
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso remove(long documentoProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().remove(documentoProcessoId);
	}

	public static DocumentoProcesso updateImpl(
		DocumentoProcesso documentoProcesso) {
		return getPersistence().updateImpl(documentoProcesso);
	}

	/**
	* Returns the documento processo with the primary key or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso findByPrimaryKey(long documentoProcessoId)
		throws it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException {
		return getPersistence().findByPrimaryKey(documentoProcessoId);
	}

	/**
	* Returns the documento processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo, or <code>null</code> if a documento processo with the primary key could not be found
	*/
	public static DocumentoProcesso fetchByPrimaryKey(long documentoProcessoId) {
		return getPersistence().fetchByPrimaryKey(documentoProcessoId);
	}

	public static java.util.Map<java.io.Serializable, DocumentoProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the documento processos.
	*
	* @return the documento processos
	*/
	public static List<DocumentoProcesso> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of documento processos
	*/
	public static List<DocumentoProcesso> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of documento processos
	*/
	public static List<DocumentoProcesso> findAll(int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of documento processos
	*/
	public static List<DocumentoProcesso> findAll(int start, int end,
		OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the documento processos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of documento processos.
	*
	* @return the number of documento processos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static DocumentoProcessoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DocumentoProcessoPersistence, DocumentoProcessoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DocumentoProcessoPersistence.class);

		ServiceTracker<DocumentoProcessoPersistence, DocumentoProcessoPersistence> serviceTracker =
			new ServiceTracker<DocumentoProcessoPersistence, DocumentoProcessoPersistence>(bundle.getBundleContext(),
				DocumentoProcessoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}