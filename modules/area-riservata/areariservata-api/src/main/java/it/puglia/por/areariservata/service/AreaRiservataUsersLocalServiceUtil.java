/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for AreaRiservataUsers. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.AreaRiservataUsersLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AreaRiservataUsersLocalService
 * @see it.puglia.por.areariservata.service.base.AreaRiservataUsersLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.AreaRiservataUsersLocalServiceImpl
 * @generated
 */
@ProviderType
public class AreaRiservataUsersLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.AreaRiservataUsersLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static int countUserOfAreaRiservata() {
		return getService().countUserOfAreaRiservata();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.util.Map<com.liferay.portal.kernel.model.UserGroup, java.util.List<com.liferay.portal.kernel.model.User>> getUserGroupAndUserOfAreaRiservata() {
		return getService().getUserGroupAndUserOfAreaRiservata();
	}

	public static java.util.List<com.liferay.portal.kernel.model.User> getUserOfAreaRiservata(
		int start, int end) {
		return getService().getUserOfAreaRiservata(start, end);
	}

	public static AreaRiservataUsersLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<AreaRiservataUsersLocalService, AreaRiservataUsersLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(AreaRiservataUsersLocalService.class);

		ServiceTracker<AreaRiservataUsersLocalService, AreaRiservataUsersLocalService> serviceTracker =
			new ServiceTracker<AreaRiservataUsersLocalService, AreaRiservataUsersLocalService>(bundle.getBundleContext(),
				AreaRiservataUsersLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}