/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link EmailConfigurazione}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailConfigurazione
 * @generated
 */
@ProviderType
public class EmailConfigurazioneWrapper implements EmailConfigurazione,
	ModelWrapper<EmailConfigurazione> {
	public EmailConfigurazioneWrapper(EmailConfigurazione emailConfigurazione) {
		_emailConfigurazione = emailConfigurazione;
	}

	@Override
	public Class<?> getModelClass() {
		return EmailConfigurazione.class;
	}

	@Override
	public String getModelClassName() {
		return EmailConfigurazione.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("emailConfigurazioneId", getEmailConfigurazioneId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("body", getBody());
		attributes.put("subject", getSubject());
		attributes.put("from", getFrom());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long emailConfigurazioneId = (Long)attributes.get(
				"emailConfigurazioneId");

		if (emailConfigurazioneId != null) {
			setEmailConfigurazioneId(emailConfigurazioneId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String body = (String)attributes.get("body");

		if (body != null) {
			setBody(body);
		}

		String subject = (String)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		String from = (String)attributes.get("from");

		if (from != null) {
			setFrom(from);
		}
	}

	@Override
	public Object clone() {
		return new EmailConfigurazioneWrapper((EmailConfigurazione)_emailConfigurazione.clone());
	}

	@Override
	public int compareTo(EmailConfigurazione emailConfigurazione) {
		return _emailConfigurazione.compareTo(emailConfigurazione);
	}

	/**
	* Returns the body of this email configurazione.
	*
	* @return the body of this email configurazione
	*/
	@Override
	public String getBody() {
		return _emailConfigurazione.getBody();
	}

	/**
	* Returns the company ID of this email configurazione.
	*
	* @return the company ID of this email configurazione
	*/
	@Override
	public long getCompanyId() {
		return _emailConfigurazione.getCompanyId();
	}

	/**
	* Returns the create date of this email configurazione.
	*
	* @return the create date of this email configurazione
	*/
	@Override
	public Date getCreateDate() {
		return _emailConfigurazione.getCreateDate();
	}

	/**
	* Returns the email configurazione ID of this email configurazione.
	*
	* @return the email configurazione ID of this email configurazione
	*/
	@Override
	public long getEmailConfigurazioneId() {
		return _emailConfigurazione.getEmailConfigurazioneId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _emailConfigurazione.getExpandoBridge();
	}

	/**
	* Returns the from of this email configurazione.
	*
	* @return the from of this email configurazione
	*/
	@Override
	public String getFrom() {
		return _emailConfigurazione.getFrom();
	}

	/**
	* Returns the group ID of this email configurazione.
	*
	* @return the group ID of this email configurazione
	*/
	@Override
	public long getGroupId() {
		return _emailConfigurazione.getGroupId();
	}

	/**
	* Returns the modified date of this email configurazione.
	*
	* @return the modified date of this email configurazione
	*/
	@Override
	public Date getModifiedDate() {
		return _emailConfigurazione.getModifiedDate();
	}

	/**
	* Returns the primary key of this email configurazione.
	*
	* @return the primary key of this email configurazione
	*/
	@Override
	public long getPrimaryKey() {
		return _emailConfigurazione.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _emailConfigurazione.getPrimaryKeyObj();
	}

	/**
	* Returns the subject of this email configurazione.
	*
	* @return the subject of this email configurazione
	*/
	@Override
	public String getSubject() {
		return _emailConfigurazione.getSubject();
	}

	/**
	* Returns the user ID of this email configurazione.
	*
	* @return the user ID of this email configurazione
	*/
	@Override
	public long getUserId() {
		return _emailConfigurazione.getUserId();
	}

	/**
	* Returns the user name of this email configurazione.
	*
	* @return the user name of this email configurazione
	*/
	@Override
	public String getUserName() {
		return _emailConfigurazione.getUserName();
	}

	/**
	* Returns the user uuid of this email configurazione.
	*
	* @return the user uuid of this email configurazione
	*/
	@Override
	public String getUserUuid() {
		return _emailConfigurazione.getUserUuid();
	}

	/**
	* Returns the uuid of this email configurazione.
	*
	* @return the uuid of this email configurazione
	*/
	@Override
	public String getUuid() {
		return _emailConfigurazione.getUuid();
	}

	@Override
	public int hashCode() {
		return _emailConfigurazione.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _emailConfigurazione.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _emailConfigurazione.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _emailConfigurazione.isNew();
	}

	@Override
	public void persist() {
		_emailConfigurazione.persist();
	}

	/**
	* Sets the body of this email configurazione.
	*
	* @param body the body of this email configurazione
	*/
	@Override
	public void setBody(String body) {
		_emailConfigurazione.setBody(body);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_emailConfigurazione.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this email configurazione.
	*
	* @param companyId the company ID of this email configurazione
	*/
	@Override
	public void setCompanyId(long companyId) {
		_emailConfigurazione.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this email configurazione.
	*
	* @param createDate the create date of this email configurazione
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_emailConfigurazione.setCreateDate(createDate);
	}

	/**
	* Sets the email configurazione ID of this email configurazione.
	*
	* @param emailConfigurazioneId the email configurazione ID of this email configurazione
	*/
	@Override
	public void setEmailConfigurazioneId(long emailConfigurazioneId) {
		_emailConfigurazione.setEmailConfigurazioneId(emailConfigurazioneId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_emailConfigurazione.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_emailConfigurazione.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_emailConfigurazione.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the from of this email configurazione.
	*
	* @param from the from of this email configurazione
	*/
	@Override
	public void setFrom(String from) {
		_emailConfigurazione.setFrom(from);
	}

	/**
	* Sets the group ID of this email configurazione.
	*
	* @param groupId the group ID of this email configurazione
	*/
	@Override
	public void setGroupId(long groupId) {
		_emailConfigurazione.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this email configurazione.
	*
	* @param modifiedDate the modified date of this email configurazione
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_emailConfigurazione.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_emailConfigurazione.setNew(n);
	}

	/**
	* Sets the primary key of this email configurazione.
	*
	* @param primaryKey the primary key of this email configurazione
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_emailConfigurazione.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_emailConfigurazione.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the subject of this email configurazione.
	*
	* @param subject the subject of this email configurazione
	*/
	@Override
	public void setSubject(String subject) {
		_emailConfigurazione.setSubject(subject);
	}

	/**
	* Sets the user ID of this email configurazione.
	*
	* @param userId the user ID of this email configurazione
	*/
	@Override
	public void setUserId(long userId) {
		_emailConfigurazione.setUserId(userId);
	}

	/**
	* Sets the user name of this email configurazione.
	*
	* @param userName the user name of this email configurazione
	*/
	@Override
	public void setUserName(String userName) {
		_emailConfigurazione.setUserName(userName);
	}

	/**
	* Sets the user uuid of this email configurazione.
	*
	* @param userUuid the user uuid of this email configurazione
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_emailConfigurazione.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this email configurazione.
	*
	* @param uuid the uuid of this email configurazione
	*/
	@Override
	public void setUuid(String uuid) {
		_emailConfigurazione.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<EmailConfigurazione> toCacheModel() {
		return _emailConfigurazione.toCacheModel();
	}

	@Override
	public EmailConfigurazione toEscapedModel() {
		return new EmailConfigurazioneWrapper(_emailConfigurazione.toEscapedModel());
	}

	@Override
	public String toString() {
		return _emailConfigurazione.toString();
	}

	@Override
	public EmailConfigurazione toUnescapedModel() {
		return new EmailConfigurazioneWrapper(_emailConfigurazione.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _emailConfigurazione.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailConfigurazioneWrapper)) {
			return false;
		}

		EmailConfigurazioneWrapper emailConfigurazioneWrapper = (EmailConfigurazioneWrapper)obj;

		if (Objects.equals(_emailConfigurazione,
					emailConfigurazioneWrapper._emailConfigurazione)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _emailConfigurazione.getStagedModelType();
	}

	@Override
	public EmailConfigurazione getWrappedModel() {
		return _emailConfigurazione;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _emailConfigurazione.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _emailConfigurazione.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_emailConfigurazione.resetOriginalValues();
	}

	private final EmailConfigurazione _emailConfigurazione;
}