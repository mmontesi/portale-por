/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException;
import it.puglia.por.areariservata.model.GruppiUtentiProcesso;

/**
 * The persistence interface for the gruppi utenti processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.persistence.impl.GruppiUtentiProcessoPersistenceImpl
 * @see GruppiUtentiProcessoUtil
 * @generated
 */
@ProviderType
public interface GruppiUtentiProcessoPersistence extends BasePersistence<GruppiUtentiProcesso> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link GruppiUtentiProcessoUtil} to access the gruppi utenti processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the gruppi utenti processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid(String uuid);

	/**
	* Returns a range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid(String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid(String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid(String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns the gruppi utenti processos before and after the current gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param gupId the primary key of the current gruppi utenti processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public GruppiUtentiProcesso[] findByUuid_PrevAndNext(long gupId,
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Removes all the gruppi utenti processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching gruppi utenti processos
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchGruppiUtentiProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the gruppi utenti processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the gruppi utenti processo that was removed
	*/
	public GruppiUtentiProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching gruppi utenti processos
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId);

	/**
	* Returns a range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUuid_C_First(String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public GruppiUtentiProcesso fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns the gruppi utenti processos before and after the current gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param gupId the primary key of the current gruppi utenti processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public GruppiUtentiProcesso[] findByUuid_C_PrevAndNext(long gupId,
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Removes all the gruppi utenti processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching gruppi utenti processos
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Caches the gruppi utenti processo in the entity cache if it is enabled.
	*
	* @param gruppiUtentiProcesso the gruppi utenti processo
	*/
	public void cacheResult(GruppiUtentiProcesso gruppiUtentiProcesso);

	/**
	* Caches the gruppi utenti processos in the entity cache if it is enabled.
	*
	* @param gruppiUtentiProcessos the gruppi utenti processos
	*/
	public void cacheResult(
		java.util.List<GruppiUtentiProcesso> gruppiUtentiProcessos);

	/**
	* Creates a new gruppi utenti processo with the primary key. Does not add the gruppi utenti processo to the database.
	*
	* @param gupId the primary key for the new gruppi utenti processo
	* @return the new gruppi utenti processo
	*/
	public GruppiUtentiProcesso create(long gupId);

	/**
	* Removes the gruppi utenti processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo that was removed
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public GruppiUtentiProcesso remove(long gupId)
		throws NoSuchGruppiUtentiProcessoException;

	public GruppiUtentiProcesso updateImpl(
		GruppiUtentiProcesso gruppiUtentiProcesso);

	/**
	* Returns the gruppi utenti processo with the primary key or throws a {@link NoSuchGruppiUtentiProcessoException} if it could not be found.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public GruppiUtentiProcesso findByPrimaryKey(long gupId)
		throws NoSuchGruppiUtentiProcessoException;

	/**
	* Returns the gruppi utenti processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo, or <code>null</code> if a gruppi utenti processo with the primary key could not be found
	*/
	public GruppiUtentiProcesso fetchByPrimaryKey(long gupId);

	@Override
	public java.util.Map<java.io.Serializable, GruppiUtentiProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the gruppi utenti processos.
	*
	* @return the gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findAll();

	/**
	* Returns a range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findAll(int start, int end);

	/**
	* Returns an ordered range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of gruppi utenti processos
	*/
	public java.util.List<GruppiUtentiProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the gruppi utenti processos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of gruppi utenti processos.
	*
	* @return the number of gruppi utenti processos
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}