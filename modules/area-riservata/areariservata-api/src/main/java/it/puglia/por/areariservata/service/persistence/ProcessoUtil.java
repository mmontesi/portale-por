/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.Processo;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the processo service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.ProcessoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.ProcessoPersistenceImpl
 * @generated
 */
@ProviderType
public class ProcessoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Processo processo) {
		getPersistence().clearCache(processo);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Processo> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Processo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Processo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Processo update(Processo processo) {
		return getPersistence().update(processo);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Processo update(Processo processo,
		ServiceContext serviceContext) {
		return getPersistence().update(processo, serviceContext);
	}

	/**
	* Returns all the processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching processos
	*/
	public static List<Processo> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of matching processos
	*/
	public static List<Processo> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByUuid(String uuid, int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByUuid(String uuid, int start, int end,
		OrderByComparator<Processo> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByUuid_First(String uuid,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUuid_First(String uuid,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByUuid_Last(String uuid,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUuid_Last(String uuid,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the processos before and after the current processo in the ordered set where uuid = &#63;.
	*
	* @param processoId the primary key of the current processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next processo
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo[] findByUuid_PrevAndNext(long processoId,
		String uuid, OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(processoId, uuid, orderByComparator);
	}

	/**
	* Removes all the processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching processos
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the processo that was removed
	*/
	public static Processo removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching processos
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching processos
	*/
	public static List<Processo> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of matching processos
	*/
	public static List<Processo> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<Processo> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the processos before and after the current processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param processoId the primary key of the current processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next processo
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo[] findByUuid_C_PrevAndNext(long processoId,
		String uuid, long companyId,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(processoId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching processos
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the processos where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @return the matching processos
	*/
	public static List<Processo> findBystato(String statoWorkflow) {
		return getPersistence().findBystato(statoWorkflow);
	}

	/**
	* Returns a range of all the processos where statoWorkflow = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param statoWorkflow the stato workflow
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of matching processos
	*/
	public static List<Processo> findBystato(String statoWorkflow, int start,
		int end) {
		return getPersistence().findBystato(statoWorkflow, start, end);
	}

	/**
	* Returns an ordered range of all the processos where statoWorkflow = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param statoWorkflow the stato workflow
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findBystato(String statoWorkflow, int start,
		int end, OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .findBystato(statoWorkflow, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the processos where statoWorkflow = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param statoWorkflow the stato workflow
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findBystato(String statoWorkflow, int start,
		int end, OrderByComparator<Processo> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findBystato(statoWorkflow, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first processo in the ordered set where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findBystato_First(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findBystato_First(statoWorkflow, orderByComparator);
	}

	/**
	* Returns the first processo in the ordered set where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchBystato_First(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchBystato_First(statoWorkflow, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findBystato_Last(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findBystato_Last(statoWorkflow, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchBystato_Last(String statoWorkflow,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchBystato_Last(statoWorkflow, orderByComparator);
	}

	/**
	* Returns the processos before and after the current processo in the ordered set where statoWorkflow = &#63;.
	*
	* @param processoId the primary key of the current processo
	* @param statoWorkflow the stato workflow
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next processo
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo[] findBystato_PrevAndNext(long processoId,
		String statoWorkflow, OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findBystato_PrevAndNext(processoId, statoWorkflow,
			orderByComparator);
	}

	/**
	* Removes all the processos where statoWorkflow = &#63; from the database.
	*
	* @param statoWorkflow the stato workflow
	*/
	public static void removeBystato(String statoWorkflow) {
		getPersistence().removeBystato(statoWorkflow);
	}

	/**
	* Returns the number of processos where statoWorkflow = &#63;.
	*
	* @param statoWorkflow the stato workflow
	* @return the number of matching processos
	*/
	public static int countBystato(String statoWorkflow) {
		return getPersistence().countBystato(statoWorkflow);
	}

	/**
	* Returns all the processos where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @return the matching processos
	*/
	public static List<Processo> findByisVisible(boolean isVisible) {
		return getPersistence().findByisVisible(isVisible);
	}

	/**
	* Returns a range of all the processos where isVisible = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isVisible the is visible
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of matching processos
	*/
	public static List<Processo> findByisVisible(boolean isVisible, int start,
		int end) {
		return getPersistence().findByisVisible(isVisible, start, end);
	}

	/**
	* Returns an ordered range of all the processos where isVisible = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isVisible the is visible
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByisVisible(boolean isVisible, int start,
		int end, OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .findByisVisible(isVisible, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the processos where isVisible = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isVisible the is visible
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching processos
	*/
	public static List<Processo> findByisVisible(boolean isVisible, int start,
		int end, OrderByComparator<Processo> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByisVisible(isVisible, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first processo in the ordered set where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByisVisible_First(boolean isVisible,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByisVisible_First(isVisible, orderByComparator);
	}

	/**
	* Returns the first processo in the ordered set where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByisVisible_First(boolean isVisible,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchByisVisible_First(isVisible, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo
	* @throws NoSuchProcessoException if a matching processo could not be found
	*/
	public static Processo findByisVisible_Last(boolean isVisible,
		OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByisVisible_Last(isVisible, orderByComparator);
	}

	/**
	* Returns the last processo in the ordered set where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching processo, or <code>null</code> if a matching processo could not be found
	*/
	public static Processo fetchByisVisible_Last(boolean isVisible,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence()
				   .fetchByisVisible_Last(isVisible, orderByComparator);
	}

	/**
	* Returns the processos before and after the current processo in the ordered set where isVisible = &#63;.
	*
	* @param processoId the primary key of the current processo
	* @param isVisible the is visible
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next processo
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo[] findByisVisible_PrevAndNext(long processoId,
		boolean isVisible, OrderByComparator<Processo> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence()
				   .findByisVisible_PrevAndNext(processoId, isVisible,
			orderByComparator);
	}

	/**
	* Removes all the processos where isVisible = &#63; from the database.
	*
	* @param isVisible the is visible
	*/
	public static void removeByisVisible(boolean isVisible) {
		getPersistence().removeByisVisible(isVisible);
	}

	/**
	* Returns the number of processos where isVisible = &#63;.
	*
	* @param isVisible the is visible
	* @return the number of matching processos
	*/
	public static int countByisVisible(boolean isVisible) {
		return getPersistence().countByisVisible(isVisible);
	}

	/**
	* Caches the processo in the entity cache if it is enabled.
	*
	* @param processo the processo
	*/
	public static void cacheResult(Processo processo) {
		getPersistence().cacheResult(processo);
	}

	/**
	* Caches the processos in the entity cache if it is enabled.
	*
	* @param processos the processos
	*/
	public static void cacheResult(List<Processo> processos) {
		getPersistence().cacheResult(processos);
	}

	/**
	* Creates a new processo with the primary key. Does not add the processo to the database.
	*
	* @param processoId the primary key for the new processo
	* @return the new processo
	*/
	public static Processo create(long processoId) {
		return getPersistence().create(processoId);
	}

	/**
	* Removes the processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param processoId the primary key of the processo
	* @return the processo that was removed
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo remove(long processoId)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().remove(processoId);
	}

	public static Processo updateImpl(Processo processo) {
		return getPersistence().updateImpl(processo);
	}

	/**
	* Returns the processo with the primary key or throws a {@link NoSuchProcessoException} if it could not be found.
	*
	* @param processoId the primary key of the processo
	* @return the processo
	* @throws NoSuchProcessoException if a processo with the primary key could not be found
	*/
	public static Processo findByPrimaryKey(long processoId)
		throws it.puglia.por.areariservata.exception.NoSuchProcessoException {
		return getPersistence().findByPrimaryKey(processoId);
	}

	/**
	* Returns the processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param processoId the primary key of the processo
	* @return the processo, or <code>null</code> if a processo with the primary key could not be found
	*/
	public static Processo fetchByPrimaryKey(long processoId) {
		return getPersistence().fetchByPrimaryKey(processoId);
	}

	public static java.util.Map<java.io.Serializable, Processo> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the processos.
	*
	* @return the processos
	*/
	public static List<Processo> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @return the range of processos
	*/
	public static List<Processo> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of processos
	*/
	public static List<Processo> findAll(int start, int end,
		OrderByComparator<Processo> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of processos
	* @param end the upper bound of the range of processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of processos
	*/
	public static List<Processo> findAll(int start, int end,
		OrderByComparator<Processo> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the processos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of processos.
	*
	* @return the number of processos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProcessoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProcessoPersistence, ProcessoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ProcessoPersistence.class);

		ServiceTracker<ProcessoPersistence, ProcessoPersistence> serviceTracker = new ServiceTracker<ProcessoPersistence, ProcessoPersistence>(bundle.getBundleContext(),
				ProcessoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}