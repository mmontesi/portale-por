/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DocumentoProcessoService}.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoService
 * @generated
 */
@ProviderType
public class DocumentoProcessoServiceWrapper implements DocumentoProcessoService,
	ServiceWrapper<DocumentoProcessoService> {
	public DocumentoProcessoServiceWrapper(
		DocumentoProcessoService documentoProcessoService) {
		_documentoProcessoService = documentoProcessoService;
	}

	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromDettaglio(
		Long idProcesso, Long idDettaglio) {
		return _documentoProcessoService.getAllDocumentsFromDettaglio(idProcesso,
			idDettaglio);
	}

	@Override
	public java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromProcesso(
		Long idProcesso) {
		return _documentoProcessoService.getAllDocumentsFromProcesso(idProcesso);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _documentoProcessoService.getOSGiServiceIdentifier();
	}

	@Override
	public DocumentoProcessoService getWrappedService() {
		return _documentoProcessoService;
	}

	@Override
	public void setWrappedService(
		DocumentoProcessoService documentoProcessoService) {
		_documentoProcessoService = documentoProcessoService;
	}

	private DocumentoProcessoService _documentoProcessoService;
}