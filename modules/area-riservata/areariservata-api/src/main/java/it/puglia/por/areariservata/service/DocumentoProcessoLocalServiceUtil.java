/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for DocumentoProcesso. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.DocumentoProcessoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoLocalService
 * @see it.puglia.por.areariservata.service.base.DocumentoProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.DocumentoProcessoLocalServiceImpl
 * @generated
 */
@ProviderType
public class DocumentoProcessoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.DocumentoProcessoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the documento processo to the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was added
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso addDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return getService().addDocumentoProcesso(documentoProcesso);
	}

	/**
	* Creates a new documento processo with the primary key. Does not add the documento processo to the database.
	*
	* @param documentoProcessoId the primary key for the new documento processo
	* @return the new documento processo
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso createDocumentoProcesso(
		long documentoProcessoId) {
		return getService().createDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Deletes the documento processo from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was removed
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso deleteDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return getService().deleteDocumentoProcesso(documentoProcesso);
	}

	/**
	* Deletes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo that was removed
	* @throws PortalException if a documento processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso deleteDocumentoProcesso(
		long documentoProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteDocumentoProcesso(documentoProcessoId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.puglia.por.areariservata.model.DocumentoProcesso fetchDocumentoProcesso(
		long documentoProcessoId) {
		return getService().fetchDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Returns the documento processo matching the UUID and group.
	*
	* @param uuid the documento processo's UUID
	* @param groupId the primary key of the group
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso fetchDocumentoProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return getService().fetchDocumentoProcessoByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromDettaglio(
		Long idProcesso, Long idDettaglio) {
		return getService().getAllDocumentsFromDettaglio(idProcesso, idDettaglio);
	}

	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromProcesso(
		Long idProcesso) {
		return getService().getAllDocumentsFromProcesso(idProcesso);
	}

	/**
	* Returns the documento processo with the primary key.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo
	* @throws PortalException if a documento processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso getDocumentoProcesso(
		long documentoProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getDocumentoProcesso(documentoProcessoId);
	}

	/**
	* Returns the documento processo matching the UUID and group.
	*
	* @param uuid the documento processo's UUID
	* @param groupId the primary key of the group
	* @return the matching documento processo
	* @throws PortalException if a matching documento processo could not be found
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso getDocumentoProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getDocumentoProcessoByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of documento processos
	*/
	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessos(
		int start, int end) {
		return getService().getDocumentoProcessos(start, end);
	}

	/**
	* Returns all the documento processos matching the UUID and company.
	*
	* @param uuid the UUID of the documento processos
	* @param companyId the primary key of the company
	* @return the matching documento processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService()
				   .getDocumentoProcessosByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of documento processos matching the UUID and company.
	*
	* @param uuid the UUID of the documento processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching documento processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getDocumentoProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.DocumentoProcesso> orderByComparator) {
		return getService()
				   .getDocumentoProcessosByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of documento processos.
	*
	* @return the number of documento processos
	*/
	public static int getDocumentoProcessosCount() {
		return getService().getDocumentoProcessosCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the documento processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param documentoProcesso the documento processo
	* @return the documento processo that was updated
	*/
	public static it.puglia.por.areariservata.model.DocumentoProcesso updateDocumentoProcesso(
		it.puglia.por.areariservata.model.DocumentoProcesso documentoProcesso) {
		return getService().updateDocumentoProcesso(documentoProcesso);
	}

	public static DocumentoProcessoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DocumentoProcessoLocalService, DocumentoProcessoLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DocumentoProcessoLocalService.class);

		ServiceTracker<DocumentoProcessoLocalService, DocumentoProcessoLocalService> serviceTracker =
			new ServiceTracker<DocumentoProcessoLocalService, DocumentoProcessoLocalService>(bundle.getBundleContext(),
				DocumentoProcessoLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}