/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for DocumentoProcesso. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.DocumentoProcessoServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see DocumentoProcessoService
 * @see it.puglia.por.areariservata.service.base.DocumentoProcessoServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.DocumentoProcessoServiceImpl
 * @generated
 */
@ProviderType
public class DocumentoProcessoServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.DocumentoProcessoServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromDettaglio(
		Long idProcesso, Long idDettaglio) {
		return getService().getAllDocumentsFromDettaglio(idProcesso, idDettaglio);
	}

	public static java.util.List<it.puglia.por.areariservata.model.DocumentoProcesso> getAllDocumentsFromProcesso(
		Long idProcesso) {
		return getService().getAllDocumentsFromProcesso(idProcesso);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static DocumentoProcessoService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DocumentoProcessoService, DocumentoProcessoService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DocumentoProcessoService.class);

		ServiceTracker<DocumentoProcessoService, DocumentoProcessoService> serviceTracker =
			new ServiceTracker<DocumentoProcessoService, DocumentoProcessoService>(bundle.getBundleContext(),
				DocumentoProcessoService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}