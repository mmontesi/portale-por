/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.puglia.por.areariservata.service.http.DettaglioProcessoServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.http.DettaglioProcessoServiceSoap
 * @generated
 */
@ProviderType
public class DettaglioProcessoSoap implements Serializable {
	public static DettaglioProcessoSoap toSoapModel(DettaglioProcesso model) {
		DettaglioProcessoSoap soapModel = new DettaglioProcessoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setDettaglioProcessoId(model.getDettaglioProcessoId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setProcessoId(model.getProcessoId());
		soapModel.setIsVisible(model.isIsVisible());
		soapModel.setTestoDettaglio(model.getTestoDettaglio());
		soapModel.setDescerizioneBreve(model.getDescerizioneBreve());
		soapModel.setDlFileId(model.getDlFileId());
		soapModel.setNomeWorkflow(model.getNomeWorkflow());
		soapModel.setStatoWorkflow(model.getStatoWorkflow());

		return soapModel;
	}

	public static DettaglioProcessoSoap[] toSoapModels(
		DettaglioProcesso[] models) {
		DettaglioProcessoSoap[] soapModels = new DettaglioProcessoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DettaglioProcessoSoap[][] toSoapModels(
		DettaglioProcesso[][] models) {
		DettaglioProcessoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DettaglioProcessoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DettaglioProcessoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DettaglioProcessoSoap[] toSoapModels(
		List<DettaglioProcesso> models) {
		List<DettaglioProcessoSoap> soapModels = new ArrayList<DettaglioProcessoSoap>(models.size());

		for (DettaglioProcesso model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DettaglioProcessoSoap[soapModels.size()]);
	}

	public DettaglioProcessoSoap() {
	}

	public long getPrimaryKey() {
		return _dettaglioProcessoId;
	}

	public void setPrimaryKey(long pk) {
		setDettaglioProcessoId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getDettaglioProcessoId() {
		return _dettaglioProcessoId;
	}

	public void setDettaglioProcessoId(long dettaglioProcessoId) {
		_dettaglioProcessoId = dettaglioProcessoId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Long getProcessoId() {
		return _processoId;
	}

	public void setProcessoId(Long processoId) {
		_processoId = processoId;
	}

	public boolean getIsVisible() {
		return _isVisible;
	}

	public boolean isIsVisible() {
		return _isVisible;
	}

	public void setIsVisible(boolean isVisible) {
		_isVisible = isVisible;
	}

	public String getTestoDettaglio() {
		return _testoDettaglio;
	}

	public void setTestoDettaglio(String testoDettaglio) {
		_testoDettaglio = testoDettaglio;
	}

	public String getDescerizioneBreve() {
		return _descerizioneBreve;
	}

	public void setDescerizioneBreve(String descerizioneBreve) {
		_descerizioneBreve = descerizioneBreve;
	}

	public long getDlFileId() {
		return _dlFileId;
	}

	public void setDlFileId(long dlFileId) {
		_dlFileId = dlFileId;
	}

	public String getNomeWorkflow() {
		return _nomeWorkflow;
	}

	public void setNomeWorkflow(String nomeWorkflow) {
		_nomeWorkflow = nomeWorkflow;
	}

	public String getStatoWorkflow() {
		return _statoWorkflow;
	}

	public void setStatoWorkflow(String statoWorkflow) {
		_statoWorkflow = statoWorkflow;
	}

	private String _uuid;
	private long _dettaglioProcessoId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Long _processoId;
	private boolean _isVisible;
	private String _testoDettaglio;
	private String _descerizioneBreve;
	private long _dlFileId;
	private String _nomeWorkflow;
	private String _statoWorkflow;
}