/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for GruppiUtentiProcesso. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.GruppiUtentiProcessoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see GruppiUtentiProcessoLocalService
 * @see it.puglia.por.areariservata.service.base.GruppiUtentiProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.GruppiUtentiProcessoLocalServiceImpl
 * @generated
 */
@ProviderType
public class GruppiUtentiProcessoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.GruppiUtentiProcessoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the gruppi utenti processo to the database. Also notifies the appropriate model listeners.
	*
	* @param gruppiUtentiProcesso the gruppi utenti processo
	* @return the gruppi utenti processo that was added
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso addGruppiUtentiProcesso(
		it.puglia.por.areariservata.model.GruppiUtentiProcesso gruppiUtentiProcesso) {
		return getService().addGruppiUtentiProcesso(gruppiUtentiProcesso);
	}

	/**
	* Creates a new gruppi utenti processo with the primary key. Does not add the gruppi utenti processo to the database.
	*
	* @param gupId the primary key for the new gruppi utenti processo
	* @return the new gruppi utenti processo
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso createGruppiUtentiProcesso(
		long gupId) {
		return getService().createGruppiUtentiProcesso(gupId);
	}

	/**
	* Deletes the gruppi utenti processo from the database. Also notifies the appropriate model listeners.
	*
	* @param gruppiUtentiProcesso the gruppi utenti processo
	* @return the gruppi utenti processo that was removed
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso deleteGruppiUtentiProcesso(
		it.puglia.por.areariservata.model.GruppiUtentiProcesso gruppiUtentiProcesso) {
		return getService().deleteGruppiUtentiProcesso(gruppiUtentiProcesso);
	}

	/**
	* Deletes the gruppi utenti processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo that was removed
	* @throws PortalException if a gruppi utenti processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso deleteGruppiUtentiProcesso(
		long gupId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteGruppiUtentiProcesso(gupId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso fetchGruppiUtentiProcesso(
		long gupId) {
		return getService().fetchGruppiUtentiProcesso(gupId);
	}

	/**
	* Returns the gruppi utenti processo matching the UUID and group.
	*
	* @param uuid the gruppi utenti processo's UUID
	* @param groupId the primary key of the group
	* @return the matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso fetchGruppiUtentiProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return getService()
				   .fetchGruppiUtentiProcessoByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	/**
	* Returns the gruppi utenti processo with the primary key.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo
	* @throws PortalException if a gruppi utenti processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso getGruppiUtentiProcesso(
		long gupId) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getGruppiUtentiProcesso(gupId);
	}

	/**
	* Returns the gruppi utenti processo matching the UUID and group.
	*
	* @param uuid the gruppi utenti processo's UUID
	* @param groupId the primary key of the group
	* @return the matching gruppi utenti processo
	* @throws PortalException if a matching gruppi utenti processo could not be found
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso getGruppiUtentiProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getGruppiUtentiProcessoByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of gruppi utenti processos
	*/
	public static java.util.List<it.puglia.por.areariservata.model.GruppiUtentiProcesso> getGruppiUtentiProcessos(
		int start, int end) {
		return getService().getGruppiUtentiProcessos(start, end);
	}

	/**
	* Returns all the gruppi utenti processos matching the UUID and company.
	*
	* @param uuid the UUID of the gruppi utenti processos
	* @param companyId the primary key of the company
	* @return the matching gruppi utenti processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.GruppiUtentiProcesso> getGruppiUtentiProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService()
				   .getGruppiUtentiProcessosByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of gruppi utenti processos matching the UUID and company.
	*
	* @param uuid the UUID of the gruppi utenti processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching gruppi utenti processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.GruppiUtentiProcesso> getGruppiUtentiProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.GruppiUtentiProcesso> orderByComparator) {
		return getService()
				   .getGruppiUtentiProcessosByUuidAndCompanyId(uuid, companyId,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of gruppi utenti processos.
	*
	* @return the number of gruppi utenti processos
	*/
	public static int getGruppiUtentiProcessosCount() {
		return getService().getGruppiUtentiProcessosCount();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the gruppi utenti processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param gruppiUtentiProcesso the gruppi utenti processo
	* @return the gruppi utenti processo that was updated
	*/
	public static it.puglia.por.areariservata.model.GruppiUtentiProcesso updateGruppiUtentiProcesso(
		it.puglia.por.areariservata.model.GruppiUtentiProcesso gruppiUtentiProcesso) {
		return getService().updateGruppiUtentiProcesso(gruppiUtentiProcesso);
	}

	public static GruppiUtentiProcessoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<GruppiUtentiProcessoLocalService, GruppiUtentiProcessoLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(GruppiUtentiProcessoLocalService.class);

		ServiceTracker<GruppiUtentiProcessoLocalService, GruppiUtentiProcessoLocalService> serviceTracker =
			new ServiceTracker<GruppiUtentiProcessoLocalService, GruppiUtentiProcessoLocalService>(bundle.getBundleContext(),
				GruppiUtentiProcessoLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}