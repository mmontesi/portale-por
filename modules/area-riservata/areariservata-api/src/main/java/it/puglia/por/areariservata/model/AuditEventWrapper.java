/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link AuditEvent}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AuditEvent
 * @generated
 */
@ProviderType
public class AuditEventWrapper implements AuditEvent, ModelWrapper<AuditEvent> {
	public AuditEventWrapper(AuditEvent auditEvent) {
		_auditEvent = auditEvent;
	}

	@Override
	public Class<?> getModelClass() {
		return AuditEvent.class;
	}

	@Override
	public String getModelClassName() {
		return AuditEvent.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("auditEventId", getAuditEventId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("descriptionEventAudit", getDescriptionEventAudit());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long auditEventId = (Long)attributes.get("auditEventId");

		if (auditEventId != null) {
			setAuditEventId(auditEventId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String descriptionEventAudit = (String)attributes.get(
				"descriptionEventAudit");

		if (descriptionEventAudit != null) {
			setDescriptionEventAudit(descriptionEventAudit);
		}
	}

	@Override
	public Object clone() {
		return new AuditEventWrapper((AuditEvent)_auditEvent.clone());
	}

	@Override
	public int compareTo(AuditEvent auditEvent) {
		return _auditEvent.compareTo(auditEvent);
	}

	/**
	* Returns the audit event ID of this audit event.
	*
	* @return the audit event ID of this audit event
	*/
	@Override
	public long getAuditEventId() {
		return _auditEvent.getAuditEventId();
	}

	/**
	* Returns the company ID of this audit event.
	*
	* @return the company ID of this audit event
	*/
	@Override
	public long getCompanyId() {
		return _auditEvent.getCompanyId();
	}

	/**
	* Returns the create date of this audit event.
	*
	* @return the create date of this audit event
	*/
	@Override
	public Date getCreateDate() {
		return _auditEvent.getCreateDate();
	}

	/**
	* Returns the description event audit of this audit event.
	*
	* @return the description event audit of this audit event
	*/
	@Override
	public String getDescriptionEventAudit() {
		return _auditEvent.getDescriptionEventAudit();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _auditEvent.getExpandoBridge();
	}

	/**
	* Returns the group ID of this audit event.
	*
	* @return the group ID of this audit event
	*/
	@Override
	public long getGroupId() {
		return _auditEvent.getGroupId();
	}

	/**
	* Returns the modified date of this audit event.
	*
	* @return the modified date of this audit event
	*/
	@Override
	public Date getModifiedDate() {
		return _auditEvent.getModifiedDate();
	}

	/**
	* Returns the primary key of this audit event.
	*
	* @return the primary key of this audit event
	*/
	@Override
	public long getPrimaryKey() {
		return _auditEvent.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _auditEvent.getPrimaryKeyObj();
	}

	/**
	* Returns the user ID of this audit event.
	*
	* @return the user ID of this audit event
	*/
	@Override
	public long getUserId() {
		return _auditEvent.getUserId();
	}

	/**
	* Returns the user name of this audit event.
	*
	* @return the user name of this audit event
	*/
	@Override
	public String getUserName() {
		return _auditEvent.getUserName();
	}

	/**
	* Returns the user uuid of this audit event.
	*
	* @return the user uuid of this audit event
	*/
	@Override
	public String getUserUuid() {
		return _auditEvent.getUserUuid();
	}

	/**
	* Returns the uuid of this audit event.
	*
	* @return the uuid of this audit event
	*/
	@Override
	public String getUuid() {
		return _auditEvent.getUuid();
	}

	@Override
	public int hashCode() {
		return _auditEvent.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _auditEvent.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _auditEvent.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _auditEvent.isNew();
	}

	@Override
	public void persist() {
		_auditEvent.persist();
	}

	/**
	* Sets the audit event ID of this audit event.
	*
	* @param auditEventId the audit event ID of this audit event
	*/
	@Override
	public void setAuditEventId(long auditEventId) {
		_auditEvent.setAuditEventId(auditEventId);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_auditEvent.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this audit event.
	*
	* @param companyId the company ID of this audit event
	*/
	@Override
	public void setCompanyId(long companyId) {
		_auditEvent.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this audit event.
	*
	* @param createDate the create date of this audit event
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_auditEvent.setCreateDate(createDate);
	}

	/**
	* Sets the description event audit of this audit event.
	*
	* @param descriptionEventAudit the description event audit of this audit event
	*/
	@Override
	public void setDescriptionEventAudit(String descriptionEventAudit) {
		_auditEvent.setDescriptionEventAudit(descriptionEventAudit);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_auditEvent.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_auditEvent.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_auditEvent.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this audit event.
	*
	* @param groupId the group ID of this audit event
	*/
	@Override
	public void setGroupId(long groupId) {
		_auditEvent.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this audit event.
	*
	* @param modifiedDate the modified date of this audit event
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_auditEvent.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_auditEvent.setNew(n);
	}

	/**
	* Sets the primary key of this audit event.
	*
	* @param primaryKey the primary key of this audit event
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_auditEvent.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_auditEvent.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this audit event.
	*
	* @param userId the user ID of this audit event
	*/
	@Override
	public void setUserId(long userId) {
		_auditEvent.setUserId(userId);
	}

	/**
	* Sets the user name of this audit event.
	*
	* @param userName the user name of this audit event
	*/
	@Override
	public void setUserName(String userName) {
		_auditEvent.setUserName(userName);
	}

	/**
	* Sets the user uuid of this audit event.
	*
	* @param userUuid the user uuid of this audit event
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_auditEvent.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this audit event.
	*
	* @param uuid the uuid of this audit event
	*/
	@Override
	public void setUuid(String uuid) {
		_auditEvent.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<AuditEvent> toCacheModel() {
		return _auditEvent.toCacheModel();
	}

	@Override
	public AuditEvent toEscapedModel() {
		return new AuditEventWrapper(_auditEvent.toEscapedModel());
	}

	@Override
	public String toString() {
		return _auditEvent.toString();
	}

	@Override
	public AuditEvent toUnescapedModel() {
		return new AuditEventWrapper(_auditEvent.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _auditEvent.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof AuditEventWrapper)) {
			return false;
		}

		AuditEventWrapper auditEventWrapper = (AuditEventWrapper)obj;

		if (Objects.equals(_auditEvent, auditEventWrapper._auditEvent)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _auditEvent.getStagedModelType();
	}

	@Override
	public AuditEvent getWrappedModel() {
		return _auditEvent;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _auditEvent.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _auditEvent.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_auditEvent.resetOriginalValues();
	}

	private final AuditEvent _auditEvent;
}