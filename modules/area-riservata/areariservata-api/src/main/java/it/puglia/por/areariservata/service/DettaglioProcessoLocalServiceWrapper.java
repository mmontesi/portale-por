/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DettaglioProcessoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcessoLocalService
 * @generated
 */
@ProviderType
public class DettaglioProcessoLocalServiceWrapper
	implements DettaglioProcessoLocalService,
		ServiceWrapper<DettaglioProcessoLocalService> {
	public DettaglioProcessoLocalServiceWrapper(
		DettaglioProcessoLocalService dettaglioProcessoLocalService) {
		_dettaglioProcessoLocalService = dettaglioProcessoLocalService;
	}

	/**
	* Adds the dettaglio processo to the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was added
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso addDettaglioProcesso(
		it.puglia.por.areariservata.model.DettaglioProcesso dettaglioProcesso) {
		return _dettaglioProcessoLocalService.addDettaglioProcesso(dettaglioProcesso);
	}

	@Override
	public int countByProcesso(long processoId) {
		return _dettaglioProcessoLocalService.countByProcesso(processoId);
	}

	/**
	* Creates a new dettaglio processo with the primary key. Does not add the dettaglio processo to the database.
	*
	* @param dettaglioProcessoId the primary key for the new dettaglio processo
	* @return the new dettaglio processo
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso createDettaglioProcesso(
		long dettaglioProcessoId) {
		return _dettaglioProcessoLocalService.createDettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Deletes the dettaglio processo from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was removed
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso deleteDettaglioProcesso(
		it.puglia.por.areariservata.model.DettaglioProcesso dettaglioProcesso) {
		return _dettaglioProcessoLocalService.deleteDettaglioProcesso(dettaglioProcesso);
	}

	/**
	* Deletes the dettaglio processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo that was removed
	* @throws PortalException if a dettaglio processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso deleteDettaglioProcesso(
		long dettaglioProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _dettaglioProcessoLocalService.deleteDettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _dettaglioProcessoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _dettaglioProcessoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _dettaglioProcessoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _dettaglioProcessoLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _dettaglioProcessoLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _dettaglioProcessoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _dettaglioProcessoLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso fetchDettaglioProcesso(
		long dettaglioProcessoId) {
		return _dettaglioProcessoLocalService.fetchDettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Returns the dettaglio processo matching the UUID and group.
	*
	* @param uuid the dettaglio processo's UUID
	* @param groupId the primary key of the group
	* @return the matching dettaglio processo, or <code>null</code> if a matching dettaglio processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso fetchDettaglioProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return _dettaglioProcessoLocalService.fetchDettaglioProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	@Override
	public java.util.List<it.puglia.por.areariservata.model.DettaglioProcesso> findByProcesso(
		long processoId) {
		return _dettaglioProcessoLocalService.findByProcesso(processoId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _dettaglioProcessoLocalService.getActionableDynamicQuery();
	}

	/**
	* Returns the dettaglio processo with the primary key.
	*
	* @param dettaglioProcessoId the primary key of the dettaglio processo
	* @return the dettaglio processo
	* @throws PortalException if a dettaglio processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso getDettaglioProcesso(
		long dettaglioProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _dettaglioProcessoLocalService.getDettaglioProcesso(dettaglioProcessoId);
	}

	/**
	* Returns the dettaglio processo matching the UUID and group.
	*
	* @param uuid the dettaglio processo's UUID
	* @param groupId the primary key of the group
	* @return the matching dettaglio processo
	* @throws PortalException if a matching dettaglio processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso getDettaglioProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _dettaglioProcessoLocalService.getDettaglioProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the dettaglio processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.DettaglioProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @return the range of dettaglio processos
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DettaglioProcesso> getDettaglioProcessos(
		int start, int end) {
		return _dettaglioProcessoLocalService.getDettaglioProcessos(start, end);
	}

	/**
	* Returns all the dettaglio processos matching the UUID and company.
	*
	* @param uuid the UUID of the dettaglio processos
	* @param companyId the primary key of the company
	* @return the matching dettaglio processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DettaglioProcesso> getDettaglioProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return _dettaglioProcessoLocalService.getDettaglioProcessosByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of dettaglio processos matching the UUID and company.
	*
	* @param uuid the UUID of the dettaglio processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of dettaglio processos
	* @param end the upper bound of the range of dettaglio processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching dettaglio processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.DettaglioProcesso> getDettaglioProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.DettaglioProcesso> orderByComparator) {
		return _dettaglioProcessoLocalService.getDettaglioProcessosByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of dettaglio processos.
	*
	* @return the number of dettaglio processos
	*/
	@Override
	public int getDettaglioProcessosCount() {
		return _dettaglioProcessoLocalService.getDettaglioProcessosCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _dettaglioProcessoLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _dettaglioProcessoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _dettaglioProcessoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _dettaglioProcessoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the dettaglio processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param dettaglioProcesso the dettaglio processo
	* @return the dettaglio processo that was updated
	*/
	@Override
	public it.puglia.por.areariservata.model.DettaglioProcesso updateDettaglioProcesso(
		it.puglia.por.areariservata.model.DettaglioProcesso dettaglioProcesso) {
		return _dettaglioProcessoLocalService.updateDettaglioProcesso(dettaglioProcesso);
	}

	@Override
	public DettaglioProcessoLocalService getWrappedService() {
		return _dettaglioProcessoLocalService;
	}

	@Override
	public void setWrappedService(
		DettaglioProcessoLocalService dettaglioProcessoLocalService) {
		_dettaglioProcessoLocalService = dettaglioProcessoLocalService;
	}

	private DettaglioProcessoLocalService _dettaglioProcessoLocalService;
}