/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.puglia.por.areariservata.service.http.EmailConfigurazioneServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.http.EmailConfigurazioneServiceSoap
 * @generated
 */
@ProviderType
public class EmailConfigurazioneSoap implements Serializable {
	public static EmailConfigurazioneSoap toSoapModel(EmailConfigurazione model) {
		EmailConfigurazioneSoap soapModel = new EmailConfigurazioneSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setEmailConfigurazioneId(model.getEmailConfigurazioneId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setBody(model.getBody());
		soapModel.setSubject(model.getSubject());
		soapModel.setFrom(model.getFrom());

		return soapModel;
	}

	public static EmailConfigurazioneSoap[] toSoapModels(
		EmailConfigurazione[] models) {
		EmailConfigurazioneSoap[] soapModels = new EmailConfigurazioneSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmailConfigurazioneSoap[][] toSoapModels(
		EmailConfigurazione[][] models) {
		EmailConfigurazioneSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmailConfigurazioneSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmailConfigurazioneSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmailConfigurazioneSoap[] toSoapModels(
		List<EmailConfigurazione> models) {
		List<EmailConfigurazioneSoap> soapModels = new ArrayList<EmailConfigurazioneSoap>(models.size());

		for (EmailConfigurazione model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmailConfigurazioneSoap[soapModels.size()]);
	}

	public EmailConfigurazioneSoap() {
	}

	public long getPrimaryKey() {
		return _emailConfigurazioneId;
	}

	public void setPrimaryKey(long pk) {
		setEmailConfigurazioneId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getEmailConfigurazioneId() {
		return _emailConfigurazioneId;
	}

	public void setEmailConfigurazioneId(long emailConfigurazioneId) {
		_emailConfigurazioneId = emailConfigurazioneId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getBody() {
		return _body;
	}

	public void setBody(String body) {
		_body = body;
	}

	public String getSubject() {
		return _subject;
	}

	public void setSubject(String subject) {
		_subject = subject;
	}

	public String getFrom() {
		return _from;
	}

	public void setFrom(String from) {
		_from = from;
	}

	private String _uuid;
	private long _emailConfigurazioneId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _body;
	private String _subject;
	private String _from;
}