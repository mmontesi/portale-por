/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Processo. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.ProcessoServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ProcessoService
 * @see it.puglia.por.areariservata.service.base.ProcessoServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.ProcessoServiceImpl
 * @generated
 */
@ProviderType
public class ProcessoServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.ProcessoServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static it.puglia.por.areariservata.model.Processo addProcesso(
		long groupId, long userId, long companyId, long currentUserId,
		long scopeGroupId, String nomeProcesso, String descrizioneProcesso,
		String workflowName, String startingState, long gruppoMittenteId,
		long[] idUserToAssign,
		com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addProcesso(groupId, userId, companyId, currentUserId,
			scopeGroupId, nomeProcesso, descrizioneProcesso, workflowName,
			startingState, gruppoMittenteId, idUserToAssign, serviceContext);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static it.puglia.por.areariservata.model.Processo getProcesso(
		long processoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProcesso(processoId);
	}

	public static ProcessoService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProcessoService, ProcessoService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ProcessoService.class);

		ServiceTracker<ProcessoService, ProcessoService> serviceTracker = new ServiceTracker<ProcessoService, ProcessoService>(bundle.getBundleContext(),
				ProcessoService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}