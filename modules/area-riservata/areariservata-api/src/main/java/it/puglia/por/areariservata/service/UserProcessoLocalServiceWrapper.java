/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link UserProcessoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see UserProcessoLocalService
 * @generated
 */
@ProviderType
public class UserProcessoLocalServiceWrapper implements UserProcessoLocalService,
	ServiceWrapper<UserProcessoLocalService> {
	public UserProcessoLocalServiceWrapper(
		UserProcessoLocalService userProcessoLocalService) {
		_userProcessoLocalService = userProcessoLocalService;
	}

	/**
	* Adds the user processo to the database. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was added
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso addUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return _userProcessoLocalService.addUserProcesso(userProcesso);
	}

	/**
	* Creates a new user processo with the primary key. Does not add the user processo to the database.
	*
	* @param userProcessoId the primary key for the new user processo
	* @return the new user processo
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso createUserProcesso(
		long userProcessoId) {
		return _userProcessoLocalService.createUserProcesso(userProcessoId);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userProcessoLocalService.deletePersistedModel(persistedModel);
	}

	/**
	* Deletes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo that was removed
	* @throws PortalException if a user processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso deleteUserProcesso(
		long userProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userProcessoLocalService.deleteUserProcesso(userProcessoId);
	}

	/**
	* Deletes the user processo from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was removed
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso deleteUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return _userProcessoLocalService.deleteUserProcesso(userProcesso);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _userProcessoLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userProcessoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _userProcessoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _userProcessoLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _userProcessoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _userProcessoLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.puglia.por.areariservata.model.UserProcesso fetchUserProcesso(
		long userProcessoId) {
		return _userProcessoLocalService.fetchUserProcesso(userProcessoId);
	}

	/**
	* Returns the user processo matching the UUID and group.
	*
	* @param uuid the user processo's UUID
	* @param groupId the primary key of the group
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso fetchUserProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return _userProcessoLocalService.fetchUserProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _userProcessoLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _userProcessoLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _userProcessoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _userProcessoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userProcessoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the user processo with the primary key.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo
	* @throws PortalException if a user processo with the primary key could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso getUserProcesso(
		long userProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userProcessoLocalService.getUserProcesso(userProcessoId);
	}

	/**
	* Returns the user processo matching the UUID and group.
	*
	* @param uuid the user processo's UUID
	* @param groupId the primary key of the group
	* @return the matching user processo
	* @throws PortalException if a matching user processo could not be found
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso getUserProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _userProcessoLocalService.getUserProcessoByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of user processos
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessos(
		int start, int end) {
		return _userProcessoLocalService.getUserProcessos(start, end);
	}

	/**
	* Returns all the user processos matching the UUID and company.
	*
	* @param uuid the UUID of the user processos
	* @param companyId the primary key of the company
	* @return the matching user processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return _userProcessoLocalService.getUserProcessosByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of user processos matching the UUID and company.
	*
	* @param uuid the UUID of the user processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching user processos, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.UserProcesso> orderByComparator) {
		return _userProcessoLocalService.getUserProcessosByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of user processos.
	*
	* @return the number of user processos
	*/
	@Override
	public int getUserProcessosCount() {
		return _userProcessoLocalService.getUserProcessosCount();
	}

	@Override
	public java.util.List<com.liferay.portal.kernel.model.User> getUtentiOfProcesso(
		it.puglia.por.areariservata.model.Processo processo) {
		return _userProcessoLocalService.getUtentiOfProcesso(processo);
	}

	/**
	* Updates the user processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was updated
	*/
	@Override
	public it.puglia.por.areariservata.model.UserProcesso updateUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return _userProcessoLocalService.updateUserProcesso(userProcesso);
	}

	@Override
	public UserProcessoLocalService getWrappedService() {
		return _userProcessoLocalService;
	}

	@Override
	public void setWrappedService(
		UserProcessoLocalService userProcessoLocalService) {
		_userProcessoLocalService = userProcessoLocalService;
	}

	private UserProcessoLocalService _userProcessoLocalService;
}