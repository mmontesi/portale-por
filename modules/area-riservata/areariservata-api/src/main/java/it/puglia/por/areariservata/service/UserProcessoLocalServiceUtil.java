/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for UserProcesso. This utility wraps
 * {@link it.puglia.por.areariservata.service.impl.UserProcessoLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see UserProcessoLocalService
 * @see it.puglia.por.areariservata.service.base.UserProcessoLocalServiceBaseImpl
 * @see it.puglia.por.areariservata.service.impl.UserProcessoLocalServiceImpl
 * @generated
 */
@ProviderType
public class UserProcessoLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.puglia.por.areariservata.service.impl.UserProcessoLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the user processo to the database. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was added
	*/
	public static it.puglia.por.areariservata.model.UserProcesso addUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return getService().addUserProcesso(userProcesso);
	}

	/**
	* Creates a new user processo with the primary key. Does not add the user processo to the database.
	*
	* @param userProcessoId the primary key for the new user processo
	* @return the new user processo
	*/
	public static it.puglia.por.areariservata.model.UserProcesso createUserProcesso(
		long userProcessoId) {
		return getService().createUserProcesso(userProcessoId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	/**
	* Deletes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo that was removed
	* @throws PortalException if a user processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.UserProcesso deleteUserProcesso(
		long userProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteUserProcesso(userProcessoId);
	}

	/**
	* Deletes the user processo from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was removed
	*/
	public static it.puglia.por.areariservata.model.UserProcesso deleteUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return getService().deleteUserProcesso(userProcesso);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.puglia.por.areariservata.model.UserProcesso fetchUserProcesso(
		long userProcessoId) {
		return getService().fetchUserProcesso(userProcessoId);
	}

	/**
	* Returns the user processo matching the UUID and group.
	*
	* @param uuid the user processo's UUID
	* @param groupId the primary key of the group
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public static it.puglia.por.areariservata.model.UserProcesso fetchUserProcessoByUuidAndGroupId(
		String uuid, long groupId) {
		return getService().fetchUserProcessoByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the user processo with the primary key.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo
	* @throws PortalException if a user processo with the primary key could not be found
	*/
	public static it.puglia.por.areariservata.model.UserProcesso getUserProcesso(
		long userProcessoId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserProcesso(userProcessoId);
	}

	/**
	* Returns the user processo matching the UUID and group.
	*
	* @param uuid the user processo's UUID
	* @param groupId the primary key of the group
	* @return the matching user processo
	* @throws PortalException if a matching user processo could not be found
	*/
	public static it.puglia.por.areariservata.model.UserProcesso getUserProcessoByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getUserProcessoByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.puglia.por.areariservata.model.impl.UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of user processos
	*/
	public static java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessos(
		int start, int end) {
		return getService().getUserProcessos(start, end);
	}

	/**
	* Returns all the user processos matching the UUID and company.
	*
	* @param uuid the UUID of the user processos
	* @param companyId the primary key of the company
	* @return the matching user processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessosByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService().getUserProcessosByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of user processos matching the UUID and company.
	*
	* @param uuid the UUID of the user processos
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching user processos, or an empty list if no matches were found
	*/
	public static java.util.List<it.puglia.por.areariservata.model.UserProcesso> getUserProcessosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<it.puglia.por.areariservata.model.UserProcesso> orderByComparator) {
		return getService()
				   .getUserProcessosByUuidAndCompanyId(uuid, companyId, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of user processos.
	*
	* @return the number of user processos
	*/
	public static int getUserProcessosCount() {
		return getService().getUserProcessosCount();
	}

	public static java.util.List<com.liferay.portal.kernel.model.User> getUtentiOfProcesso(
		it.puglia.por.areariservata.model.Processo processo) {
		return getService().getUtentiOfProcesso(processo);
	}

	/**
	* Updates the user processo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param userProcesso the user processo
	* @return the user processo that was updated
	*/
	public static it.puglia.por.areariservata.model.UserProcesso updateUserProcesso(
		it.puglia.por.areariservata.model.UserProcesso userProcesso) {
		return getService().updateUserProcesso(userProcesso);
	}

	public static UserProcessoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<UserProcessoLocalService, UserProcessoLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(UserProcessoLocalService.class);

		ServiceTracker<UserProcessoLocalService, UserProcessoLocalService> serviceTracker =
			new ServiceTracker<UserProcessoLocalService, UserProcessoLocalService>(bundle.getBundleContext(),
				UserProcessoLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}