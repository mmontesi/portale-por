/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link GruppiUtentiProcesso}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GruppiUtentiProcesso
 * @generated
 */
@ProviderType
public class GruppiUtentiProcessoWrapper implements GruppiUtentiProcesso,
	ModelWrapper<GruppiUtentiProcesso> {
	public GruppiUtentiProcessoWrapper(
		GruppiUtentiProcesso gruppiUtentiProcesso) {
		_gruppiUtentiProcesso = gruppiUtentiProcesso;
	}

	@Override
	public Class<?> getModelClass() {
		return GruppiUtentiProcesso.class;
	}

	@Override
	public String getModelClassName() {
		return GruppiUtentiProcesso.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("gupId", getGupId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("gupUserId", getGupUserId());
		attributes.put("gupGroupId", getGupGroupId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long gupId = (Long)attributes.get("gupId");

		if (gupId != null) {
			setGupId(gupId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long gupUserId = (Long)attributes.get("gupUserId");

		if (gupUserId != null) {
			setGupUserId(gupUserId);
		}

		Long gupGroupId = (Long)attributes.get("gupGroupId");

		if (gupGroupId != null) {
			setGupGroupId(gupGroupId);
		}
	}

	@Override
	public Object clone() {
		return new GruppiUtentiProcessoWrapper((GruppiUtentiProcesso)_gruppiUtentiProcesso.clone());
	}

	@Override
	public int compareTo(GruppiUtentiProcesso gruppiUtentiProcesso) {
		return _gruppiUtentiProcesso.compareTo(gruppiUtentiProcesso);
	}

	/**
	* Returns the company ID of this gruppi utenti processo.
	*
	* @return the company ID of this gruppi utenti processo
	*/
	@Override
	public long getCompanyId() {
		return _gruppiUtentiProcesso.getCompanyId();
	}

	/**
	* Returns the create date of this gruppi utenti processo.
	*
	* @return the create date of this gruppi utenti processo
	*/
	@Override
	public Date getCreateDate() {
		return _gruppiUtentiProcesso.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _gruppiUtentiProcesso.getExpandoBridge();
	}

	/**
	* Returns the group ID of this gruppi utenti processo.
	*
	* @return the group ID of this gruppi utenti processo
	*/
	@Override
	public long getGroupId() {
		return _gruppiUtentiProcesso.getGroupId();
	}

	/**
	* Returns the gup group ID of this gruppi utenti processo.
	*
	* @return the gup group ID of this gruppi utenti processo
	*/
	@Override
	public long getGupGroupId() {
		return _gruppiUtentiProcesso.getGupGroupId();
	}

	/**
	* Returns the gup ID of this gruppi utenti processo.
	*
	* @return the gup ID of this gruppi utenti processo
	*/
	@Override
	public long getGupId() {
		return _gruppiUtentiProcesso.getGupId();
	}

	/**
	* Returns the gup user ID of this gruppi utenti processo.
	*
	* @return the gup user ID of this gruppi utenti processo
	*/
	@Override
	public long getGupUserId() {
		return _gruppiUtentiProcesso.getGupUserId();
	}

	/**
	* Returns the gup user uuid of this gruppi utenti processo.
	*
	* @return the gup user uuid of this gruppi utenti processo
	*/
	@Override
	public String getGupUserUuid() {
		return _gruppiUtentiProcesso.getGupUserUuid();
	}

	/**
	* Returns the modified date of this gruppi utenti processo.
	*
	* @return the modified date of this gruppi utenti processo
	*/
	@Override
	public Date getModifiedDate() {
		return _gruppiUtentiProcesso.getModifiedDate();
	}

	/**
	* Returns the primary key of this gruppi utenti processo.
	*
	* @return the primary key of this gruppi utenti processo
	*/
	@Override
	public long getPrimaryKey() {
		return _gruppiUtentiProcesso.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _gruppiUtentiProcesso.getPrimaryKeyObj();
	}

	/**
	* Returns the user ID of this gruppi utenti processo.
	*
	* @return the user ID of this gruppi utenti processo
	*/
	@Override
	public long getUserId() {
		return _gruppiUtentiProcesso.getUserId();
	}

	/**
	* Returns the user name of this gruppi utenti processo.
	*
	* @return the user name of this gruppi utenti processo
	*/
	@Override
	public String getUserName() {
		return _gruppiUtentiProcesso.getUserName();
	}

	/**
	* Returns the user uuid of this gruppi utenti processo.
	*
	* @return the user uuid of this gruppi utenti processo
	*/
	@Override
	public String getUserUuid() {
		return _gruppiUtentiProcesso.getUserUuid();
	}

	/**
	* Returns the uuid of this gruppi utenti processo.
	*
	* @return the uuid of this gruppi utenti processo
	*/
	@Override
	public String getUuid() {
		return _gruppiUtentiProcesso.getUuid();
	}

	@Override
	public int hashCode() {
		return _gruppiUtentiProcesso.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _gruppiUtentiProcesso.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _gruppiUtentiProcesso.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _gruppiUtentiProcesso.isNew();
	}

	@Override
	public void persist() {
		_gruppiUtentiProcesso.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_gruppiUtentiProcesso.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this gruppi utenti processo.
	*
	* @param companyId the company ID of this gruppi utenti processo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_gruppiUtentiProcesso.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this gruppi utenti processo.
	*
	* @param createDate the create date of this gruppi utenti processo
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_gruppiUtentiProcesso.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_gruppiUtentiProcesso.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_gruppiUtentiProcesso.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_gruppiUtentiProcesso.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this gruppi utenti processo.
	*
	* @param groupId the group ID of this gruppi utenti processo
	*/
	@Override
	public void setGroupId(long groupId) {
		_gruppiUtentiProcesso.setGroupId(groupId);
	}

	/**
	* Sets the gup group ID of this gruppi utenti processo.
	*
	* @param gupGroupId the gup group ID of this gruppi utenti processo
	*/
	@Override
	public void setGupGroupId(long gupGroupId) {
		_gruppiUtentiProcesso.setGupGroupId(gupGroupId);
	}

	/**
	* Sets the gup ID of this gruppi utenti processo.
	*
	* @param gupId the gup ID of this gruppi utenti processo
	*/
	@Override
	public void setGupId(long gupId) {
		_gruppiUtentiProcesso.setGupId(gupId);
	}

	/**
	* Sets the gup user ID of this gruppi utenti processo.
	*
	* @param gupUserId the gup user ID of this gruppi utenti processo
	*/
	@Override
	public void setGupUserId(long gupUserId) {
		_gruppiUtentiProcesso.setGupUserId(gupUserId);
	}

	/**
	* Sets the gup user uuid of this gruppi utenti processo.
	*
	* @param gupUserUuid the gup user uuid of this gruppi utenti processo
	*/
	@Override
	public void setGupUserUuid(String gupUserUuid) {
		_gruppiUtentiProcesso.setGupUserUuid(gupUserUuid);
	}

	/**
	* Sets the modified date of this gruppi utenti processo.
	*
	* @param modifiedDate the modified date of this gruppi utenti processo
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_gruppiUtentiProcesso.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_gruppiUtentiProcesso.setNew(n);
	}

	/**
	* Sets the primary key of this gruppi utenti processo.
	*
	* @param primaryKey the primary key of this gruppi utenti processo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_gruppiUtentiProcesso.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_gruppiUtentiProcesso.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this gruppi utenti processo.
	*
	* @param userId the user ID of this gruppi utenti processo
	*/
	@Override
	public void setUserId(long userId) {
		_gruppiUtentiProcesso.setUserId(userId);
	}

	/**
	* Sets the user name of this gruppi utenti processo.
	*
	* @param userName the user name of this gruppi utenti processo
	*/
	@Override
	public void setUserName(String userName) {
		_gruppiUtentiProcesso.setUserName(userName);
	}

	/**
	* Sets the user uuid of this gruppi utenti processo.
	*
	* @param userUuid the user uuid of this gruppi utenti processo
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_gruppiUtentiProcesso.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this gruppi utenti processo.
	*
	* @param uuid the uuid of this gruppi utenti processo
	*/
	@Override
	public void setUuid(String uuid) {
		_gruppiUtentiProcesso.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<GruppiUtentiProcesso> toCacheModel() {
		return _gruppiUtentiProcesso.toCacheModel();
	}

	@Override
	public GruppiUtentiProcesso toEscapedModel() {
		return new GruppiUtentiProcessoWrapper(_gruppiUtentiProcesso.toEscapedModel());
	}

	@Override
	public String toString() {
		return _gruppiUtentiProcesso.toString();
	}

	@Override
	public GruppiUtentiProcesso toUnescapedModel() {
		return new GruppiUtentiProcessoWrapper(_gruppiUtentiProcesso.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _gruppiUtentiProcesso.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GruppiUtentiProcessoWrapper)) {
			return false;
		}

		GruppiUtentiProcessoWrapper gruppiUtentiProcessoWrapper = (GruppiUtentiProcessoWrapper)obj;

		if (Objects.equals(_gruppiUtentiProcesso,
					gruppiUtentiProcessoWrapper._gruppiUtentiProcesso)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _gruppiUtentiProcesso.getStagedModelType();
	}

	@Override
	public GruppiUtentiProcesso getWrappedModel() {
		return _gruppiUtentiProcesso;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _gruppiUtentiProcesso.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _gruppiUtentiProcesso.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_gruppiUtentiProcesso.resetOriginalValues();
	}

	private final GruppiUtentiProcesso _gruppiUtentiProcesso;
}