/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link DettaglioProcesso}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DettaglioProcesso
 * @generated
 */
@ProviderType
public class DettaglioProcessoWrapper implements DettaglioProcesso,
	ModelWrapper<DettaglioProcesso> {
	public DettaglioProcessoWrapper(DettaglioProcesso dettaglioProcesso) {
		_dettaglioProcesso = dettaglioProcesso;
	}

	@Override
	public Class<?> getModelClass() {
		return DettaglioProcesso.class;
	}

	@Override
	public String getModelClassName() {
		return DettaglioProcesso.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("dettaglioProcessoId", getDettaglioProcessoId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("processoId", getProcessoId());
		attributes.put("isVisible", isIsVisible());
		attributes.put("testoDettaglio", getTestoDettaglio());
		attributes.put("descerizioneBreve", getDescerizioneBreve());
		attributes.put("dlFileId", getDlFileId());
		attributes.put("nomeWorkflow", getNomeWorkflow());
		attributes.put("statoWorkflow", getStatoWorkflow());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long dettaglioProcessoId = (Long)attributes.get("dettaglioProcessoId");

		if (dettaglioProcessoId != null) {
			setDettaglioProcessoId(dettaglioProcessoId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long processoId = (Long)attributes.get("processoId");

		if (processoId != null) {
			setProcessoId(processoId);
		}

		Boolean isVisible = (Boolean)attributes.get("isVisible");

		if (isVisible != null) {
			setIsVisible(isVisible);
		}

		String testoDettaglio = (String)attributes.get("testoDettaglio");

		if (testoDettaglio != null) {
			setTestoDettaglio(testoDettaglio);
		}

		String descerizioneBreve = (String)attributes.get("descerizioneBreve");

		if (descerizioneBreve != null) {
			setDescerizioneBreve(descerizioneBreve);
		}

		Long dlFileId = (Long)attributes.get("dlFileId");

		if (dlFileId != null) {
			setDlFileId(dlFileId);
		}

		String nomeWorkflow = (String)attributes.get("nomeWorkflow");

		if (nomeWorkflow != null) {
			setNomeWorkflow(nomeWorkflow);
		}

		String statoWorkflow = (String)attributes.get("statoWorkflow");

		if (statoWorkflow != null) {
			setStatoWorkflow(statoWorkflow);
		}
	}

	@Override
	public Object clone() {
		return new DettaglioProcessoWrapper((DettaglioProcesso)_dettaglioProcesso.clone());
	}

	@Override
	public int compareTo(DettaglioProcesso dettaglioProcesso) {
		return _dettaglioProcesso.compareTo(dettaglioProcesso);
	}

	/**
	* Returns the company ID of this dettaglio processo.
	*
	* @return the company ID of this dettaglio processo
	*/
	@Override
	public long getCompanyId() {
		return _dettaglioProcesso.getCompanyId();
	}

	/**
	* Returns the create date of this dettaglio processo.
	*
	* @return the create date of this dettaglio processo
	*/
	@Override
	public Date getCreateDate() {
		return _dettaglioProcesso.getCreateDate();
	}

	/**
	* Returns the descerizione breve of this dettaglio processo.
	*
	* @return the descerizione breve of this dettaglio processo
	*/
	@Override
	public String getDescerizioneBreve() {
		return _dettaglioProcesso.getDescerizioneBreve();
	}

	/**
	* Returns the dettaglio processo ID of this dettaglio processo.
	*
	* @return the dettaglio processo ID of this dettaglio processo
	*/
	@Override
	public long getDettaglioProcessoId() {
		return _dettaglioProcesso.getDettaglioProcessoId();
	}

	/**
	* Returns the dl file ID of this dettaglio processo.
	*
	* @return the dl file ID of this dettaglio processo
	*/
	@Override
	public long getDlFileId() {
		return _dettaglioProcesso.getDlFileId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _dettaglioProcesso.getExpandoBridge();
	}

	/**
	* Returns the group ID of this dettaglio processo.
	*
	* @return the group ID of this dettaglio processo
	*/
	@Override
	public long getGroupId() {
		return _dettaglioProcesso.getGroupId();
	}

	/**
	* Returns the is visible of this dettaglio processo.
	*
	* @return the is visible of this dettaglio processo
	*/
	@Override
	public boolean getIsVisible() {
		return _dettaglioProcesso.getIsVisible();
	}

	/**
	* Returns the modified date of this dettaglio processo.
	*
	* @return the modified date of this dettaglio processo
	*/
	@Override
	public Date getModifiedDate() {
		return _dettaglioProcesso.getModifiedDate();
	}

	/**
	* Returns the nome workflow of this dettaglio processo.
	*
	* @return the nome workflow of this dettaglio processo
	*/
	@Override
	public String getNomeWorkflow() {
		return _dettaglioProcesso.getNomeWorkflow();
	}

	/**
	* Returns the primary key of this dettaglio processo.
	*
	* @return the primary key of this dettaglio processo
	*/
	@Override
	public long getPrimaryKey() {
		return _dettaglioProcesso.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _dettaglioProcesso.getPrimaryKeyObj();
	}

	/**
	* Returns the processo ID of this dettaglio processo.
	*
	* @return the processo ID of this dettaglio processo
	*/
	@Override
	public Long getProcessoId() {
		return _dettaglioProcesso.getProcessoId();
	}

	/**
	* Returns the stato workflow of this dettaglio processo.
	*
	* @return the stato workflow of this dettaglio processo
	*/
	@Override
	public String getStatoWorkflow() {
		return _dettaglioProcesso.getStatoWorkflow();
	}

	/**
	* Returns the testo dettaglio of this dettaglio processo.
	*
	* @return the testo dettaglio of this dettaglio processo
	*/
	@Override
	public String getTestoDettaglio() {
		return _dettaglioProcesso.getTestoDettaglio();
	}

	/**
	* Returns the user ID of this dettaglio processo.
	*
	* @return the user ID of this dettaglio processo
	*/
	@Override
	public long getUserId() {
		return _dettaglioProcesso.getUserId();
	}

	/**
	* Returns the user name of this dettaglio processo.
	*
	* @return the user name of this dettaglio processo
	*/
	@Override
	public String getUserName() {
		return _dettaglioProcesso.getUserName();
	}

	/**
	* Returns the user uuid of this dettaglio processo.
	*
	* @return the user uuid of this dettaglio processo
	*/
	@Override
	public String getUserUuid() {
		return _dettaglioProcesso.getUserUuid();
	}

	/**
	* Returns the uuid of this dettaglio processo.
	*
	* @return the uuid of this dettaglio processo
	*/
	@Override
	public String getUuid() {
		return _dettaglioProcesso.getUuid();
	}

	@Override
	public int hashCode() {
		return _dettaglioProcesso.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _dettaglioProcesso.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _dettaglioProcesso.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this dettaglio processo is is visible.
	*
	* @return <code>true</code> if this dettaglio processo is is visible; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsVisible() {
		return _dettaglioProcesso.isIsVisible();
	}

	@Override
	public boolean isNew() {
		return _dettaglioProcesso.isNew();
	}

	@Override
	public void persist() {
		_dettaglioProcesso.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_dettaglioProcesso.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this dettaglio processo.
	*
	* @param companyId the company ID of this dettaglio processo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_dettaglioProcesso.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this dettaglio processo.
	*
	* @param createDate the create date of this dettaglio processo
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_dettaglioProcesso.setCreateDate(createDate);
	}

	/**
	* Sets the descerizione breve of this dettaglio processo.
	*
	* @param descerizioneBreve the descerizione breve of this dettaglio processo
	*/
	@Override
	public void setDescerizioneBreve(String descerizioneBreve) {
		_dettaglioProcesso.setDescerizioneBreve(descerizioneBreve);
	}

	/**
	* Sets the dettaglio processo ID of this dettaglio processo.
	*
	* @param dettaglioProcessoId the dettaglio processo ID of this dettaglio processo
	*/
	@Override
	public void setDettaglioProcessoId(long dettaglioProcessoId) {
		_dettaglioProcesso.setDettaglioProcessoId(dettaglioProcessoId);
	}

	/**
	* Sets the dl file ID of this dettaglio processo.
	*
	* @param dlFileId the dl file ID of this dettaglio processo
	*/
	@Override
	public void setDlFileId(long dlFileId) {
		_dettaglioProcesso.setDlFileId(dlFileId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_dettaglioProcesso.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_dettaglioProcesso.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_dettaglioProcesso.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this dettaglio processo.
	*
	* @param groupId the group ID of this dettaglio processo
	*/
	@Override
	public void setGroupId(long groupId) {
		_dettaglioProcesso.setGroupId(groupId);
	}

	/**
	* Sets whether this dettaglio processo is is visible.
	*
	* @param isVisible the is visible of this dettaglio processo
	*/
	@Override
	public void setIsVisible(boolean isVisible) {
		_dettaglioProcesso.setIsVisible(isVisible);
	}

	/**
	* Sets the modified date of this dettaglio processo.
	*
	* @param modifiedDate the modified date of this dettaglio processo
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_dettaglioProcesso.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_dettaglioProcesso.setNew(n);
	}

	/**
	* Sets the nome workflow of this dettaglio processo.
	*
	* @param nomeWorkflow the nome workflow of this dettaglio processo
	*/
	@Override
	public void setNomeWorkflow(String nomeWorkflow) {
		_dettaglioProcesso.setNomeWorkflow(nomeWorkflow);
	}

	/**
	* Sets the primary key of this dettaglio processo.
	*
	* @param primaryKey the primary key of this dettaglio processo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_dettaglioProcesso.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_dettaglioProcesso.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the processo ID of this dettaglio processo.
	*
	* @param processoId the processo ID of this dettaglio processo
	*/
	@Override
	public void setProcessoId(Long processoId) {
		_dettaglioProcesso.setProcessoId(processoId);
	}

	/**
	* Sets the stato workflow of this dettaglio processo.
	*
	* @param statoWorkflow the stato workflow of this dettaglio processo
	*/
	@Override
	public void setStatoWorkflow(String statoWorkflow) {
		_dettaglioProcesso.setStatoWorkflow(statoWorkflow);
	}

	/**
	* Sets the testo dettaglio of this dettaglio processo.
	*
	* @param testoDettaglio the testo dettaglio of this dettaglio processo
	*/
	@Override
	public void setTestoDettaglio(String testoDettaglio) {
		_dettaglioProcesso.setTestoDettaglio(testoDettaglio);
	}

	/**
	* Sets the user ID of this dettaglio processo.
	*
	* @param userId the user ID of this dettaglio processo
	*/
	@Override
	public void setUserId(long userId) {
		_dettaglioProcesso.setUserId(userId);
	}

	/**
	* Sets the user name of this dettaglio processo.
	*
	* @param userName the user name of this dettaglio processo
	*/
	@Override
	public void setUserName(String userName) {
		_dettaglioProcesso.setUserName(userName);
	}

	/**
	* Sets the user uuid of this dettaglio processo.
	*
	* @param userUuid the user uuid of this dettaglio processo
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_dettaglioProcesso.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this dettaglio processo.
	*
	* @param uuid the uuid of this dettaglio processo
	*/
	@Override
	public void setUuid(String uuid) {
		_dettaglioProcesso.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<DettaglioProcesso> toCacheModel() {
		return _dettaglioProcesso.toCacheModel();
	}

	@Override
	public DettaglioProcesso toEscapedModel() {
		return new DettaglioProcessoWrapper(_dettaglioProcesso.toEscapedModel());
	}

	@Override
	public String toString() {
		return _dettaglioProcesso.toString();
	}

	@Override
	public DettaglioProcesso toUnescapedModel() {
		return new DettaglioProcessoWrapper(_dettaglioProcesso.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _dettaglioProcesso.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DettaglioProcessoWrapper)) {
			return false;
		}

		DettaglioProcessoWrapper dettaglioProcessoWrapper = (DettaglioProcessoWrapper)obj;

		if (Objects.equals(_dettaglioProcesso,
					dettaglioProcessoWrapper._dettaglioProcesso)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _dettaglioProcesso.getStagedModelType();
	}

	@Override
	public DettaglioProcesso getWrappedModel() {
		return _dettaglioProcesso;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _dettaglioProcesso.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _dettaglioProcesso.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_dettaglioProcesso.resetOriginalValues();
	}

	private final DettaglioProcesso _dettaglioProcesso;
}