/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.GruppiUtentiProcesso;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the gruppi utenti processo service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.GruppiUtentiProcessoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GruppiUtentiProcessoPersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.GruppiUtentiProcessoPersistenceImpl
 * @generated
 */
@ProviderType
public class GruppiUtentiProcessoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(GruppiUtentiProcesso gruppiUtentiProcesso) {
		getPersistence().clearCache(gruppiUtentiProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<GruppiUtentiProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<GruppiUtentiProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<GruppiUtentiProcesso> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static GruppiUtentiProcesso update(
		GruppiUtentiProcesso gruppiUtentiProcesso) {
		return getPersistence().update(gruppiUtentiProcesso);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static GruppiUtentiProcesso update(
		GruppiUtentiProcesso gruppiUtentiProcesso, ServiceContext serviceContext) {
		return getPersistence().update(gruppiUtentiProcesso, serviceContext);
	}

	/**
	* Returns all the gruppi utenti processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid(String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid(String uuid, int start,
		int end, OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso findByUuid_First(String uuid,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUuid_First(String uuid,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso findByUuid_Last(String uuid,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUuid_Last(String uuid,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the gruppi utenti processos before and after the current gruppi utenti processo in the ordered set where uuid = &#63;.
	*
	* @param gupId the primary key of the current gruppi utenti processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public static GruppiUtentiProcesso[] findByUuid_PrevAndNext(long gupId,
		String uuid, OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence()
				   .findByUuid_PrevAndNext(gupId, uuid, orderByComparator);
	}

	/**
	* Removes all the gruppi utenti processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching gruppi utenti processos
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchGruppiUtentiProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the gruppi utenti processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the gruppi utenti processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the gruppi utenti processo that was removed
	*/
	public static GruppiUtentiProcesso removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching gruppi utenti processos
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso findByUuid_C_First(String uuid,
		long companyId,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUuid_C_First(String uuid,
		long companyId,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso findByUuid_C_Last(String uuid,
		long companyId,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching gruppi utenti processo, or <code>null</code> if a matching gruppi utenti processo could not be found
	*/
	public static GruppiUtentiProcesso fetchByUuid_C_Last(String uuid,
		long companyId,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the gruppi utenti processos before and after the current gruppi utenti processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param gupId the primary key of the current gruppi utenti processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public static GruppiUtentiProcesso[] findByUuid_C_PrevAndNext(long gupId,
		String uuid, long companyId,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(gupId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the gruppi utenti processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of gruppi utenti processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching gruppi utenti processos
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Caches the gruppi utenti processo in the entity cache if it is enabled.
	*
	* @param gruppiUtentiProcesso the gruppi utenti processo
	*/
	public static void cacheResult(GruppiUtentiProcesso gruppiUtentiProcesso) {
		getPersistence().cacheResult(gruppiUtentiProcesso);
	}

	/**
	* Caches the gruppi utenti processos in the entity cache if it is enabled.
	*
	* @param gruppiUtentiProcessos the gruppi utenti processos
	*/
	public static void cacheResult(
		List<GruppiUtentiProcesso> gruppiUtentiProcessos) {
		getPersistence().cacheResult(gruppiUtentiProcessos);
	}

	/**
	* Creates a new gruppi utenti processo with the primary key. Does not add the gruppi utenti processo to the database.
	*
	* @param gupId the primary key for the new gruppi utenti processo
	* @return the new gruppi utenti processo
	*/
	public static GruppiUtentiProcesso create(long gupId) {
		return getPersistence().create(gupId);
	}

	/**
	* Removes the gruppi utenti processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo that was removed
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public static GruppiUtentiProcesso remove(long gupId)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().remove(gupId);
	}

	public static GruppiUtentiProcesso updateImpl(
		GruppiUtentiProcesso gruppiUtentiProcesso) {
		return getPersistence().updateImpl(gruppiUtentiProcesso);
	}

	/**
	* Returns the gruppi utenti processo with the primary key or throws a {@link NoSuchGruppiUtentiProcessoException} if it could not be found.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo
	* @throws NoSuchGruppiUtentiProcessoException if a gruppi utenti processo with the primary key could not be found
	*/
	public static GruppiUtentiProcesso findByPrimaryKey(long gupId)
		throws it.puglia.por.areariservata.exception.NoSuchGruppiUtentiProcessoException {
		return getPersistence().findByPrimaryKey(gupId);
	}

	/**
	* Returns the gruppi utenti processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param gupId the primary key of the gruppi utenti processo
	* @return the gruppi utenti processo, or <code>null</code> if a gruppi utenti processo with the primary key could not be found
	*/
	public static GruppiUtentiProcesso fetchByPrimaryKey(long gupId) {
		return getPersistence().fetchByPrimaryKey(gupId);
	}

	public static java.util.Map<java.io.Serializable, GruppiUtentiProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the gruppi utenti processos.
	*
	* @return the gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @return the range of gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findAll(int start, int end,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the gruppi utenti processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link GruppiUtentiProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of gruppi utenti processos
	* @param end the upper bound of the range of gruppi utenti processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of gruppi utenti processos
	*/
	public static List<GruppiUtentiProcesso> findAll(int start, int end,
		OrderByComparator<GruppiUtentiProcesso> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the gruppi utenti processos from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of gruppi utenti processos.
	*
	* @return the number of gruppi utenti processos
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static GruppiUtentiProcessoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<GruppiUtentiProcessoPersistence, GruppiUtentiProcessoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(GruppiUtentiProcessoPersistence.class);

		ServiceTracker<GruppiUtentiProcessoPersistence, GruppiUtentiProcessoPersistence> serviceTracker =
			new ServiceTracker<GruppiUtentiProcessoPersistence, GruppiUtentiProcessoPersistence>(bundle.getBundleContext(),
				GruppiUtentiProcessoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}