/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.puglia.por.areariservata.exception.NoSuchUserProcessoException;
import it.puglia.por.areariservata.model.UserProcesso;

/**
 * The persistence interface for the user processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.persistence.impl.UserProcessoPersistenceImpl
 * @see UserProcessoUtil
 * @generated
 */
@ProviderType
public interface UserProcessoPersistence extends BasePersistence<UserProcesso> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link UserProcessoUtil} to access the user processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the user processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid(String uuid);

	/**
	* Returns a range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid(String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the user processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the first user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the last user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the last user processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the user processos before and after the current user processo in the ordered set where uuid = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso[] findByUuid_PrevAndNext(long userProcessoId,
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Removes all the user processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of user processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching user processos
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchUserProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchUserProcessoException;

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the user processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the user processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the user processo that was removed
	*/
	public UserProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchUserProcessoException;

	/**
	* Returns the number of user processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching user processos
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid_C(String uuid, long companyId);

	/**
	* Returns a range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the user processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the first user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the last user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the user processos before and after the current user processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso[] findByUuid_C_PrevAndNext(long userProcessoId,
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Removes all the user processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of user processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching user processos
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Returns all the user processos where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @return the matching user processos
	*/
	public java.util.List<UserProcesso> findByuserIdProcesso(
		long userIdProcesso);

	/**
	* Returns a range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public java.util.List<UserProcesso> findByuserIdProcesso(
		long userIdProcesso, int start, int end);

	/**
	* Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByuserIdProcesso(
		long userIdProcesso, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the user processos where userIdProcesso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userIdProcesso the user ID processo
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByuserIdProcesso(
		long userIdProcesso, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByuserIdProcesso_First(long userIdProcesso,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the first user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByuserIdProcesso_First(long userIdProcesso,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByuserIdProcesso_Last(long userIdProcesso,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the last user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByuserIdProcesso_Last(long userIdProcesso,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the user processos before and after the current user processo in the ordered set where userIdProcesso = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param userIdProcesso the user ID processo
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso[] findByuserIdProcesso_PrevAndNext(
		long userProcessoId, long userIdProcesso,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Removes all the user processos where userIdProcesso = &#63; from the database.
	*
	* @param userIdProcesso the user ID processo
	*/
	public void removeByuserIdProcesso(long userIdProcesso);

	/**
	* Returns the number of user processos where userIdProcesso = &#63;.
	*
	* @param userIdProcesso the user ID processo
	* @return the number of matching user processos
	*/
	public int countByuserIdProcesso(long userIdProcesso);

	/**
	* Returns all the user processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching user processos
	*/
	public java.util.List<UserProcesso> findByprocessoId(long processoId);

	/**
	* Returns a range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of matching user processos
	*/
	public java.util.List<UserProcesso> findByprocessoId(long processoId,
		int start, int end);

	/**
	* Returns an ordered range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByprocessoId(long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the user processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching user processos
	*/
	public java.util.List<UserProcesso> findByprocessoId(long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByprocessoId_First(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the first user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByprocessoId_First(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the last user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo
	* @throws NoSuchUserProcessoException if a matching user processo could not be found
	*/
	public UserProcesso findByprocessoId_Last(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Returns the last user processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching user processo, or <code>null</code> if a matching user processo could not be found
	*/
	public UserProcesso fetchByprocessoId_Last(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns the user processos before and after the current user processo in the ordered set where processoId = &#63;.
	*
	* @param userProcessoId the primary key of the current user processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso[] findByprocessoId_PrevAndNext(long userProcessoId,
		long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator)
		throws NoSuchUserProcessoException;

	/**
	* Removes all the user processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public void removeByprocessoId(long processoId);

	/**
	* Returns the number of user processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching user processos
	*/
	public int countByprocessoId(long processoId);

	/**
	* Caches the user processo in the entity cache if it is enabled.
	*
	* @param userProcesso the user processo
	*/
	public void cacheResult(UserProcesso userProcesso);

	/**
	* Caches the user processos in the entity cache if it is enabled.
	*
	* @param userProcessos the user processos
	*/
	public void cacheResult(java.util.List<UserProcesso> userProcessos);

	/**
	* Creates a new user processo with the primary key. Does not add the user processo to the database.
	*
	* @param userProcessoId the primary key for the new user processo
	* @return the new user processo
	*/
	public UserProcesso create(long userProcessoId);

	/**
	* Removes the user processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo that was removed
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso remove(long userProcessoId)
		throws NoSuchUserProcessoException;

	public UserProcesso updateImpl(UserProcesso userProcesso);

	/**
	* Returns the user processo with the primary key or throws a {@link NoSuchUserProcessoException} if it could not be found.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo
	* @throws NoSuchUserProcessoException if a user processo with the primary key could not be found
	*/
	public UserProcesso findByPrimaryKey(long userProcessoId)
		throws NoSuchUserProcessoException;

	/**
	* Returns the user processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param userProcessoId the primary key of the user processo
	* @return the user processo, or <code>null</code> if a user processo with the primary key could not be found
	*/
	public UserProcesso fetchByPrimaryKey(long userProcessoId);

	@Override
	public java.util.Map<java.io.Serializable, UserProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the user processos.
	*
	* @return the user processos
	*/
	public java.util.List<UserProcesso> findAll();

	/**
	* Returns a range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @return the range of user processos
	*/
	public java.util.List<UserProcesso> findAll(int start, int end);

	/**
	* Returns an ordered range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of user processos
	*/
	public java.util.List<UserProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the user processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link UserProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of user processos
	* @param end the upper bound of the range of user processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of user processos
	*/
	public java.util.List<UserProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<UserProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the user processos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of user processos.
	*
	* @return the number of user processos
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}