/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */

package it.puglia.por.areariservata.constants;

/**
 * @author Michele Perissinotto
 */
public class AreaRiservataPortletKeys {

    public static final String PORTLET_NAME = "areariservata";


}
