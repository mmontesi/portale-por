/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Processo}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Processo
 * @generated
 */
@ProviderType
public class ProcessoWrapper implements Processo, ModelWrapper<Processo> {
	public ProcessoWrapper(Processo processo) {
		_processo = processo;
	}

	@Override
	public Class<?> getModelClass() {
		return Processo.class;
	}

	@Override
	public String getModelClassName() {
		return Processo.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("processoId", getProcessoId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nomeProcesso", getNomeProcesso());
		attributes.put("descrizioneProcesso", getDescrizioneProcesso());
		attributes.put("isVisible", isIsVisible());
		attributes.put("folderId", getFolderId());
		attributes.put("nomeWorkflow", getNomeWorkflow());
		attributes.put("statoWorkflow", getStatoWorkflow());
		attributes.put("gruppoMittenteId", getGruppoMittenteId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long processoId = (Long)attributes.get("processoId");

		if (processoId != null) {
			setProcessoId(processoId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nomeProcesso = (String)attributes.get("nomeProcesso");

		if (nomeProcesso != null) {
			setNomeProcesso(nomeProcesso);
		}

		String descrizioneProcesso = (String)attributes.get(
				"descrizioneProcesso");

		if (descrizioneProcesso != null) {
			setDescrizioneProcesso(descrizioneProcesso);
		}

		Boolean isVisible = (Boolean)attributes.get("isVisible");

		if (isVisible != null) {
			setIsVisible(isVisible);
		}

		Long folderId = (Long)attributes.get("folderId");

		if (folderId != null) {
			setFolderId(folderId);
		}

		String nomeWorkflow = (String)attributes.get("nomeWorkflow");

		if (nomeWorkflow != null) {
			setNomeWorkflow(nomeWorkflow);
		}

		String statoWorkflow = (String)attributes.get("statoWorkflow");

		if (statoWorkflow != null) {
			setStatoWorkflow(statoWorkflow);
		}

		Long gruppoMittenteId = (Long)attributes.get("gruppoMittenteId");

		if (gruppoMittenteId != null) {
			setGruppoMittenteId(gruppoMittenteId);
		}
	}

	@Override
	public Object clone() {
		return new ProcessoWrapper((Processo)_processo.clone());
	}

	@Override
	public int compareTo(Processo processo) {
		return _processo.compareTo(processo);
	}

	/**
	* Returns the company ID of this processo.
	*
	* @return the company ID of this processo
	*/
	@Override
	public long getCompanyId() {
		return _processo.getCompanyId();
	}

	/**
	* Returns the create date of this processo.
	*
	* @return the create date of this processo
	*/
	@Override
	public Date getCreateDate() {
		return _processo.getCreateDate();
	}

	/**
	* Returns the descrizione processo of this processo.
	*
	* @return the descrizione processo of this processo
	*/
	@Override
	public String getDescrizioneProcesso() {
		return _processo.getDescrizioneProcesso();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _processo.getExpandoBridge();
	}

	/**
	* Returns the folder ID of this processo.
	*
	* @return the folder ID of this processo
	*/
	@Override
	public long getFolderId() {
		return _processo.getFolderId();
	}

	/**
	* Returns the group ID of this processo.
	*
	* @return the group ID of this processo
	*/
	@Override
	public long getGroupId() {
		return _processo.getGroupId();
	}

	/**
	* Returns the gruppo mittente ID of this processo.
	*
	* @return the gruppo mittente ID of this processo
	*/
	@Override
	public long getGruppoMittenteId() {
		return _processo.getGruppoMittenteId();
	}

	/**
	* Returns the is visible of this processo.
	*
	* @return the is visible of this processo
	*/
	@Override
	public boolean getIsVisible() {
		return _processo.getIsVisible();
	}

	/**
	* Returns the modified date of this processo.
	*
	* @return the modified date of this processo
	*/
	@Override
	public Date getModifiedDate() {
		return _processo.getModifiedDate();
	}

	/**
	* Returns the nome processo of this processo.
	*
	* @return the nome processo of this processo
	*/
	@Override
	public String getNomeProcesso() {
		return _processo.getNomeProcesso();
	}

	/**
	* Returns the nome workflow of this processo.
	*
	* @return the nome workflow of this processo
	*/
	@Override
	public String getNomeWorkflow() {
		return _processo.getNomeWorkflow();
	}

	/**
	* Returns the primary key of this processo.
	*
	* @return the primary key of this processo
	*/
	@Override
	public long getPrimaryKey() {
		return _processo.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _processo.getPrimaryKeyObj();
	}

	/**
	* Returns the processo ID of this processo.
	*
	* @return the processo ID of this processo
	*/
	@Override
	public long getProcessoId() {
		return _processo.getProcessoId();
	}

	/**
	* Returns the stato workflow of this processo.
	*
	* @return the stato workflow of this processo
	*/
	@Override
	public String getStatoWorkflow() {
		return _processo.getStatoWorkflow();
	}

	/**
	* Returns the user ID of this processo.
	*
	* @return the user ID of this processo
	*/
	@Override
	public long getUserId() {
		return _processo.getUserId();
	}

	/**
	* Returns the user name of this processo.
	*
	* @return the user name of this processo
	*/
	@Override
	public String getUserName() {
		return _processo.getUserName();
	}

	/**
	* Returns the user uuid of this processo.
	*
	* @return the user uuid of this processo
	*/
	@Override
	public String getUserUuid() {
		return _processo.getUserUuid();
	}

	/**
	* Returns the uuid of this processo.
	*
	* @return the uuid of this processo
	*/
	@Override
	public String getUuid() {
		return _processo.getUuid();
	}

	@Override
	public int hashCode() {
		return _processo.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _processo.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _processo.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this processo is is visible.
	*
	* @return <code>true</code> if this processo is is visible; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsVisible() {
		return _processo.isIsVisible();
	}

	@Override
	public boolean isNew() {
		return _processo.isNew();
	}

	@Override
	public void persist() {
		_processo.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_processo.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this processo.
	*
	* @param companyId the company ID of this processo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_processo.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this processo.
	*
	* @param createDate the create date of this processo
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_processo.setCreateDate(createDate);
	}

	/**
	* Sets the descrizione processo of this processo.
	*
	* @param descrizioneProcesso the descrizione processo of this processo
	*/
	@Override
	public void setDescrizioneProcesso(String descrizioneProcesso) {
		_processo.setDescrizioneProcesso(descrizioneProcesso);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_processo.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_processo.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_processo.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the folder ID of this processo.
	*
	* @param folderId the folder ID of this processo
	*/
	@Override
	public void setFolderId(long folderId) {
		_processo.setFolderId(folderId);
	}

	/**
	* Sets the group ID of this processo.
	*
	* @param groupId the group ID of this processo
	*/
	@Override
	public void setGroupId(long groupId) {
		_processo.setGroupId(groupId);
	}

	/**
	* Sets the gruppo mittente ID of this processo.
	*
	* @param gruppoMittenteId the gruppo mittente ID of this processo
	*/
	@Override
	public void setGruppoMittenteId(long gruppoMittenteId) {
		_processo.setGruppoMittenteId(gruppoMittenteId);
	}

	/**
	* Sets whether this processo is is visible.
	*
	* @param isVisible the is visible of this processo
	*/
	@Override
	public void setIsVisible(boolean isVisible) {
		_processo.setIsVisible(isVisible);
	}

	/**
	* Sets the modified date of this processo.
	*
	* @param modifiedDate the modified date of this processo
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_processo.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_processo.setNew(n);
	}

	/**
	* Sets the nome processo of this processo.
	*
	* @param nomeProcesso the nome processo of this processo
	*/
	@Override
	public void setNomeProcesso(String nomeProcesso) {
		_processo.setNomeProcesso(nomeProcesso);
	}

	/**
	* Sets the nome workflow of this processo.
	*
	* @param nomeWorkflow the nome workflow of this processo
	*/
	@Override
	public void setNomeWorkflow(String nomeWorkflow) {
		_processo.setNomeWorkflow(nomeWorkflow);
	}

	/**
	* Sets the primary key of this processo.
	*
	* @param primaryKey the primary key of this processo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_processo.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_processo.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the processo ID of this processo.
	*
	* @param processoId the processo ID of this processo
	*/
	@Override
	public void setProcessoId(long processoId) {
		_processo.setProcessoId(processoId);
	}

	/**
	* Sets the stato workflow of this processo.
	*
	* @param statoWorkflow the stato workflow of this processo
	*/
	@Override
	public void setStatoWorkflow(String statoWorkflow) {
		_processo.setStatoWorkflow(statoWorkflow);
	}

	/**
	* Sets the user ID of this processo.
	*
	* @param userId the user ID of this processo
	*/
	@Override
	public void setUserId(long userId) {
		_processo.setUserId(userId);
	}

	/**
	* Sets the user name of this processo.
	*
	* @param userName the user name of this processo
	*/
	@Override
	public void setUserName(String userName) {
		_processo.setUserName(userName);
	}

	/**
	* Sets the user uuid of this processo.
	*
	* @param userUuid the user uuid of this processo
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_processo.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this processo.
	*
	* @param uuid the uuid of this processo
	*/
	@Override
	public void setUuid(String uuid) {
		_processo.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Processo> toCacheModel() {
		return _processo.toCacheModel();
	}

	@Override
	public Processo toEscapedModel() {
		return new ProcessoWrapper(_processo.toEscapedModel());
	}

	@Override
	public String toString() {
		return _processo.toString();
	}

	@Override
	public Processo toUnescapedModel() {
		return new ProcessoWrapper(_processo.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _processo.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProcessoWrapper)) {
			return false;
		}

		ProcessoWrapper processoWrapper = (ProcessoWrapper)obj;

		if (Objects.equals(_processo, processoWrapper._processo)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _processo.getStagedModelType();
	}

	@Override
	public Processo getWrappedModel() {
		return _processo;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _processo.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _processo.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_processo.resetOriginalValues();
	}

	private final Processo _processo;
}