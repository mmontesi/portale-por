/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link UserProcesso}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see UserProcesso
 * @generated
 */
@ProviderType
public class UserProcessoWrapper implements UserProcesso,
	ModelWrapper<UserProcesso> {
	public UserProcessoWrapper(UserProcesso userProcesso) {
		_userProcesso = userProcesso;
	}

	@Override
	public Class<?> getModelClass() {
		return UserProcesso.class;
	}

	@Override
	public String getModelClassName() {
		return UserProcesso.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("userProcessoId", getUserProcessoId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("userIdProcesso", getUserIdProcesso());
		attributes.put("processoId", getProcessoId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long userProcessoId = (Long)attributes.get("userProcessoId");

		if (userProcessoId != null) {
			setUserProcessoId(userProcessoId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long userIdProcesso = (Long)attributes.get("userIdProcesso");

		if (userIdProcesso != null) {
			setUserIdProcesso(userIdProcesso);
		}

		Long processoId = (Long)attributes.get("processoId");

		if (processoId != null) {
			setProcessoId(processoId);
		}
	}

	@Override
	public Object clone() {
		return new UserProcessoWrapper((UserProcesso)_userProcesso.clone());
	}

	@Override
	public int compareTo(UserProcesso userProcesso) {
		return _userProcesso.compareTo(userProcesso);
	}

	/**
	* Returns the company ID of this user processo.
	*
	* @return the company ID of this user processo
	*/
	@Override
	public long getCompanyId() {
		return _userProcesso.getCompanyId();
	}

	/**
	* Returns the create date of this user processo.
	*
	* @return the create date of this user processo
	*/
	@Override
	public Date getCreateDate() {
		return _userProcesso.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _userProcesso.getExpandoBridge();
	}

	/**
	* Returns the group ID of this user processo.
	*
	* @return the group ID of this user processo
	*/
	@Override
	public long getGroupId() {
		return _userProcesso.getGroupId();
	}

	/**
	* Returns the modified date of this user processo.
	*
	* @return the modified date of this user processo
	*/
	@Override
	public Date getModifiedDate() {
		return _userProcesso.getModifiedDate();
	}

	/**
	* Returns the primary key of this user processo.
	*
	* @return the primary key of this user processo
	*/
	@Override
	public long getPrimaryKey() {
		return _userProcesso.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _userProcesso.getPrimaryKeyObj();
	}

	/**
	* Returns the processo ID of this user processo.
	*
	* @return the processo ID of this user processo
	*/
	@Override
	public long getProcessoId() {
		return _userProcesso.getProcessoId();
	}

	/**
	* Returns the user ID of this user processo.
	*
	* @return the user ID of this user processo
	*/
	@Override
	public long getUserId() {
		return _userProcesso.getUserId();
	}

	/**
	* Returns the user ID processo of this user processo.
	*
	* @return the user ID processo of this user processo
	*/
	@Override
	public long getUserIdProcesso() {
		return _userProcesso.getUserIdProcesso();
	}

	/**
	* Returns the user name of this user processo.
	*
	* @return the user name of this user processo
	*/
	@Override
	public String getUserName() {
		return _userProcesso.getUserName();
	}

	/**
	* Returns the user processo ID of this user processo.
	*
	* @return the user processo ID of this user processo
	*/
	@Override
	public long getUserProcessoId() {
		return _userProcesso.getUserProcessoId();
	}

	/**
	* Returns the user uuid of this user processo.
	*
	* @return the user uuid of this user processo
	*/
	@Override
	public String getUserUuid() {
		return _userProcesso.getUserUuid();
	}

	/**
	* Returns the uuid of this user processo.
	*
	* @return the uuid of this user processo
	*/
	@Override
	public String getUuid() {
		return _userProcesso.getUuid();
	}

	@Override
	public int hashCode() {
		return _userProcesso.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _userProcesso.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _userProcesso.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _userProcesso.isNew();
	}

	@Override
	public void persist() {
		_userProcesso.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_userProcesso.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this user processo.
	*
	* @param companyId the company ID of this user processo
	*/
	@Override
	public void setCompanyId(long companyId) {
		_userProcesso.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this user processo.
	*
	* @param createDate the create date of this user processo
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_userProcesso.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_userProcesso.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_userProcesso.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_userProcesso.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this user processo.
	*
	* @param groupId the group ID of this user processo
	*/
	@Override
	public void setGroupId(long groupId) {
		_userProcesso.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this user processo.
	*
	* @param modifiedDate the modified date of this user processo
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_userProcesso.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_userProcesso.setNew(n);
	}

	/**
	* Sets the primary key of this user processo.
	*
	* @param primaryKey the primary key of this user processo
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_userProcesso.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_userProcesso.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the processo ID of this user processo.
	*
	* @param processoId the processo ID of this user processo
	*/
	@Override
	public void setProcessoId(long processoId) {
		_userProcesso.setProcessoId(processoId);
	}

	/**
	* Sets the user ID of this user processo.
	*
	* @param userId the user ID of this user processo
	*/
	@Override
	public void setUserId(long userId) {
		_userProcesso.setUserId(userId);
	}

	/**
	* Sets the user ID processo of this user processo.
	*
	* @param userIdProcesso the user ID processo of this user processo
	*/
	@Override
	public void setUserIdProcesso(long userIdProcesso) {
		_userProcesso.setUserIdProcesso(userIdProcesso);
	}

	/**
	* Sets the user name of this user processo.
	*
	* @param userName the user name of this user processo
	*/
	@Override
	public void setUserName(String userName) {
		_userProcesso.setUserName(userName);
	}

	/**
	* Sets the user processo ID of this user processo.
	*
	* @param userProcessoId the user processo ID of this user processo
	*/
	@Override
	public void setUserProcessoId(long userProcessoId) {
		_userProcesso.setUserProcessoId(userProcessoId);
	}

	/**
	* Sets the user uuid of this user processo.
	*
	* @param userUuid the user uuid of this user processo
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_userProcesso.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this user processo.
	*
	* @param uuid the uuid of this user processo
	*/
	@Override
	public void setUuid(String uuid) {
		_userProcesso.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<UserProcesso> toCacheModel() {
		return _userProcesso.toCacheModel();
	}

	@Override
	public UserProcesso toEscapedModel() {
		return new UserProcessoWrapper(_userProcesso.toEscapedModel());
	}

	@Override
	public String toString() {
		return _userProcesso.toString();
	}

	@Override
	public UserProcesso toUnescapedModel() {
		return new UserProcessoWrapper(_userProcesso.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _userProcesso.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof UserProcessoWrapper)) {
			return false;
		}

		UserProcessoWrapper userProcessoWrapper = (UserProcessoWrapper)obj;

		if (Objects.equals(_userProcesso, userProcessoWrapper._userProcesso)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _userProcesso.getStagedModelType();
	}

	@Override
	public UserProcesso getWrappedModel() {
		return _userProcesso;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _userProcesso.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _userProcesso.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_userProcesso.resetOriginalValues();
	}

	private final UserProcesso _userProcesso;
}