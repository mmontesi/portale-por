/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import it.puglia.por.areariservata.exception.NoSuchDocumentoProcessoException;
import it.puglia.por.areariservata.model.DocumentoProcesso;

/**
 * The persistence interface for the documento processo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see it.puglia.por.areariservata.service.persistence.impl.DocumentoProcessoPersistenceImpl
 * @see DocumentoProcessoUtil
 * @generated
 */
@ProviderType
public interface DocumentoProcessoPersistence extends BasePersistence<DocumentoProcesso> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DocumentoProcessoUtil} to access the documento processo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the documento processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid(String uuid);

	/**
	* Returns a range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findByUuid_PrevAndNext(
		long documentoProcessoId, String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of documento processos where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching documento processos
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByUUID_G(String uuid, long groupId)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the documento processo where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the documento processo where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the documento processo that was removed
	*/
	public DocumentoProcesso removeByUUID_G(String uuid, long groupId)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the number of documento processos where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching documento processos
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId);

	/**
	* Returns a range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findByUuid_C_PrevAndNext(
		long documentoProcessoId, String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of documento processos where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching documento processos
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Returns all the documento processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocesso(long processoId);

	/**
	* Returns a range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end);

	/**
	* Returns an ordered range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocesso(long processoId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocesso_First(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocesso_First(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocesso_Last(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocesso_Last(long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findByprocesso_PrevAndNext(
		long documentoProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where processoId = &#63; from the database.
	*
	* @param processoId the processo ID
	*/
	public void removeByprocesso(long processoId);

	/**
	* Returns the number of documento processos where processoId = &#63;.
	*
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public int countByprocesso(long processoId);

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId);

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findBydettaglioProcesso(
		long dettaglioProcessoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findBydettaglioProcesso_First(
		long dettaglioProcessoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchBydettaglioProcesso_First(
		long dettaglioProcessoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchBydettaglioProcesso_Last(
		long dettaglioProcessoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findBydettaglioProcesso_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	*/
	public void removeBydettaglioProcesso(long dettaglioProcessoId);

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @return the number of matching documento processos
	*/
	public int countBydettaglioProcesso(long dettaglioProcessoId);

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId);

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoAndDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocessoAndDettaglio_First(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocessoAndDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findByprocessoAndDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	*/
	public void removeByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId);

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public int countByprocessoAndDettaglio(long dettaglioProcessoId,
		long processoId);

	/**
	* Returns all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId);

	/**
	* Returns a range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching documento processos
	*/
	public java.util.List<DocumentoProcesso> findByprocessoOrDettaglio(
		long dettaglioProcessoId, long processoId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the first documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocessoOrDettaglio_First(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo
	* @throws NoSuchDocumentoProcessoException if a matching documento processo could not be found
	*/
	public DocumentoProcesso findByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the last documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching documento processo, or <code>null</code> if a matching documento processo could not be found
	*/
	public DocumentoProcesso fetchByprocessoOrDettaglio_Last(
		long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns the documento processos before and after the current documento processo in the ordered set where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param documentoProcessoId the primary key of the current documento processo
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso[] findByprocessoOrDettaglio_PrevAndNext(
		long documentoProcessoId, long dettaglioProcessoId, long processoId,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator)
		throws NoSuchDocumentoProcessoException;

	/**
	* Removes all the documento processos where dettaglioProcessoId = &#63; and processoId = &#63; from the database.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	*/
	public void removeByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId);

	/**
	* Returns the number of documento processos where dettaglioProcessoId = &#63; and processoId = &#63;.
	*
	* @param dettaglioProcessoId the dettaglio processo ID
	* @param processoId the processo ID
	* @return the number of matching documento processos
	*/
	public int countByprocessoOrDettaglio(long dettaglioProcessoId,
		long processoId);

	/**
	* Caches the documento processo in the entity cache if it is enabled.
	*
	* @param documentoProcesso the documento processo
	*/
	public void cacheResult(DocumentoProcesso documentoProcesso);

	/**
	* Caches the documento processos in the entity cache if it is enabled.
	*
	* @param documentoProcessos the documento processos
	*/
	public void cacheResult(
		java.util.List<DocumentoProcesso> documentoProcessos);

	/**
	* Creates a new documento processo with the primary key. Does not add the documento processo to the database.
	*
	* @param documentoProcessoId the primary key for the new documento processo
	* @return the new documento processo
	*/
	public DocumentoProcesso create(long documentoProcessoId);

	/**
	* Removes the documento processo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo that was removed
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso remove(long documentoProcessoId)
		throws NoSuchDocumentoProcessoException;

	public DocumentoProcesso updateImpl(DocumentoProcesso documentoProcesso);

	/**
	* Returns the documento processo with the primary key or throws a {@link NoSuchDocumentoProcessoException} if it could not be found.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo
	* @throws NoSuchDocumentoProcessoException if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso findByPrimaryKey(long documentoProcessoId)
		throws NoSuchDocumentoProcessoException;

	/**
	* Returns the documento processo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param documentoProcessoId the primary key of the documento processo
	* @return the documento processo, or <code>null</code> if a documento processo with the primary key could not be found
	*/
	public DocumentoProcesso fetchByPrimaryKey(long documentoProcessoId);

	@Override
	public java.util.Map<java.io.Serializable, DocumentoProcesso> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the documento processos.
	*
	* @return the documento processos
	*/
	public java.util.List<DocumentoProcesso> findAll();

	/**
	* Returns a range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @return the range of documento processos
	*/
	public java.util.List<DocumentoProcesso> findAll(int start, int end);

	/**
	* Returns an ordered range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of documento processos
	*/
	public java.util.List<DocumentoProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator);

	/**
	* Returns an ordered range of all the documento processos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link DocumentoProcessoModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of documento processos
	* @param end the upper bound of the range of documento processos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of documento processos
	*/
	public java.util.List<DocumentoProcesso> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<DocumentoProcesso> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the documento processos from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of documento processos.
	*
	* @return the number of documento processos
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}