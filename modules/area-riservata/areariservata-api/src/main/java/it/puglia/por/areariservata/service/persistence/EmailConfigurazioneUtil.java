/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.puglia.por.areariservata.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import it.puglia.por.areariservata.model.EmailConfigurazione;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the email configurazione service. This utility wraps {@link it.puglia.por.areariservata.service.persistence.impl.EmailConfigurazionePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EmailConfigurazionePersistence
 * @see it.puglia.por.areariservata.service.persistence.impl.EmailConfigurazionePersistenceImpl
 * @generated
 */
@ProviderType
public class EmailConfigurazioneUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(EmailConfigurazione emailConfigurazione) {
		getPersistence().clearCache(emailConfigurazione);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<EmailConfigurazione> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<EmailConfigurazione> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<EmailConfigurazione> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static EmailConfigurazione update(
		EmailConfigurazione emailConfigurazione) {
		return getPersistence().update(emailConfigurazione);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static EmailConfigurazione update(
		EmailConfigurazione emailConfigurazione, ServiceContext serviceContext) {
		return getPersistence().update(emailConfigurazione, serviceContext);
	}

	/**
	* Returns all the email configuraziones where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid(String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid(String uuid, int start,
		int end, OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid(String uuid, int start,
		int end, OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione findByUuid_First(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUuid_First(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione findByUuid_Last(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUuid_Last(String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63;.
	*
	* @param emailConfigurazioneId the primary key of the current email configurazione
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public static EmailConfigurazione[] findByUuid_PrevAndNext(
		long emailConfigurazioneId, String uuid,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence()
				   .findByUuid_PrevAndNext(emailConfigurazioneId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the email configuraziones where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of email configuraziones where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching email configuraziones
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione findByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the email configurazione where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the email configurazione where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the email configurazione that was removed
	*/
	public static EmailConfigurazione removeByUUID_G(String uuid, long groupId)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of email configuraziones where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching email configuraziones
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching email configuraziones
	*/
	public static List<EmailConfigurazione> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione findByUuid_C_First(String uuid,
		long companyId, OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUuid_C_First(String uuid,
		long companyId, OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione
	* @throws NoSuchEmailConfigurazioneException if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione findByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching email configurazione, or <code>null</code> if a matching email configurazione could not be found
	*/
	public static EmailConfigurazione fetchByUuid_C_Last(String uuid,
		long companyId, OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the email configuraziones before and after the current email configurazione in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param emailConfigurazioneId the primary key of the current email configurazione
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public static EmailConfigurazione[] findByUuid_C_PrevAndNext(
		long emailConfigurazioneId, String uuid, long companyId,
		OrderByComparator<EmailConfigurazione> orderByComparator)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(emailConfigurazioneId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the email configuraziones where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of email configuraziones where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching email configuraziones
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Caches the email configurazione in the entity cache if it is enabled.
	*
	* @param emailConfigurazione the email configurazione
	*/
	public static void cacheResult(EmailConfigurazione emailConfigurazione) {
		getPersistence().cacheResult(emailConfigurazione);
	}

	/**
	* Caches the email configuraziones in the entity cache if it is enabled.
	*
	* @param emailConfiguraziones the email configuraziones
	*/
	public static void cacheResult(
		List<EmailConfigurazione> emailConfiguraziones) {
		getPersistence().cacheResult(emailConfiguraziones);
	}

	/**
	* Creates a new email configurazione with the primary key. Does not add the email configurazione to the database.
	*
	* @param emailConfigurazioneId the primary key for the new email configurazione
	* @return the new email configurazione
	*/
	public static EmailConfigurazione create(long emailConfigurazioneId) {
		return getPersistence().create(emailConfigurazioneId);
	}

	/**
	* Removes the email configurazione with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione that was removed
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public static EmailConfigurazione remove(long emailConfigurazioneId)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().remove(emailConfigurazioneId);
	}

	public static EmailConfigurazione updateImpl(
		EmailConfigurazione emailConfigurazione) {
		return getPersistence().updateImpl(emailConfigurazione);
	}

	/**
	* Returns the email configurazione with the primary key or throws a {@link NoSuchEmailConfigurazioneException} if it could not be found.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione
	* @throws NoSuchEmailConfigurazioneException if a email configurazione with the primary key could not be found
	*/
	public static EmailConfigurazione findByPrimaryKey(
		long emailConfigurazioneId)
		throws it.puglia.por.areariservata.exception.NoSuchEmailConfigurazioneException {
		return getPersistence().findByPrimaryKey(emailConfigurazioneId);
	}

	/**
	* Returns the email configurazione with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param emailConfigurazioneId the primary key of the email configurazione
	* @return the email configurazione, or <code>null</code> if a email configurazione with the primary key could not be found
	*/
	public static EmailConfigurazione fetchByPrimaryKey(
		long emailConfigurazioneId) {
		return getPersistence().fetchByPrimaryKey(emailConfigurazioneId);
	}

	public static java.util.Map<java.io.Serializable, EmailConfigurazione> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the email configuraziones.
	*
	* @return the email configuraziones
	*/
	public static List<EmailConfigurazione> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @return the range of email configuraziones
	*/
	public static List<EmailConfigurazione> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of email configuraziones
	*/
	public static List<EmailConfigurazione> findAll(int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the email configuraziones.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link EmailConfigurazioneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email configuraziones
	* @param end the upper bound of the range of email configuraziones (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of email configuraziones
	*/
	public static List<EmailConfigurazione> findAll(int start, int end,
		OrderByComparator<EmailConfigurazione> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the email configuraziones from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of email configuraziones.
	*
	* @return the number of email configuraziones
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static EmailConfigurazionePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<EmailConfigurazionePersistence, EmailConfigurazionePersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(EmailConfigurazionePersistence.class);

		ServiceTracker<EmailConfigurazionePersistence, EmailConfigurazionePersistence> serviceTracker =
			new ServiceTracker<EmailConfigurazionePersistence, EmailConfigurazionePersistence>(bundle.getBundleContext(),
				EmailConfigurazionePersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}