<%@ page import="it.puglia.por.areariservata.model.Processo" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.service.ProcessoLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/init.jsp" %>


<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    Long idProcesso = (Long) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_PROCESSOID);
    Processo processo = ProcessoLocalServiceUtil.getProcesso(idProcesso);
    String landingUrl = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REDIRECT);
    String currentURL = portletURL.toString();
    String redirectUrl = (String) request.getAttribute(AreaRiservataPortletKeys.REQ_REDIRECTURL);
    request.setAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW, AreaRiservataPortletKeys.VIEW_GESTIONE_PROCESSI);
%>


<portlet:actionURL name="/createDetailProcesso" var="createDetailProcessoURL">
    <portlet:param name="processoId" value="<%= String.valueOf(idProcesso) %>"/>
    <portlet:param name="redirect" value="<%=landingUrl%>"/>
</portlet:actionURL>


<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>


    <div class="panel panel-secondary">
        <div class="panel-header">Inserimento dettaglio per il processo: <b><%=processo.getNomeProcesso()%>
        </b></div>
        <div class="panel-body">
            <aui:form cssClass="form-group" action="<%= createDetailProcessoURL %>" method="post"
                      name="fm"
                      enctype="multipart/form-data">

                    <aui:input cssClass="form-control lfr-textarea-container" name="testoDettaglio"
                               placeholder=""
                               inlineLabel="true"
                               label="Testo"
                               required="<%= true %>" type="textarea" value="">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input multiple="true" type="file" name="fileAllegato"
                               label="Allegato">
                    </aui:input>

                <aui:button-row>
                    <aui:button type="submit" value="Aggiungi Dettaglio"/>
                    <aui:button type="button" cssClass="btn-default" href="<%=redirectUrl%>"
                                value="Indietro"/>
                </aui:button-row>
            </aui:form>

        </div>
    </div>
</div>



