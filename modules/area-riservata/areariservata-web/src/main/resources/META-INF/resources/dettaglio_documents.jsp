<%--
  Created by IntelliJ IDEA.
  User: luigi
  Date: 2019-06-04
  Time: 09:50
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="init.jsp" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="list-group">
    <li class="list-group-item list-group-item-flex">
        <div class="autofit-col">
            <div class="custom-control custom-checkbox">
                <label>
                    <input class="custom-control-input" type="checkbox"/>
                    <span class="custom-control-indicator"></span>
                </label>
            </div>
        </div>
        <div class="autofit-col">
            <div class="sticker sticker-secondary">
				<span class="inline-item">
					<svg class="lexicon-icon lexicon-icon-folder" focusable="false" role="presentation">
						<use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#folder" />
					</svg>
				</span>
            </div>
        </div>
        <div class="autofit-col autofit-col-expand">
            <p class="list-group-title text-truncate">
                <a href="#1">Account Example One</a>
            </p>
            <p class="list-group-subtitle text-truncate">Second Level Text</p>
        </div>
        <div class="autofit-col">
            <div class="quick-action-menu">
                <a class="component-action quick-action-item" href="#1" role="button">
                    <svg class="lexicon-icon lexicon-icon-trash" focusable="false" role="presentation">
                        <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#trash" />
                    </svg>
                </a>
                <a class="component-action quick-action-item" href="#1" role="button">
                    <svg class="lexicon-icon lexicon-icon-download" focusable="false" role="presentation">
                        <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#download" />
                    </svg>
                </a>
                <a class="component-action quick-action-item" href="#1" role="button">
                    <svg class="lexicon-icon lexicon-icon-info-circle-open" focusable="false" role="presentation">
                        <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#info-circle-open" />
                    </svg>
                </a>
            </div>
            <div class="dropdown dropdown-action">
                <a aria-expanded="false" aria-haspopup="true" class="component-action dropdown-toggle" data-toggle="dropdown" href="#1" id="dropdownAction1" role="button">
                    <svg class="lexicon-icon lexicon-icon-ellipsis-v" focusable="false" role="presentation">
                        <use href="<%=themeDisplay.getPathThemeImages() %>/icons/icons.svg#ellipsis-v" />
                    </svg>
                </a>
                <div aria-labelledby="" class="dropdown-menu dropdown-menu-right">
                    <ul class="list-unstyled">
                        <li><a class="dropdown-item" href="#1" role="button">Download</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
</ul>