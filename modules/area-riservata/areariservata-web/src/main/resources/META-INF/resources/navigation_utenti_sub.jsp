<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.service.ProcessoLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ include file="init.jsp" %>

<%
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String currentViewName = request.getAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW) != null ? (String) request.getAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW) : AreaRiservataPortletKeys.VIEW_DEFAULT_VIEW;
    String currentSubViewName = request.getAttribute(AreaRiservataPortletKeys.VIEW_SUB_CURRENT_VIEW) != null ? (String) request.getAttribute(AreaRiservataPortletKeys.VIEW_SUB_CURRENT_VIEW) : AreaRiservataPortletKeys.VIEW_SUB_DEFAULT_VIEW;
%>


<portlet:renderURL var="showElencoUtentiURL">
    <portlet:param name="mvcRenderCommandName" value="/showElencoUtenti"/>
    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
</portlet:renderURL>

<portlet:renderURL var="showFormGestioneUtentiURL">
    <portlet:param name="mvcRenderCommandName" value="/showElencoUtenti"/>
    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
</portlet:renderURL>


<nav class="navbar navbar-collapse-absolute navbar-expand-md navbar-underline navigation-bar navigation-bar-detail">
    <div class="container-fluid container-fluid-max-xl">
        <a aria-controls="navigationBarCollapse01" aria-expanded="false" aria-label="Toggle navigation"
           class="collapsed navbar-toggler navbar-toggler-link" data-toggle="collapse"
           href="#navigationBarCollapse01" role="button">
            <span class="navbar-text-truncate">Gestione Processii</span>
            <svg class="lexicon-icon lexicon-icon-caret-bottom" focusable="false" role="presentation">
                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#caret-bottom"/>
            </svg>
        </a>
        <div class="collapse navbar-collapse" id="navigationBarCollapse01">
            <div class="container-fluid container-fluid-max-xl">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <%=currentSubViewName.equalsIgnoreCase(AreaRiservataPortletKeys.VIEWSUB_UTENTI_ELENCO) ? "active":""%>"
                           href="<%=showElencoUtentiURL%>">
                            <span class="navbar-text-truncate">Elenco utenti</span>
                        </a>
                    </li>
                    <li aria-label="Current Page" class="nav-item">
                        <a class="nav-link <%=currentSubViewName.equalsIgnoreCase(AreaRiservataPortletKeys.VIEWSUB_UTENTI_AGGIUNGI) ? "active":""%>"
                           href="<%=showFormGestioneUtentiURL%>">
                            <span class="navbar-text-truncate">Aggiungi utente</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</nav>
