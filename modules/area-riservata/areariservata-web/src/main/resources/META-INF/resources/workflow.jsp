<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.Workflow" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.WorkflowFactory" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/init.jsp" %>
<%@ include file="init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    Long idProcesso = (Long) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_PROCESSOID);
    String nomeWorkflow = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_NOMEWORKFLOW);
    String currentStatoWorkflow = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_STATOWORKFLOW);
    Workflow workflow = WorkflowFactory.getWorkflow(nomeWorkflow);
        currentStatoWorkflow = currentStatoWorkflow.replaceAll("_", " ");
%>

<script>
    AUI().ready(
        function () {
            timeline(document.querySelectorAll('.timeline'));
        }
    );

</script>

<div class="sampletimeline">
    <div class="timeline">
        <div class="timeline__wrap">
            <div class="timeline__items">

                <% List<String> workflowStateList = workflow.getWorkflowStructureAsList();%>

                <% for (String sorveglianzaEnum : workflowStateList) { %>
                <div class="timeline__item">
                    <div class="timeline__content">
                        <% if (sorveglianzaEnum.equals(currentStatoWorkflow)) { %>
                        <font color="red"><b><%=sorveglianzaEnum %>
                        </b></font>
                        <% } else { %>
                        <%=sorveglianzaEnum %>
                        <% } %>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</div>