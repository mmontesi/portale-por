<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.model.UserGroup" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.Map" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.WorkflowSorveglianza" %>
<%@ page import="it.puglia.por.areariservata.common.enums.PartenariatoEnum" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.WorkflowFactory" %>
<%@ include file="/init.jsp" %>


<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = (String) request.getAttribute(AreaRiservataPortletKeys.REQ_REDIRECTURL);
    UserGroup defaultUserGroup = (UserGroup) request.getAttribute(AreaRiservataPortletKeys.REQ_DEFAULT_USERGROUP);
    Map<UserGroup, List<User>> userGroupUserMap = (Map<UserGroup, List<User>>) request.getAttribute(AreaRiservataPortletKeys.REQ_USERMAP);
%>


<portlet:actionURL name="/createProcesso" var="createProcessoURL">
</portlet:actionURL>


<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>

    <div class="panel panel-secondary">
        <div class="panel-header">Inserimento nuovo processo</div>
        <div class="panel-body">
            <aui:form cssClass="form-group" action="<%= createProcessoURL %>" method="post" name="fm"
                      enctype="multipart/form-data">
                <aui:fieldset markupView="lexicon" column="12" label="Compilare tutti i campi necessari (*)">
                    <aui:input name="userToAdd" type="hidden"/>

                    <aui:input name="nomeProcesso" placeholder=""
                               label="Nome processo"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input cssClass="form-control" name="descrizioneProcesso" placeholder=""
                               label="Descrizione"
                               required="<%= true %>" type="textarea" value="">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:select cssClass="w100" name="req_workflow_name" label="Workflow">

                        <aui:option label="Workflow Partenariato"
                                    value="<%=WorkflowFactory.WORKFLOW_PARTENARIATO%>"/>
                        <aui:option label="Workflow Sorveglianza"
                                    value="<%=WorkflowFactory.WORKFLOW_SORVEGLIANZA%>"/>

                    </aui:select>

                    <aui:select cssClass="w100" name="gruppoMittenteId" label="Gruppo mittente del processo">

                        <aui:option label="--- Mittente di default (Segreteria Tecnica) ----"
                                    value="<%=defaultUserGroup != null ? defaultUserGroup.getUserGroupId() : ""%>"/>

                        <%
                            for (UserGroup userGroup : userGroupUserMap.keySet()) {

                                if (userGroup.getUserGroupId() != defaultUserGroup.getUserGroupId()) {
                        %>
                        <aui:option label="<%=userGroup.getDescription()%>"
                                    value="<%=userGroup.getUserGroupId()%>"/>
                        <%
                                }
                            }
                        %>


                    </aui:select>

                    <aui:input type="file" multiple="true" name="fileAllegato" label="Documento da allegare">
                    </aui:input>

                    <div class="table-responsive lfr-table">
                        <table class="show-quick-actions-on-hover table table-autofit table-hover table-list table-nowrap">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Gruppo</th>
                                <th>
                                    Utente
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <% for (UserGroup userGroup : userGroupUserMap.keySet()) {
                                int idx    = 0;
                                int global = 0;%>

                            <tr>
                                <td>
                                    <input name="fake" value="<%=userGroup.getUserGroupId()%>"
                                           label="" id="group#<%=userGroup.getUserGroupId()%>"
                                           class="checkboxGroup <%="group#"+userGroup.getUserGroupId()%>"
                                           type="checkbox"/>
                                </td>
                                <td><%=  userGroup.getDescription() %>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <% for (User userr : userGroupUserMap.get(userGroup)) {%>
                            <tr>
                                <td>
                                    <input name="userToAdd[<%=global%>]" value="<%=userr.getUserId()%>"
                                           label=""
                                           id="<%="group#"+userGroup.getUserGroupId()+"#"+userr.getUserId()%>"
                                           class="checkboxUser <%="group#"+userGroup.getUserGroupId()+"#"+userr.getUserId()%>"
                                           type="checkbox"/>
                                </td>
                                <td></td>
                                <td><%=userr.getFullName()%>
                                </td>
                            </tr>
                            <%
                                        idx += 1;
                                        global += 1;
                                    }
                                }
                            %>
                            <tr>
                                <td colspan="3">
                                    <aui:button type="button" onClick="checkAll()" value="Seleziona tutti"/>
                                    <aui:button type="button" onClick="resetCheck()" value="Reset Selezione"/>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </aui:fieldset>
                <aui:button type="submit" value="Salva"/>
                <aui:button type="button" href="<%=redirectUrl%>" value="Indietro"/>
            </aui:form>
        </div>
    </div>
</div>


<script type="text/javascript">

    var userToAdd = [];

    function checkAll() {
        $(".checkboxUser").prop("checked", "checked");
        $(".checkboxGroup").prop("checked", "checked");
        $("input:checkbox").trigger("change");

    }

    function resetCheck() {
        $(".checkboxUser").prop("checked", "");
        $(".checkboxGroup").prop("checked", "");
        userToAdd = [];
    }

    function addOrRemoveElement(checkValue, idToAddOrRemove) {
        if (checkValue) {
            userToAdd.push(idToAddOrRemove);
        } else {
            userToAdd = _.reject(userToAdd, function (d) {
                return d === idToAddOrRemove;
            });
        }
        document.<portlet:namespace/>fm.<portlet:namespace/>userToAdd.value = _.uniq(userToAdd).join(",");
    }

    AUI().ready(
        function () {
            $("input:checkbox").change(function (value) {
                var checkValue = this.checked;
                var isAGroup = $(this).attr("id").split("#").length == 2;

                if (isAGroup) {
                    var idTocheck = $(this).attr("id").split("#")[1];
                    $('input[id*="group#' + idTocheck + '#"]').each(function (element, inputTag) {
                        $(inputTag).prop('checked', checkValue);
                        addOrRemoveElement(checkValue, idTocheck);
                    });
                }
            });
        }
    );
</script>