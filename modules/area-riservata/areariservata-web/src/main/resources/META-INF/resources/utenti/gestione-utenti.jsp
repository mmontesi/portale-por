<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.web.util.AreaRiservataRolesUtil" %>
<%@ page import="it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/META-INF/resources/init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = ParamUtil.getString(request, "currentURL", "");
%>

<liferay-ui:success key="reg-success" message="reg-succes"/>
<liferay-ui:error key="reg-error-mail" message="reg-error-mail"/>

<liferay-ui:error key="user-update-error" message="user-update-error"/>
<liferay-ui:success key="user-update-success" message="user-update-success"/>

<portlet:actionURL name="/createUtente" var="createUtenteURL">
    <portlet:param name="redirect" value="<%=currentURL%>"/>
</portlet:actionURL>

<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_utenti_sub.jsp" servletContext="<%= application %>"/>


    <div class="panel panel-secondary">
        <div class="panel-header">Elenco Utenti</div>
        <div class="panel-body">
            <ul class="list-group">
                <%

                    List<User> elencoUtenti = AreaRiservataUsersLocalServiceUtil.getUserOfAreaRiservata(0, 100);

                    for (User utente : elencoUtenti) {
                %>
                <li class="list-group-item list-group-item-flex">
                    <div class="autofit-col">
                        <p class="list-group-title text-truncate">
                            <c:if test="<%=utente.getStatus() == 0%>">
                            <span class="label label-success">
	                            <span class="label-item label-item-expand">Abilitato</span>
                            </span>
                            </c:if>
                            <c:if test="<%=utente.getStatus() == 5%>">
                            <span class="label label-danger">
	                            <span class="label-item label-item-expand">Disattivato</span>
                            </span>
                            </c:if>
                        </p>
                    </div>
                    <div class="autofit-col">
                        <p class="list-group-title text-truncate">
                            <a href="#1"><%=utente.getFullName()%>
                            </a>
                        </p>
                        <p class="list-group-subtitle text-truncate"><%=utente.getEmailAddress()%>

                        </p>
                        <p class="list-group-subtitle text-truncate">
                            <% for (String roleName : AreaRiservataRolesUtil.groupNamesOfUser(utente)) {%>

                            <span class="label label-secondary">
                            <span class="label-item label-item-expand"><%=roleName%></span>
                        </span>
                            <% } %>
                        </p>
                    </div>
                    <div class="autofit-col autofit-col-expand">
                        &nbsp;
                    </div>

                    <liferay-portlet:renderURL var="showFormGestioneUtentiForUpdateURL">
                        <portlet:param name="mvcRenderCommandName" value="/showFormGestioneUtentiForUpdate"/>
                        <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%= currentURL %>"/>
                        <portlet:param name="<%=AreaRiservataPortletKeys.REQ_ID_USER_TO_UPDATE%>"
                                       value="<%= String.valueOf(utente.getUserId()) %>"/>
                    </liferay-portlet:renderURL>

                    <portlet:actionURL name="/enableDisableUser" var="enableDisableUserURL">
                        <portlet:param name="redirect" value="<%=currentURL%>"/>
                        <portlet:param name="<%=AreaRiservataPortletKeys.REQ_ID_USER_TO_UPDATE%>"
                                       value="<%= String.valueOf(utente.getUserId()) %>"/>
                    </portlet:actionURL>


                    <div class="dropdown dropdown-action">
                        <a aria-expanded="false" aria-haspopup="true" class="component-action dropdown-toggle"
                           data-toggle="dropdown" href="#1" id="dropdownAction1" role="button">
                            <clay:icon symbol="ellipsis-v"/>
                        </a>
                        <div aria-labelledby="" class="dropdown-menu dropdown-menu-right">
                            <ul class="list-unstyled">
                                <li><a class="dropdown-item" href="<%=showFormGestioneUtentiForUpdateURL%>"
                                       role="button">Modifica</a></li>
                                <li><a class="dropdown-item" href="<%=enableDisableUserURL%>"
                                       role="button"><%=utente.getStatus() == 0 ? "Disattiva" : "Attiva"%>
                                </a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <% } %>
            </ul>

        </div>
    </div>
</div>