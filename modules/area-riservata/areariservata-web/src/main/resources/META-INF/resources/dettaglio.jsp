<%@ page import="com.liferay.document.library.kernel.service.DLAppLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.portlet.PortletURLFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.repository.model.FileEntry" %>
<%@ page import="com.liferay.portal.kernel.service.UserLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PortletKeys" %>
<%@ page import="com.liferay.portal.kernel.util.TextFormatter" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.WorkflowFactory" %>
<%@ page import="it.puglia.por.areariservata.model.DettaglioProcesso" %>
<%@ page import="it.puglia.por.areariservata.model.DocumentoProcesso" %>
<%@ page import="it.puglia.por.areariservata.service.DettaglioProcessoLocalServiceUtil" %>
<%@ page import="it.puglia.por.areariservata.service.DocumentoProcessoLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletRequest" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ include file="/init.jsp" %>
<%@ include file="init.jsp" %>


<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = PortalUtil.getCurrentURL(renderRequest);//portletURL.toString();
    Long idProcesso = (Long) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_PROCESSOID);
    String nomeProcesso = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_NOMEPROCESSO);
    String descrizioneProcesso = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_DESCRIZIONEPROCESSO);
    String statoProcesso = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_STATOWORKFLOW);
    String workflowName = (String) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_WORKFLOW_NAME);
    Long idFolderProcesso = (Long) renderRequest.getAttribute(AreaRiservataPortletKeys.REQ_FOLDER_ID_PROCESSO);

    PortletURL viewMetadataSetsURL = PortletURLFactoryUtil.create(
            request,
            PortletKeys.DOCUMENT_LIBRARY_ADMIN,
            PortletRequest.RENDER_PHASE);

        viewMetadataSetsURL.setParameter("mvcRenderCommandName", "/document_library/view_folder");
        viewMetadataSetsURL.setParameter("groupId", String.valueOf(themeDisplay.getScopeGroupId()));
        viewMetadataSetsURL.setParameter("refererPortletName", PortletKeys.DOCUMENT_LIBRARY_ADMIN);

    String nextStateWorkflow = WorkflowFactory.getWorkflow(workflowName).getNextState(statoProcesso);
    List<DocumentoProcesso> documentiList = DocumentoProcessoLocalServiceUtil.getAllDocumentsFromProcesso(idProcesso);
    List<DettaglioProcesso> dettaglioProcessoList = DettaglioProcessoLocalServiceUtil.findByProcesso(idProcesso);
%>


<% if (idProcesso != null) { %>
<div class="container-fluid w100">

    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>

    <div class="panel panel-secondary">
        <div class="panel-header">
            <span class="pull-left">Dettaglio del processo: <b><%=nomeProcesso%>            </b></span>
            <span class="pull-right"><clay:label label="<%=statoProcesso%>" style="info"/>&nbsp;<clay:label
                    label="<%=workflowName%>" style="default"/></span>
            <div class="clearfix"></div>
        </div>
        <div class="panel-header">
            <p><%=descrizioneProcesso%></p>
        </div>
        <div class="panel-body">
            <portlet:renderURL var="showFormCreateDetailProcessoURL">
                <portlet:param name="mvcRenderCommandName" value="/showFormCreateDetailProcesso"/>
                <portlet:param name="processoId" value="<%= String.valueOf(idProcesso) %>"/>
                <portlet:param name="redirect" value="<%=currentURL%>"/>
            </portlet:renderURL>

            <portlet:actionURL name="/redirectToDocument" var="redirectToDocumentURL">
                <portlet:param name="processoId" value="<%= String.valueOf(idProcesso) %>"/>
            </portlet:actionURL>

            <portlet:actionURL name="/updateFlowProcesso" var="updateFlowProcessoURL">
                <portlet:param name="<%=AreaRiservataPortletKeys.REQ_PROCESSOID%>"
                               value="<%=String.valueOf(idProcesso) %>"/>
                <portlet:param name="<%=AreaRiservataPortletKeys.REQ_WORKFLOW_NEXT%>"
                               value="<%=String.valueOf(nextStateWorkflow) %>"/>
            </portlet:actionURL>



            <liferay-ui:search-container total="<%= DettaglioProcessoLocalServiceUtil.countByProcesso(idProcesso) %>">

                <liferay-ui:search-container-results
                        results="<%= DettaglioProcessoLocalServiceUtil.findByProcesso(idProcesso) %>"
                />

                <liferay-ui:search-container-row
                        className="it.puglia.por.areariservata.model.DettaglioProcesso"
                        escapedModel="true"
                        modelVar="dettaglio">

                    <liferay-ui:search-container-column-text
                            name="ID" title="ID">
                        <%= dettaglio.getDettaglioProcessoId()%>
                    </liferay-ui:search-container-column-text>

                    <liferay-ui:search-container-column-text   cssClass="w200px"
                            name="Dettaglio" title="Dettaglio">
                        <%= StringUtil.shorten(dettaglio.getTestoDettaglio(), 200)%>
                    </liferay-ui:search-container-column-text>


                    <liferay-ui:search-container-column-text
                            name="Stato" title="Stato workflow">
                        <%=dettaglio.getNomeWorkflow() + " - " + dettaglio.getStatoWorkflow() %>
                    </liferay-ui:search-container-column-text>

                    <liferay-ui:search-container-column-text
                            name="Inserito il" title="Inserito il ">
                        <%=sdf.format(dettaglio.getCreateDate()) %>
                    </liferay-ui:search-container-column-text>

                    <liferay-ui:search-container-column-text
                            name="Inserito da" title="Inserito da">
                        <%=UserLocalServiceUtil.getUser(dettaglio.getUserId()).getEmailAddress() %>
                    </liferay-ui:search-container-column-text>

                </liferay-ui:search-container-row>

                <liferay-ui:search-iterator/>

            </liferay-ui:search-container>
        </div>

        <div class="panel-footer">
            <a href="<%=showFormCreateDetailProcessoURL%>" onclick=""
               class="btn btn-primary">Inserisci Dettaglio</a>
            <%
                boolean hasNextStateWorkflow     = nextStateWorkflow != null && !"".equals(nextStateWorkflow.trim());
            %>
            <c:if test="<%=hasNextStateWorkflow%>">
                <a href="<%=updateFlowProcessoURL%>" onclick=""
                   class="btn btn-success">Avanzamento a stato successivo - <%=nextStateWorkflow%>
                </a>
            </c:if>
        </div>
    </div>


    <div class="panel panel-secondary">
        <div class="panel-header">Documenti allegati</div>
        <div class="panel-body">
            <% for (DocumentoProcesso documentoProcesso : documentiList) {


                if (documentoProcesso.getDlFileId() > 0) {
                    FileEntry fileEntry   = DLAppLocalServiceUtil.getFileEntry(documentoProcesso.getDlFileId());
                    String    portalURL   = PortalUtil.getPortalURL(request);
                    String    allegatoUrl = portalURL + "/c/document_library/get_file?uuid=" + fileEntry.getUuid() + "&groupId=" + fileEntry.getGroupId();

            %>

            <li class="list-group-item list-group-item-flex">
                <div class="autofit-col">
                    <div class="custom-control custom-checkbox">
                        <label>
                            <input class="custom-control-input" type="checkbox"/>
                            <span class="custom-control-indicator"></span>
                        </label>
                    </div>
                </div>
                <div class="autofit-col">
                    <div class="sticker sticker-secondary">
                        <span class="inline-item">
                            <svg class="lexicon-icon lexicon-icon-folder" focusable="false" role="presentation">
                                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#folder"/>
                            </svg>
                        </span>
                    </div>
                </div>

                <div class="autofit-col autofit-col-expand">
                    <p class="list-group-title text-truncate">
                        <a href="<%=allegatoUrl%>"><%=fileEntry.getFileName()%>
                        </a>
                    </p>
                    <p class="list-group-subtitle text-truncate"><%= TextFormatter.formatStorageSize(fileEntry.getSize(), locale)%>
                    </p>
                    <b><%=sdf.format(documentoProcesso.getCreateDate())%>
                    </b>
                </div>
                <div class="autofit-col">
                    <div class="custom-control">
                        <span class="inline-item">
                            <c:if test="<%=documentoProcesso.getProcessoId() > 0  && documentoProcesso.getDettaglioProcessoId() == 0%>">
                                Documento caricato contestualmente alla creazione del processo.
                            </c:if>
                            <c:if test="<%=documentoProcesso.getProcessoId() > 0  && documentoProcesso.getDettaglioProcessoId() > 0%>">
                                Dettaglio ID# <%=documentoProcesso.getDettaglioProcessoId() + ""%>
                            </c:if>
                        </span>
                    </div>
                </div>
                <div class="autofit-col">
                    <div class="quick-action-menu">
                        <a class="component-action quick-action-item" href="#1" role="button">
                            <svg class="lexicon-icon lexicon-icon-trash" focusable="false" role="presentation">
                                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#trash"/>
                            </svg>
                        </a>
                        <a class="component-action quick-action-item" href="#1" role="button">
                            <svg class="lexicon-icon lexicon-icon-download" focusable="false" role="presentation">
                                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#download"/>
                            </svg>
                        </a>
                        <a class="component-action quick-action-item" href="#1" role="button">
                            <svg class="lexicon-icon lexicon-icon-info-circle-open" focusable="false"
                                 role="presentation">
                                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#info-circle-open"/>
                            </svg>
                        </a>
                    </div>
                    <div class="dropdown dropdown-action">
                        <a aria-expanded="false" aria-haspopup="true" class="component-action dropdown-toggle"
                           data-toggle="dropdown" href="#1" id="dropdownAction1" role="button">
                            <svg class="lexicon-icon lexicon-icon-ellipsis-v" focusable="false" role="presentation">
                                <use href="<%=themeDisplay.getPathThemeImages() %>/icons/icons.svg#ellipsis-v"/>
                            </svg>
                        </a>
                        <div aria-labelledby="" class="dropdown-menu dropdown-menu-right">
                            <ul class="list-unstyled">
                                <li><a class="dropdown-item" href="<%=allegatoUrl%>" role="button">Download</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
            <% }
            } %>
        </div>
    </div>

    <div class="panel panel-secondary">
        <div class="panel-header">Timeline Processo</div>
        <div class="panel-body">
            <%--
                        <liferay-util:include page="/dettaglio_documents.jsp" servletContext="<%= application %>"/>
            --%>

            <div class="sampletimeline">
                <div class="timeline">
                    <div class="timeline__wrap">
                        <div class="timeline__items">

                            <% for (DettaglioProcesso dettaglio : dettaglioProcessoList) { %>
                            <div class="timeline__item">
                                <div class="timeline__content">
                                    <b><%="ID#" + dettaglio.getDettaglioProcessoId() + " "%>
                                    </b>
                                    <p><%=dettaglio.getTestoDettaglio() %>
                                    </p>
                                    <div class="clearfix"></div>
                                    <small style="color:#4f66ff;"><%=sdf.format(dettaglio.getCreateDate())%>
                                    </small>
                                    <div class="clearfix"></div>
                                    <small style="color:#2b627a;"><%=UserLocalServiceUtil.getUser(dettaglio.getUserId()).getEmailAddress()%>
                                    </small>
                                    <div class="clearfix"></div>

                                    <%
                                        List<DocumentoProcesso> documentiDettaglioList = DocumentoProcessoLocalServiceUtil.getAllDocumentsFromDettaglio(idProcesso, dettaglio.getDettaglioProcessoId());

                                        for (DocumentoProcesso documentoProcesso : documentiDettaglioList) {
                                            String    portalURL   = PortalUtil.getPortalURL(request);
                                            String    allegatoUrl = null;
                                            FileEntry fileEntry   = null;

                                            if (documentoProcesso.getDlFileId() > 0) {
                                                fileEntry   = DLAppLocalServiceUtil.getFileEntry(documentoProcesso.getDlFileId());
                                                allegatoUrl = portalURL + "/c/document_library/get_file?uuid=" + documentoProcesso.getUuid() + "&groupId=" + documentoProcesso.getGroupId();

                                    %>
                                    <small>
                                        <a href="<%=allegatoUrl%>">Download <%=fileEntry.getFileName()%>
                                        </a>
                                    </small>
                                    <%
                                            }
                                        }
                                    %>

                                </div>
                            </div>
                            <% } %>
                        </div>
                    </div>
                </div>
            </div>

            <% } else { %>
            <h2>Non &egrave; disponibile alcun dettaglio.</h2>
            <% } %>
        </div>
    </div>
</div>

<script>
    AUI().ready(
        function () {
            timeline(document.querySelectorAll('.timeline'));
        }
    );

</script>