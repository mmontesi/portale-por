<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ include file="/init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = ParamUtil.getString(request, "currentURL", "");
    String contentBody = "";


    String subject = portletPreferences.getValue(AreaRiservataPortletKeys.REQ_CONF_SUBJECT, StringPool.BLANK);
    String fromUser = portletPreferences.getValue(AreaRiservataPortletKeys.REQ_CONF_FROM_USER, StringPool.BLANK);
    String bodyMail = portletPreferences.getValue(AreaRiservataPortletKeys.REQ_CONF_BODY_MAIL, StringPool.BLANK);

    String subjectEmail = GetterUtil.getString(portletPreferences.
            getValue(AreaRiservataPortletKeys.REQ_CONF_SUBJECT, StringPool.TRUE));
%>

<portlet:actionURL name="/createUtente" var="createUtenteURL">
    <portlet:param name="redirect" value="<%=currentURL%>"/>
</portlet:actionURL>


<div class="container-fluid container-fluid-max-lg w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>

    <div class="panel panel-secondary">
        <div class="panel-header">Configura messaggio email</div>
        <div class="panel-body">
            <form class="form-group" method="post" name="fm">
                <aui:fieldset markupView="lexicon" column="12" label="Compilare tutti i campi necessari (*)">

                    <aui:input name="subject" placeholder="" disabled="true"
                               label="Oggetto della mail"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="<%=subjectEmail%>">
                    </aui:input>

                    <aui:input name="fromuser" placeholder=""
                               label="Mittente"
                               disabled="true"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="<%=fromUser%>">
                        <aui:validator name="required"/>
                    </aui:input>

                    <div class="alloy-editor-container">
                        <aui:input
                                type="textarea"
                                disabled="true"
                                contents="<%=bodyMail%>"
                                cssClass="my-alloy-editor"
                                name="bodyMail"
                                label="Body email"
                        />
                    </div>
                </aui:fieldset>


            </form>
        </div>
    </div>
</div>