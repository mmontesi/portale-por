<%@ include file="init.jsp" %>


<nav class="application-bar application-bar-dark navbar navbar-expand-md">
    <div class="container-fluid container-fluid-max-xl">
        <ul class="navbar-nav">
            <li class="nav-item">

            </li>
        </ul>
        <div class="navbar-title navbar-text-truncate">Area Riservata</div>
        <ul class="navbar-nav">
            <li class="dropdown nav-item">
                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle nav-link nav-link-monospaced"
                   data-toggle="dropdown" href="#1" role="button">
                    <svg class="lexicon-icon lexicon-icon-ellipsis-v" focusable="false" role="presentation">
                        <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#ellipsis-v"/>
                    </svg>
                </a>

            </li>
        </ul>
    </div>
</nav>