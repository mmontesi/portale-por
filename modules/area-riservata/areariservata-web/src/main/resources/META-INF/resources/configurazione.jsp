<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="it.puglia.por.areariservata.web.portlet.configuration.AreaRiservataConfiguration" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = ParamUtil.getString(request, "currentURL", "");
    String contentBody = "";


    AreaRiservataConfiguration areaRiservataConfiguration =
            (AreaRiservataConfiguration)
                    request.getAttribute(AreaRiservataConfiguration.class.getName());
    String fromUser = StringPool.BLANK;
    String subject = StringPool.BLANK;
    String bodyMail = StringPool.BLANK;
    if (Validator.isNotNull(areaRiservataConfiguration)) {
        fromUser = portletPreferences.getValue("fromUser", areaRiservataConfiguration.fromUser());
        subject  = portletPreferences.getValue("subject", areaRiservataConfiguration.subject());
        bodyMail = portletPreferences.getValue("bodyMail", areaRiservataConfiguration.bodyMail());
    }
%>


<liferay-portlet:actionURL portletConfiguration="<%= true %>"
                           var="configurationActionURL"/>

<liferay-portlet:renderURL portletConfiguration="<%= true %>"
                           var="configurationRenderURL"/>




<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>


    <div class="panel panel-secondary">
        <div class="panel-header">Configura messaggio email</div>
        <div class="panel-body">
            <aui:form action="<%=configurationActionURL%>" method="post" name="fm">
                <aui:fieldset-group>
                    <aui:spacer/>
                    <p>Per poter procedere all'iscrizione sul portale, compilare il seguente form:</p>
                    <aui:spacer/>
                    <aui:fieldset column="6" label="Dati Anagrafici">
                        <aui:col cssClass="col-lg-6 col-xs-12">
                            <aui:input name="subject" placeholder="" inlineLabel="true" label="Oggetto"
                                       required="<%= true %>" type="text" value="<%=subject%>">
                                <aui:validator name="required"/>
                            </aui:input>
                            <aui:input name="fromUser" placeholder="" inlineLabel="true" label="Email da"
                                       required="<%= true %>" type="text" value="<%=fromUser%>">
                                <aui:validator name="required"/>
                                <aui:validator name="email"/>
                            </aui:input>

                            <aui:input name="bodyMail" placeholder=""  inlineLabel="true" label="Corpo "
                                       required="<%= true %>" type="textarea" value="<%=bodyMail%>">
                                <aui:validator name="required"/>
                            </aui:input>
                        </aui:col>

                    </aui:fieldset>
                </aui:fieldset-group>


                <aui:button-row>
                    <aui:button type="submit" value="Aggiorna e inizializza ruoli"/>
                </aui:button-row>
            </aui:form>

        </div>
    </div>
</div>
