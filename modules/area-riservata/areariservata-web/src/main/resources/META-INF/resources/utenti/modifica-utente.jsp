<%@ page import="com.liferay.portal.kernel.model.User" %>
<%@ page import="com.liferay.portal.kernel.model.UserGroup" %>
<%@ page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.web.util.AreaRiservataRolesUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/META-INF/resources/init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = ParamUtil.getString(request, AreaRiservataPortletKeys.REQ_USER_TO_UPDATE, "");
    User userToUpdate = (User) request.getAttribute(AreaRiservataPortletKeys.REQ_USER_TO_UPDATE);

%>

<liferay-ui:success key="reg-success" message="reg-succes"/>
<liferay-ui:error key="reg-error-mail" message="reg-error-mail"/>

<portlet:actionURL name="/updateUtenteAR" var="updateUtenteARURL">
    <portlet:param name="redirect" value="<%=currentURL%>"/>
</portlet:actionURL>


<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_utenti_sub.jsp" servletContext="<%= application %>"/>


    <div class="panel panel-secondary">
        <div class="panel-header">Aggiornamento dati utente: <b><%=userToUpdate.getFullName()%>
        </b>
        </div>
        <div class="panel-body">
            <aui:form cssClass="form-group" action="<%= updateUtenteARURL %>" method="post" name="fm"
                      enctype="multipart/form-data">
                <aui:fieldset markupView="lexicon" column="12" label="Compilare tutti i campi necessari (*)">
                    <aui:input name="<%=AreaRiservataPortletKeys.REQ_ID_USER_TO_UPDATE%>" type="hidden" value="<%=userToUpdate.getUserId()%>"/>

                    <aui:input name="name" disabled="true" placeholder=""
                               label="Nome"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="<%=userToUpdate.getFirstName()%>">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input name="cognome" placeholder="" disabled="true"
                               label="Cognome"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="<%=userToUpdate.getLastName()%>">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input name="email" placeholder=""
                               label="E-Mail"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="<%=userToUpdate.getEmailAddress()%>">
                        <aui:validator name="required"/>
                        <aui:validator name="email"/>
                    </aui:input>

                    <aui:input name="password" placeholder=""
                               label="Password"
                               cssClass="form-control"
                               required="<%= true %>" type="password" value="">
                        <aui:validator name="required"/>

                    </aui:input>


                    <aui:input name="confirm" placeholder=""
                               label="Conferma password"
                               cssClass="form-control"
                               required="<%= false %>" type="password" value="">
                        <aui:validator name="required"/>

                        <aui:validator name="equalTo"
                                       errorMessage="Le password non corrispondono">'#<portlet:namespace/>password'</aui:validator>

                    </aui:input>
                    <aui:select name="gruppo" label="Gruppo" required="<%= true %>">
                        <aui:option label="-----" value=""/>
                        <% for (UserGroup userGroup : UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())) {%>
                        <aui:option selected="<%=AreaRiservataRolesUtil.userIsInTheGroup(userToUpdate,userGroup)%>"
                                    label="<%=userGroup.getDescription()%>" value="<%=userGroup.getUserGroupId()%>"/>
                        <%}%>
                    </aui:select>

                </aui:fieldset>
                <aui:button type="submit" value="Salva"/>
                <aui:button type="button" href="<%=redirectUrl%>" value="Indietro"/>
                <%=redirectUrl%>

            </aui:form>
        </div>
    </div>
</div>