<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ include file="init.jsp" %>

<%
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String currentViewName = request.getAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW) != null ? (String) request.getAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW) : AreaRiservataPortletKeys.VIEW_DEFAULT_VIEW;
%>


<portlet:renderURL var="showFormGestioneUtentiURL">
    <portlet:param name="mvcRenderCommandName" value="/showFormGestioneUtenti"/>
    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
</portlet:renderURL>

<portlet:renderURL var="showAuditURL">
    <portlet:param name="mvcRenderCommandName" value="/showAuditPage"/>
    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
</portlet:renderURL>



<liferay-portlet:renderURL  var="showFormGestioneProcessoURL">
    <portlet:param name="mvcPath" value="/view.jsp"/>

</liferay-portlet:renderURL>

<portlet:renderURL var="showFormGestioneEmailURL">
    <portlet:param name="mvcRenderCommandName" value="/showFormGestioneEmail"/>
    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
</portlet:renderURL>

<nav class="navbar navbar-collapse-absolute navbar-expand-md navbar-underline navigation-bar navigation-bar-secondary">
    <div class="container-fluid container-fluid-max-xl">
        <a aria-controls="navigationBarCollapse01" aria-expanded="false" aria-label="Toggle navigation"
           class="collapsed navbar-toggler navbar-toggler-link" data-toggle="collapse"
           href="#navigationBarCollapse01" role="button">
            <span class="navbar-text-truncate">Gestione Processii</span>
            <svg class="lexicon-icon lexicon-icon-caret-bottom" focusable="false" role="presentation">
                <use href="<%= themeDisplay.getPathThemeImages() %>/icons/icons.svg#caret-bottom"/>
            </svg>
        </a>
        <div class="collapse navbar-collapse" id="navigationBarCollapse01">
            <div class="container-fluid container-fluid-max-xl">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link <%=currentViewName.equalsIgnoreCase(AreaRiservataPortletKeys.VIEW_GESTIONE_PROCESSI) ? "active":""%>" href="<%=showFormGestioneProcessoURL%>">
                            <span class="navbar-text-truncate">Gestione Processi</span>
                        </a>
                    </li>
                    <li aria-label="Current Page" class="nav-item">
                        <a class="nav-link <%=currentViewName.equalsIgnoreCase(AreaRiservataPortletKeys.VIEW_GESTIONE_UTENTI) ? "active":""%>" href="<%=showFormGestioneUtentiURL%>">
                            <span class="navbar-text-truncate">Gestione utenti</span>
                        </a>
                    </li>
                    <li aria-label="Audit Page" class="nav-item">
                        <a class="nav-link <%=currentViewName.equalsIgnoreCase(AreaRiservataPortletKeys.VIEW_AUDIT) ? "active":""%>" href="<%=showAuditURL%>">
                            <span class="navbar-text-truncate">Audit Log</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <%=currentViewName.equalsIgnoreCase(AreaRiservataPortletKeys.GESTIONE_IMPOSTAZIONI_EMAIL) ? "active":""%>" href="<%=showFormGestioneEmailURL%>">
                            <span class="navbar-text-truncate">Impostazioni Email</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
