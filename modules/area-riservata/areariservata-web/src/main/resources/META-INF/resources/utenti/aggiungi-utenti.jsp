<%@ page import="com.liferay.portal.kernel.model.UserGroup" %>
<%@ page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/META-INF/resources/init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
    String redirectUrl = ParamUtil.getString(request, "currentURL", "");
%>

<liferay-ui:success key="reg-success" message="reg-succes"/>
<liferay-ui:error key="reg-error-mail" message="reg-error-mail"/>

<portlet:actionURL name="/createUtente" var="createUtenteURL">
    <portlet:param name="redirect" value="<%=currentURL%>"/>
</portlet:actionURL>


<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_utenti_sub.jsp" servletContext="<%= application %>"/>

    <div class="panel panel-secondary">
        <div class="panel-header">Inserimento nuovo utente per accesso ad area riservata</div>
        <div class="panel-body">
            <aui:form cssClass="form-group" action="<%= createUtenteURL %>" method="post" name="fm"
                      enctype="multipart/form-data">
                <aui:fieldset markupView="lexicon" column="12" label="Compilare tutti i campi necessari (*)">
                    <aui:input name="userToAdd" type="hidden"/>

                    <aui:input name="name" placeholder=""
                               label="Nome"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input name="cognome" placeholder=""
                               label="Cognome"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="">
                        <aui:validator name="required"/>
                    </aui:input>

                    <aui:input name="email" placeholder=""
                               label="E-Mail"
                               cssClass="form-control"
                               required="<%= true %>" type="text" value="">
                        <aui:validator name="required"/>
                        <aui:validator name="email"/>
                    </aui:input>

                    <aui:input name="password" placeholder=""
                               label="Password"
                               cssClass="form-control"
                               required="<%= true %>" type="password" value="">
                        <aui:validator name="required"/>
                    </aui:input>


                    <aui:input name="confirm" placeholder=""
                               label="Conferma password"
                               cssClass="form-control"
                               required="<%= true %>" type="password" value="">
                        <aui:validator name="required"/>
                        <aui:validator name="equalTo"
                                       errorMessage="Le password non corrispondono">'#<portlet:namespace/>password'</aui:validator>

                    </aui:input>


                    <aui:select name="gruppo" label="Gruppo" required="<%= true %>">
                        <% for (UserGroup userGroup : UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())) { %>
                        <aui:option label="<%=userGroup.getDescription()%>" value="<%=userGroup.getUserGroupId()%>"/>
                        <%}%>
                    </aui:select>

                </aui:fieldset>
                <aui:button type="submit" value="Salva"/>
            </aui:form>
        </div>
    </div>
</div>