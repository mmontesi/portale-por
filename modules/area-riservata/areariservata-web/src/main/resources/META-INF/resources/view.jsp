<%@ page import="com.liferay.portal.kernel.model.UserGroup" %>
<%@ page import="com.liferay.portal.kernel.service.UserGroupLocalServiceUtil" %>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="it.puglia.por.areariservata.common.enums.PartenariatoEnum" %>
<%@ page import="it.puglia.por.areariservata.common.enums.SorveglianzaEnum" %>
<%@ page import="it.puglia.por.areariservata.service.ProcessoLocalServiceUtil" %>
<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.web.portlet.comparators.ProcessoCreateDateComparator" %>
<%@ page import="it.puglia.por.areariservata.web.portlet.comparators.ProcessoDescrizioneComparator" %>
<%@ page import="it.puglia.por.areariservata.web.portlet.comparators.ProcessoNameComparator" %>
<%@ page import="it.puglia.por.areariservata.web.portlet.comparators.ProcessoStatoComparator" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.Workflow" %>
<%@ page import="it.puglia.por.areariservata.web.workflow.WorkflowFactory" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/init.jsp" %>
<%@ include file="init.jsp" %>

<%


    boolean showAddProcessoButton = ManageProcessoPermission.contains(permissionChecker, scopeGroupId, ActionKeys.ADD_PROCESSO);
    boolean showAddDettaglioButton = ManageProcessoPermission.contains(permissionChecker, scopeGroupId, ActionKeys.ADD_DETTAGLIO);


    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    SimpleDateFormat sdfDetailed = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
        request.setAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW, AreaRiservataPortletKeys.VIEW_GESTIONE_PROCESSI);
        request.setAttribute(AreaRiservataPortletKeys.VIEW_SUB_CURRENT_VIEW, AreaRiservataPortletKeys.VIEWSUB_TUTTI_PROCESSI);

    /* Filter */
    Long statoToFilter = ParamUtil.getLong(request, "statoWorkflow");

    /* Order */
    String orderByCol = ParamUtil.getString(request, "orderByCol", "name");
    boolean orderByAsc = false;
    String orderByType = ParamUtil.getString(request, "orderByType", "asc");
    if (orderByType.equals("asc")) {
        orderByAsc = true;
    }
    OrderByComparator orderByComparator = null;
    if (orderByCol.equals("nomeProcesso")) {
        orderByComparator = new ProcessoNameComparator(orderByAsc);
    }
    if (orderByCol.equals("createDate")) {
        orderByComparator = new ProcessoCreateDateComparator(orderByAsc);
    }
    if (orderByCol.equals("descrizioneProcesso")) {
        orderByComparator = new ProcessoDescrizioneComparator(orderByAsc);
    }
    if (orderByCol.equals("statoWorkflow")) {
        orderByComparator = new ProcessoStatoComparator(orderByAsc);
    }


    Workflow workflowPartenariato = WorkflowFactory.getWorkflow(WorkflowFactory.WORKFLOW_PARTENARIATO);
    Workflow workflowSorveglianza = WorkflowFactory.getWorkflow(WorkflowFactory.WORKFLOW_SORVEGLIANZA);
%>


<script>
    AUI().ready(
        function () {
            timeline(document.querySelectorAll('.timeline'));
        }
    );

</script>

<liferay-ui:error key="errorAdd" message="Errore durante l'inserimento"/>
<liferay-ui:error key="errorDel" message="Errore durante la cancellazione"/>
<liferay-ui:success key="successDelete" message="Operazione conclusa con successo"/>
<liferay-portlet:renderURL varImpl="viewPageURL">
    <portlet:param name="mvcPath" value="/view.jsp"/>
    <portlet:param name="orderByCol" value="<%= orderByCol %>"/>
    <portlet:param name="orderByType" value="<%= orderByType %>"/>
</liferay-portlet:renderURL>

<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_processo_sub.jsp" servletContext="<%= application %>"/>

    <div class="container-fluid container-fluid-max-xl container-view">
        <ul class="list-group list-group-notification show-quick-actions-on-hover">
            <li class="list-group-header">
                <h3 class="list-group-header-title">Elenco Processi
                </h3>
            </li>
            <li class="list-group-item list-group-item-flex processilist">

                <%
                    if (workflowPartenariato != null && workflowSorveglianza != null) {
                %>


                <aui:form action="<%=viewPageURL%>" id="formFilter" cssClass="w100" method="post">
                    <aui:fieldset cssClass="w100" collapsed="<%= false %>" collapsible="<%= true %>"
                                  label="Filtra per stato workflow"
                                  markupView="lexicon">
                        <aui:select cssClass="w100" name="statoWorkflow" label="Stati workflow"
                        >
                            <aui:option label="--- Nessun filtro ----" value="0"/>


                            <optgroup label="<%=workflowSorveglianza.workflowName()%>">
                                <%
                                    for (SorveglianzaEnum sorveglianzaEnum : SorveglianzaEnum.values()) {
                                %>

                                <aui:option label="<%=sorveglianzaEnum.descrizione%>"
                                            value="<%=sorveglianzaEnum.getId()%>"/>
                                <% } %>
                            </optgroup>
                            <optgroup label="<%=workflowPartenariato.workflowName()%>">
                                <%
                                    for (PartenariatoEnum partenariatoEnum : PartenariatoEnum.values()) {
                                %>
                                <aui:option
                                        label="<%=partenariatoEnum.descrizione%>"
                                        value="<%=partenariatoEnum.getId()%>"/>
                                value="<%=partenariatoEnum.getId()%>"/>
                                <%= partenariatoEnum%>

                                <% } %>
                            </optgroup>
                        </aui:select>
                    </aui:fieldset>
                    <aui:button type="submit" value="Filtra"/>
                    <aui:button type="reset" onClick="resetForm()" value="Reset"/>
                </aui:form>

                <%}%>
            </li>
            <li class="list-group-item list-group-item-flex processilist">

                <liferay-ui:search-container cssClass="w100" id="elencoprocessi"
                                             compactEmptyResultsMessage="false"
                                             total="<%= ProcessoLocalServiceUtil.countByIsVisibleAndStato(statoToFilter+"") %>"
                                             emptyResultsMessage="no-processo-results">

                    <liferay-ui:search-container-results
                            results="<%= ProcessoLocalServiceUtil.getProcessiVisibiliByStato(searchContainer.getStart(), searchContainer.getEnd(),orderByComparator,statoToFilter) %>"
                    />

                    <liferay-ui:search-container-row
                            className="it.puglia.por.areariservata.model.Processo"
                            escapedModel="true"
                            modelVar="processo">

                        <liferay-ui:search-container-column-text
                                property="nomeProcesso"
                                orderable="true"
                                orderableProperty="nomeProcesso"
                                name="Titolo Processo"
                                title="Titolo processo">
                            <%= processo.getNomeProcesso()%>
                        </liferay-ui:search-container-column-text>

                        <%--  <liferay-ui:search-container-column-text
                                  cssClass="truncate-text-cell"
                                  orderable="true"
                                  orderableProperty="descrizioneProcesso"
                                  name="Descrizione" title="Descrizione">
                              <%=  StringUtil.shorten(processo.getDescrizioneProcesso(), 200)%>
                          </liferay-ui:search-container-column-text>--%>

                        <liferay-ui:search-container-column-text
                                orderable="true"
                                orderableProperty="createDate"
                                name="Creazione" title="Creazione">
                            <%= sdfDetailed.format(processo.getCreateDate())%>
                        </liferay-ui:search-container-column-text>


                        <liferay-ui:search-container-column-text
                                orderable="true"
                                orderableProperty="gruppoMittenteId"
                                name="Gruppo Mittente" title="Gruppo mittente">
                            <%
                                String result = "";
                                if (processo.getGruppoMittenteId() <= 0) {
                                    result = "Segreteria tecnica";
                                } else {
                                    UserGroup gruppoMittente = UserGroupLocalServiceUtil.fetchUserGroup(processo.getGruppoMittenteId());
                                    result = gruppoMittente != null ? gruppoMittente.getDescription() : "----";
                                }
                            %>
                            <%=result%>
                        </liferay-ui:search-container-column-text>

                        <liferay-ui:search-container-column-text
                                orderable="true"
                                orderableProperty="createDate"
                                name="Workflow" title="Workflow">

                            <%
                                String style = processo.getNomeWorkflow().equals("SORVEGLIANZA") ? "success" : "error";
                            %>
                            <clay:label label="<%=processo.getNomeWorkflow()%>" style="<%=style%>"/>
                        </liferay-ui:search-container-column-text>

                        <portlet:renderURL var="showFormWorkflowProcessoURL">
                            <portlet:param name="mvcRenderCommandName" value="/showFormWorkflowProcesso"/>
                            <portlet:param name="processoId"
                                           value="<%= String.valueOf(processo.getProcessoId()) %>"/>
                        </portlet:renderURL>

                        <liferay-ui:search-container-column-text
                                orderable="true"
                                orderableProperty="statoWorkflow"
                                name="Stato" title="Stato" href="<%=showFormWorkflowProcessoURL%>">

                            <clay:label label="<%=processo.getStatoWorkflow()%>" style="info"/>
                        </liferay-ui:search-container-column-text>

                        <portlet:renderURL var="showFormDetailProcessoURL">
                            <portlet:param name="mvcRenderCommandName" value="/showFormDetailProcesso"/>
                            <portlet:param name="processoId"
                                           value="<%=String.valueOf(processo.getProcessoId()) %>"/>
                        </portlet:renderURL>


                        <portlet:actionURL name="/deleteProcesso" var="deleteProcessoURL">
                            <portlet:param name="processoId"
                                           value="<%=String.valueOf(processo.getProcessoId()) %>"/>
                            <portlet:param name="redirect" value="<%=currentURL%>"/>
                        </portlet:actionURL>


                        <liferay-ui:search-container-column-text orderable="false">


                            <div class="btn-group" role="group">
                                <button onclick="window.open('<%=showFormDetailProcessoURL%>','_self')" type="button"
                                        class="btn btn-xs btn-primary">
                                    <clay:icon symbol="info-circle"/>
                                </button>
                                <button onclick="confirmOperation('<%=deleteProcessoURL%>')" type="button"
                                        class="btn btn-xs btn-danger">
                                    <clay:icon symbol="trash"/>
                                </button>
                            </div>

                        </liferay-ui:search-container-column-text>

                    </liferay-ui:search-container-row>

                    <liferay-ui:search-iterator/>

                </liferay-ui:search-container>

                <portlet:renderURL var="showFormCreateProcessoURL">
                    <portlet:param name="mvcRenderCommandName" value="/showFormCreateProcesso"/>
                    <portlet:param name="<%=AreaRiservataPortletKeys.REQ_REDIRECTURL%>" value="<%=currentURL%>"/>
                </portlet:renderURL>

                showAddProcessoButton
            </li>
            <c:if test="<%= showAddProcessoButton  %>">
                <li class="list-group-item list-group-item-flex">
                    <a href="<%=showFormCreateProcessoURL%>"
                       class="btn btn-primary btn-block">Inserisci nuovo</a>
                </li>
            </c:if>
        </ul>
    </div>
</div>

<script type="text/javascript">


    function <portlet:namespace />_filterProcessi() {

        var portletURL = Liferay.PortletURL.createRenderURL(themeDisplay.getURLControlPanel());

        portletURL.setDoAsGroupId('true');
        portletURL.setLifecycle(Liferay.PortletURL.RENDER_PHASE);
        portletURL.setParameter('cmd', 'add_temp');
        portletURL.setParameter('javax.portlet.action', '/document_library/upload_file_entry');
        portletURL.setParameter('p_auth', Liferay.authToken);
        portletURL.setPortletId(Liferay.PortletKeys.DOCUMENT_LIBRARY);


    }

    function resetForm() {
        var form = $("#formFilter");
        console.log(form);
        form.reset()
    }

    function confirmOperation(url) {
        swal({
            title: "Eliminare il processo selezionato ?",
            text: "Attenzione: L'operazione implica la rimozione di tutti i documenti abbinati. ",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (willDelete) {
            if (willDelete) {
                swal("Il processo e' stato eliminato", {
                    icon: "success",
                });
                $.get(url, function (data) {
                    window.location.reload();
                });

            } else {
                swal("Operazione Annullata !");
            }
        });


    }


</script>