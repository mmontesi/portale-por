<%@ page import="it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys" %>
<%@ page import="it.puglia.por.areariservata.service.AuditEventLocalServiceUtil" %>
<%@ page import="it.puglia.por.areariservata.service.ProcessoLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Locale" %>
<%@ include file="/init.jsp" %>
<%@ include file="init.jsp" %>

<%
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ITALIAN);
    SimpleDateFormat sdfDetailed = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.ITALIAN);
    PortletURL portletURL = renderResponse.createRenderURL();
    String currentURL = portletURL.toString();
        request.setAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW, AreaRiservataPortletKeys.VIEW_AUDIT);

%>


<script>

</script>

<div class="container-fluid w100">
    <liferay-util:include page="/navigation_header.jsp" servletContext="<%= application %>"/>
    <liferay-util:include page="/navigation_sub.jsp" servletContext="<%= application %>"/>

    <div class="container-fluid container-fluid-max-xl container-view">
        <ul class="list-group list-group-notification show-quick-actions-on-hover">
            <li class="list-group-header">
                <h3 class="list-group-header-title">Audit Log
                </h3>
            </li>
            <li class="list-group-item list-group-item-flex processilist">

                <liferay-ui:search-container cssClass="w100" id="elencoAudit"
                                             compactEmptyResultsMessage="false"
                                             total="<%= AuditEventLocalServiceUtil.getAuditEventsCount() %>"
                                             emptyResultsMessage="no-audit-results">

                    <liferay-ui:search-container-results
                            results="<%= AuditEventLocalServiceUtil.getAuditEvents(0,AuditEventLocalServiceUtil.getAuditEventsCount()) %>"
                    />

                    <liferay-ui:search-container-row
                            className="it.puglia.por.areariservata.model.AuditEvent"
                            escapedModel="true"
                            modelVar="audit">

                        <liferay-ui:search-container-column-text
                                property="auditEventId"
                                name="ID"
                                title="ID">
                            <%= audit.getAuditEventId()%>
                        </liferay-ui:search-container-column-text>

                        <liferay-ui:search-container-column-text
                                name="Creazione" title="Creazione">
                            <%= sdfDetailed.format(audit.getCreateDate())%>
                        </liferay-ui:search-container-column-text>


                        <liferay-ui:search-container-column-text
                                name="Dettaglio" title="Dettaglio">
                            <%=audit.getDescriptionEventAudit()%>
                        </liferay-ui:search-container-column-text>

                    </liferay-ui:search-container-row>

                    <liferay-ui:search-iterator/>

                </liferay-ui:search-container>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript">



</script>