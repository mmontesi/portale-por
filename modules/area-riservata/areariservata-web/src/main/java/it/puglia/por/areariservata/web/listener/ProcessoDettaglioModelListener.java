package it.puglia.por.areariservata.web.listener;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import it.puglia.por.areariservata.web.processors.AuditProcessor;
import it.puglia.por.areariservata.web.processors.EmailAreaRiservataProcessor;
import it.puglia.por.areariservata.model.AuditEvent;
import it.puglia.por.areariservata.model.DettaglioProcesso;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.AuditEventLocalServiceUtil;
import it.puglia.por.areariservata.service.ProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;


@Component(
        immediate = true,
        service = ModelListener.class
)
public class ProcessoDettaglioModelListener extends BaseModelListener<DettaglioProcesso> {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    @Reference(policyOption = ReferencePolicyOption.GREEDY)
    private AuditProcessor auditProcessor;

    @Reference(policyOption = ReferencePolicyOption.GREEDY)
    private EmailAreaRiservataProcessor emailAreaRiservataProcessor;

    @Override
    public void onAfterCreate(DettaglioProcesso model) throws ModelListenerException {
        _log.info("onAfterCreate\t" + model.getDettaglioProcessoId());

        // Audit
        try {

            User   user         = UserLocalServiceUtil.getUser(model.getUserId());
            String auditMessage = user.getFullName() + " ha appena creato un dettaglio per il processo id: " + model.getProcessoId();

            // Audit event
            auditProcessor.auditEvent(model.getCompanyId(), model.getUserId(), model.getGroupId(), user, auditMessage);

            // Send email
            Processo processo = ProcessoLocalServiceUtil.fetchProcesso(model.getProcessoId());
            emailAreaRiservataProcessor.sendEmailNotifyToGroupAreaRiservata(processo);

        } catch (PortalException e) {
            _log.error(e.getMessage());
        }


        super.onAfterCreate(model);
    }

    @Override
    public void onAfterUpdate(DettaglioProcesso model) throws ModelListenerException {

        _log.info("onAfterUpdate\t" + model.getDettaglioProcessoId());
        try {
            Long       auditId    = CounterLocalServiceUtil.increment(AuditEvent.class.getName());
            AuditEvent auditEvent = AuditEventLocalServiceUtil.createAuditEvent(auditId);
            auditEvent.setCompanyId(model.getCompanyId());
            auditEvent.setUserId(model.getUserId());
            auditEvent.setGroupId(model.getGroupId());

            User user = UserLocalServiceUtil.getUser(model.getUserId());
            if (user == null) {
                throw new PortalException("Utente non torvato");
            }
            auditEvent.setDescriptionEventAudit(user.getFullName() + " ha appena aggiornato il dettaglio: " + model.getDettaglioProcessoId());
            AuditEventLocalServiceUtil.updateAuditEvent(auditEvent);

        } catch (PortalException e) {
            _log.error(e.getMessage());
        }
        super.onAfterUpdate(model);
    }
}
