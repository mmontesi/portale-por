package it.puglia.por.areariservata.web.portlet.actions.processo;


import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.service.ProcessoService;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.web.util.FileManagerUtil;
import it.puglia.por.areariservata.model.DettaglioProcesso;
import it.puglia.por.areariservata.model.DocumentoProcesso;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.DettaglioProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.DocumentoProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.ProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

//import java.text.SimpleDateFormat;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/createDetailProcesso"
        },
        service = MVCActionCommand.class
)

/**
 * Action per l'inserimenbto di un nuovo processo
 */
public class CreateDetailProcessoActionCommand implements MVCActionCommand {

    private static final Log            _log = LogFactoryUtil.getLog(CreateDetailProcessoActionCommand.class.getName());
    private              ServiceContext _serviceContext;

    private            Long                    companyId;
    private            Long                    currentUserId;
    private            User                    user;
    private            Long                    scopeGroupIp;

    static List<FileEntry> uploadDocumentToProcesso(ActionRequest actionRequest, Processo processo, String nomeProcesso, ServiceContext serviceContext) throws PortalException {

        List<FileEntry> fileEntryList = new ArrayList<FileEntry>();
        long            repositoryId  = DLFolderConstants.getDataRepositoryId(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);

        UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);

        File[] files = uploadPortletRequest.getFiles("fileAllegato");

        if (files != null && files.length > 0) {
            Arrays.stream(files).forEach(fileToUpload -> {

                if (fileToUpload != null) {

                    long fileSize = fileToUpload.length();
                    if (fileSize > 0) {

                        try {
                            FileEntry fileEntry = FileManagerUtil.uploadFileEntity(serviceContext, fileToUpload, processo.getFolderId(), "");
                            fileEntryList.add(fileEntry);
                        } catch (PortalException e) {
                            _log.error(e.getMessage());
                        }

                    }
                }
            });
        }

        return fileEntryList;
    }

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Inserimento Dettaglio Processo ---");

        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupIp  = _serviceContext.getScopeGroupId();

            String testo       = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_TESTODETTAGLIO);
            Long   idProcesso  = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_PROCESSOID);
            String redirectURL = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REDIRECT);

            Processo processo     = ProcessoLocalServiceUtil.fetchProcesso(idProcesso);
            String   nomeProcesso = processo.getNomeProcesso();

            Long dettaglioProcessoId = CounterLocalServiceUtil.increment(DettaglioProcesso.class.getName());

            DettaglioProcesso dettaglioProcesso = DettaglioProcessoLocalServiceUtil.createDettaglioProcesso(dettaglioProcessoId);
            dettaglioProcesso.setCompanyId(companyId);
            dettaglioProcesso.setUserId(currentUserId);
            dettaglioProcesso.setGroupId(scopeGroupIp);
            dettaglioProcesso.setIsVisible(true);
            Date date = new Date();
            dettaglioProcesso.setCreateDate(date);
            dettaglioProcesso.setModifiedDate(date);
            dettaglioProcesso.setProcessoId(idProcesso);
            dettaglioProcesso.setTestoDettaglio(testo);
            dettaglioProcesso.setNomeWorkflow(processo.getNomeWorkflow());
            dettaglioProcesso.setStatoWorkflow(processo.getStatoWorkflow());

            DettaglioProcessoLocalServiceUtil.updateDettaglioProcesso(dettaglioProcesso);

            List<FileEntry> fileList = new ArrayList<>();
            fileList.addAll(uploadDocumentToProcesso(actionRequest, processo, nomeProcesso, _serviceContext));
            fileList.forEach(fileEntry -> {

                Long idDocumentoProcesso = CounterLocalServiceUtil.increment(DocumentoProcesso.class.getName());

                DocumentoProcesso documentoProcesso = DocumentoProcessoLocalServiceUtil.createDocumentoProcesso(idDocumentoProcesso);
                documentoProcesso.setCompanyId(companyId);
                documentoProcesso.setUserId(currentUserId);
                documentoProcesso.setGroupId(scopeGroupIp);
                documentoProcesso.setCreateDate(new Date());
                documentoProcesso.setProcessoId(processo.getProcessoId());
                documentoProcesso.setDettaglioProcessoId(dettaglioProcessoId);
                documentoProcesso.setDlFileId(fileEntry.getFileEntryId());

                DocumentoProcessoLocalServiceUtil.updateDocumentoProcesso(documentoProcesso);

            });

        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "errorAdd");
            return false;
        }


        return true;
    }
}