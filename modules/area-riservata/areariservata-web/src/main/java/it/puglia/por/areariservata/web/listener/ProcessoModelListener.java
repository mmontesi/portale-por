package it.puglia.por.areariservata.web.listener;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import it.puglia.por.areariservata.web.processors.AuditProcessor;
import it.puglia.por.areariservata.web.processors.EmailAreaRiservataProcessor;
import it.puglia.por.areariservata.model.Processo;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;


@Component(
        immediate = true,
        service = ModelListener.class
)
public class ProcessoModelListener extends BaseModelListener<Processo> {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    @Reference(policyOption = ReferencePolicyOption.GREEDY)
    private AuditProcessor auditProcessor;

    @Reference(policyOption = ReferencePolicyOption.GREEDY)
    private EmailAreaRiservataProcessor emailAreaRiservataProcessor;


    @Override
    public void onAfterCreate(Processo model) throws ModelListenerException {
        _log.info("onAfterCreate\t" + model.getProcessoId());

        try {

            User   user         = UserLocalServiceUtil.getUser(model.getUserId());
            String auditMessage = user.getFullName() + " ha appena creato il processo: " + model.getNomeProcesso();

            // Audit event
            auditProcessor.auditEvent(model.getCompanyId(), model.getUserId(), model.getGroupId(), user, auditMessage);

            // Send email
            emailAreaRiservataProcessor.sendEmailNotifyToGroupAreaRiservata(model);

        } catch (PortalException e) {
            _log.error(e.getMessage());
        }

        super.onAfterCreate(model);
    }

    @Override
    public void onAfterUpdate(Processo model) throws ModelListenerException {

        _log.info("onAfterUpdate\t" + model.getProcessoId());

        // Audit
        try {

            User   user         = UserLocalServiceUtil.getUser(model.getUserId());
            String auditMessage = user.getFullName() + " ha appena aggiornato il processo: " + model.getNomeProcesso();

            // Audit event
            auditProcessor.auditEvent(model.getCompanyId(), model.getUserId(), model.getGroupId(), user, auditMessage);

            // Send email
            emailAreaRiservataProcessor.sendEmailNotifyToGroupAreaRiservata(model);

        } catch (PortalException e) {
            _log.error(e.getMessage());
        }


        super.onAfterUpdate(model);
    }
}
