package it.puglia.por.areariservata.web.constants;

/**
 * @author luigi
 */
public class AreaRiservataPortletKeys {

    public static final String PORTLET_NAME                = "areariservata";
    public static final String REDIRECT                    = "redirect";
    public static final String DL_FOLDER_NAME_FOR_PROCESSO = "Documenti Processo";

    public static final String REQ_PROCESSOID          = "processoId";
    public static final String REQ_NOMEPROCESSO        = "nomeProcesso";
    public static final String REQ_DESCRIZIONEPROCESSO = "descrizioneProcesso";
    public static final String REQ_TESTODETTAGLIO      = "testoDettaglio";
    public static final String REQ_NOMEWORKFLOW        = "nomeWorkflow";
    public static final String REQ_STATOWORKFLOW       = "statoWorkflow";
    public static final String REQ_REDIRECTURL         = "redirectUrl";
    public static final String REQ_USERMAP             = "REQ_Usermap";
    public static final String REQ_ELENCO_UTENTI       = "userToAdd";
    public static final String REQ_FOLDER_ID_PROCESSO  = "reqFolderId";
    public static final String REQ_CONF_SUBJECT        = "subject";
    public static final String REQ_CONF_FROM_USER      = "fromUser";
    public static final String REQ_CONF_BODY_MAIL      = "bodyMail";
    public static final String REQ_WORKFLOW_NEXT       = "wf_next_state";
    public static final String REQ_WORKFLOW_TYPE_NEXT  = "wf_next_type";
    public static final String REQ_WORKFLOW_NAME       = "req_workflow_name";
    public static final String REQ_DEFAULT_USERGROUP   = "defaultUserG";
    public static final String REQ_GRUPPO_MITTENTE     = "gruppoMittenteId";
    public static final String REQ_LIST_UTENTIPOR      = "listUtentiPor";
    public static final String REQ_ID_USER_TO_UPDATE   = "idUserToUpdate";
    public static final String REQ_USER_TO_UPDATE      = "userToUpdate";

    public static final String ROLE_OPR_USER                  = "UtenteAreaRiservata";
    public static final String ROLE_AMMININISTRATORE          = "ADMIN AreaRiservata";
    public static final String ROLE_LR_ADMINISTRATOR          = "Administrator";
    public static final String USERGROUP_UTENTIAMMINISTRATORE = "Utenti Amministratore";
    public static final String ROLE_OPR_AMMINISTRATORE        = "OPR_AR_Amministratore";

    /* Ruoli da documento di Analisi v. SPCL4_-_POR v.1.4 */
    /* | GRUPPO                  | RUOLO                      |
       |-------------------------|----------------------------|
       |SEGRETERIA TECNICA       | AMMINISTRATORE             |
       |AUTORITA DI GESTIONE     | VALIDATORE                 |
       |COMITATO DI  SORVEGLIANZA| REVISORE                   |
       |RESPONSABILI AZIONE      | CONTROLLER                 |
       |PARTENARIATO             | VALUTATORE                 |
       |AGENZIE REGIONALI        | VALUTATORE                 |
       |ASSESSORI                | VALUTATORE                 |
       |AUTORITA DI AUDIT        | TBD                        |
     */
    public static final String ROLE_OPR_VALIDATORE                    = "OPR_AR_Validatore";
    public static final String ROLE_OPR_REVISORE                      = "OPR_AR_Revisore";
    public static final String ROLE_OPR_CONTROLLER                    = "OPR_AR_Controller";
    public static final String ROLE_OPR_VALUTATORE                    = "OPR_AR_Valutatore";
    public static final String USERGROUP_OPR_SEGRETERIA               = "OPR_AR_Segreteria Tecnica";
    public static final String USERGROUP_OPR_AUTORITA_DI_GESTIONE     = "OPR_AR_Autorità di Gestione";
    public static final String USERGROUP_OPR_COMITATO_DI_SORVEGLIANZA = "OPR_AR_Comitato di Sorveglianza";
    public static final String USERGROUP_OPR_RESPONSABILI_AZIONE      = "OPR_AR_Responsabili Azione";
    public static final String USERGROUP_OPR_PARTENARIATO             = "OPR_AR_Partenariato";
    public static final String USERGROUP_OPR_AGENZIE_REGIONALI        = "OPR_AR_Agenzie Regionali";
    public static final String USERGROUP_OPR_ASSESSORI                = "OPR_AR_Assessori";
    public static final String VIEW_GESTIONE_PROCESSI                 = "gestioneProcessi";
    public static final String VIEW_GESTIONE_UTENTI                   = "gestioneUtenti";
    public static final String VIEWSUB_PROCESSI_IN_SCADENZA           = "processInScadenza";
    public static final String VIEWSUB_TUTTI_PROCESSI                 = "tuttiProcessi";
    public static final String VIEWSUB_UTENTI_ELENCO                  = "utentiElenco";
    public static final String VIEWSUB_UTENTI_AGGIUNGI                = "utentiAggiungi";
    public static final String GESTIONE_IMPOSTAZIONI_EMAIL            = "gestioneImpostazioneEmail";
    public static final String VIEW_DEFAULT_VIEW                      = VIEW_GESTIONE_UTENTI;
    public static final String VIEW_SUB_DEFAULT_VIEW                  = VIEWSUB_TUTTI_PROCESSI;
    public static final String VIEW_CURRENT_VIEW                      = VIEW_DEFAULT_VIEW;
    public static final String VIEW_SUB_CURRENT_VIEW                  = "subCurrentView";
    public static final String VIEW_AUDIT                             = "view_audit";


}
