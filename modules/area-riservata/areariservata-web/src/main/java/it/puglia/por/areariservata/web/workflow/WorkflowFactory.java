package it.puglia.por.areariservata.web.workflow;

public final class WorkflowFactory {
	
	public static final String WORKFLOW_SORVEGLIANZA = "SORVEGLIANZA";
	public static final String WORKFLOW_PARTENARIATO = "PARTENARIATO";
	
	public static Workflow getWorkflow(String workflowName) {
		Workflow workflow = null;
		if (workflowName.equalsIgnoreCase(WORKFLOW_SORVEGLIANZA)) {
			workflow = new WorkflowSorveglianza();
		}
		if (workflowName.equalsIgnoreCase(WORKFLOW_PARTENARIATO)) {
			workflow = new WorkflowPartenariato();
		}
		return workflow;
	}
}
