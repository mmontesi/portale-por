package it.puglia.por.areariservata.web.portlet.renders.processo;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/showFormCreateProcesso"
        },
        service = MVCRenderCommand.class
)
/**
 * RenderCommand per la visualizzazione del form di inserimento nuovo processo
 */
public class ShowFormCreateProcesso implements MVCRenderCommand {

    private final Log                        _log             = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext             _serviceContext;
    private       Long                       companyId;
    private       Long                       currentUserId;
    private       User                       user;
    private       Long                       scopeGroupIp;
    private       Map<UserGroup, List<User>> userGroupUserMap = new HashMap<>();

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

        try {
            /* Basic data */
            _serviceContext = ServiceContextFactory.getInstance(renderRequest);
            companyId       = _serviceContext.getCompanyId();
            currentUserId   = _serviceContext.getUserId();
            user            = PortalUtil.getUser(renderRequest);
            scopeGroupIp    = _serviceContext.getScopeGroupId();

            String redirectUrl = ParamUtil.getString(renderRequest, AreaRiservataPortletKeys.REQ_REDIRECTURL, "");

            /* Get group and user for OPR */
            UserGroupLocalServiceUtil.getUserGroups(0, UserGroupLocalServiceUtil.getUserGroupsCount())
                    .stream()
                    .filter(userGroup -> userGroup.getName().startsWith("OPR"))
                    .forEach(userGroup -> userGroupUserMap.put(userGroup, UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId())));

            Optional<UserGroup> defaultUserGroup = userGroupUserMap.keySet().stream().filter(us -> us.getName().contains("ecnica")).findAny();


            renderRequest.setAttribute(AreaRiservataPortletKeys.REQ_REDIRECTURL, redirectUrl);
            renderRequest.setAttribute(AreaRiservataPortletKeys.REQ_USERMAP, userGroupUserMap);
            renderRequest.setAttribute(AreaRiservataPortletKeys.REQ_DEFAULT_USERGROUP, defaultUserGroup.orElse(null));
            renderRequest.setAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW, AreaRiservataPortletKeys.VIEW_GESTIONE_PROCESSI);

        } catch (PortalException e) {
            _log.error(e.getMessage());
            SessionErrors.add(renderRequest, "error");
            return "/view.jsp";
        }

        return "/create.jsp";
    }



}