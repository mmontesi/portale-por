package it.puglia.por.areariservata.web.workflow;

import java.util.List;

public interface Workflow {

    public String getWorkflowType();

    public boolean isThisMoveAllowed(String stateFromName, String stateToName);

    public List<String> getWorkflowStructureAsList();

    public String getStartingState();

    String getNextState(String statoProcesso);

    String getNextWorkflowType();

    public String workflowName();
}
