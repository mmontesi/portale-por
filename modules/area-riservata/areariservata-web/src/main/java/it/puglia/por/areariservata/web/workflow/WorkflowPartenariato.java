package it.puglia.por.areariservata.web.workflow;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import it.puglia.por.areariservata.common.enums.PartenariatoEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WorkflowPartenariato implements Workflow {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    @Override
    public List<String> getWorkflowStructureAsList() {
        List<String>       statuses       = new ArrayList<>();
        PartenariatoEnum[] statusesArrray = PartenariatoEnum.values();
        for (int i = 0; i < statusesArrray.length; i++) {
            statuses.add(statusesArrray[i].toString().replaceAll("_", " "));
        }
        return statuses;
    }

    @Override
    public String getWorkflowType() {
        return WorkflowFactory.WORKFLOW_PARTENARIATO;
    }

    @Override
    public boolean isThisMoveAllowed(String stateFromName, String stateToName) {
        PartenariatoEnum partenariatoEnumFrom = PartenariatoEnum.valueOf(stateFromName);
        PartenariatoEnum partenariatoEnumTo   = PartenariatoEnum.valueOf(stateToName);
        return partenariatoEnumFrom.getAllowedStatusesList().contains(partenariatoEnumTo);
    }

    @Override
    public String getStartingState() {
        return PartenariatoEnum.CONVOCAZIONE.toString();
    }

    @Override public String getNextState(String statoProcesso) {
        String result = "";

        try {
            PartenariatoEnum partenariatoEnum = PartenariatoEnum.valueOf(statoProcesso);
            result = partenariatoEnum.getAllowedStatusesList().stream().map(PartenariatoEnum::toString).collect(Collectors.joining(","));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    @Override public String getNextWorkflowType() {
        return  WorkflowFactory.WORKFLOW_PARTENARIATO;
    }

    @Override public String workflowName() {
        return "Workflow Partenariato";
    }

}
