package it.puglia.por.areariservata.web.util;

import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;

import java.io.File;

public class FileManagerUtil {

    private static final Log _log = LogFactoryUtil.getLog(FileManagerUtil.class);

    @Deprecated
    public static void uploadFileEntity(ServiceContext serviceContext, UploadPortletRequest request, String folderName,
                                        String description, String fileJSPName) throws PortalException {

        String   filename       = "";
        long     repositoryId   = DLFolderConstants.getDataRepositoryId(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
        DLFolder parentFolder   = DLFolderLocalServiceUtil.getFolder(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, AreaRiservataPortletKeys.DL_FOLDER_NAME_FOR_PROCESSO);
        long     parentFolderId = parentFolder.getFolderId();

        Folder f        = DLAppLocalServiceUtil.getFolder(getOrCreateDLFolder(repositoryId, parentFolderId, folderName, serviceContext));
        long   folderId = f.getFolderId();

        File file = request.getFile(fileJSPName);
        filename = request.getFileName(fileJSPName);
        String mimeType = MimeTypesUtil.getContentType(file);

        DLAppLocalServiceUtil.addFileEntry(serviceContext.getUserId(), repositoryId, folderId, filename, mimeType,
                filename, description, "", file, serviceContext);

    }

    public static FileEntry uploadFileEntity(ServiceContext serviceContext, UploadPortletRequest request, Long folderId,
                                             String description, String fileJSPName) throws PortalException {

        String   filename       = "";
        long     repositoryId   = DLFolderConstants.getDataRepositoryId(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
        DLFolder parentFolder   = DLFolderLocalServiceUtil.getFolder(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, AreaRiservataPortletKeys.DL_FOLDER_NAME_FOR_PROCESSO);
        long     parentFolderId = parentFolder.getFolderId();

        File file = request.getFile(fileJSPName);
        filename = request.getFileName(fileJSPName);
        String mimeType = MimeTypesUtil.getContentType(file);

        FileEntry result = DLAppLocalServiceUtil.addFileEntry(serviceContext.getUserId(), repositoryId, folderId, filename, mimeType,
                filename, description, "", file, serviceContext);

        return result;
    }

    public static FileEntry uploadFileEntity(ServiceContext serviceContext, File file, Long destinationFolderId,
                                             String description) throws PortalException {

        String   filename       = file.getName();
        String   mimeType       = MimeTypesUtil.getContentType(file);
        long     repositoryId   = DLFolderConstants.getDataRepositoryId(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
        DLFolder parentFolder   = DLFolderLocalServiceUtil.getFolder(serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, AreaRiservataPortletKeys.DL_FOLDER_NAME_FOR_PROCESSO);
        long     parentFolderId = parentFolder.getFolderId();


        FileEntry result = DLAppLocalServiceUtil.addFileEntry(
                serviceContext.getUserId(),
                repositoryId,
                destinationFolderId,
                filename,
                mimeType,
                filename,
                description,
                "",
                file,
                serviceContext);

        return result;
    }

    public static long createDLFolder(ServiceContext serviceContext, String folderName, String description)
            throws PortalException {

        long    userId       = serviceContext.getUserId();
        long    groupId      = serviceContext.getScopeGroupId();
        long    repositoryId = serviceContext.getScopeGroupId();// repository id is same as groupId
        boolean mountPoint   = false;

        long rootFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID; // or 0
        // TODO Renedere la chiamata generica
        DLFolder parentFolder   = DLFolderLocalServiceUtil.getFolder(groupId, rootFolderId, AreaRiservataPortletKeys.DL_FOLDER_NAME_FOR_PROCESSO);
        long     parentFolderId = parentFolder.getFolderId();
        boolean  hidden         = false; // true if you want to hidden the folder

        DLFolder folder = null;

        try {
            folder = DLFolderLocalServiceUtil.addFolder(userId, groupId, repositoryId, mountPoint, parentFolderId,
                    folderName, description, hidden, serviceContext);
        } catch (PortalException e) {
            folder = DLFolderLocalServiceUtil.getFolder(groupId, parentFolderId, folderName);
        }

        return folder.getFolderId();

    }

    public static Long getOrCreateDLFolder(Long repositoryId, Long parentFolderId, String nameFolder, ServiceContext serviceContext) {

        Folder  folderResult = null;
        boolean folderFound  = false;

        try {
            folderResult = DLAppLocalServiceUtil.getFolder(repositoryId, parentFolderId, nameFolder);
            folderFound  = true;
        } catch (Exception e) {
            _log.error("Folder non trovato");
            _log.error(e.getMessage());
        }

        if (!folderFound) {
            try {
                folderResult = DLAppLocalServiceUtil.addFolder(serviceContext.getUserId(), repositoryId, parentFolderId, nameFolder, "", serviceContext);
            } catch (PortalException e) {
                _log.error(e.getMessage());
            }
        }

        return folderResult != null ? folderResult.getFolderId() : -1;

    }
}
