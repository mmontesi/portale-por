package it.puglia.por.areariservata.web.portlet.configuration;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(
        id = "it.puglia.por.areariservata.portlet.portlet.configuration.AreaRiservataConfiguration"
)
public interface AreaRiservataConfiguration {

    @Meta.AD(required = false, description = "Oggetto della mail")
    public String subject();


    @Meta.AD(required = false)
    public String fromUser();


    @Meta.AD(required = false)
    public String bodyMail();

}
