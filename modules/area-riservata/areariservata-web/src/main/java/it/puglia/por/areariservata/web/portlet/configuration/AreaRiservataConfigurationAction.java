package it.puglia.por.areariservata.web.portlet.configuration;

import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.web.util.AreaRiservataRolesUtil;
import it.puglia.por.areariservata.web.util.FileManagerUtil;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component(
        configurationPid = "it.puglia.por.areariservata.portlet.portlet.configuration.AreaRiservataConfiguration ",
        configurationPolicy = ConfigurationPolicy.OPTIONAL, immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME
        },
        service = ConfigurationAction.class
)
public class AreaRiservataConfigurationAction extends DefaultConfigurationAction {


    private static final Log                        _log = LogFactoryUtil.getLog(AreaRiservataConfigurationAction.class);
    private volatile     AreaRiservataConfiguration areaRiservataConfigurationAction;

    @Override
    public void include(
            PortletConfig portletConfig, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {

        if (_log.isDebugEnabled()) {
            _log.debug("Blade Message Portlet configuration include");
        }

        httpServletRequest.setAttribute(AreaRiservataConfiguration.class.getName(), areaRiservataConfigurationAction);

        super.include(portletConfig, httpServletRequest, httpServletResponse);
    }

    @Override
    public void processAction(
            PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
            throws Exception {

        String subject  = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_CONF_SUBJECT);
        String fromUser = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_CONF_FROM_USER);
        String bodyMail = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_CONF_BODY_MAIL);

        setPreference(actionRequest, AreaRiservataPortletKeys.REQ_CONF_SUBJECT, subject);
        setPreference(actionRequest, AreaRiservataPortletKeys.REQ_CONF_FROM_USER, fromUser);
        setPreference(actionRequest, AreaRiservataPortletKeys.REQ_CONF_BODY_MAIL, bodyMail);

        _log.info("----- Init AreaRiservata Portlet -----");

        try {
            mangeBaseRole(actionRequest);
            manageBaseRoleForOPR(actionRequest);
            manageFoldersOPR(actionRequest);
            manageRolesForOPR(actionRequest);
        } catch (PortalException ex) {
            _log.error(ex.getMessage());
        }

        super.processAction(portletConfig, actionRequest, actionResponse);
    }

    @Activate
    @Modified
    protected void activate(Map<Object, Object> properties) {
        areaRiservataConfigurationAction = ConfigurableUtil.createConfigurable(
                AreaRiservataConfiguration.class, properties);
    }



    private void mangeBaseRole(ActionRequest actionRequest) throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);

        AreaRiservataRolesUtil.addRoleIfNotExist(serviceContext.getUserId(), serviceContext.getCompanyId(), AreaRiservataPortletKeys.ROLE_OPR_USER);


    }

    private void manageFoldersOPR(ActionRequest actionRequest) throws PortalException {

        ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
        long           repositoryId   = serviceContext.getScopeGroupId();// repository
        FileManagerUtil.getOrCreateDLFolder(repositoryId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, AreaRiservataPortletKeys.DL_FOLDER_NAME_FOR_PROCESSO, serviceContext);
    }

    private void manageBaseRoleForOPR(ActionRequest actionRequest) throws PortalException {
        List<Role> listRoles = RoleLocalServiceUtil.getRoles(0, RoleLocalServiceUtil.getRolesCount());
        assert listRoles != null;
        boolean roleAdminOPRisPresent = listRoles.stream().anyMatch(role -> role.getName().equalsIgnoreCase(AreaRiservataPortletKeys.ROLE_AMMININISTRATORE));

        /* Aggiunta ruolo amministratore area riservata */
        if (!roleAdminOPRisPresent) {

            _log.info("Ruolo amministrazione OPR non presente. Aggiunta ruolo in corso.");

            ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);

            Role roleAdministrator = listRoles.stream().filter(role -> role.getName().equalsIgnoreCase(AreaRiservataPortletKeys.ROLE_LR_ADMINISTRATOR)).findFirst().get();
            long companyId         = roleAdministrator.getCompanyId();
            long roleUserId        = roleAdministrator.getUserId();

            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_UTENTIAMMINISTRATORE, serviceContext);
            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_AMMININISTRATORE);

        }
    }

    private void manageRolesForOPR(ActionRequest actionRequest) throws PortalException {
        List<Role> listRoles = RoleLocalServiceUtil.getRoles(0, RoleLocalServiceUtil.getRolesCount());
        assert listRoles != null;
        boolean roleAmministratoreOPRisPresent = listRoles.stream().anyMatch(role ->
                role.getName().equalsIgnoreCase(AreaRiservataPortletKeys.ROLE_OPR_AMMINISTRATORE));

        /* Aggiunta ruolo amministratore area riservata */
        if (!roleAmministratoreOPRisPresent) {

            ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);

            Role roleAdministrator = listRoles.stream().filter(role -> role.getName().equalsIgnoreCase(AreaRiservataPortletKeys.ROLE_LR_ADMINISTRATOR)).findFirst().get();
            long companyId         = roleAdministrator.getCompanyId();
            long roleUserId        = roleAdministrator.getUserId();

            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_AGENZIE_REGIONALI, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_ASSESSORI, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_AUTORITA_DI_GESTIONE, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_SEGRETERIA, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_COMITATO_DI_SORVEGLIANZA, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_RESPONSABILI_AZIONE, serviceContext);
            AreaRiservataRolesUtil.addUserGroupIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.USERGROUP_OPR_PARTENARIATO, serviceContext);

            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_OPR_AMMINISTRATORE);
            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_OPR_CONTROLLER);
            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_OPR_REVISORE);
            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_OPR_VALIDATORE);
            AreaRiservataRolesUtil.addRoleIfNotExist(roleUserId, companyId, AreaRiservataPortletKeys.ROLE_OPR_VALUTATORE);

        }
    }


}
