package it.puglia.por.areariservata.web.util;

import com.liferay.blogs.kernel.model.BlogsEntry;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.template.*;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.SubscriptionSender;
import it.puglia.por.areariservata.model.Processo;

import javax.mail.internet.InternetAddress;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class EmailManagerUtil {

    private static final Log _log = LogFactoryUtil.getLog(EmailManagerUtil.class.getName());

    public static void sendMail(String from, String to, String subject, String body, boolean isHTML, File file) {

        try {
            MailMessage mailMessage = new MailMessage();

            mailMessage.setFrom(new InternetAddress(from));
            mailMessage.setTo(new InternetAddress(to));
            mailMessage.setHTMLFormat(isHTML);
            mailMessage.setSubject(subject);
            mailMessage.setBody(body);
            if (file != null) {
                mailMessage.addFileAttachment(file, file.getName());
            }
            MailServiceUtil.sendEmail(mailMessage);

        } catch (Exception ex) {
            _log.error(ex.getMessage());
        }
    }


    public boolean startNewsletter(long idBatch, Long userId, ThemeDisplay themeDisplay) throws PortalException {

        /* Step New */

        TemplateResource templateResource = new URLTemplateResource("0", this.getClass().getClassLoader().getResource("/template/emailTemplateDefault.ftl"));
        Template         template         = TemplateManagerUtil.getTemplate(TemplateConstants.LANG_TYPE_FTL, templateResource, false);
        template.put("newsletterName", "param");
        template.put("baseUrl", "param");

        for (BlogsEntry blogsEntry : new ArrayList<BlogsEntry>()) {
            HashMap<String, String> blogEntryMap = new HashMap<>();
            blogEntryMap.put("title", blogsEntry.getTitle());
            blogEntryMap.put("content", StringUtil.shorten(blogsEntry.getContent(), 180) + " [...]");
            blogEntryMap.put("url", blogsEntry.getUrlTitle() + "/maximized");
            blogEntryMap.put("img", blogsEntry.getCoverImageURL(themeDisplay));

        }


        StringWriter out = new StringWriter();
        template.processTemplate(out);
        String body = out.toString();

        /* Send */


        return true;
    }

    private void sendEmail(List<User> utentiEmail, Processo processo, String body) throws PortalException {

        if (utentiEmail != null && utentiEmail.size() > 0) {

            long companyId = utentiEmail.get(0).getCompanyId();

            String fromAddress = "fromAdddressss";
            String fromName    = "fromName";


            String subject = "[OPR Processo]  " + processo.getNomeProcesso();
//        Map<String, Object> contextObjects = new HashMap<String, Object>();
//        contextObjects.put("subscriber", subscriber);
//
//        String body = "";
//
//        // TODO
//        body = "TODO: implement body setup";
//        // body = mailingService.prepareMailing(contextObjects,
//        // mailing.getMailingId());

            SubscriptionSender subscriptionSender = new SubscriptionSender();

            subscriptionSender.setSubject(subject);
            subscriptionSender.setCompanyId(companyId);
            subscriptionSender.setBody(body);
            subscriptionSender.setFrom(fromAddress, fromName);
            subscriptionSender.setHtmlFormat(true);
            subscriptionSender.setPortletId("portletId");


            subscriptionSender.setMailId("newsletter", UUID.randomUUID().toString());

            for (User utenti : utentiEmail) {

                String toAddress = utenti.getEmailAddress();
                String toName    = utenti.getFullName();

                subscriptionSender.setScopeGroupId(utenti.getGroupId());
                subscriptionSender.addRuntimeSubscribers(toAddress, toName);
            }

            subscriptionSender.flushNotificationsAsync();
        }


    }
}
