package it.puglia.por.areariservata.web.processors;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import it.puglia.por.areariservata.web.util.EmailManagerUtil;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.UserProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import java.util.List;


@Component(
        immediate = true,
        service = EmailAreaRiservataProcessor.class
)
public class EmailAreaRiservataProcessor {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());


    public void sendEmailNotifyToGroupAreaRiservata(Processo processoModel) {

        _log.info("sendEmailNotifyToGroupAreaRiservata\t" + processoModel.getProcessoId());

        try {

            List<User> elencoUtentiDiProcesso = UserProcessoLocalServiceUtil.getUtentiOfProcesso(processoModel);
            elencoUtentiDiProcesso.stream().forEach(user -> {
                _log.debug("Invio email: \tProcesso:" + processoModel.getProcessoId() + "\tUtente:" + user.getEmailAddress());

                if (user.getStatus() == WorkflowConstants.STATUS_APPROVED) {
                    EmailManagerUtil.sendMail("test@liferay.com", user.getEmailAddress(), "[POR - AreaRiservata - Aggiornamento stato] ", "E' stato appena aggiornato il processo " + processoModel.getNomeProcesso(), false, null);
                } else {
                    _log.info(" -- Utente "+user.getUserId()+" non abilitato a ricevere comunicazioni");
                }
            });

        } catch (Exception e) {
            _log.error(e.getMessage());
        }
    }


}
