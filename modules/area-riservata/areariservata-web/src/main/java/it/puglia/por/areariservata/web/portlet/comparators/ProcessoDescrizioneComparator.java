package it.puglia.por.areariservata.web.portlet.comparators;

import com.liferay.portal.kernel.util.OrderByComparator;
import it.puglia.por.areariservata.model.Processo;

public class ProcessoDescrizioneComparator extends OrderByComparator<Processo> {

    public static final String   ORDER_BY_ASC    = "Processo.descrizioneProcesso ASC";
    public static final String   ORDER_BY_DESC   = "Processo.descrizioneProcesso DESC";
    public static final String[] ORDER_BY_FIELDS = {"descrizioneProcesso"};
    private final       boolean  _ascending;

    public ProcessoDescrizioneComparator() {
        this(false);
    }

    public ProcessoDescrizioneComparator(boolean ascending) {
        _ascending = ascending;
    }

    @Override public int compare(Processo o1, Processo o2) {
        String name1 = o1.getDescrizioneProcesso();
        String name2 = o2.getDescrizioneProcesso();

        int value = name1.compareTo(name2);

        if (_ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override public String getOrderBy() {
        if (_ascending) {
            return ORDER_BY_ASC;
        }
        else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public boolean isAscending() {
        return _ascending;
    }
}
