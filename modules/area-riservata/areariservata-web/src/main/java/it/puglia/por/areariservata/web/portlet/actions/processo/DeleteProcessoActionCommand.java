package it.puglia.por.areariservata.web.portlet.actions.processo;


import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.service.ProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.text.SimpleDateFormat;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/deleteProcesso"
        },
        service = MVCActionCommand.class
)

/**
 * Action per l'inserimenbto di un nuovo processo
 */
public class DeleteProcessoActionCommand implements MVCActionCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;

    @Reference private AssetEntryLocalService  assetEntryLocalService;
    @Reference private DLFileEntryLocalService dlFileEntryLocalService;
    @Reference private DLFolderLocalService    dlFolderLocalService;
    private            Long                    companyId;
    private            Long                    currentUserId;
    private            User                    user;
    private            Long                    scopeGroupIp;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Cancellazione Processo ---");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupIp  = _serviceContext.getScopeGroupId();
            
            Long idProcesso = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_PROCESSOID);
            Processo processo = ProcessoLocalServiceUtil.getProcesso(idProcesso);
            processo.setIsVisible(false);
            ProcessoLocalServiceUtil.updateProcesso(processo);

        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "errorDel");
            return false;
        }


        return true;
    }
}