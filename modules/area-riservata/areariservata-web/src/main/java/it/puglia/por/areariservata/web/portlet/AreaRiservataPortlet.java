package it.puglia.por.areariservata.web.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

/**
 * @author luigi
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.header-portlet-css=/css/main.css",
                "com.liferay.portlet.header-portlet-javascript=/js/main.js",
                "com.liferay.portlet.header-portlet-javascript=/js/timeline.min.js",
                "com.liferay.portlet.header-portlet-javascript=/js/sweetalert.min.js",
                "com.liferay.portlet.header-portlet-javascript=/js/underscore-min.js",
                "com.liferay.portlet.display-category=OPR",
                "com.liferay.portlet.instanceable=true",
                "com.liferay.portlet.add-default-resource=true",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.init-param.config-template=/configurazione.jsp",
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user"
        },
        service = Portlet.class
)
public class AreaRiservataPortlet extends MVCPortlet {

    private static final Log _log = LogFactoryUtil.getLog(AreaRiservataPortlet.class);

    @Override public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {



        super.doView(renderRequest, renderResponse);

    }


}