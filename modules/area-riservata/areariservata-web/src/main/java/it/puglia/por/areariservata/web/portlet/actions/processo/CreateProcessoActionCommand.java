package it.puglia.por.areariservata.web.portlet.actions.processo;


import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.model.DocumentoProcesso;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.DocumentoProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.ProcessoService;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.web.util.FileManagerUtil;
import it.puglia.por.areariservata.web.workflow.WorkflowFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/createProcesso"
        },
        service = MVCActionCommand.class
)

/**
 * Action per l'inserimenbto di un nuovo processo
 */
public class CreateProcessoActionCommand implements MVCActionCommand {

    private final Log             _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext  _serviceContext;
    @Reference(policyOption = ReferencePolicyOption.GREEDY)
    private       ProcessoService _processoService;
    private       Long            companyId;
    private       Long            currentUserId;
    private       User            user;
    private       Long            scopeGroupId;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Inserimento Processo ---");

        SimpleDateFormat sdf         = new SimpleDateFormat("dd-MM-yyyy");
        Date             currentDate = new Date();

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupId  = _serviceContext.getScopeGroupId();

            String nomeProcesso        = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_NOMEPROCESSO);
            String descrizioneProcesso = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_DESCRIZIONEPROCESSO);
            Long   gruppoMittenteId    = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_GRUPPO_MITTENTE, -1L);
            long[] idUserToAssign      = ParamUtil.getLongValues(actionRequest, AreaRiservataPortletKeys.REQ_ELENCO_UTENTI);
            String workflowName        = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REQ_WORKFLOW_NAME);


            /*  ProcessoLocalServiceUtil.addProcesso(
                    scopeGroupId,currentUserId,companyId,currentUserId, scopeGroupId,nomeProcesso,descrizioneProcesso,workflowName, WorkflowFactory.getWorkflow(workflowName).getStartingState(),
            gruppoMittenteId,idUserToAssign, _serviceContext);
            */
/*
            Long     idProcesso = CounterLocalServiceUtil.increment(Processo.class.getName());
            Processo processo   = ProcessoLocalServiceUtil.createProcesso(idProcesso);

            processo.setCompanyId(companyId);
            processo.setUserId(currentUserId);
            processo.setGroupId(scopeGroupId);
            processo.setIsVisible(true);
            processo.setGruppoMittenteId(gruppoMittenteId);
            processo.setCreateDate(currentDate);
            processo.setModifiedDate(currentDate);
            processo.setNomeProcesso(nomeProcesso);
            processo.setDescrizioneProcesso(descrizioneProcesso);
            processo.setNomeWorkflow(workflowName);
            processo.setStatoWorkflow(WorkflowFactory.getWorkflow(workflowName).getStartingState());*/


            _processoService.addProcesso(scopeGroupId,
                    currentUserId,
                    companyId,
                    currentUserId,
                    scopeGroupId,
                    nomeProcesso,
                    descrizioneProcesso,
                    workflowName,
                    WorkflowFactory.getWorkflow(workflowName).getStartingState(),
                    gruppoMittenteId,
                    idUserToAssign,
                    _serviceContext);

            /*  *//* Upload multiple files *//*
            uploadAttachmentsProcesso(actionRequest, currentDate, processo);

            // Abbinamento utenti
            for (Long idUserFromPage : idUserToAssign) {
                long         userProcessoId = CounterLocalServiceUtil.increment(UserProcesso.class.getName());
                UserProcesso userProcesso   = UserProcessoLocalServiceUtil.createUserProcesso(userProcessoId);
                userProcesso.setCompanyId(companyId);
                userProcesso.setUserId(currentUserId);
                userProcesso.setGroupId(scopeGroupId);
                userProcesso.setCreateDate(currentDate);
                userProcesso.setModifiedDate(currentDate);
                userProcesso.setUserIdProcesso(idUserFromPage);
                userProcesso.setProcessoId(idProcesso);

                UserProcessoLocalServiceUtil.updateUserProcesso(userProcesso);

            }*/


        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "errorAdd");
            return false;
        }

        return true;
    }

    private void uploadAttachmentsProcesso(ActionRequest actionRequest, Date currentDate, Processo processo) {
        long repositoryId = DLFolderConstants.getDataRepositoryId(_serviceContext.getScopeGroupId(), DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);

        UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
        File[]               files                = uploadPortletRequest.getFiles("fileAllegato");

        if (files != null && files.length > 0) {
            Arrays.stream(files).forEach(fileToUpload -> {

                if (fileToUpload != null) {
                    long fileSize = fileToUpload.length();
                    if (fileSize > 0) {

                        try {
                            FileEntry fileEntry = FileManagerUtil.uploadFileEntity(_serviceContext, fileToUpload, processo.getFolderId(), "");
                            createDocumentoProcessoFromFileEntry(currentDate, processo, fileEntry);

                        } catch (PortalException e) {
                            _log.error(e.getMessage());
                        }

                    }
                }
            });
        }
    }

    private void createDocumentoProcessoFromFileEntry(Date currentDate, Processo processo, FileEntry fileEntry) {
        Long              idDocumentoProcesso = CounterLocalServiceUtil.increment(DocumentoProcesso.class.getName());
        DocumentoProcesso documentoProcesso   = DocumentoProcessoLocalServiceUtil.createDocumentoProcesso(idDocumentoProcesso);
        documentoProcesso.setCompanyId(companyId);
        documentoProcesso.setUserId(currentUserId);
        documentoProcesso.setGroupId(scopeGroupId);
        documentoProcesso.setCreateDate(currentDate);

        documentoProcesso.setProcessoId(processo.getProcessoId());
        documentoProcesso.setDlFileId(fileEntry.getFileEntryId());


        DocumentoProcessoLocalServiceUtil.updateDocumentoProcesso(documentoProcesso);
    }
}