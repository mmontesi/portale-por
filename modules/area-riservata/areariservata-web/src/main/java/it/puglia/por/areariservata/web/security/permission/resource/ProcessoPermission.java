package it.puglia.por.areariservata.web.security.permission.resource;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import it.puglia.por.areariservata.model.Processo;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = ProcessoPermission.class)
public class ProcessoPermission {

    private static ModelResourcePermission<Processo>
            _taskModelResourcePermission;

    public static boolean contains(
            PermissionChecker permissionChecker, Processo processo,
            String actionId)
            throws PortalException {

        return _taskModelResourcePermission.contains(
                permissionChecker, processo, actionId);
    }

    public static boolean contains(
            PermissionChecker permissionChecker, long processoId, String actionId)
            throws PortalException {

        return _taskModelResourcePermission.contains(
                permissionChecker, processoId, actionId);
    }

    @Reference(target = "(model.class.name=it.puglia.por.areariservata.model.Processo)",
            unbind = "-")
    protected void setProcesso(
            ModelResourcePermission<Processo> taskModelResourcePermission) {

        _taskModelResourcePermission = taskModelResourcePermission;
    }
}
