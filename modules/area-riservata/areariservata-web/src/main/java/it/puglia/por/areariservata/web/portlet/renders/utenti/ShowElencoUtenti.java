package it.puglia.por.areariservata.web.portlet.renders.utenti;


import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.service.AreaRiservataUsersLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/showElencoUtenti"
        },
        service = MVCRenderCommand.class
)
/**
 * RenderCommand per la visualizzazione del form di inserimento nuovo processo
 */
public class ShowElencoUtenti implements MVCRenderCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;
    private       Long           companyId;
    private       Long           currentUserId;
    private       User           user;
    private       Long           scopeGroupIp;
    private       String         currentUrl;


    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {

        Map<UserGroup, List<User>> listUtentiPor = new HashMap<>();

        try {
            /* Basic data */
            _serviceContext = ServiceContextFactory.getInstance(renderRequest);
            companyId       = _serviceContext.getCompanyId();
            currentUserId   = _serviceContext.getUserId();
            user            = PortalUtil.getUser(renderRequest);
            scopeGroupIp    = _serviceContext.getScopeGroupId();
            currentUrl      = ParamUtil.getString(renderRequest, AreaRiservataPortletKeys.REQ_REDIRECTURL);


            listUtentiPor = AreaRiservataUsersLocalServiceUtil.getUserGroupAndUserOfAreaRiservata();

        } catch (PortalException e) {
            _log.error(e.getMessage());
            SessionErrors.add(renderRequest, "error");
            return "/view.jsp";
        }

        renderRequest.setAttribute(AreaRiservataPortletKeys.REQ_REDIRECTURL, currentUrl);
        renderRequest.setAttribute(AreaRiservataPortletKeys.VIEW_CURRENT_VIEW, AreaRiservataPortletKeys.VIEW_GESTIONE_UTENTI);
        renderRequest.setAttribute(AreaRiservataPortletKeys.VIEW_SUB_CURRENT_VIEW, AreaRiservataPortletKeys.VIEWSUB_UTENTI_ELENCO);
        renderRequest.setAttribute(AreaRiservataPortletKeys.REQ_LIST_UTENTIPOR, listUtentiPor);


        return "/utenti/gestione-utenti.jsp";
    }

}