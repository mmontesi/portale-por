package it.puglia.por.areariservata.web.security.permission.resource;

import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import it.puglia.por.areariservata.constants.PortletAreaRiservataConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

@Component(immediate = true, service = {})
public class ManageProcessoPermission {

    private static PortletResourcePermission _portletResourcePermission;

    public static boolean contains(PermissionChecker permissionChecker, long groupId, String actionId) {

        return _portletResourcePermission.contains(
                permissionChecker, groupId, actionId);
    }

    @Reference(
            target = "(resource.name=" + PortletAreaRiservataConstants.RESOURCE_NAME + ")",
            unbind = "-"
    )
    protected void setPortletResourcePermission(
            PortletResourcePermission portletResourcePermission) {

        _portletResourcePermission = portletResourcePermission;
    }
}
