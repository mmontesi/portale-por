package it.puglia.por.areariservata.web.portlet.actions.processo;


import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.model.DettaglioProcesso;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.service.DettaglioProcessoLocalServiceUtil;
import it.puglia.por.areariservata.service.ProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.text.SimpleDateFormat;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/updateFlowProcesso"
        },
        service = MVCActionCommand.class
)

/**
 * Action per l'inserimento di un nuovo dettaglio.
 */
public class UpdateWorkflowProcessoActionCommand implements MVCActionCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;

    private Long companyId;
    private Long currentUserId;
    private User user;
    private Long scopeGroupIp;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Cancellazione Processo ---");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupIp  = _serviceContext.getScopeGroupId();

            Long     idProcesso           = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_PROCESSOID);
            String[] workflowName         = ParamUtil.getStringValues(actionRequest, AreaRiservataPortletKeys.REQ_WORKFLOW_NEXT);

            Processo processo             = ProcessoLocalServiceUtil.getProcesso(idProcesso);
            String   currentStatoWorkflow = processo.getStatoWorkflow();
            processo.setStatoWorkflow(workflowName[0]);
            ProcessoLocalServiceUtil.updateProcesso(processo);

            // Creazione dettaglio aggiornamento workflow
            Long idDettaglioProcesso = CounterLocalServiceUtil.increment(DettaglioProcesso.class.getName());
            DettaglioProcesso dettaglioProcesso = DettaglioProcessoLocalServiceUtil.createDettaglioProcesso(idDettaglioProcesso);
            dettaglioProcesso.setCompanyId(companyId);
            dettaglioProcesso.setUserId(currentUserId);
            dettaglioProcesso.setGroupId(scopeGroupIp);
            dettaglioProcesso.setIsVisible(true);
            dettaglioProcesso.setProcessoId(idProcesso);
            dettaglioProcesso.setTestoDettaglio("Avanzamento workflow: "+currentStatoWorkflow+" -> "+workflowName[0]);

            DettaglioProcessoLocalServiceUtil.updateDettaglioProcesso(dettaglioProcesso);


        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "errorDel");
            return false;
        }


        return true;
    }
}