package it.puglia.por.areariservata.web.portlet.comparators;

import com.liferay.portal.kernel.util.OrderByComparator;
import it.puglia.por.areariservata.model.Processo;

import java.util.Date;

public class ProcessoCreateDateComparator extends OrderByComparator<Processo> {

    public static final String   ORDER_BY_ASC    = "Processo.createDate ASC";
    public static final String   ORDER_BY_DESC   = "Processo.createDate DESC";
    public static final String[] ORDER_BY_FIELDS = {"createDate"};
    private final       boolean  _ascending;

    public ProcessoCreateDateComparator() {
        this(false);
    }

    public ProcessoCreateDateComparator(boolean ascending) {
        _ascending = ascending;
    }

    @Override public int compare(Processo o1, Processo o2) {
        Date name1 = o1.getCreateDate();
        Date name2 = o2.getCreateDate();

        int value = name1.compareTo(name2);

        if (_ascending) {
            return value;
        } else {
            return -value;
        }
    }

    @Override public String getOrderBy() {
        if (_ascending) {
            return ORDER_BY_ASC;
        }
        else {
            return ORDER_BY_DESC;
        }
    }

    @Override
    public String[] getOrderByFields() {
        return ORDER_BY_FIELDS;
    }

    @Override
    public boolean isAscending() {
        return _ascending;
    }
}
