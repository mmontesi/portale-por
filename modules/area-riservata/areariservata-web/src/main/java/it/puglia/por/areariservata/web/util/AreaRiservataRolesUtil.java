package it.puglia.por.areariservata.web.util;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.*;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;

import java.util.*;
import java.util.stream.Collectors;

public class AreaRiservataRolesUtil {

    private static final Log _log = LogFactoryUtil.getLog(AreaRiservataRolesUtil.class);

    public static User getPortalAdminUser() {
        User result = null;
        return result;
    }

    public static long[] getRolesByNames(List<String> names, ServiceContext serviceContext) {
        List<Long> roleIds = new ArrayList<>();
        for (String roleName : names) {
            Role role = checkIfExistRole(serviceContext.getCompanyId(), roleName);
            if (role != null) {
                roleIds.add(role.getRoleId());
            }
        }

        return roleIds.stream().mapToLong(l -> l).toArray();
    }

    public static Role addRoleIfNotExist(long userId, long companyId, String roleString) {
        Role roleToAdd = checkIfExistRole(companyId, roleString);
        if (roleToAdd == null) {

            _log.info("Ruolo " + roleString + " non trovato.\nAggiunta ruolo.....");

            Map<Locale, String> titleMap = new HashMap<>();
            titleMap.put(Locale.ITALIAN, roleString);

            Map<Locale, String> descriptionMap = new HashMap<>();
            descriptionMap.put(Locale.ITALIAN, roleString);

            try {

                long roleId = CounterLocalServiceUtil.increment(Role.class.getName());

                roleToAdd = RoleLocalServiceUtil.addRole(
                        userId,
                        Role.class.getName(),
                        roleId,
                        roleString,
                        titleMap,
                        descriptionMap,
                        RoleConstants.TYPE_REGULAR,
                        null,
                        null);


            } catch (Exception e) {
                _log.error(e.getMessage());
            }


            _log.info("Ruolo " + roleToAdd.getName() + " aggiunto!");

        } else {
            _log.info("Ruolo " + roleString + " già presente.");

        }

        return roleToAdd;
    }

    public static UserGroup addUserGroupIfNotExist(long userId, long companyId, String userGroupName, ServiceContext serviceContext) {
        UserGroup userGroupToAdd = checkIfUserGroupExist(companyId, userGroupName);
        if (userGroupToAdd == null) {

            _log.info("Gruppo " + userGroupName + " non trovato.\nAggiunta gruppo.....");

            Map<Locale, String> titleMap = new HashMap<Locale, String>();
            titleMap.put(Locale.ITALIAN, userGroupName);

            Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
            descriptionMap.put(Locale.ITALIAN, userGroupName);
            try {

                long userGruopId = CounterLocalServiceUtil.increment(UserGroup.class.getName());

                userGroupToAdd = UserGroupLocalServiceUtil.addUserGroup(
                        userId,
                        companyId,
                        userGroupName,
                        userGroupName,
                        serviceContext);

            } catch (Exception e) {
                _log.error(e.getMessage());
            }


            _log.info("Gruppo " + userGroupToAdd.getName() + " aggiunto!");

        } else {
            _log.info("Gruppo " + userGroupName + " già presente.");

        }

        return userGroupToAdd;
    }

    private static Role checkIfExistRole(long companyId, String role) {
        Role roleToFind = null;
        try {
            roleToFind = RoleLocalServiceUtil.getRole(companyId, role);
            _log.info("--- Role\t" + role + " " + (role != null ? "Trovato" : "Non Trovato"));
            if (roleToFind != null) {
                _log.info("----- Ho trovato:\t" + roleToFind.getDescriptiveName() + ";" + roleToFind.getRoleId());
            }
        } catch (PortalException e) {
            _log.error(e.getMessage());
        }

        return roleToFind;
    }

    private static UserGroup checkIfUserGroupExist(long companyId, String userGroupName) {
        UserGroup userGroup = null;
        try {
            userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, userGroupName);
            _log.info("--- UserGroup\t" + userGroupName + " " + (userGroupName != null ? "Trovato" : "Non Trovato"));
            if (userGroup != null) {
                _log.info("----- Ho trovato:\t" + userGroup.getName() + ";" + userGroup.getUserGroupId());
            }
        } catch (PortalException e) {
            _log.error(e.getMessage());
        }

        return userGroup;
    }

    public static List<Long> groupIdsOfUser(User user) {
        List<Long> idsGruppoUtente = new ArrayList<>();

        try {
            List<UserGroup> elencoGruppiByUtente = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
            idsGruppoUtente = elencoGruppiByUtente.stream().map(UserGroupModel::getUserGroupId).collect(Collectors.toList());

        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return idsGruppoUtente;
    }

    public static List<String> groupNamesOfUser(User user) {
        List<String> elencoGruppiUtente = new ArrayList<>();

        try {
            List<UserGroup> elencoGruppiByUtente = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
            elencoGruppiUtente = elencoGruppiByUtente.stream().map(UserGroupModel::getDescription).collect(Collectors.toList());

        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return elencoGruppiUtente;
    }

    public static Boolean userIsInTheGroup(User user, UserGroup userGroup) {

        boolean result = false;

        try {
            List<UserGroup> elencoGruppiByUtente = UserGroupLocalServiceUtil.getUserUserGroups(user.getUserId());
            List<Long> idsGruppoUtente = elencoGruppiByUtente.stream().map(UserGroupModel::getUserGroupId).collect(Collectors.toList());

            result = idsGruppoUtente.contains(userGroup.getUserGroupId());

        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;

    }
}
