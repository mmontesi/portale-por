package it.puglia.por.areariservata.web.portlet.actions;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.PortletDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PortletKeys;
import it.puglia.por.areariservata.model.Processo;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.service.ProcessoLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.*;

//import java.text.SimpleDateFormat;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/redirectToDocument"
        },
        service = MVCActionCommand.class
)

/**
 * Action per l'inserimenbto di un nuovo processo
 */
public class RedirectToDocumentActionCommand implements MVCActionCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;

    private Long companyId;
    private Long currentUserId;
    private User user;
    private Long scopeGroupIp;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Inserimento RedirectToDocumentActionCommand Processo ---");

        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupIp  = _serviceContext.getScopeGroupId();

            Long           idProcesso     = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_PROCESSOID);
            String         redirectURL    = ParamUtil.getString(actionRequest, AreaRiservataPortletKeys.REDIRECT);
            PortletDisplay portletDisplay = _serviceContext.getThemeDisplay().getPortletDisplay();
            Processo       processo       = ProcessoLocalServiceUtil.getProcesso(idProcesso);


            PortletURL documentPortletUrl = PortletURLFactoryUtil.create(actionRequest, PortletKeys.DOCUMENT_LIBRARY_ADMIN, PortletRequest.RENDER_PHASE);
            documentPortletUrl.setParameter("folderId", String.valueOf(processo.getFolderId()));
            documentPortletUrl.setParameter("mvcRenderCommandName", "/document_library/view_folder");
            documentPortletUrl.setParameter("state", WindowState.MAXIMIZED.toString());

            actionResponse.sendRedirect(documentPortletUrl.toString());

        } catch (Exception e) {
            _log.error(e.getMessage());
            return false;
        }


        return true;
    }
}