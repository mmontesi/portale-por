package it.puglia.por.areariservata.web.portlet.actions.utente;


import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.model.UserGroup;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserGroupLocalServiceUtil;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import it.puglia.por.areariservata.web.util.AreaRiservataRolesUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Locale;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/createUtente"
        },
        service = MVCActionCommand.class
)

public class CreateNewUtenteActionCommand implements MVCActionCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;

    @Reference private AssetEntryLocalService  assetEntryLocalService;
    @Reference private DLFileEntryLocalService dlFileEntryLocalService;
    @Reference private DLFolderLocalService    dlFolderLocalService;
    private            Long                    companyId;
    private            Long                    currentUserId;
    private            User                    user;
    private            Long                    scopeGroupIp;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- Creazione nuovo utente ---");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId     = _serviceContext.getCompanyId();
            currentUserId = _serviceContext.getUserId();
            user          = PortalUtil.getUser(actionRequest);
            scopeGroupIp  = _serviceContext.getScopeGroupId();

            /* Form data */
            String    nome        = ParamUtil.getString(actionRequest, "name");
            String    cognome     = ParamUtil.getString(actionRequest, "cognome");
            String    email       = ParamUtil.getString(actionRequest, "email");
            String    password    = ParamUtil.getString(actionRequest, "password");
            Long      idUserGroup = ParamUtil.getLong(actionRequest, "gruppo");
            UserGroup userGroup   = UserGroupLocalServiceUtil.getUserGroup(idUserGroup);


            /* Create user */
           /* Long userToAddId = CounterLocalServiceUtil.increment(User.class.getName());
            User userToAdd   = UserLocalServiceUtil.createUser(userToAddId);
            userToAdd.setFirstName(nome);
            userToAdd.setLastName(cognome);
            userToAdd.setEmailAddress(email);
            userToAdd.setPassword(password);
            userToAdd.setAgreedToTermsOfUse(true);
            userToAdd.setCompanyId(companyId);
            userToAdd.setCreateDate(new Date());

            UserLocalServiceUtil.u(userToAdd);*/

            long[] rolesToAdd = AreaRiservataRolesUtil.getRolesByNames(Arrays.asList(AreaRiservataPortletKeys.ROLE_OPR_USER, RoleConstants.USER), _serviceContext);
            long[] groupIds   = new long[1];
            groupIds[0] = _serviceContext.getScopeGroup().getGroupId();

            long[] userGroupIds = new long[1];
            userGroupIds[0] = idUserGroup;

            User newUser = UserLocalServiceUtil.addUser(
                    currentUserId,
                    companyId,
                    false,
                    password,
                    password,
                    true,
                    "",
                    email,
                    0l,
                    null,
                    Locale.ITALIAN,
                    nome,
                    "",
                    cognome,
                    0L,
                    0L,
                    true,
                    1,
                    1,
                    1970,
                    "",
                    groupIds,
                    null,
                    rolesToAdd,
                    userGroupIds,
                    true,
                    _serviceContext
            );

            /* Assign group to user */
            UserGroupLocalServiceUtil.addUserUserGroup(newUser.getUserId(), userGroup);

            SessionMessages.add(actionRequest, "reg-success");
            actionResponse.setRenderParameter("jspPage", "/utenti/gestione-utenti.jsp");

        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "reg-error-mail");
            SessionErrors.add(actionRequest, e.getMessage());
            return false;
        }


        return true;
    }
}