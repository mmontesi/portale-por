package it.puglia.por.areariservata.web.portlet.actions.utente;


import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.document.library.kernel.service.DLFileEntryLocalService;
import com.liferay.document.library.kernel.service.DLFolderLocalService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import it.puglia.por.areariservata.web.constants.AreaRiservataPortletKeys;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.text.SimpleDateFormat;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + AreaRiservataPortletKeys.PORTLET_NAME,
                "mvc.command.name=/enableDisableUser"
        },
        service = MVCActionCommand.class
)

public class EnableDisableUtenteAreaRiservata implements MVCActionCommand {

    private final Log            _log = LogFactoryUtil.getLog(this.getClass().getName());
    private       ServiceContext _serviceContext;

    @Reference private AssetEntryLocalService  assetEntryLocalService;
    @Reference private DLFileEntryLocalService dlFileEntryLocalService;
    @Reference private DLFolderLocalService    dlFolderLocalService;
    private            Long                    companyId;
    private            Long                    currentUserId;
    private            User                    user;
    private            Long                    scopeGroupIp;
    private            long                    userIdToModify;
    private            User                    userToModify = null;

    @Override
    public boolean processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortletException {

        _log.info("--- EnableDisableUtenteAreaRiservata ---");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            _serviceContext = ServiceContextFactory.getInstance(actionRequest);

            companyId      = _serviceContext.getCompanyId();
            currentUserId  = _serviceContext.getUserId();
            user           = PortalUtil.getUser(actionRequest);
            scopeGroupIp   = _serviceContext.getScopeGroupId();
            userIdToModify = ParamUtil.getLong(actionRequest, AreaRiservataPortletKeys.REQ_ID_USER_TO_UPDATE);


            /* Form data */
            userToModify = UserLocalServiceUtil.getUser(userIdToModify);
            if (userToModify != null) {

                if (userToModify.getStatus() == WorkflowConstants.STATUS_APPROVED) {
                    userToModify.setStatus(WorkflowConstants.STATUS_INACTIVE);
                } else {
                    userToModify.setStatus(WorkflowConstants.STATUS_APPROVED);
                }
            }


            UserLocalServiceUtil.updateUser(userToModify);

            SessionMessages.add(actionRequest, "user-update-success");
            actionResponse.setRenderParameter("jspPage", "/utenti/gestione-utenti.jsp");

        } catch (Exception e) {
            _log.error(e.getMessage());
            SessionErrors.add(actionRequest, "user-update-error");
            SessionErrors.add(actionRequest, e.getMessage());
            return false;
        }


        return true;
    }
}