package it.puglia.por.areariservata.web.workflow;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import it.puglia.por.areariservata.common.enums.SorveglianzaEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class WorkflowSorveglianza implements Workflow {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());

    @Override
    public List<String> getWorkflowStructureAsList() {
        List<String>       statuses       = new ArrayList<>();
        SorveglianzaEnum[] statusesArrray = SorveglianzaEnum.values();
        for (int i = 0; i < statusesArrray.length; i++) {
            statuses.add(statusesArrray[i].toString().replaceAll("_", " "));
        }
        return statuses;
    }

    @Override
    public String getWorkflowType() {
        return WorkflowFactory.WORKFLOW_SORVEGLIANZA;
    }

    @Override
    public boolean isThisMoveAllowed(String stateFromName, String stateToName) {
        SorveglianzaEnum sorveglianzaEnumFrom = SorveglianzaEnum.valueOf(stateFromName);
        SorveglianzaEnum sorveglianzaEnumTo   = SorveglianzaEnum.valueOf(stateToName);
        return sorveglianzaEnumFrom.getAllowedStatusesList().contains(sorveglianzaEnumTo);
    }

    @Override
    public String getStartingState() {
        return SorveglianzaEnum.CONVOCAZIONE.toString();
    }

    @Override public String getNextState(String statoProcesso) {
        String result = "";

        try {
            SorveglianzaEnum sorveglianzaEnum = SorveglianzaEnum.valueOf(statoProcesso);
            result = sorveglianzaEnum.getAllowedStatusesList().stream().map(SorveglianzaEnum::toString).collect(Collectors.joining(","));
        } catch (Exception e) {
            _log.error(e.getMessage());
        }

        return result;
    }

    @Override public String getNextWorkflowType() {
        return WorkflowFactory.WORKFLOW_SORVEGLIANZA;
    }

    @Override public String workflowName() {
        return "Workflow Sorveglianza";
    }

}
