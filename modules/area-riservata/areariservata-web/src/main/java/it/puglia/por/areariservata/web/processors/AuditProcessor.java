package it.puglia.por.areariservata.web.processors;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import it.puglia.por.areariservata.model.AuditEvent;
import it.puglia.por.areariservata.service.AuditEventLocalServiceUtil;
import org.osgi.service.component.annotations.Component;


@Component(
        immediate = true,
        service = AuditProcessor.class
)
public class AuditProcessor {

    private final Log _log = LogFactoryUtil.getLog(this.getClass().getName());


    public AuditEvent auditEvent(Long companyId, Long userId, Long groupId, User user, String auditMessage) {

        AuditEvent auditEventResult = null;

        try {
            // Requisites
            if (user == null || auditMessage == null) {
                throw new PortalException("Errore durante audit " + user + " " + auditMessage);
            }

            // Store audit event
            Long auditId = CounterLocalServiceUtil.increment(AuditEvent.class.getName());
            auditEventResult = AuditEventLocalServiceUtil.createAuditEvent(auditId);
            auditEventResult.setCompanyId(companyId);
            auditEventResult.setUserId(userId);
            auditEventResult.setGroupId(groupId);
            auditEventResult.setDescriptionEventAudit(auditMessage);
            AuditEventLocalServiceUtil.updateAuditEvent(auditEventResult);


        } catch (PortalException e) {
            _log.error(e.getMessage());
        }

        return auditEventResult;
    }


}
