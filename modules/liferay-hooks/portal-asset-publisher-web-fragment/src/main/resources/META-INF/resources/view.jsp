<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>

<%
	long assetCategoryId = ParamUtil.getLong(request, "categoryId");

	HttpServletRequest originaRequest = PortalUtil.getOriginalServletRequest(request);
	String assetCategoryIds = originaRequest.getParameter("p_o_r_categoryIds");

	if (assetCategoryId > 0) {
		AssetCategory assetCategory = AssetCategoryLocalServiceUtil.getCategory(assetCategoryId);

		PortalUtil.setPageKeywords(HtmlUtil.escape(assetCategory.getTitle(locale)), request);
	}

	String assetTagName = ParamUtil.getString(request, "tag");

	if (Validator.isNotNull(assetTagName)) {
		PortalUtil.setPageKeywords(assetTagName, request);
	}

	if (assetPublisherDisplayContext.isEnableTagBasedNavigation() && assetPublisherDisplayContext.isSelectionStyleManual() && ((assetPublisherDisplayContext.getAllAssetCategoryIds().length > 0) || (assetPublisherDisplayContext.getAllAssetTagNames().length > 0))) {
		assetPublisherDisplayContext.setSelectionStyle("dynamic");
	}
%>

<div class="subscribe-action">
	<c:if test="<%= !portletName.equals(AssetPublisherPortletKeys.HIGHEST_RATED_ASSETS) && !portletName.equals(AssetPublisherPortletKeys.MOST_VIEWED_ASSETS) && !portletName.equals(AssetPublisherPortletKeys.RECENT_CONTENT) && !portletName.equals(AssetPublisherPortletKeys.RELATED_ASSETS) && PortletPermissionUtil.contains(permissionChecker, 0, layout, portletDisplay.getId(), ActionKeys.SUBSCRIBE, false, false) && assetPublisherWebUtil.getEmailAssetEntryAddedEnabled(portletPreferences) %>">
		<c:choose>
			<c:when test="<%= assetPublisherWebUtil.isSubscribed(themeDisplay.getCompanyId(), user.getUserId(), themeDisplay.getPlid(), portletDisplay.getId()) %>">
				<portlet:actionURL name="unsubscribe" var="unsubscribeURL">
					<portlet:param name="redirect" value="<%= currentURL %>" />
				</portlet:actionURL>

				<liferay-ui:icon
						icon="start"
						label="<%= true %>"
						markupView="lexicon"
						message="unsubscribe"
						url="<%= unsubscribeURL %>"
				/>
			</c:when>
			<c:otherwise>
				<portlet:actionURL name="subscribe" var="subscribeURL">
					<portlet:param name="redirect" value="<%= currentURL %>" />
				</portlet:actionURL>

				<liferay-ui:icon
						icon="start-o"
						label="<%= true %>"
						markupView="lexicon"
						message="subscribe"
						url="<%= subscribeURL %>"
				/>
			</c:otherwise>
		</c:choose>
	</c:if>

	<%
		boolean enableRSS = !PortalUtil.isRSSFeedsEnabled() ? false : assetPublisherDisplayContext.isEnableRSS();
	%>

	<c:if test="<%= enableRSS %>">
		<liferay-portlet:resourceURL id="getRSS" varImpl="rssURL" />

		<liferay-rss:rss
				resourceURL="<%= rssURL %>"
		/>
	</c:if>
</div>

<%
	PortletURL portletURL = renderResponse.createRenderURL();

	SearchContainer searchContainer = new SearchContainer(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, assetPublisherDisplayContext.getDelta(), portletURL, null, null);

	if (!assetPublisherDisplayContext.isPaginationTypeNone()) {
		searchContainer.setDelta(assetPublisherDisplayContext.getDelta());
		searchContainer.setDeltaConfigurable(false);
	}
%>

<c:if test="<%= assetPublisherDisplayContext.isShowMetadataDescriptions() %>">
	<liferay-asset:categorization-filter
			assetType="content"
			portletURL="<%= portletURL %>"
	/>
</c:if>

<%
	request.setAttribute("view.jsp-viewInContext", assetPublisherDisplayContext.isAssetLinkBehaviorViewInPortlet());
%>

<c:choose>
	<c:when test="<%= assetPublisherDisplayContext.isSelectionStyleDynamic() %>">
		<%@ include file="/view_dynamic_list.jspf" %>
	</c:when>
	<c:when test="<%= assetPublisherDisplayContext.isSelectionStyleManual() %>">
		<%@ include file="/view_manual.jspf" %>
	</c:when>
</c:choose>

<%
	String customPaginationType = portletPreferences.getValue("paginationType", "none").trim();
%>

<c:if test="<%= !"none".equalsIgnoreCase(customPaginationType) && (searchContainer.getTotal() > searchContainer.getResults().size()) %>">
	<c:choose>
		<c:when test="<%= "por".equalsIgnoreCase(customPaginationType) %>">
			<%
				int totalPages = (int) Math.ceil((double)searchContainer.getTotal() / searchContainer.getDelta());
				int cur = searchContainer.getCur();
				PortletURL iterURL = searchContainer.getIteratorURL();
			%>
			<div class="d-flex justify-content-center py-3 por-asset-pagination">
				<%
					if (cur > 1) {
						iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(cur - 1));
				%>
				<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'>&lt;</aui:a>
				<%
					}
					if (cur - 4 > 1) {
						iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(1));
				%>
				<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'>1</aui:a><span class="dots">...</span>
				<%
					}
					for (int i = cur - 3; i < cur + 4; i++) {
						if (i <= 0 || i > totalPages) {
							continue;
						}
						// If it is the second will see also the first
						if (i == cur - 3 && i == 2) {
							iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(1));
				%>
				<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'><%= 1 %></aui:a>
				<%
					}
					iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(i));

					if (i == cur) {
						%>
						<span class='page px-2 current-page' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'><%= i %></span>
						<%
					}
					else {
						%>
						<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'><%= i %></aui:a>
						<%
					}
					// If it is the second last will see also the last
					if (i == cur + 3 && i == totalPages - 1) {
						iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(i + 1));
				%>
				<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'><%= i + 1 %></aui:a>
				<%
						}
					}
					if (cur + 4 < totalPages) {
						iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(totalPages));
				%>
				<span class="dots">...</span><aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'><%= totalPages %></aui:a>
				<%
					}
					if (cur < totalPages) {
						iterURL.setParameter(searchContainer.getCurParam(), Integer.toString(cur + 1));
				%>
				<aui:a cssClass='page px-2' href='<%= addCategoriesParameter(iterURL, assetCategoryIds) %>'>&gt;</aui:a>
				<%
					}
				%>
			</div>
		</c:when>
		<c:otherwise>
			<liferay-ui:search-paginator searchContainer="<%= searchContainer %>" type="<%= assetPublisherDisplayContext.getPaginationType() %>" />
		</c:otherwise>
	</c:choose>
</c:if>

<c:if test="<%= !assetPublisherDisplayContext.isPaginationTypeNone() && (searchContainer.getTotal() > searchContainer.getResults().size()) %>">
	<liferay-ui:search-paginator
			searchContainer="<%= searchContainer %>"
			type="<%= assetPublisherDisplayContext.getPaginationType() %>"
	/>
</c:if>

<aui:script use="querystring-parse">
	var queryString = window.location.search.substring(1);

	var queryParamObj = new A.QueryString.parse(queryString);

	var assetEntryId = queryParamObj['<portlet:namespace />assetEntryId'];

	if (assetEntryId) {
	window.location.hash = assetEntryId;
	}
</aui:script>

<%!
	private static Log _log = LogFactoryUtil.getLog("com_liferay_asset_publisher_web.view_jsp");
%>

<%!
	private String addCategoriesParameter(PortletURL url, String assetCategoryIds) {
		String categoriesParamenter = StringPool.BLANK;

		if (Validator.isNotNull(assetCategoryIds) && !assetCategoryIds.isEmpty()) {
			categoriesParamenter = "&p_o_r_categoryIds=" + assetCategoryIds;
		}

		return url.toString() + categoriesParamenter;
	};
%>
