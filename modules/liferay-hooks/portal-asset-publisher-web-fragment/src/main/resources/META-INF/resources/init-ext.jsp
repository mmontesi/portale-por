<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@ page import="com.liferay.portal.kernel.module.configuration.ConfigurationException" %>
 
<%@ page import="javax.portlet.PortletResponse" %><%@ 
page import="javax.portlet.PortletPreferences" %><%@ 
page import="java.lang.Long" %>

<%!
public class CustomAssetPublisherDisplayContext extends AssetPublisherDisplayContext {

	public CustomAssetPublisherDisplayContext(
			AssetPublisherCustomizer assetPublisherCustomizer,
			PortletRequest portletRequest, PortletResponse portletResponse,
			PortletPreferences portletPreferences)
		throws ConfigurationException {

		super(assetPublisherCustomizer, portletRequest, portletResponse, portletPreferences);

		_portletPreferences = portletPreferences;
		_request = PortalUtil.getHttpServletRequest(portletRequest);
	}

	@Override
	public long[] getAllAssetCategoryIds() throws Exception {
		if (_allAssetCategoryIds != null) {
			return _allAssetCategoryIds;
		}
		
		_allAssetCategoryIds = new long[0];
		
		long assetCategoryId = ParamUtil.getLong(_request, "categoryId");
		
		HttpServletRequest originaRequest = PortalUtil.getOriginalServletRequest(_request);
		String assetCategoryIds = originaRequest.getParameter("p_o_r_categoryIds");

		String selectionStyle = getSelectionStyle();

		if (selectionStyle.equals("dynamic")) {
			_allAssetCategoryIds = AssetPublisherUtil.getAssetCategoryIds(
				_portletPreferences);
		}

		if ((assetCategoryId > 0) &&
			!ArrayUtil.contains(_allAssetCategoryIds, assetCategoryId)) {

			_allAssetCategoryIds = ArrayUtil.append(
				_allAssetCategoryIds, assetCategoryId);
		}

		if (Validator.isNotNull(assetCategoryIds) && !assetCategoryIds.isEmpty()) {
			// this workaround is used in case of escape characters
			assetCategoryIds.replace("%2C", ",");

			for (String id : assetCategoryIds.split(",")) {
				_allAssetCategoryIds = ArrayUtil.append(
					_allAssetCategoryIds, Long.parseLong(id));
			}
		}

		return _allAssetCategoryIds;
	}

	private long[] _allAssetCategoryIds;
	private final PortletPreferences _portletPreferences;
	private final HttpServletRequest _request;
}
%>

<%
assetPublisherDisplayContext = new CustomAssetPublisherDisplayContext(assetPublisherCustomizer, liferayPortletRequest, liferayPortletResponse, portletPreferences);
%>