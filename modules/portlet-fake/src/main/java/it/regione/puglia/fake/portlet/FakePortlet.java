package it.regione.puglia.fake.portlet;

import com.liferay.portal.kernel.service.LayoutLocalServiceWrapper;
import it.regione.puglia.fake.constants.FakePortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author luigi
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + FakePortletKeys.Fake,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class FakePortlet extends MVCPortlet {

	public void test() {
		LayoutLocalServiceWrapper layoutLocalServiceWrapper;
	}
}