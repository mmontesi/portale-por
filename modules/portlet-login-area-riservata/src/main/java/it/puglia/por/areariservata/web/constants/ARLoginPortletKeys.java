package it.puglia.por.areariservata.web.constants;

/**
 * @author luigi
 */
public class ARLoginPortletKeys {

	public static final String PORTLET_LOGIN_AREA_RISERVATA = "loginAreaRisercata";

}