package it.puglia.por.areariservata.web.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import it.puglia.por.areariservata.web.constants.ARLoginPortletKeys;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

/**
 * @author luigi
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=OPR",
                "com.liferay.portlet.instanceable=true",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.name=" + ARLoginPortletKeys.PORTLET_LOGIN_AREA_RISERVATA,
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user"
        },
        service = Portlet.class
)
public class ARLoginPortlet extends MVCPortlet {


    private static final Log _log = LogFactoryUtil.getLog(ARLoginPortlet.class);

    @Override public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

        _log.info("----- Init ARLoginPortlet -----");


        super.doView(renderRequest, renderResponse);

    }
}