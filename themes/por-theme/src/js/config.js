Liferay.Loader.define('jquery', function() {
	return window.jQuery;
});

Liferay.Loader.addModule({
	name: 'slick',
	dependencies: ['jquery'],
	path: MODULE_PATH + '/js/vendor/slick-1.8.0-modified.js',
	anonymous: false
});