'use strict';

/*
This function gets loaded when all the HTML, not including the portlets, is
loaded.
*/

AUI().ready(
	'liferay-sign-in-modal',
	function (A) {
		var signIn = A.one('.sign-in > a');

		if (signIn && signIn.getData('redirect') !== 'true') {
			signIn.plug(Liferay.SignInModal);
		}
	}
);


/*
This function gets loaded after each and every portlet on the page.
portletId: the current portlet's id
node: the Alloy Node object of the current portlet
*/
Liferay.Portlet.ready(function(portletId, node) {

});


/*
This function gets loaded when everything, including the portlets, is on
the page.
*/
Liferay.on('allPortletsReady', function() {

});

/*
This function gets loaded when the DOM has been loaded.
*/
$(document).ready(function() {
	manageShowMenuMobile();

	$('.mobile-header .icon-search').on('click', function () {
		$('.header-search-form').toggle();
	})
});


/*
This function gets loaded every time the window is resized.
*/
$(window).resize(function() {

});

/*
This function gets loaded every time the window is scrolled.
*/
$(window).scroll(function() {

});

/*
This function gets loaded when slick.js has been successfully loaded.
*/
Liferay.Loader.require(['jquery', 'slick'], function () {
	managePORCarousel('.por-carousel');
});

function managePORCarousel(carouselClass) {
	$(carouselClass).each(function () {
		var carousel = $(this).find('.slider');
		var prevArrow = carousel.data('prevarrow');
		var nextArrow = carousel.data('nextarrow');

		$(carousel).slick({
			prevArrow: prevArrow,
			nextArrow: nextArrow
		});

		var sliderFor = $(this).find('.slider-for');
		var sliderNav = $(this).find('.slider-nav');

		var prevArrowFor = sliderFor.data('prevarrow');
		var nextArrowFor = sliderFor.data('nextarrow');
		var prevArrowNav = sliderNav.data('prevarrow');
		var nextArrowNav = sliderNav.data('nextarrow');

		$(sliderFor).slick({
			prevArrow: prevArrowFor,
			nextArrow: nextArrowFor
		});
		$(sliderNav).slick({
			prevArrow: prevArrowNav,
			nextArrow: nextArrowNav
		});
	});
}

function manageShowMenuMobile() {
	var isOpen = false;
	var menuContainer = $('.main-navigation-mobile');
	$('.button-open-menu-mobile, .button-close-menu-mobile').on('click', function() {
		isOpen = !isOpen;

		if (isOpen) {
			$(menuContainer).removeClass('closed');
			$(menuContainer).addClass('open sidenav-transition');
			$(menuContainer).one(
				'webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend',
				function() {
					$(this).removeClass('sidenav-transition');
				}
			);
		} else {
			$(menuContainer).removeClass('open');
			$(menuContainer).addClass('closed sidenav-transition');
			$(menuContainer).one(
				'webkitTransitionEnd mozTransitionEnd msTransitionEnd oTransitionEnd transitionend',
				function() {
					$(this).removeClass('sidenav-transition');
				}
			);
		}
	});
}
