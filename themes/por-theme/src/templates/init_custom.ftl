<#assign
	journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")
	show_footer = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-footer"))
	show_header = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header"))
	show_header_search = getterUtil.getBoolean(themeDisplay.getThemeSetting("show-header-search"))
	wrap_content = getterUtil.getBoolean(themeDisplay.getThemeSetting("wrap-content"))
	footerTitle = getterUtil.getString(theme_settings["footer-title"])
	groupid = themeDisplay.getScopeGroupId()
	footerArticle = ""
/>

<#if footerTitle?? && footerTitle?has_content>
	<#assign article = journalArticleLocalService.getArticleByUrlTitle(groupid, footerTitle)>
	<#assign footerArticle = journalArticleLocalService.getArticleContent(article, article.getDDMTemplateKey(), "VIEW", locale, themeDisplay)>
</#if>

<#if wrap_content>
	<#assign portal_content_css_class = "container-fluid" />
<#else>
	<#assign portal_content_css_class = "" />
</#if>