<header id="banner">
	<div class="navbar navbar-classic">
		<div class="col-md-12">
			<div class="d-none d-md-block">
				<div class="row px-5 d-flex align-content-baseline justify-content-end utility">
					<div class="col-md-auto d-flex justify-content-center align-items-center languages">
						<@liferay.languages default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
					</div>
					<div class="col-md-auto">
						<div class="col-md-12 px-3 d-flex justify-content-around align-items-center badges">
							<img class="mx-2" alt="European Union" src="${images_folder}/custom/badges/ue.png">
							<img class="mx-2" alt="Repubblica Italiana" src="${images_folder}/custom/badges/logo_repubblica.png">
							<img class="mx-2" alt="Regione Puglia" src="${images_folder}/custom/badges/regione_puglia.png">
						</div>
					</div>
				</div>
				<div class="row py-4 px-5 d-flex justify-content-between">
					<a class="${logo_css_class} align-items-center d-md-inline-flex logo-md" href="${site_default_url}" title="<@liferay.language_format arguments="" key="go-to-x" />">
						<div class="col-auto">
							<img alt="${logo_description}" class="mr-2" height="80" src="${site_logo}" />
						</div>
					</a>
					<div class="col-md-3 align-self-end search">
						<@liferay.search_bar default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
					</div>
				</div>
			</div>

			<div class="d-block d-sm-block d-md-none mobile-header">
				<div class="row utility"></div>
				<div class="row pt-2">
					<div class="col-auto mr-auto logo">
						<a class="${logo_css_class} logo-md" href="${site_default_url}" title="<@liferay.language_format arguments="" key="go-to-x" />">
							<div class="row d-flex flex-row">
								<div class="col-auto pl-4">
									<img alt="${logo_description}" height="50" src="${site_logo}" />
								</div>
							</div>
						</a>
					</div>
					<div class="col-auto align-self-center search">
						<i class="icon-large icon-search"></i>
					</div>
					<div class="col-auto align-self-center px-4">
						<i class="icon-large icon-reorder button-open-menu-mobile"></i>
					</div>
				</div>
				<div class="row header-search-form" style="display: none">
					<div class="col px-4 py-2">
						<@liferay.search_bar default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
					</div>
				</div>
			</div>
			<#include "${full_templates_path}/navigation.ftl" />
		</div>
	</div>
</header>