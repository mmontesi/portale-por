<#if has_navigation && is_setup_complete>
	<div class="row">
		<div class="col-md-12 p-0">
			<div class="navigation-menu" aria-expanded="false">
				<@liferay.navigation_menu default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
			</div>
		</div>
	</div>
	<div class="d-none d-md-block">
		<div class="row">
			<div class="col-md-12 px-0">
				<#assign currentUrl =  htmlUtil.escape(themeDisplay.getPortalURL()) + htmlUtil.escape(theme_display.getURLCurrent()) >
				<#assign homeUrl = htmlUtil.escape(theme_display.getURLHome()) >
				<#assign portalUrl = htmlUtil.escape(theme_display.getPortalURL()) + "/" >
				<#if page.friendlyURL?string != "/home">
					<div class="por-breadcrumbs px-5" aria-expanded="false">
						<@liferay.breadcrumbs default_preferences=freeMarkerPortletPreferences.getPreferences("portletSetupPortletDecoratorId", "barebone") />
					</div>
				</#if>
			</div>
		</div>
	</div>
</#if>